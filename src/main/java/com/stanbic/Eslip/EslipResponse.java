package com.stanbic.Eslip;

public class EslipResponse {
	String messageCode=null;
	String message=null;
    String token=null;
    String eslip_no=null;
    
    
    
    
	public EslipResponse(String messageCode, String message, String eslip_no) {
		this.messageCode = messageCode;
		this.message = message;
		this.eslip_no = eslip_no;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getEslip_no() {
		return eslip_no;
	}
	public void setEslip_no(String eslip_no) {
		this.eslip_no = eslip_no;
	}
    
    




}
