package com.stanbic.Eslip;

public class EslipProduceJSon {

	String biller_code=null;
	String token=null;
	String eslip_no=null;
	String client_name=null;
	String expiry_date=null;
	String account_no=null;
	String eslip_date=null;
	String status=null;
	String payer_code=null;
	String amount_due=null;
	String amount_to_pay=null;
	
	
	
	public String getAmount_due() {
		return amount_due;
	}
	public void setAmount_due(String amount_due) {
		this.amount_due = amount_due;
	}
	public String getAmount_to_pay() {
		return amount_to_pay;
	}
	public void setAmount_to_pay(String amount_to_pay) {
		this.amount_to_pay = amount_to_pay;
	}
	public String getBiller_code() {
		return biller_code;
	}
	public void setBiller_code(String biller_code) {
		this.biller_code = biller_code;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getEslip_no() {
		return eslip_no;
	}
	public void setEslip_no(String eslip_no) {
		this.eslip_no = eslip_no;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public String getExpiry_date() {
		return expiry_date;
	}
	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}
	public String getAccount_no() {
		return account_no;
	}
	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}
	public String getEslip_date() {
		return eslip_date;
	}
	public void setEslip_date(String eslip_date) {
		this.eslip_date = eslip_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPayer_code() {
		return payer_code;
	}
	public void setPayer_code(String payer_code) {
		this.payer_code = payer_code;
	}
	
	
	
	
}
