package com.stanbic.Eslip;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class EslipVerification {

	
	@RequestMapping(value = "/v1/verifyEslip", method = RequestMethod.POST)
	public ResponseEntity<?> verifyEslip(@RequestBody String jsondata) {
		Connection conn = null;
		JSONObject obj = new JSONObject(jsondata);
		String eslip_no=obj.getString("eslip_no");
		String token=obj.getString("token");
	
		int v_count=checkIfSlipExist(eslip_no);
		
		if(v_count==1) {
			
			HashMap<String,String> map=new HashMap<String,String>();
			try {
				String sql = "SELECT if(eslip_no is null,'',eslip_no) as eslip_no,if(amount_due is null,'',amount_due) as amount_due"
						+ ",if(expiry_date is null,'',expiry_date) as expiry_date,if(amount_to_pay is null,'',amount_to_pay) as amount_to_pay,"
						+ "if(status is null,'',status)as status,if(account_no is null,'',account_no) as account_no FROM `eslip` WHERE eslip_no=?";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, eslip_no);
				ResultSet rs = prep.executeQuery();
				while (rs.next()) {
					map.put("message", "success");
					map.put("messageCode", "00");
					map.put("eslip_no", rs.getString("eslip_no"));
					map.put("expiry_date", rs.getString("expiry_date"));
					map.put("amount_to_pay", rs.getString("amount_to_pay"));
					map.put("status", rs.getString("status"));
				}
				
				conn.close();
				return ResponseEntity.ok(map);
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
			}
		}
		else if(v_count==0) {
			
			return ResponseEntity.ok(new AuthProduceJson("07", "Eslip does not exist", "", ""));
		}else if(v_count>1) {
			
			return ResponseEntity.ok(new AuthProduceJson("08", "More than one record exist", "", ""));
		}
		else {
			
			return ResponseEntity.ok(new AuthProduceJson("02", "System error", "", ""));
		}
		
	}
	
	public int checkIfSlipExist(String eslip) {
		Connection conn = null;
		int v_count=0;
		try {
			conn = DbManager.getConnection();
			String sql="SELECT COUNT(*) AS V_COUNT FROM eslip WHERE eslip_no=?";

			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslip);
			ResultSet rs = prep.executeQuery();
			while(rs.next()) {
				v_count=Integer.parseInt(rs.getString("V_COUNT")) ;
			}
			System.out.println("rrrrrrr"+v_count);
			conn.close();
			return v_count;
		}catch(Exception e) {
			System.out.println("gjghjgvhhgvkhv"+e.getMessage());
			//e.printStackTrace();
			return v_count;
		}
	}
}
