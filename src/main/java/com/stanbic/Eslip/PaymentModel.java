package com.stanbic.Eslip;

public class PaymentModel {
	
String eslipNo="";
String paidAmount="";
String paymentRefNo="";
String paymentDate="";
String responseMessage="";
String responseCode="";
String id="";
String ft="";



public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getFt() {
	return ft;
}
public void setFt(String ft) {
	this.ft = ft;
}
public String getEslipNo() {
	return eslipNo;
}
public void setEslipNo(String eslipNo) {
	this.eslipNo = eslipNo;
}
public String getPaidAmount() {
	return paidAmount;
}
public void setPaidAmount(String paidAmount) {
	this.paidAmount = paidAmount;
}
public String getPaymentRefNo() {
	return paymentRefNo;
}
public void setPaymentRefNo(String paymentRefNo) {
	this.paymentRefNo = paymentRefNo;
}
public String getPaymentDate() {
	return paymentDate;
}
public void setPaymentDate(String paymentDate) {
	this.paymentDate = paymentDate;
}
public String getResponseMessage() {
	return responseMessage;
}
public void setResponseMessage(String responseMessage) {
	this.responseMessage = responseMessage;
}
public String getResponseCode() {
	return responseCode;
}
public void setResponseCode(String responseCode) {
	this.responseCode = responseCode;
}




}
