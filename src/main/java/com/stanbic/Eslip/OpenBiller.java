package com.stanbic.Eslip;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class OpenBiller {
	static Connection conn = null;
	
	@RequestMapping(value = "/v1/getMyEslipsBiller", method = RequestMethod.POST)
	public ResponseEntity<?> getMyEslipsBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata) {
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
	
		try {
			conn = DbManager.getConnection();
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay"
					+ ",approved_by,created_at,status,created_by,bank_ref_no,amount_charged,charge_status,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `eslip` b WHERE biller_code=? ORDER BY id DESC"; 
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				MyESlipsProduceJSon data = new MyESlipsProduceJSon();
				data.id=rs.getString("id");
				data.eslip_no=rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.expiry_date=rs.getString("expiry_date");
				data.clients_name=rs.getString("client_name");
				data.amount_to_pay=rs.getString("amount_to_pay");
				data.status=rs.getString("status");
				data.account_no=rs.getString("account_no");
				data.accounts=rs.getString("accounts");
				data.biller_code=rs.getString("biller_code");
				data.payer_code=rs.getString("payer_code");
				data.approved_by=rs.getString("approved_by");
				data.created_at=rs.getString("created_at");
				data.created_by=rs.getString("created_by");
				data.bank_ref_no=rs.getString("bank_ref_no");
				data.charge_status=rs.getString("charge_status");
				data.amount_charged=rs.getString("amount_charged");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
}
