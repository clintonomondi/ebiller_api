package com.stanbic.Eslip;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class EslipPayment {

	@RequestMapping(value = "/v1/eslipPayment", method = RequestMethod.POST)
	public ResponseEntity<?> verifyEslip(@RequestBody String jsondata) {

		try {
			JSONObject obj = new JSONObject(jsondata);
			String eslip_no = obj.getString("EslipNo");
			String PaidAmount = obj.getString("PaidAmount");
			String payment_ref_no = obj.getString("PaymentRefNo");
			String payment_date = obj.getString("PaymentDate");

			System.out.println("Slip number ________ "+eslip_no);
			
			int v_countSlipExistInQueue = checkIfPaymentNotifAlreadyInQueue(eslip_no);

			if (v_countSlipExistInQueue == 0) {
				
				if(EslipHelper.CheckAmount(eslip_no,Integer.parseInt(PaidAmount) )==true) {
					
					putInAqueueForUpdate(eslip_no, "ONWAIT", "THE SLIP IS ON QUEUE WAITING TO BE PROCESSED",payment_ref_no,payment_date,PaidAmount);
				    return ResponseEntity.ok(new AuthProduceJson("00", "Payment notification in progress", "", ""));
				}else {					
					return ResponseEntity.ok(new AuthProduceJson("07", "The amount do not match", "", ""));
				}
				
			} else {
				
			int v_count=getTotalSlipRecord(eslip_no,"PAID");
				
			if(v_count==0) {
			
				return ResponseEntity.ok(new AuthProduceJson("20", "Payment notification complete", "", ""));
			}
			else {
				
				//output error message for t24 to retry
				return ResponseEntity.ok(new AuthProduceJson("10", "Error can not get updated record", "", ""));
			}
				
			}

		} catch (Exception n) {
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error or invalid data", "", ""));
		}

	}

	public int getTotalSlipRecord(String slipNo,String status) {
		int v_count=0;
		Connection conn=null;
		try {
			String sql="SELECT COUNT(*) AS V_COUNT FROM `eslip_bills` WHERE STATUS ='"+status+"' AND eslip_no=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, slipNo);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				v_count=rs.getInt("V_COUNT");
			}
			conn.close();
			return v_count;
		}catch(Exception e) {
			e.printStackTrace();
			return v_count;
		}
	}
	
	
	
	public void updateEslipBills(String eslip_no, String payment_date, String payment_ref_no) {
		try {
			String sql = "UPDATE eslip_bills SET status=?,payment_date=?,biller_payment_ref=? WHERE eslip_no=?";
			Connection conn = null;
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Paid");
			prep.setObject(2, payment_date);
			prep.setObject(3, payment_ref_no);
			prep.setObject(4, eslip_no);
			prep.execute();
			conn.close();
		} catch (Exception n) {
			System.out.println(n.getMessage());
		}
	}

	public void putInAqueueForUpdate(String slipNo, String processState, String processDescription,String paymentRefNo,String payDate,String payAmount) {
		Connection conn = null;
		try {
			String sql = "INSERT INTO `payment_update_monitor`(SLIP_NO,PROCESS_STATE,PROCESS_DESCRIPTION,PAYMENT_REF_NO,PAID_AMOUNT,PAYMENT_DATE) VALUES(?,?,?,?,?,?)";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, slipNo);
			prep.setObject(2, processState);
			prep.setObject(3, processDescription);
			prep.setObject(4, paymentRefNo);
			prep.setObject(5, payAmount);
			prep.setObject(6, payDate);
			prep.execute();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int checkIfPaymentNotifAlreadyInQueue(String eslip) {
		Connection conn=null;
		int v_count = 0;
		try {
			String sql = "SELECT COUNT(*) AS V_COUNT FROM payment_update_monitor WHERE SLIP_NO=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslip);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				v_count = rs.getInt("V_COUNT");
				
				System.out.println("Count number:"+v_count);
			}
			prep.close();
			conn.close();
			return v_count;
		} catch (Exception e) {
			return v_count;
		}
	}

	public void fetchEslipBills(String eslip_no) {
		try {
			String sql = "SELECT account_no FROM eslip_bills WHERE eslip_no=?";
			Connection conn = null;
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslip_no);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				updateAccountbalance(rs.getString("account_no"));
			}
			conn.close();
		} catch (Exception n) {
			System.out.println(n.getMessage());
		}
	}

	public void updateAccountbalance(String account_no) {
		try {
			String sql = "UPDATE account set amount_due=? WHERE account_no=?";
			Connection conn = null;
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "0");
			prep.setObject(2, account_no);
			prep.execute();
			conn.close();

		} catch (Exception n) {
			System.out.println(n.getMessage());
		}
	}

	public void updatePayment(String eslip_no, String payment_ref_no, String payment_date) {
		try {
			String sql = "UPDATE eslip SET status=?,bank_ref_no=?,payment_date=? WHERE eslip_no=?";
			Connection conn = null;
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Paid");
			prep.setObject(2, payment_ref_no);
			prep.setObject(3, payment_date);
			prep.setObject(4, eslip_no);
			prep.execute();
			conn.close();
			// updating eslip bills
			updateEslipBills(eslip_no, payment_date, payment_ref_no);
			// Updating Account balances paid
			fetchEslipBills(eslip_no);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
