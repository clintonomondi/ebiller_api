package com.stanbic.Eslip;

public class EslipBillsProduceJson {
String amount_due="";
String amount_to_pay="";
String due_date="";
String status="";
String eslip_no="";
String account_no="";
String account_name="";
String description="";
String biller_ref="";
String response_date="";
String biller="";
String payer="";




public String getBiller() {
	return biller;
}
public void setBiller(String biller) {
	this.biller = biller;
}
public String getPayer() {
	return payer;
}
public void setPayer(String payer) {
	this.payer = payer;
}
public String getResponse_date() {
	return response_date;
}
public void setResponse_date(String response_date) {
	this.response_date = response_date;
}
public String getBiller_ref() {
	return biller_ref;
}
public void setBiller_ref(String biller_ref) {
	this.biller_ref = biller_ref;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getAccount_name() {
	return account_name;
}
public void setAccount_name(String account_name) {
	this.account_name = account_name;
}
public String getAccount_no() {
	return account_no;
}
public void setAccount_no(String account_no) {
	this.account_no = account_no;
}
public String getEslip_no() {
	return eslip_no;
}
public void setEslip_no(String eslip_no) {
	this.eslip_no = eslip_no;
}
public String getAmount_due() {
	return amount_due;
}
public void setAmount_due(String amount_due) {
	this.amount_due = amount_due;
}
public String getAmount_to_pay() {
	return amount_to_pay;
}
public void setAmount_to_pay(String amount_to_pay) {
	this.amount_to_pay = amount_to_pay;
}
public String getDue_date() {
	return due_date;
}
public void setDue_date(String due_date) {
	this.due_date = due_date;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}


}
