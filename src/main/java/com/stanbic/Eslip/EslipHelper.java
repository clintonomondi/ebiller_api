package com.stanbic.Eslip;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.ResponseEntity;

import java.io.FileInputStream;
import java.sql.Connection;
import com.stanbic.comm.DbManager;
import com.stanbic.ebiller.auth.AuthProduceJson;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class EslipHelper {
	static Connection conn = null;
	String SECRET_KEY = "536ghss38%()((*892902";
	private static final Random generator = new Random();

	public static String getPayerName(String comp_code) {
		String count = null;
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT personel_f_name FROM biller_profile WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				count = rs.getString("personel_f_name");
			}
			conn.close();

		} catch (Exception n) {
			System.out.println(n.getMessage());
		}
		return count;
	}

	public static String getPayerEmail(String comp_code) {
		String count = null;
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT email FROM biller_profile WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				count = rs.getString("email");
			}
			conn.close();

		} catch (Exception n) {
			System.out.println(n.getMessage());
		}
		return count;
	}

	public static boolean CheckAmount(String eslip_no, int amount) {
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT IF(amount_to_pay IS NULL,'0',amount_to_pay) AS amount_to_pay FROM eslip WHERE eslip_no=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslip_no);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				if (amount == rs.getInt("amount_to_pay")) {
					return true;
				}

			}
			conn.close();
		} catch (Exception n) {
			System.out.println(n.getMessage());
			return false;
		}
		return false;
	}

	public static double saveToFileData(Vector dataHolder, String payer_code, String biller_code, String eslip_no,
			String due_date, String account_to_add, double oddcent) {
		double total = 0.0;
		double amount_to_pay = 0.0;
		System.out.println("oddcent sent>>>>>>>>>>>>>" + oddcent);
		try {
			conn = DbManager.getConnection();
			String sql = "INSERT INTO eslip_bills (eslip_no,account_no,amount_to_pay,due_date,count,biller_code,payer_code)VALUES(?,?,?,?,?,?,?)";
			PreparedStatement prep = conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			int ii = 0;
			int chuncode = 1;
			double amountflag = 0.0;
			int count = 1;
			for (int i = 0; i < dataHolder.size(); i++) {
				count++;
				Vector cellStoreVector = (Vector) dataHolder.elementAt(i);
				if (account_to_add.equalsIgnoreCase(cellStoreVector.toArray()[0].toString())) {
					amount_to_pay = Double.parseDouble(cellStoreVector.toArray()[1].toString()) + oddcent;
					System.out.println("Account to add oddcent found>>>>>>>>>>>>>" + account_to_add);
					System.out.println("Amount in excel is>>>>>>>" + cellStoreVector.toArray()[1].toString());
					System.out.println("New Amount_to_Pay is" + amount_to_pay);
				} else {
					amount_to_pay = Double.parseDouble(cellStoreVector.toArray()[1].toString());
				}
				amountflag = Double.parseDouble(cellStoreVector.toArray()[1].toString());
				if (amountflag <= 0) {

				} else {
					prep.setObject(1, eslip_no);
					prep.setObject(2, cellStoreVector.toArray()[0].toString());
					prep.setObject(3, amount_to_pay);
					prep.setObject(4, due_date);
					prep.setObject(5, count);
					prep.setObject(6, biller_code);
					prep.setObject(7, payer_code);
					prep.addBatch();
				}

				ii++;
				if (ii % 1000 == 0 || ii == dataHolder.size()) {
					System.out.println(">Inserting eslip bills record in eslip_bills initial stage=" + ii);
					prep.executeBatch(); // Execute every 1000 items.
					chuncode++;
					conn.commit();
					prep.clearBatch();

					if (ii == dataHolder.size()) {
						String sql2 = "SELECT SUM(amount_to_pay) AS total FROM eslip_bills WHERE eslip_no=?";
						PreparedStatement prep2 = conn.prepareStatement(sql2);
						prep2.setObject(1, eslip_no);
						ResultSet rs = prep2.executeQuery();
						while (rs.next()) {
							total = rs.getDouble("total");
						}

					}
				}
			}
			conn.setAutoCommit(true);
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return total;
	}

	public static void deleteHistory(String eslip_no) {
		try {
			String sql = "DELETE FROM eslip_bills WHERE eslip_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslip_no);
			prep.execute();
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int checkEslip(String eslip_no, String amount_to_pay) {
		int vcount = 0;
		try {
			String sql = "SELECT COUNT(*)vcount FROM eslip WHERE eslip_no=? AND amount_to_pay=? AND status=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslip_no);
			prep.setObject(2, amount_to_pay);
			prep.setObject(3, "Pending");
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				vcount = rs.getInt("vcount");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vcount;
	}

	public static void approveEslip(String eslip_no, String approved_by) {
		try {
			String sql = "UPDATE eslip SET approved=?,approved_by=? WHERE eslip_no=?";
			String sql2 = "DELETE FROM exception_logs WHERE eslip_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep.setObject(1, "Y");
			prep.setObject(2, approved_by);
			prep.setObject(3, eslip_no);

			prep2.setObject(1, eslip_no);
			prep.execute();
			prep2.execute();
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Vector readException(String path,String prefix,String biller_code) {
		String ft="FT1";
		String reference="NA";
		Vector vector = new Vector();
		try {
			File file = new File(path);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getElementsByTagName("Row");
			for (int itr = 0; itr < nodeList.getLength(); itr++) {
				Node node = nodeList.item(itr);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) node;
				if(eElement.getElementsByTagName("Description").item(0).getTextContent().contains(prefix) || 
						eElement.getElementsByTagName("Description").item(0).getTextContent().contains(prefix.toLowerCase())){
					
				}else {
					String newString=eElement.getElementsByTagName("Description").item(0).getTextContent().replaceAll("\n", " ").replaceAll("/", " ").replaceAll("-", " ");
					//pick ft here
					for (String word : newString.split(" ")){
					    if(word.contains(ft)){
					    	reference=word;	
					    }
					}
					
					Vector holder = new Vector();
					holder.addElement(eElement.getElementsByTagName("Date").item(0).getTextContent());
					holder.addElement(eElement.getElementsByTagName("Credit").item(0).getTextContent());
					holder.addElement(eElement.getElementsByTagName("Description").item(0).getTextContent().replaceAll("\n", " ").replaceAll("/", " ").replaceAll("-", " "));
					holder.addElement(reference);
					holder.addElement(biller_code);
					vector.addElement(holder);
					
				}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vector;

	}

	public static void saveExceptions(Vector dataHolder) {
		try {
			conn = DbManager.getConnection();
			String sql = "INSERT INTO exception_logs(date,amount,reference,ft,biller_code)VALUES(?,?,?,?,?)";
			PreparedStatement prep = conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			int ii = 0;
			for (int i = 0; i < dataHolder.size(); i++) {
				Vector cellStoreVector = (Vector) dataHolder.elementAt(i);
				prep.setObject(1, cellStoreVector.toArray()[0].toString());
				prep.setObject(2, cellStoreVector.toArray()[1].toString());
				prep.setObject(3, cellStoreVector.toArray()[2].toString());
				prep.setObject(4, cellStoreVector.toArray()[3].toString());
				prep.setObject(5, cellStoreVector.toArray()[4].toString());
				prep.addBatch();

				ii++;
				if (ii % 1000 == 0 || ii == dataHolder.size()) {
					System.out.println(">Inserting exceptions at=" + ii);
					prep.executeBatch(); // Execute every 1000 items.
					conn.commit();
					prep.clearBatch();
				}
			}
			conn.setAutoCommit(true);
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	
	public static String formartDate(String date2) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");  
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");  
		
		try {  
	        Date date = formatter.parse(date2);  
	        return formatter2.format(date);  
	    } catch (Exception e) {
	    	return date2;
	    	}  

	}
	
	public static String formatNumber(double number) {
		
	       String pattern = "###,###.##";
	       DecimalFormat decimalFormat = new DecimalFormat(pattern);

	       String format = decimalFormat.format(number);
	       return format;
		
	}

}
