package com.stanbic.Eslip;

public class EslipAccInfoRespModel {

String accountNo="";
String accountAmount="";
String account_name="";
String biller_payment_ref="";
String count="";



public String getCount() {
	return count;
}
public void setCount(String count) {
	this.count = count;
}
public String getBiller_payment_ref() {
	return biller_payment_ref;
}
public void setBiller_payment_ref(String biller_payment_ref) {
	this.biller_payment_ref = biller_payment_ref;
}
public String getAccount_name() {
	return account_name;
}
public void setAccount_name(String account_name) {
	this.account_name = account_name;
}
public String getAccountNo() {
	return accountNo;
}
public void setAccountNo(String accountNo) {
	this.accountNo = accountNo;
}
public String getAccountAmount() {
	return accountAmount;
}
public void setAccountAmount(String accountAmount) {
	this.accountAmount = accountAmount;
}

}
