package com.stanbic.Eslip;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Accounts.AccountProduceJson;
import com.stanbic.Accounts.AccountsConsumeJson;
import com.stanbic.Accounts.MultipleAccountsAutoSlips;
import com.stanbic.Billers.BillerHelper;
import com.stanbic.Billers.BillersAccountProduceJson;
import com.stanbic.KPLC.QueryBulkBillsService;
import com.stanbic.OTP.OTPHelper;
import com.stanbic.Report.AccountReportModel;
import com.stanbic.Report.ESlipReportModel;
import com.stanbic.Responses.Response;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;
import javax.xml.parsers.DocumentBuilderFactory;  
import javax.xml.parsers.DocumentBuilder;  
import org.w3c.dom.Document;  
import org.w3c.dom.NodeList;  
import org.w3c.dom.Node;  
import org.w3c.dom.Element;  
import java.io.File;  
@SpringBootApplication
@RequestMapping("/api")
public class EslipBank {
	Connection conn=null;
	
	@RequestMapping(value = "/v1/generateEslipBank", method = RequestMethod.POST)
	public ResponseEntity<?> generateEslip(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
		Vector vector = new Vector();
	try {
		// extract e slip from
		JSONObject obj = new JSONObject(jsondata);
		String biller_code = obj.getString("biller_code");
		String total_amount_due = obj.getString("total_amount_due");
		String total_amount_to_pay = obj.getString("total_amount_to_pay");
		String account_no=null;
		String amount_to_pay = "";
		String amount_due = "";
		String eslip_no = "";
		String due_date = "";
		String account_name="";
		String payer_code = obj.getString("payer_code");
		String username=TokenManager.tokenIssuedCompCode(token);


		// generate e slip number
		eslip_no = EslipService.eslipNoGen(biller_code);
		due_date=EslipService.expiryDate();
		JSONArray slipInfoArray = obj.getJSONArray("eslipInfo");
		int count=1;
//		boolean flag=true;
		double amt=0.0;
		for (int i = 0; i < slipInfoArray.length(); i++) {
			JSONObject object = slipInfoArray.getJSONObject(i);
			account_no = object.getString("account_no");
			amount_to_pay = object.getString("amount_to_pay");
			amount_due = object.getString("amount_due");
			account_name=object.getString("account_name");
			
			amt=Double.parseDouble(amount_to_pay);
			if(amt<=0.0) {
				 return ResponseEntity.ok(new EslipResponse("07","Some account numbers have zero amount to pay",""));
			}
			Vector holder = new Vector();
			holder.addElement(eslip_no);
			holder.addElement(amount_due);
			holder.addElement(amount_to_pay);
			holder.addElement(due_date);
			holder.addElement(account_no);
			holder.addElement(count);
			holder.addElement(account_name);
			vector.addElement(holder);
			count++;
		}
		String sql = "INSERT INTO eslip_bills (eslip_no,amount_due,amount_to_pay,due_date,account_no,count,account_name,biller_code,payer_code)VALUES(?,?,?,?,?,?,?,?,?)";
		conn = DbManager.getConnection();
		PreparedStatement prep = conn.prepareStatement(sql);
		conn.setAutoCommit(false);
		int ii=0;
		for (int i = 0; i < vector.size(); i++) {
			Vector data = (Vector) vector.elementAt(i);
			prep.setObject(1, data.toArray()[0].toString()); 
			prep.setObject(2, data.toArray()[1].toString());
			prep.setObject(3, data.toArray()[2].toString());
			prep.setObject(4, data.toArray()[3].toString());
			prep.setObject(5, data.toArray()[4].toString());
			prep.setObject(6, data.toArray()[5].toString());
			prep.setObject(7, data.toArray()[6].toString());
			prep.setObject(8, biller_code);
			prep.setObject(9, payer_code);
			prep.addBatch();
			ii++;
			if (ii % 1000 == 0 || ii == vector.size()) {
				System.out.println(">Creating eslip accounts at initial stage="+ii);
				prep.executeBatch(); // Execute every 1000 items
				conn.commit();
	            prep.clearBatch();
			}
		}
		conn.setAutoCommit(true);
		conn.close();
		vector.clear();
		EslipService.insertEslipBank(eslip_no , due_date, total_amount_due,total_amount_to_pay,biller_code, payer_code,"",username);
		 return ResponseEntity.ok(new EslipResponse("00","Eslip generated successfully",eslip_no));	
	}catch(Exception e) {
		System.out.println(e.getMessage());
		return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
	}
	
	}
	
	@RequestMapping(value = "/v1/getAllEslipsBank", method = RequestMethod.POST)
	public ResponseEntity<?> getAllEslipsBank(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata) {
		if(jsondata.getBiller_code().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide biller code", "", ""));
		}
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		if(jsondata.getBiller_code().isEmpty() ||   jsondata.getDatefrom().isEmpty()  || jsondata.getTodate().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please select biller and dates","",""));
		}
		
		try {
			if(jsondata.getPayer_code().isEmpty()) {
				conn = DbManager.getConnection();
				String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code,approved, amount_to_pay"
						+ ",approved_by,created_at,status,created_by,bank_ref_no,amount_charged,charge_status,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
						+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
						+"(SELECT company_name  FROM biller_profile E WHERE E.comp_code=b.payer_code) payer_name, \n"
						+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
						"FROM `eslip` b WHERE biller_code=? AND created_at  BETWEEN ? AND ?";
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, jsondata.getBiller_code());
				prep.setObject(2, EslipHelper.formartDate(jsondata.getDatefrom())+" 00:00");
				prep.setObject(3, EslipHelper.formartDate(jsondata.getTodate())+" 23:59");
				ResultSet rs = prep.executeQuery();
				while (rs.next()) {
					MyESlipsProduceJSon data = new MyESlipsProduceJSon();
					data.id=rs.getString("id");
					data.eslip_no=rs.getString("eslip_no");
					data.amount_due = rs.getString("amount_due");
					data.expiry_date=rs.getString("expiry_date");
					data.clients_name=rs.getString("client_name");
					data.amount_to_pay=rs.getString("amount_to_pay");
					data.status=rs.getString("status");
					data.account_no=rs.getString("account_no");
					data.accounts=rs.getString("accounts");
					data.biller_code=rs.getString("biller_code");
					data.payer_code=rs.getString("payer_code");
					data.approved_by=rs.getString("approved_by");
					data.created_at=rs.getString("created_at");
					data.created_by=rs.getString("created_by");
					data.bank_ref_no=rs.getString("bank_ref_no");
					data.charge_status=rs.getString("charge_status");
					data.amount_charged=rs.getString("amount_charged");
					data.approved=rs.getString("approved");
					data.payer_name=rs.getString("payer_name");
					model.add(data);
				}
			}else {
			conn = DbManager.getConnection();
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code,approved, amount_to_pay"
					+ ",approved_by,created_at,status,created_by,bank_ref_no,amount_charged,charge_status,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
					+"(SELECT company_name  FROM biller_profile E WHERE E.comp_code=b.payer_code) payer_name, \n"
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `eslip` b WHERE payer_code=? AND biller_code=? AND created_at  BETWEEN ? AND ?"; 
			PreparedStatement prep = conn.prepareStatement(sql);
			 prep.setObject(1, jsondata.getPayer_code());
			 prep.setObject(2, jsondata.getBiller_code());
			 prep.setObject(3, EslipHelper.formartDate(jsondata.getDatefrom())+" 00:00");
			 prep.setObject(4, EslipHelper.formartDate(jsondata.getTodate())+" 23:59");
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				MyESlipsProduceJSon data = new MyESlipsProduceJSon();
				data.id=rs.getString("id");
				data.eslip_no=rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.expiry_date=rs.getString("expiry_date");
				data.clients_name=rs.getString("client_name");
				data.amount_to_pay=rs.getString("amount_to_pay");
				data.status=rs.getString("status");
				data.account_no=rs.getString("account_no");
				data.accounts=rs.getString("accounts");
				data.biller_code=rs.getString("biller_code");
				data.payer_code=rs.getString("payer_code");
				data.approved_by=rs.getString("approved_by");
				data.created_at=rs.getString("created_at");
				data.created_by=rs.getString("created_by");
				data.bank_ref_no=rs.getString("bank_ref_no");
				data.charge_status=rs.getString("charge_status");
				data.amount_charged=rs.getString("amount_charged");
				data.approved=rs.getString("approved");
				data.payer_name=rs.getString("payer_name");
				model.add(data);
			}
		}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	
	@RequestMapping(value = "/v1/getPaidEslipsBank", method = RequestMethod.POST)
	public ResponseEntity<?> getPaidEslipsBank(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata) {
		if(jsondata.getBiller_code().isEmpty() || jsondata.getDatefrom().isEmpty() || jsondata.getTodate().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide all fields", "", ""));
		}
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		try {
			if(jsondata.getPayer_code().isEmpty()) {
				conn = DbManager.getConnection();
				String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code,approved, amount_to_pay"
						+ ",approved_by,created_at,status,created_by,bank_ref_no,amount_charged,charge_status,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
						+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
						+"(SELECT company_name  FROM biller_profile E WHERE E.comp_code=b.payer_code) payer_name, \n"
						+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
						"FROM `eslip` b WHERE biller_code=? AND status=? AND created_at  BETWEEN ? AND ? ORDER BY id DESC"; 
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, jsondata.getBiller_code());
				prep.setObject(2, "PAID");
				 prep.setObject(3, EslipHelper.formartDate(jsondata.getDatefrom())+" 00:00");
				 prep.setObject(4, EslipHelper.formartDate(jsondata.getTodate())+" 23:59");
				ResultSet rs = prep.executeQuery();
				while (rs.next()) {
					MyESlipsProduceJSon data = new MyESlipsProduceJSon();
					data.id=rs.getString("id");
					data.eslip_no=rs.getString("eslip_no");
					data.amount_due = rs.getString("amount_due");
					data.expiry_date=rs.getString("expiry_date");
					data.clients_name=rs.getString("client_name");
					data.amount_to_pay=rs.getString("amount_to_pay");
					data.status=rs.getString("status");
					data.account_no=rs.getString("account_no");
					data.accounts=rs.getString("accounts");
					data.biller_code=rs.getString("biller_code");
					data.payer_code=rs.getString("payer_code");
					data.approved_by=rs.getString("approved_by");
					data.created_at=rs.getString("created_at");
					data.created_by=rs.getString("created_by");
					data.bank_ref_no=rs.getString("bank_ref_no");
					data.charge_status=rs.getString("charge_status");
					data.amount_charged=rs.getString("amount_charged");
					data.approved=rs.getString("approved");
					data.payer_name=rs.getString("payer_name");
					model.add(data);
				}
			}else {
			conn = DbManager.getConnection();
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code,approved, amount_to_pay"
					+ ",approved_by,created_at,status,created_by,bank_ref_no,amount_charged,charge_status,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
					+"(SELECT company_name  FROM biller_profile E WHERE E.comp_code=b.payer_code) payer_name, \n"
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `eslip` b WHERE biller_code=? AND payer_code=? AND status=? AND created_at  BETWEEN ? AND ? ORDER BY id DESC"; 
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getBiller_code());
			prep.setObject(2, jsondata.getPayer_code());
			prep.setObject(3, "PAID");
			 prep.setObject(4, EslipHelper.formartDate(jsondata.getDatefrom())+" 00:00");
			 prep.setObject(5, EslipHelper.formartDate(jsondata.getTodate())+" 23:59");
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				MyESlipsProduceJSon data = new MyESlipsProduceJSon();
				data.id=rs.getString("id");
				data.eslip_no=rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.expiry_date=rs.getString("expiry_date");
				data.clients_name=rs.getString("client_name");
				data.amount_to_pay=rs.getString("amount_to_pay");
				data.status=rs.getString("status");
				data.account_no=rs.getString("account_no");
				data.accounts=rs.getString("accounts");
				data.biller_code=rs.getString("biller_code");
				data.payer_code=rs.getString("payer_code");
				data.approved_by=rs.getString("approved_by");
				data.created_at=rs.getString("created_at");
				data.created_by=rs.getString("created_by");
				data.bank_ref_no=rs.getString("bank_ref_no");
				data.charge_status=rs.getString("charge_status");
				data.amount_charged=rs.getString("amount_charged");
				data.approved=rs.getString("approved");
				data.payer_name=rs.getString("payer_name");
				model.add(data);
			}
		}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	@RequestMapping(value = "/v1/getAllEslipService", method = RequestMethod.POST)
	public ResponseEntity<?> getAllEslipService(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata) {
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		System.out.print(EslipHelper.formartDate(jsondata.getTodate() +" 00:00"));
		try {
			conn = DbManager.getConnection();
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay"
					+ ",approved_by,created_at,status,created_by,bank_ref_no,amount_charged,charge_status,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `eslip` b WHERE status=? ORDER BY id DESC"; 
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "PAID");
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				MyESlipsProduceJSon data = new MyESlipsProduceJSon();
				data.id=rs.getString("id");
				data.eslip_no=rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.expiry_date=rs.getString("expiry_date");
				data.clients_name=rs.getString("client_name");
				data.amount_to_pay=rs.getString("amount_to_pay");
				data.status=rs.getString("status");
				data.account_no=rs.getString("account_no");
				data.accounts=rs.getString("accounts");
				data.biller_code=rs.getString("biller_code");
				data.payer_code=rs.getString("payer_code");
				data.approved_by=rs.getString("approved_by");
				data.created_at=rs.getString("created_at");
				data.created_by=rs.getString("created_by");
				data.bank_ref_no=rs.getString("bank_ref_no");
				data.charge_status=rs.getString("charge_status");
				data.amount_charged=rs.getString("amount_charged");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	@RequestMapping(value = "/v1/eslipReconcileReport", method = RequestMethod.POST)
    public ResponseEntity<?> eslipReconcileReport(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata){
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		try {
			if(jsondata.getEslip_no().isEmpty()) {
				if(jsondata.getBiller_code().isEmpty() ||   jsondata.getDatefrom().isEmpty()  || jsondata.getTodate().isEmpty()) {
					 return ResponseEntity.ok(new AuthProduceJson("06","Please select biller and dates","",""));
				}
				
			if(jsondata.getPayer_code().isEmpty()) {
				String sql="SELECT *,\r\n" + 
						"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name,\r\n" + 
						"(SELECT company_name FROM biller_profile C WHERE C.comp_code=A.payer_code)payer_name,\r\n" + 
						"(SELECT approved_by FROM eslip D WHERE D.eslip_no=A.eslip_no)approved_by,\r\n" + 
						"(SELECT created_by FROM eslip E WHERE E.eslip_no=A.eslip_no)created_by,\r\n" + 
						"(SELECT bank_ref_no FROM eslip F WHERE F.eslip_no=A.eslip_no)bank_ref_no,\r\n" + 
						"(SELECT amount_to_pay FROM eslip G WHERE G.eslip_no=A.eslip_no)eslip_amount,\r\n" + 
						"(SELECT COUNT(*)vcount FROM eslip_bills H WHERE H.eslip_no=A.eslip_no)accounts\r\n" + 
						" FROM eslip_bills A WHERE status=?  AND biller_code=? AND created_at  BETWEEN ? AND ?";
				  conn=DbManager.getConnection();
					 PreparedStatement prep=conn.prepareStatement(sql);
					 prep.setObject(1, "PAID");
					 prep.setObject(2, jsondata.getBiller_code());
					 prep.setObject(3, EslipHelper.formartDate(jsondata.getDatefrom())+" 00:00");
					 prep.setObject(4, EslipHelper.formartDate(jsondata.getTodate())+" 23:59");
					 ResultSet rs=prep.executeQuery();
					 while(rs.next()) {
						  MyESlipsProduceJSon data = new MyESlipsProduceJSon();
							data.id=rs.getString("id");
							data.eslip_no=rs.getString("eslip_no");
							data.amount_due = rs.getString("amount_due");
							data.amount_to_pay = rs.getString("amount_to_pay");
							data.expiry_date=rs.getString("due_date");
							data.payer_name=rs.getString("payer_name");
							data.biller_name=rs.getString("biller_name");
							data.status=rs.getString("status");
							data.account_no=rs.getString("account_no");
							data.biller_code=rs.getString("biller_code");
							data.payer_code=rs.getString("payer_code");
							data.approved_by=rs.getString("approved_by");
							data.created_at=rs.getString("created_at");
							data.created_by=rs.getString("created_by");
							data.bank_ref_no=rs.getString("bank_ref_no");
							data.payment_date=rs.getString("payment_date");
							data.biller_payment_ref=rs.getString("biller_payment_ref");
							data.accounts=rs.getString("accounts");
							data.eslip_amount=rs.getString("eslip_amount");
							data.account_name=rs.getString("account_name");
							model.add(data);
					  }
					  conn.close();
					  return ResponseEntity.ok(model);	
			}else {
			String sql="SELECT *,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name,\r\n" + 
					"(SELECT company_name FROM biller_profile C WHERE C.comp_code=A.payer_code)payer_name,\r\n" + 
					"(SELECT approved_by FROM eslip D WHERE D.eslip_no=A.eslip_no)approved_by,\r\n" + 
					"(SELECT created_by FROM eslip E WHERE E.eslip_no=A.eslip_no)created_by,\r\n" + 
					"(SELECT bank_ref_no FROM eslip F WHERE F.eslip_no=A.eslip_no)bank_ref_no,\r\n" + 
					"(SELECT amount_to_pay FROM eslip G WHERE G.eslip_no=A.eslip_no)eslip_amount,\r\n" + 
					"(SELECT COUNT(*)vcount FROM eslip_bills H WHERE H.eslip_no=A.eslip_no)accounts\r\n" + 
					" FROM eslip_bills A WHERE status=? AND  payer_code=? AND biller_code=? AND created_at  BETWEEN ? AND ?";
			  conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, "PAID");
				 prep.setObject(2, jsondata.getPayer_code());
				 prep.setObject(3, jsondata.getBiller_code());
				 prep.setObject(4, EslipHelper.formartDate(jsondata.getDatefrom())+" 00:00");
				 prep.setObject(5, EslipHelper.formartDate(jsondata.getTodate())+" 23:59");
				 ResultSet rs=prep.executeQuery();
				 while(rs.next()) {
					  MyESlipsProduceJSon data = new MyESlipsProduceJSon();
						data.id=rs.getString("id");
						data.eslip_no=rs.getString("eslip_no");
						data.amount_due = rs.getString("amount_due");
						data.amount_to_pay = rs.getString("amount_to_pay");
						data.expiry_date=rs.getString("due_date");
						data.payer_name=rs.getString("payer_name");
						data.biller_name=rs.getString("biller_name");
						data.status=rs.getString("status");
						data.account_no=rs.getString("account_no");
						data.biller_code=rs.getString("biller_code");
						data.payer_code=rs.getString("payer_code");
						data.approved_by=rs.getString("approved_by");
						data.created_at=rs.getString("created_at");
						data.created_by=rs.getString("created_by");
						data.bank_ref_no=rs.getString("bank_ref_no");
						data.payment_date=rs.getString("payment_date");
						data.biller_payment_ref=rs.getString("biller_payment_ref");
						data.accounts=rs.getString("accounts");
						data.eslip_amount=rs.getString("eslip_amount");
						data.account_name=rs.getString("account_name");
						model.add(data);
				  }
				  conn.close();
				  return ResponseEntity.ok(model);	
			}
			}else {
				
				String sql="SELECT *,\r\n" + 
						"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name,\r\n" + 
						"(SELECT company_name FROM biller_profile C WHERE C.comp_code=A.payer_code)payer_name,\r\n" + 
						"(SELECT approved_by FROM eslip D WHERE D.eslip_no=A.eslip_no)approved_by,\r\n" + 
						"(SELECT created_by FROM eslip E WHERE E.eslip_no=A.eslip_no)created_by,\r\n" + 
						"(SELECT bank_ref_no FROM eslip F WHERE F.eslip_no=A.eslip_no)bank_ref_no,\r\n" + 
						"(SELECT amount_to_pay FROM eslip G WHERE G.eslip_no=A.eslip_no)eslip_amount,\r\n" + 
						"(SELECT COUNT(*)vcount FROM eslip_bills H WHERE H.eslip_no=A.eslip_no)accounts\r\n" + 
						" FROM eslip_bills A WHERE eslip_no=?";
				  conn=DbManager.getConnection();
					 PreparedStatement prep=conn.prepareStatement(sql);
					 prep.setObject(1, jsondata.getEslip_no());
					 ResultSet rs=prep.executeQuery();
					 while(rs.next()) {
						  MyESlipsProduceJSon data = new MyESlipsProduceJSon();
							data.id=rs.getString("id");
							data.eslip_no=rs.getString("eslip_no");
							data.amount_due = rs.getString("amount_due");
							data.amount_to_pay = rs.getString("amount_to_pay");
							data.expiry_date=rs.getString("due_date");
							data.payer_name=rs.getString("payer_name");
							data.biller_name=rs.getString("biller_name");
							data.status=rs.getString("status");
							data.account_no=rs.getString("account_no");
							data.biller_code=rs.getString("biller_code");
							data.payer_code=rs.getString("payer_code");
							data.approved_by=rs.getString("approved_by");
							data.created_at=rs.getString("created_at");
							data.created_by=rs.getString("created_by");
							data.bank_ref_no=rs.getString("bank_ref_no");
							data.payment_date=rs.getString("payment_date");
							data.biller_payment_ref=rs.getString("biller_payment_ref");
							data.accounts=rs.getString("accounts");
							data.eslip_amount=rs.getString("eslip_amount");
							data.account_name=rs.getString("account_name");
							model.add(data);
					  }
					  conn.close();
					  return ResponseEntity.ok(model);	
				
				
			}
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		
		
	}
	
	@RequestMapping(value = "/v1/updateServiceCharge", method = RequestMethod.POST)
	public ResponseEntity<?> updateServiceCharge(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
		String eslip_no="";
		try {
			JSONObject obj = new JSONObject(jsondata);
			JSONArray slipInfoArray = obj.getJSONArray("eslips");
			String sql = "UPDATE eslip SET charge_status=? WHERE eslip_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			int ii=0;
			for (int i = 0; i < slipInfoArray.length(); i++) {
				JSONObject object = slipInfoArray.getJSONObject(i);
				eslip_no = object.getString("eslip_no");
				prep.setObject(1, "Paid"); 
				prep.setObject(2, eslip_no); 
				prep.addBatch();
				ii++;
				if (ii % 1000 == 0 || ii == slipInfoArray.length()) {
					System.out.println(">Updating some eslips="+ii);
					prep.executeBatch(); // Execute every 1000 items
					conn.commit();
		            prep.clearBatch();
				}
			}
			return ResponseEntity.ok(new AuthProduceJson("00", "Eslips updated successfully", "", ""));
			
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	@RequestMapping(value = "/v1/getExceptionLogs", method = RequestMethod.POST)
	public ResponseEntity<?> getExceptionLogs(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody ExceptionLogResponse jsondata) {
		List<ExceptionLogResponse> model = new ArrayList<ExceptionLogResponse>();
		try {
			conn = DbManager.getConnection();
			String sql="SELECT * ,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name\r\n" + 
					"FROM exception_logs A";
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				ExceptionLogResponse data=new ExceptionLogResponse();
				data.id=rs.getString("id");
				data.date=rs.getString("date");
				data.amount=rs.getString("amount");
				data.reference=rs.getString("reference");
				data.ft=rs.getString("ft");
				data.eslip_no=rs.getString("eslip_no");
				data.created_at=rs.getString("created_at");
				data.updated_at=rs.getString("updated_at");
				data.status=rs.getString("status");
				data.biller_name=rs.getString("biller_name");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}

	}
	
	@RequestMapping(value = "/v1/getExceptionLogsBiller", method = RequestMethod.POST)
	public ResponseEntity<?> getExceptionLogsBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody ExceptionLogResponse jsondata) {
		List<ExceptionLogResponse> model = new ArrayList<ExceptionLogResponse>();
		String biller_code=TokenManager.tokenIssuedCompCode(token);
		try {
			conn = DbManager.getConnection();
			String sql="SELECT * ,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name\r\n" + 
					"FROM exception_logs A WHERE biller_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, biller_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				ExceptionLogResponse data=new ExceptionLogResponse();
				data.id=rs.getString("id");
				data.date=rs.getString("date");
				data.amount=rs.getString("amount");
				data.reference=rs.getString("reference");
				data.ft=rs.getString("ft");
				data.eslip_no=rs.getString("eslip_no");
				data.created_at=rs.getString("created_at");
				data.updated_at=rs.getString("updated_at");
				data.status=rs.getString("status");
				data.biller_name=rs.getString("biller_name");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}

	}
	
	@RequestMapping(value = "/v1/ignoreExceptionLogs", method = RequestMethod.POST)
	public ResponseEntity<?> ignoreExceptionLogs(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody ExceptionLogResponse jsondata) {
		if(jsondata.getId().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide ID"));
		}
		try {
			conn = DbManager.getConnection();
			String sql="DELETE FROM exception_logs WHERE id=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getId());
		    prep.execute();
			conn.close();
			return ResponseEntity.ok(new Response("00", "Exception removed successfully"));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}

	}
	
	@RequestMapping(value = "/v1/rejectPayment", method = RequestMethod.POST)
	public ResponseEntity<?> rejectPayment(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody ExceptionLogResponse jsondata) {
		if(jsondata.getEslip_no().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide eslip no"));
		}
		try {
			conn = DbManager.getConnection();
			String sql="UPDATE  exception_logs set status=? WHERE eslip_no=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Pending");
			prep.setObject(2,jsondata.getEslip_no());
		    prep.execute();
			conn.close();
			return ResponseEntity.ok(new Response("00", "Exception rejected successfully"));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}

	}
	
	@RequestMapping(value = "/v1/validateEslip", method = RequestMethod.POST)
	public ResponseEntity<?> validateEslip(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PaymentModel jsondata) {
		if(jsondata.getEslipNo().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide eslip number"));
		}
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		try {
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code,approved, amount_to_pay"
					+ ",approved_by,created_at,status,created_by,bank_ref_no,amount_charged,charge_status,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name FROM biller_profile C WHERE C.comp_code=b.biller_code)biller_name, \n"
					+ "(SELECT company_name FROM biller_profile D WHERE D.comp_code=b.payer_code)payer_name, \n"
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code)account_no \n" + 
					"FROM `eslip` b WHERE eslip_no=? AND status=?"; 
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEslipNo());
			prep.setObject(2, "Pending");
			ResultSet rs = prep.executeQuery();
			while(rs.next()) {
				MyESlipsProduceJSon data = new MyESlipsProduceJSon();
				data.id=rs.getString("id");
				data.eslip_no=rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.expiry_date=rs.getString("expiry_date");
				data.amount_to_pay=rs.getString("amount_to_pay");
				data.status=rs.getString("status");
				data.account_no=rs.getString("account_no");
				data.accounts=rs.getString("accounts");
				data.biller_code=rs.getString("biller_code");
				data.payer_code=rs.getString("payer_code");
				data.approved_by=rs.getString("approved_by");
				data.created_at=rs.getString("created_at");
				data.created_by=rs.getString("created_by");
				data.bank_ref_no=rs.getString("bank_ref_no");
				data.charge_status=rs.getString("charge_status");
				data.amount_charged=rs.getString("amount_charged");
				data.approved=rs.getString("approved");
				data.biller_name=rs.getString("biller_name");
				data.payer_name=rs.getString("payer_name");
				model.add(data);
			}
			conn.close();
			if(model.isEmpty()) {
				return ResponseEntity.ok(new AuthProduceJson("06", "No such eslip", "", ""));
			}
			return ResponseEntity.ok(model);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}

	}
	
	
	
	@RequestMapping(value = "/v1/payEslip", method = RequestMethod.POST)
	public ResponseEntity<?> payEslip(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PaymentModel jsondata) {
		
		if(jsondata.getEslipNo().isEmpty() || jsondata.getPaidAmount().isEmpty() || jsondata.getPaymentDate().isEmpty()
				|| jsondata.getFt().isEmpty() || jsondata.getId().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please enter all fields", "", ""));
		}
		String paid_by=TokenManager.tokenIssuedCompCode(token);
		int checkeslip=EslipHelper.checkEslip(jsondata.getEslipNo(), jsondata.getPaidAmount());
		if(checkeslip!=1) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Invalid eslip number or amount", "", ""));
		}
		
		try {
			String sql="UPDATE eslip SET approved=?,payment_type=?,payment_refno=?,paid_by=?,payment_date=? WHERE eslip_no=?";
			String sql2="UPDATE exception_logs set status=?,eslip_no=?,paid_by=? WHERE id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep.setObject(1, "N");
			prep.setObject(2, "Manual");
			prep.setObject(3, jsondata.getFt());
			prep.setObject(4, paid_by);
			prep.setObject(5, jsondata.getPaymentDate());
			prep.setObject(6, jsondata.getEslipNo());
			
			prep2.setObject(1, "InProgress");
			prep2.setObject(2, jsondata.getEslipNo());
			prep2.setObject(3, paid_by);
			prep2.setObject(4, jsondata.getId());
			prep.execute();
			prep2.execute();
			conn.close();
			return ResponseEntity.ok(new AuthProduceJson("00", "Eslip queued successfully for approval", "", ""));
			
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	@RequestMapping(value = "/v1/approveEslip", method = RequestMethod.POST)
	public ResponseEntity<?> approveEslip(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PaymentModel jsondata) {
		if(jsondata.getEslipNo().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide eslip number", "", ""));
		}
		String approved_by=TokenManager.tokenIssuedCompCode(token);
		String paidamount="";
		String paymentref="";
		String paymentdate="";
		String paid_by="";
		try {
			String sql="SELECT * FROM exception_logs WHERE eslip_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEslipNo());
			ResultSet rs = prep.executeQuery();
			while(rs.next()) {
				paidamount=rs.getString("amount");
				paymentref=rs.getString("ft");
				paymentdate=rs.getString("date");
				paid_by=rs.getString("paid_by");
			}
			conn.close();
			System.out.println("Amount Paid==>>"+paidamount);
			System.out.println("PaymentRefNo==>>"+paymentref);
			System.out.println("PaymentDate==>>"+paymentdate);
			System.out.println("Eslip no==>>"+jsondata.getEslipNo());
			
			if(paid_by.equalsIgnoreCase(approved_by)) {
				return ResponseEntity.ok(new Response("06", "You are not authorised to approve this payment"));
			}
			if(paidamount.isEmpty() || paymentdate.isEmpty() || paymentref.isEmpty()) {
				return ResponseEntity.ok(new Response("06", "There is no such eslip pending or invalid exception"));
			}
//			System.out.println("Amount Paid==>>"+paidamount);
//			System.out.println("PaymentRefNo==>>"+paymentref);
//			System.out.println("PaymentDate==>>"+paymentdate);
//			System.out.println("Eslip no==>>"+jsondata.getEslipNo());
			
			Hashtable<String,String>map=new Hashtable<String,String>();
			 map.put("EslipNo",jsondata.getEslipNo());
			 map.put("PaidAmount",paidamount);
			 map.put("PaymentRefNo",paymentref);
			 map.put("PaymentDate",paymentdate);
			 String response=GeneralCodes.encryptedPostRequest(map, ExternalFile.getPaymentUrl(), "JSON");
			 System.out.println("Response==>>"+response);
			 JSONObject obj = new JSONObject(response);
	         String jsonResponse = obj.toString();
	         JsonParser parser = new JsonParser();
	         JsonElement jsonTree = parser.parse(jsonResponse);
	         com.google.gson.JsonObject jsonObject = jsonTree.getAsJsonObject();
	         JsonElement ResponseCode = jsonObject.get("ResponseCode");
	         JsonElement ResponseMessage = jsonObject.get("ResponseMessage");
	         if(ResponseCode.toString().equalsIgnoreCase("10") || ResponseCode.toString().equalsIgnoreCase("00")) {
	        	 EslipHelper.approveEslip(jsondata.getEslipNo(), approved_by);
		         return ResponseEntity.ok(new Response(ResponseCode.toString(), ResponseMessage.toString()));
	         }else {
	        	  return ResponseEntity.ok(new Response(ResponseCode.toString(), ResponseMessage.toString())); 
	         }
	         
		}catch(Exception n) {
			n.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
		
		
		
	}
	
	
	@RequestMapping(value = "/v1/readExceptionLogs", method = RequestMethod.POST)
	public ResponseEntity<?> readExceptionLogs2(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody ExceptionLogResponse jsondata) {
		if(jsondata.getFile_name().isEmpty()) {
			return ResponseEntity.ok(new Response("06","Provide file name"));
		}
		String path=ExternalFile.getExceptionURL()+jsondata.getFile_name()+".xml";
	    String account_no="";
	    String biller_code="";
	    String prefix="";
	    
		try{  
		File file = new File(path);   
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  
		DocumentBuilder db = dbf.newDocumentBuilder();  
		Document doc = db.parse(file);  
		doc.getDocumentElement().normalize();   
		NodeList nodeList = doc.getElementsByTagName("Dataset");    
		for (int itr = 0; itr < 1; itr++){  
		Node node = nodeList.item(itr);    
		if (node.getNodeType() == Node.ELEMENT_NODE){  
		Element eElement = (Element) node;  
		account_no=eElement.getElementsByTagName("AccountNo").item(0).getTextContent();
		  }  
		} 
		String sql="SELECT prefix,comp_code FROM biller_profile WHERE stb_acc_no=?";
		 conn=DbManager.getConnection();
		 PreparedStatement prep=conn.prepareStatement(sql);
		 prep.setObject(1, account_no);
		 ResultSet rs=prep.executeQuery();
		while(rs.next()) {
			biller_code=rs.getString("comp_code");
			prefix=rs.getString("prefix");
		}
		conn.close();
		}catch (Exception e)  {  
		e.printStackTrace();  
		return ResponseEntity.ok(new Response("02","System technical error in the first step"));
		}  
		if(biller_code.isEmpty() || prefix.isEmpty()) {
			return ResponseEntity.ok(new Response("06","Could not get details of the account number in the file"));	
		}
		
		Vector data=EslipHelper.readException(path,prefix,biller_code);
		if(data.isEmpty()) {
			return ResponseEntity.ok(new Response("06", "No relevant data read from the file"));
		}
		
		EslipHelper.saveExceptions(data);
		return ResponseEntity.ok(new Response("00","File read successfully"));	
	}
	
	
}
