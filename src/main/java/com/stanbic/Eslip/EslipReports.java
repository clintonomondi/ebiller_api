package com.stanbic.Eslip;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.ini4j.Ini;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;

import com.stanbic.Report.EslipPdfReportRequestModel;
import com.stanbic.Report.ReportInit;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class EslipReports {
	Connection conn = null;
	
	@RequestMapping(value = "/v1/getEslipReports", method = RequestMethod.POST)
	public ResponseEntity<?> getEslipReports(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata) {
		if(jsondata.getBiller_code().isEmpty() || jsondata.getBiller_code().equalsIgnoreCase("")) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please provide biller code","",""));
		}
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
		 
		try {
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay"
					+ ",approved_by,created_at,status,bank_ref_no,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n" 
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `eslip` b WHERE payer_code=? AND biller_code=?  GROUP BY id,eslip_no,amount_due,expiry_date,payer_code,biller_code,status, amount_to_pay,approved_by,created_at,bank_ref_no ORDER BY id DESC"; 
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			prep.setObject(2, jsondata.getBiller_code());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				MyESlipsProduceJSon data = new MyESlipsProduceJSon();
				data.id=rs.getString("id"); 
				data.messageCode="00";
				data.message="success";
				data.eslip_no=rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.expiry_date=rs.getString("expiry_date");
				data.clients_name=rs.getString("client_name");
				data.amount_due=rs.getString("amount_due");
				data.amount_to_pay=rs.getString("amount_to_pay");
				data.status=rs.getString("status");
				data.account_no=rs.getString("account_no");
				data.accounts=rs.getString("accounts");
				data.biller_code=rs.getString("biller_code");
				data.payer_code=rs.getString("payer_code");
				data.approved_by=rs.getString("approved_by");
				data.created_at=rs.getString("created_at");
				data.bank_ref_no=rs.getString("bank_ref_no");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	
	@RequestMapping(value = "/v1/eslipReconcileReportBiller", method = RequestMethod.POST)
    public ResponseEntity<?> eslipReconcileReportBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata){
		if(jsondata.getDatefrom().isEmpty() || jsondata.getTodate().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide all fields","",""));
		}
		String biller_code=TokenManager.tokenIssuedCompCode(token);
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		try {
			String sql="SELECT *,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name,\r\n" + 
					"(SELECT company_name FROM biller_profile C WHERE C.comp_code=A.payer_code)payer_name,\r\n" + 
					"(SELECT approved_by FROM eslip D WHERE D.eslip_no=A.eslip_no)approved_by,\r\n" + 
					"(SELECT created_by FROM eslip E WHERE E.eslip_no=A.eslip_no)created_by,\r\n" + 
					"(SELECT bank_ref_no FROM eslip F WHERE F.eslip_no=A.eslip_no)bank_ref_no,\r\n" + 
					"(SELECT amount_to_pay FROM eslip G WHERE G.eslip_no=A.eslip_no)eslip_amount,\r\n" + 
					"(SELECT COUNT(*)vcount FROM eslip_bills H WHERE H.eslip_no=A.eslip_no)accounts\r\n" + 
					" FROM eslip_bills A WHERE status=? AND biller_code=? AND created_at  BETWEEN ? AND ?";
			  conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, "PAID");
				 prep.setObject(2, biller_code);
				 prep.setObject(3, EslipHelper.formartDate(jsondata.getDatefrom())+" 00:00");
				 prep.setObject(4, EslipHelper.formartDate(jsondata.getTodate())+" 23:59");
				  ResultSet rs=prep.executeQuery();
				  while(rs.next()) {
					  MyESlipsProduceJSon data = new MyESlipsProduceJSon();
						data.id=rs.getString("id");
						data.eslip_no=rs.getString("eslip_no");
						data.amount_due = rs.getString("amount_due");
						data.amount_to_pay = rs.getString("amount_to_pay");
						data.expiry_date=rs.getString("due_date");
						data.payer_name=rs.getString("payer_name");
						data.biller_name=rs.getString("biller_name");
						data.status=rs.getString("status");
						data.account_no=rs.getString("account_no");
						data.biller_code=rs.getString("biller_code");
						data.payer_code=rs.getString("payer_code");
						data.approved_by=rs.getString("approved_by");
						data.created_at=rs.getString("created_at");
						data.created_by=rs.getString("created_by");
						data.bank_ref_no=rs.getString("bank_ref_no");
						data.payment_date=rs.getString("payment_date");
						data.biller_payment_ref=rs.getString("biller_payment_ref");
						data.accounts=rs.getString("accounts");
						data.eslip_amount=rs.getString("eslip_amount");
						data.account_name=rs.getString("account_name");
						model.add(data);
				  }
				  conn.close();
				  return ResponseEntity.ok(model);	
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	
	}
	
	
	@RequestMapping(value = "/v1/eslipReconcileReportPayer", method = RequestMethod.POST)
    public ResponseEntity<?> eslipReconcileReportPayer(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata){
		if(jsondata.getBiller_code().isEmpty()||jsondata.getDatefrom().isEmpty() || jsondata.getTodate().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please select biller","",""));
		}
		String payer_code=TokenManager.tokenIssuedCompCode(token);
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		try {
			String sql="SELECT *,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name,\r\n" + 
					"(SELECT company_name FROM biller_profile C WHERE C.comp_code=A.payer_code)payer_name,\r\n" + 
					"(SELECT approved_by FROM eslip D WHERE D.eslip_no=A.eslip_no)approved_by,\r\n" + 
					"(SELECT created_by FROM eslip E WHERE E.eslip_no=A.eslip_no)created_by,\r\n" + 
					"(SELECT bank_ref_no FROM eslip F WHERE F.eslip_no=A.eslip_no)bank_ref_no,\r\n" + 
					"(SELECT amount_to_pay FROM eslip G WHERE G.eslip_no=A.eslip_no)eslip_amount,\r\n" + 
					"(SELECT COUNT(*)vcount FROM eslip_bills H WHERE H.eslip_no=A.eslip_no)accounts\r\n" + 
					" FROM eslip_bills A WHERE status='PAID' AND  biller_code=? AND payer_code=? AND created_at  BETWEEN ? AND ?";
			  conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, jsondata.getBiller_code());
				 prep.setObject(2, payer_code);
				 prep.setObject(3, EslipHelper.formartDate(jsondata.getDatefrom())+" 00:00");
				 prep.setObject(4, EslipHelper.formartDate(jsondata.getTodate())+" 23:59");
				  ResultSet rs=prep.executeQuery();
				  while(rs.next()) {
					  MyESlipsProduceJSon data = new MyESlipsProduceJSon();
						data.id=rs.getString("id");
						data.eslip_no=rs.getString("eslip_no");
						data.amount_due = rs.getString("amount_due");
						data.amount_to_pay = rs.getString("amount_to_pay");
						data.expiry_date=rs.getString("due_date");
						data.payer_name=rs.getString("payer_name");
						data.biller_name=rs.getString("biller_name");
						data.status=rs.getString("status");
						data.account_no=rs.getString("account_no");
						data.biller_code=rs.getString("biller_code");
						data.payer_code=rs.getString("payer_code");
						data.approved_by=rs.getString("approved_by");
						data.created_at=rs.getString("created_at");
						data.created_by=rs.getString("created_by");
						data.bank_ref_no=rs.getString("bank_ref_no");
						data.payment_date=rs.getString("payment_date");
						data.biller_payment_ref=rs.getString("biller_payment_ref");
						data.accounts=rs.getString("accounts");
						data.eslip_amount=rs.getString("eslip_amount");
						data.account_name=rs.getString("account_name");
						model.add(data);
				  }
				  conn.close();
				  return ResponseEntity.ok(model);	
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	
	}
	
	
	


}
