package com.stanbic.Eslip;

public class ExceptionLogResponse {
	
	String id = "";
	String date = "";
	String amount = "";
	String reference = "";
	String ft = "";
	String eslip_no = "";
	String created_at = "";
	String updated_at = "";
	String file_name="";
	String status="";
	String biller_name="";
	
	
	
	public String getBiller_name() {
		return biller_name;
	}
	public void setBiller_name(String biller_name) {
		this.biller_name = biller_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getComp_code() {
		return comp_code;
	}
	public void setComp_code(String comp_code) {
		this.comp_code = comp_code;
	}
	public String getPayer_name() {
		return payer_name;
	}
	public void setPayer_name(String payer_name) {
		this.payer_name = payer_name;
	}
	String comp_code="";
	String payer_name = "";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getFt() {
		return ft;
	}
	public void setFt(String ft) {
		this.ft = ft;
	}
	public String getEslip_no() {
		return eslip_no;
	}
	public void setEslip_no(String eslip_no) {
		this.eslip_no = eslip_no;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

}
