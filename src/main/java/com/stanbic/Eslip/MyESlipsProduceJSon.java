package com.stanbic.Eslip;

public class MyESlipsProduceJSon {
String messageCode="00";
String message="success";
String id=null;
String clients_name="";
String eslip_no="";
String expiry_date="";
String amount_due="";
String amount_to_pay="";
String status="";
String account_no="";
String accounts="";
String biller_code="";
String payer_code="";
String approved_by="";
String created_at="";
String bank_ref_no="";
String created_by="";
String ref_no="";
String file_id="";
String path="";
String account_to_add="";
String oddcent="";
String charge_status="";
String amount_charged="";
String payer_name="";
String biller_name="";
String payment_date="";
String biller_payment_ref="";
String approved="";
String eslip_amount="";
String account_name="";



public String getAccount_name() {
	return account_name;
}
public void setAccount_name(String account_name) {
	this.account_name = account_name;
}
public String getEslip_amount() {
	return eslip_amount;
}
public void setEslip_amount(String eslip_amount) {
	this.eslip_amount = eslip_amount;
}
public String getApproved() {
	return approved;
}
public void setApproved(String approved) {
	this.approved = approved;
}
public String getBiller_payment_ref() {
	return biller_payment_ref;
}
public void setBiller_payment_ref(String biller_payment_ref) {
	this.biller_payment_ref = biller_payment_ref;
}
public String getPayment_date() {
	return payment_date;
}
public void setPayment_date(String payment_date) {
	this.payment_date = payment_date;
}
public String getPayer_name() {
	return payer_name;
}
public void setPayer_name(String payer_name) {
	this.payer_name = payer_name;
}
public String getBiller_name() {
	return biller_name;
}
public void setBiller_name(String biller_name) {
	this.biller_name = biller_name;
}
public String getCharge_status() {
	return charge_status;
}
public void setCharge_status(String charge_status) {
	this.charge_status = charge_status;
}
public String getAmount_charged() {
	return amount_charged;
}
public void setAmount_charged(String amount_charged) {
	this.amount_charged = amount_charged;
}
public String getAccount_to_add() {
	return account_to_add;
}
public void setAccount_to_add(String account_to_add) {
	this.account_to_add = account_to_add;
}
public String getOddcent() {
	return oddcent;
}
public void setOddcent(String oddcent) {
	this.oddcent = oddcent;
}
public String getPath() {
	return path;
}
public void setPath(String path) {
	this.path = path;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getFile_id() {
	return file_id;
}
public void setFile_id(String file_id) {
	this.file_id = file_id;
}
public String getRef_no() {
	return ref_no;
}
public void setRef_no(String ref_no) {
	this.ref_no = ref_no;
}
public String getCreated_by() {
	return created_by;
}
public void setCreated_by(String created_by) {
	this.created_by = created_by;
}
public String getMessageCode() {
	return messageCode;
}
public void setMessageCode(String messageCode) {
	this.messageCode = messageCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getBank_ref_no() {
	return bank_ref_no;
}
public void setBank_ref_no(String bank_ref_no) {
	this.bank_ref_no = bank_ref_no;
}
public String getClients_name() {
	return clients_name;
}
public void setClients_name(String clients_name) {
	this.clients_name = clients_name;
}
public String getEslip_no() {
	return eslip_no;
}
public void setEslip_no(String eslip_no) {
	this.eslip_no = eslip_no;
}
public String getExpiry_date() {
	return expiry_date;
}
public void setExpiry_date(String expiry_date) {
	this.expiry_date = expiry_date;
}
public String getAmount_due() {
	return amount_due;
}
public void setAmount_due(String amount_due) {
	this.amount_due = amount_due;
}
public String getAmount_to_pay() {
	return amount_to_pay;
}
public void setAmount_to_pay(String amount_to_pay) {
	this.amount_to_pay = amount_to_pay;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getAccount_no() {
	return account_no;
}
public void setAccount_no(String account_no) {
	this.account_no = account_no;
}
public String getAccounts() {
	return accounts;
}
public void setAccounts(String accounts) {
	this.accounts = accounts;
}
public String getBiller_code() {
	return biller_code;
}
public void setBiller_code(String biller_code) {
	this.biller_code = biller_code;
}
public String getPayer_code() {
	return payer_code;
}
public void setPayer_code(String payer_code) {
	this.payer_code = payer_code;
}
public String getApproved_by() {
	return approved_by;
}
public void setApproved_by(String approved_by) {
	this.approved_by = approved_by;
}
public String getCreated_at() {
	return created_at;
}
public void setCreated_at(String created_at) {
	this.created_at = created_at;
}


}
