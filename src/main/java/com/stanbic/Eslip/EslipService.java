package com.stanbic.Eslip;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.JsonObject;

import java.io.File;
import java.sql.Connection;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Billers.BillersQueryConsumeJSon;
import com.stanbic.Logs.System_Logs;
import com.stanbic.backendUsers.BankUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class EslipService {
	static Connection conn = null;
	 static String username="";
	
	@RequestMapping(value = "/v1/generateEslip", method = RequestMethod.POST)
	public ResponseEntity<?> generateEslip(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
		Vector vector = new Vector();
	try {
		// extract e slip from
		JSONObject obj = new JSONObject(jsondata);
		String biller_code = obj.getString("biller_code");
		String total_amount_due = obj.getString("total_amount_due");
		double total_amount_to_pay =Double.parseDouble(obj.getString("total_amount_to_pay"));
		String account_no=null;
		double amount_to_pay = 0.0;
		String amount_due = "";
		String eslip_no = "";
		String due_date = "";
		String account_name="";
		String payer_code = TokenManager.tokenIssuedCompCode(token);
		String user_email=TokenManager.tokenIssuedId( token);

		System.out.println("Amount to pay total=>"+obj.getString("total_amount_to_pay"));

		// generate e slip number
		eslip_no = eslipNoGen(biller_code);
		due_date=expiryDate();
		JSONArray slipInfoArray = obj.getJSONArray("eslipInfo");
		int count=1;
		for (int i = 0; i < slipInfoArray.length(); i++) {
			JSONObject object = slipInfoArray.getJSONObject(i);
			account_no = object.getString("account_no");
			amount_to_pay =Double.parseDouble(object.getString("amount_to_pay"));
			amount_due = object.getString("amount_due");
			account_name=object.getString("account_name");
			
			if(amount_to_pay<=0.0) {
				 return ResponseEntity.ok(new EslipResponse("07","Some account numbers have zero amount to pay",""));
			}
			Vector holder = new Vector();
			holder.addElement(eslip_no);
			holder.addElement(amount_due);
			holder.addElement(amount_to_pay);
			holder.addElement(due_date);
			holder.addElement(account_no);
			holder.addElement(count);
			holder.addElement(account_name);
			vector.addElement(holder);
			count++;
		}
		String sql = "INSERT INTO eslip_bills (eslip_no,amount_due,amount_to_pay,due_date,account_no,count,account_name,biller_code,payer_code)VALUES(?,?,?,?,?,?,?,?,?)";
		conn = DbManager.getConnection();
		PreparedStatement prep = conn.prepareStatement(sql);
		conn.setAutoCommit(false);
		int ii=0;
		for (int i = 0; i < vector.size(); i++) {
			Vector data = (Vector) vector.elementAt(i);
			prep.setObject(1, data.toArray()[0].toString()); 
			prep.setObject(2, data.toArray()[1].toString());
			prep.setObject(3, data.toArray()[2].toString());
			prep.setObject(4, data.toArray()[3].toString());
			prep.setObject(5, data.toArray()[4].toString());
			prep.setObject(6, data.toArray()[5].toString());
			prep.setObject(7, data.toArray()[6].toString());
			prep.setObject(8, biller_code);
			prep.setObject(9, payer_code);
			prep.addBatch();
			ii++;
			if (ii % 1000 == 0 || ii == vector.size()) {
				System.out.println(">Creating eslip accounts at initial stage="+ii);
				prep.executeBatch(); // Execute every 1000 items
				conn.commit();
	            prep.clearBatch();
			}
		}
		conn.setAutoCommit(true);
		conn.close();
		vector.clear();
		insertEslip(eslip_no , due_date, total_amount_due,total_amount_to_pay,biller_code, payer_code,"",user_email);
		System_Logs.log_system(username, "", "user generated eslip:"+eslip_no, "Success", "");
		 return ResponseEntity.ok(new EslipResponse("00","Eslip generated successfully",eslip_no));	
	}catch(Exception e) {
		System.out.println(e.getMessage());
		return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
	}
	
	}
	
	@RequestMapping(value = "/v1/getMyEslips", method = RequestMethod.POST)
	public ResponseEntity<?> getMyEslips(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata) {
		if(jsondata.getBiller_code().isEmpty() || jsondata.getBiller_code().equalsIgnoreCase("")) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide biller_code","",""));
		}
		List<MyESlipsProduceJSon> model = new ArrayList<MyESlipsProduceJSon>();
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
	
		try {
			conn = DbManager.getConnection();
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay"
					+ ",approved_by,created_at,status,created_by,bank_ref_no,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `eslip` b WHERE payer_code=? AND biller_code=?  GROUP BY id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay,"
					+ "approved_by,created_at,status,created_by,bank_ref_no ORDER BY id DESC"; 
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			prep.setObject(2, jsondata.getBiller_code());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				MyESlipsProduceJSon data = new MyESlipsProduceJSon();
				data.id=rs.getString("id");
				data.eslip_no=rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.expiry_date=rs.getString("expiry_date");
				data.clients_name=rs.getString("client_name");
				data.amount_to_pay=rs.getString("amount_to_pay");
				data.status=rs.getString("status");
				data.account_no=rs.getString("account_no");
				data.accounts=rs.getString("accounts");
				data.biller_code=rs.getString("biller_code");
				data.payer_code=rs.getString("payer_code");
				data.approved_by=rs.getString("approved_by");
				data.created_at=rs.getString("created_at");
				data.created_by=rs.getString("created_by");
				data.bank_ref_no=rs.getString("bank_ref_no");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
//			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}

	@RequestMapping(value = "/v1/getEslipInfo", method = RequestMethod.POST)
	public ResponseEntity<?> getEslipInfo(@RequestBody EslipConsumeJson jsondata) {
		if(jsondata.getEslip_no().isEmpty() || jsondata.getEslip_no().equalsIgnoreCase("")) {
			return ResponseEntity.ok(new AuthProduceJson("02", "Please provide eslip number", "", ""));
		}
		String amount_to_pay="";
		String eslip_no="";
		String status="";
		String due_date="";
		String account_no="";
		String amount_due="";
		String client_name="";
		String expiry_date="";
		String eslip_date="";
		String payer_code="";
		String biller_code="";
		String created_by="";
		 String base64="";
		 String company_website="55";
		 String company_customer_care="55";
		 String email="";
		 String biller_phone="";
		 String alias="";
		 String paybill="";
		 String prefix="";
		 String ft="";
		try {
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay"
					+ ",approved_by,created_at,status,bank_ref_no,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name  FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
					+ "(SELECT personel_f_name  FROM biller_profile Y WHERE Y.comp_code=b.payer_code) f_name, \n"
					+ "(SELECT personel_l_name  FROM biller_profile X WHERE X.comp_code=b.payer_code) l_name, \n"
					+ "(SELECT paybill  FROM biller_profile X WHERE X.comp_code=b.biller_code) paybill, \n"
					+ "(SELECT prefix  FROM biller_profile X WHERE X.comp_code=b.biller_code) prefix, \n"
					+ "(SELECT alias  FROM biller_profile X WHERE X.comp_code=b.biller_code) alias, \n"
					+ "(SELECT email  FROM biller_profile Z WHERE Z.comp_code=b.biller_code) email, \n"
					+ "(SELECT if(company_website is null,'',company_website)  FROM biller_profile S WHERE S.comp_code=b.biller_code) company_website, \n"
					+ "(SELECT if(company_customer_care is null,'',company_customer_care)  FROM biller_profile L WHERE L.comp_code=b.biller_code) company_customer_care, \n"
					+ "(SELECT if(biller_phone is null,'',biller_phone)  FROM biller_profile Q WHERE Q.comp_code=b.biller_code) biller_phone, \n"
					+ "(SELECT logo_url  FROM interface_settings T WHERE T.comp_code=b.biller_code) logo_url, \n"
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `eslip` b WHERE eslip_no=? ORDER BY id DESC"; 
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEslip_no());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				 if(rs.getString("logo_url")==null) { 
					 base64="no image";
				 }else {
					 File file=new File(rs.getString("logo_url"));
		    	 base64=FileManager.encodeFileToBase64Binary(file); 
				 }
				
				amount_due = rs.getString("amount_due");
				amount_to_pay = rs.getString("amount_to_pay");
				eslip_no = rs.getString("eslip_no");
				client_name = rs.getString("client_name");
				expiry_date = rs.getString("expiry_date");
				account_no = rs.getString("account_no");
				eslip_date = rs.getString("created_at");
				status = rs.getString("status");
				payer_code = rs.getString("payer_code");
				biller_code = rs.getString("biller_code");
				created_by=rs.getString("f_name")+" "+rs.getString("l_name");
				company_customer_care=rs.getString("company_customer_care");
				company_website=rs.getString("company_website");
				email=rs.getString("email");
				biller_phone=rs.getString("biller_phone");
				alias=rs.getString("alias");
				paybill=rs.getString("paybill");
				prefix=rs.getString("prefix");
				ft=rs.getString("bank_ref_no");
				
			}
			conn.close();
			Hashtable<String, Object> map=new Hashtable<String,Object>();
			map.put("amount_due", amount_due);
			map.put("amount_to_pay", amount_to_pay);
			map.put("eslip_no", eslip_no);
			map.put("status", status);
			map.put("due_date", due_date);
			map.put("account_no", account_no);
			map.put("client_name", client_name);
			map.put("expiry_date", expiry_date);
			map.put("eslip_date", eslip_date);
			map.put("payer_code", payer_code);
			map.put("biller_code", biller_code);
			map.put("created_by",created_by);
			map.put("accountDetails",  accountsForEslip(jsondata.getEslip_no()));
			map.put("billerlogo", base64);
			map.put("company_customer_care",company_customer_care);
			map.put("company_website",company_website);
			map.put("email",email);
			map.put("biller_phone", biller_phone);
			map.put("alias", alias);
			map.put("paybill", paybill);
			map.put("prefix",prefix);
			map.put("ft_no",ft);
			return ResponseEntity.ok(map);
		} catch (Exception e) {
//			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	@RequestMapping(value = "/v1/getEslipBills", method = RequestMethod.POST)
	public ResponseEntity<?> getEslipBills(@RequestBody EslipAccountsReq jsondata) {
		if(jsondata.getEslip_no().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Provide eslip_no", "", ""));
		}
		List<EslipBillsProduceJson> model = new ArrayList<EslipBillsProduceJson>();
		try {
			String sql = "SELECT eslip_no,account_no,amount_due,amount_to_pay,STATUS,due_date,account_name FROM `eslip_bills` WHERE eslip_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEslip_no());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				EslipBillsProduceJson data = new EslipBillsProduceJson();
				data.amount_due = rs.getString("amount_due");
				data.amount_to_pay = rs.getString("amount_to_pay");
				data.eslip_no = rs.getString("eslip_no");
				data.status = rs.getString("status");
				data.due_date=rs.getString("due_date");
				data.account_no=rs.getString("account_no");
				data.account_name=rs.getString("account_name");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	@RequestMapping(value = "/v1/EslipAccounts", method = RequestMethod.POST)
	public ResponseEntity<?> getEslipAccount(@RequestBody EslipAccountsReq jsondata) {
		String eslipNo=jsondata.getEslip_no();
		if(jsondata.getEslip_no().isEmpty() || jsondata.getEslip_no().equals("")) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide eslip no", "", ""));
		}
		List<EslipBillsProduceJson> model = new ArrayList<EslipBillsProduceJson>();
		try {
			String sql = "SELECT *,\r\n" + 
					"(SELECT account_name FROM account B WHERE B.account_no=A.account_no AND B.biller_code=A.biller_code AND B.payer_code=A.payer_code)account_name,\r\n" + 
					"(SELECT amount_due FROM account B WHERE B.account_no=A.account_no AND B.biller_code=A.biller_code AND B.payer_code=A.payer_code)amount_due\r\n" + 
					" FROM `eslip_bills` A WHERE eslip_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslipNo);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				EslipBillsProduceJson data = new EslipBillsProduceJson();
				data.amount_due = rs.getString("amount_due");
				data.amount_to_pay = rs.getString("amount_to_pay");
				data.eslip_no = rs.getString("eslip_no");
				data.status = rs.getString("status");
				data.due_date=rs.getString("due_date");
				data.account_no=rs.getString("account_no");
				data.account_name=rs.getString("account_name");
				data.description=rs.getString("description");
				data.biller_ref=rs.getString("biller_payment_ref");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	
	@RequestMapping(value = "/v1/FailedPaidEslips", method = RequestMethod.POST)
	public ResponseEntity<?> FailedPaidEslips(@RequestBody EslipAccountsReq jsondata) {
		List<EslipBillsProduceJson> model = new ArrayList<EslipBillsProduceJson>();
		try {
			String sql = "SELECT eslip_no,status,description,biller_payment_ref,response_date,\r\n" + 
					"(SELECT amount_to_pay FROM eslip B WHERE B.eslip_no=A.eslip_no)amount_to_pay,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE  B.comp_code=A.biller_code)biller,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE  B.comp_code=A.payer_code)payer\r\n" + 
					" FROM `eslip_bills` A WHERE STATUS='ERROR' ";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				EslipBillsProduceJson data = new EslipBillsProduceJson();
				data.amount_to_pay = rs.getString("amount_to_pay");
				data.eslip_no = rs.getString("eslip_no");
				data.status = rs.getString("status");
				data.description=rs.getString("description");
				data.biller_ref=rs.getString("biller_payment_ref");
				data.response_date=rs.getString("response_date");
				data.payer=rs.getString("payer");
				data.biller=rs.getString("biller");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	@RequestMapping(value = "/v1/PayFailedPaidEslips", method = RequestMethod.POST)
	public ResponseEntity<?> PayFailedPaidEslips(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipAccountsReq jsondata) {
		if(jsondata.getEslip_no().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Provide eslip number", "", ""));
		}
		try {
			conn = DbManager.getConnection();
			
			String sql = "UPDATE `eslip_bills` SET STATUS='Pending' WHERE eslip_no=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEslip_no());
			prep.execute();
			
			
			String sql2 = "DELETE FROM `payment_update_monitor` WHERE SLIP_NO=?";
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep2.setObject(1, jsondata.getEslip_no());
			prep2.execute();
			
			conn.close();
			
			return ResponseEntity.ok(new AuthProduceJson("00", "Eslip payment initiated successfully", "", ""));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		
	}
	
	public static void insertEslip(String eslip_no, String due_date, String amount_due,double amount_to_pay,
			String biller_code, String payer_code,String file_id,String user_email) {
		username=AccountHelper.getName(user_email);
		String date=TimeManager.getEmailTime();
		try {
			String sql = "INSERT INTO eslip (eslip_no,expiry_date,amount_due,amount_to_pay,biller_code,payer_code,file_id,created_by)VALUES(?,?,?,?,?,?,?,?)";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslip_no);
			prep.setObject(2, due_date);
			prep.setObject(3, amount_due);
			prep.setObject(4, amount_to_pay);
			prep.setObject(5, biller_code);
			prep.setObject(6, payer_code);
			prep.setObject(7, file_id);
			prep.setObject(8, user_email);
			prep.execute();
			conn.close();
			Thread t = new Thread() {
				public void run() {
					String subject="Stanbic E-biller";
					  VelocityContext vc = new VelocityContext();
			          vc.put("e_slip_no", eslip_no);
			          vc.put("user_name", username);
			          vc.put("date", date);
			          vc.put("amount", EslipHelper.formatNumber(amount_to_pay));
			          vc.put("time",AccountHelper.getCurrentTime());
			          String email_template = EmailManager.email_message_template(vc,"eslipGenerated.vm");
						EmailManager.send_mail(user_email,email_template,subject,username,"eslip generated="+eslip_no);
				}
			};
			t.start(); 
		} catch (Exception n) {
			System.out.println(n.getMessage());
		}
	}
	
	
	public static void insertEslipBank(String eslip_no, String due_date, String amount_due,String amount_to_pay,
			String biller_code, String payer_code,String file_id,String user_email) {
		username=BankUsers.getBankName(user_email);
		String date=TimeManager.getEmailTime();
		double amount=Double.parseDouble(amount_to_pay);
		try {
			String sql = "INSERT INTO eslip (eslip_no,expiry_date,amount_due,amount_to_pay,biller_code,payer_code,file_id,created_by)VALUES(?,?,?,?,?,?,?,?)";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslip_no);
			prep.setObject(2, due_date);
			prep.setObject(3, amount_due);
			prep.setObject(4, amount_to_pay);
			prep.setObject(5, biller_code);
			prep.setObject(6, payer_code);
			prep.setObject(7, file_id);
			prep.setObject(8, user_email);
			prep.execute();
			conn.close();
			Thread t = new Thread() {
				public void run() {
					String subject="Stanbic E-biller";
					  VelocityContext vc = new VelocityContext();
			          vc.put("e_slip_no", eslip_no);
			          vc.put("user_name", username);
			          vc.put("amount", EslipHelper.formatNumber(amount));
			          vc.put("date", date);
			          vc.put("time",AccountHelper.getCurrentTime());
			          String email_template = EmailManager.email_message_template(vc,"eslipGenerated.vm");
						EmailManager.send_mail(EslipHelper.getPayerEmail(payer_code),email_template,subject,username,"eslip generated="+eslip_no);
				}
			};
			t.start(); 
		} catch (Exception n) {
			System.out.println(n.getMessage());
		}
	}
	
	public List<EslipAccInfoRespModel> accountsForEslip(String eslipNo) {
		
		List<EslipAccInfoRespModel> model=new ArrayList<EslipAccInfoRespModel>();
		try {
			String sql="SELECT ACCOUNT_NO,AMOUNT_TO_PAY,account_name,biller_payment_ref,count FROM eslip_bills where eslip_no=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, eslipNo);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				EslipAccInfoRespModel c=new EslipAccInfoRespModel();
				c.accountAmount=rs.getString("AMOUNT_TO_PAY");
				c.accountNo=rs.getString("ACCOUNT_NO");
				c.account_name=rs.getString("account_name");
				c.biller_payment_ref=rs.getString("biller_payment_ref");
				c.count=rs.getString("count");
				model.add(c);
			}
			conn.close();
		}catch(Exception e) {
//			e.printStackTrace();
		}
		return model;
	}

	public static String genRandomNo(String biller_code) {
		int biller_id=0;
		String padded="";
		try {
			String sql = "SELECT COUNT(*)vcount FROM `eslip` WHERE biller_code=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, biller_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				biller_id = rs.getInt("vcount")+1;
			}
			conn.close();
			 padded = String.format("%07d" , biller_id);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		return padded;
	}
	
	
	
	
public static int genRandom() {
	Random generator = new Random();
	return 1000 + generator.nextInt(9000);
}




	public static String eslipNoGen(String compCode) {
		String slipNo = "";
		String prefix = "";

		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);

		try {

			String sql = "SELECT prefix FROM `biller_profile` WHERE comp_code=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, compCode);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				prefix = rs.getString("prefix");
			}
			conn.close();
			slipNo = prefix+year+ genRandomNo(compCode);
			return slipNo;

		} catch (Exception e) {
			e.printStackTrace();
			return slipNo;
		}

	}

	public static String expiryDate() {
		String expiry_date="";
		Calendar c = Calendar.getInstance(); 
		c.add(Calendar.DATE, 60);
		Date d = c.getTime();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		expiry_date = dateFormat.format(d);
		return expiry_date;
	}
	
	

}
