package com.stanbic.SystemMonitoring;

import java.sql.Connection;
import java.time.Duration;
import java.time.Instant;
import java.util.Hashtable;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.ebiller.fetchBills.FetchBillsResponse;
import com.stanbic.Responses.Response;


@SpringBootApplication
@RequestMapping("/api")
public class SystemMonitor {
	Connection conn=null;
	 @RequestMapping(value = "/v1/getSystemMonitoring", method = RequestMethod.POST)
		public ResponseEntity<?>getSystemMonitoring(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody SystemModel jsondata){
		 try {
			 Instant start = Instant.now();
			 Hashtable<String,String>map=new Hashtable<String,String>();
			 Hashtable<String,String>map2=new Hashtable<String,String>();
			 map.put("AccountRefNo","46847816");
			 map.put("BillerCode","biller");
			 map.put("BillerUrl","KPLC_URL");
			 String response=GeneralCodes.encryptedPostRequest(map, ExternalFile.getUsbBaseUrl(), "JSON");
			 JSONObject obj = new JSONObject(response);
	         String jsonResponse = obj.toString();
	       Gson gson = new GsonBuilder().create();
	       FetchBillsResponse res = gson.fromJson(jsonResponse, FetchBillsResponse.class);
	   
	       Instant end = Instant.now();
	       Duration timeElapsed = Duration.between(start, end);
//       Random random = new Random(); //random.nextInt(11)+""
	       map2.put("messageCode",res.getResponseCode());
	       map2.put("message",res.getResponseMessage());
	       map2.put("kplctime",timeElapsed.toMillis()+"");
	       return ResponseEntity.ok(map2);
		 }catch(Exception n) {
			 n.printStackTrace();
			 return ResponseEntity.ok(new Response("02","System technical error"));
		 }
		 
	 }
	
}
