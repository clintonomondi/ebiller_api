package com.stanbic.Team;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Eslip.EslipConsumeJson;
import com.stanbic.Eslip.EslipHelper;
import com.stanbic.Eslip.MyESlipsProduceJSon;
import com.stanbic.Responses.Response;
import com.stanbic.comm.CheckUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;
import com.stanbic.ebiller.auth.LoginHelper;
import com.stanbic.ebiller.auth.PayerAuth;

@SpringBootApplication
@RequestMapping("/api")
public class MyTeamService {
	Connection conn = null;
	
	String FROM = null;
	String FROMNAME = null;
	String SMTP_USERNAME = null;
	String SMTP_PASSWORD = null;
	String HOST = null;
	String PORT = null;
	String SUBJECT = null;
	String ebiller_base_url = null;
	String message = null;
	
	@Autowired
	private JavaMailSender sender;
	@RequestMapping(value = "/v1/addCompanyUser", method = RequestMethod.POST)
	public ResponseEntity<?> addMyTeam(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CompanyUsersUpdateReqModel jsondata) {
		String date=TimeManager.getEmailTime();
		String comp_code=TokenManager.tokenIssuedCompCode(token);
		String email=TokenManager.tokenIssuedId(token);
		if(jsondata.getEmail().isEmpty()|| jsondata.getGroup_id().isEmpty() || jsondata.getPersonel_f_name().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please enter all fields correctly","",""));
		}
		if(jsondata.getPhone().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter valid phone number"));
		}
		 int checkphone=LoginHelper.isPhoneNumberExist(jsondata.getPhone().substring(1));
		 if(checkphone>0) {
				return ResponseEntity.ok(new Response("06", "The phone number is already in use"));
		 }
		 if(jsondata.getPhone().substring(1).length()!=12) {
				return ResponseEntity.ok(new Response("06", "The phone number must be 12 characters"));
		 }
	     String name=AccountHelper.getName(email);
	     
	     int v_count= CheckUsers.checkPayer(jsondata.getEmail());
	     int v_count2= CheckUsers.checkBiller(jsondata.getEmail());
	     
			if(v_count>0 || v_count2>0) {
				  return ResponseEntity.ok(new AuthProduceJson("07","User already exist","",""));
			  }
			
		try {
			
			new Users().addCompanyUsers(jsondata.getPersonel_f_name(),jsondata.getPersonel_l_name(), jsondata.getGroup_id(), comp_code, jsondata.getEmail(), jsondata.getEmployeeCode(), jsondata.getNational_id(), jsondata.getPhone(), email,"");
	
			  token = TokenManager.createToken2(comp_code, jsondata.getEmail(), "", "comp_user");
		    
			 String path=PathUrl.getBillerAppPath("biller");
				String url= path+"account-opening/activate-account"+"?token=" + token;
				  String subject="Ebiller Account";
				  VelocityContext vc = new VelocityContext();
		          vc.put("login_link", url);
		          vc.put("date", date);
		          vc.put("name", name);
		          String email_template = EmailManager.email_message_template(vc,"payer_team.vm");
					EmailManager.send_mail(jsondata.getEmail(),email_template,subject,name,"Company user added");
			 
		}catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("01","User could not be saved","",""));
		}
		 return ResponseEntity.ok(new AuthProduceJson("00","User added successfully","",""));	     
	}
	
	
	@RequestMapping(value = "/v1/resendInvite", method = RequestMethod.POST)
	public ResponseEntity<?> resendInvite(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CompanyUsersUpdateReqModel jsondata) {
		String comp_code=TokenManager.tokenIssuedCompCode(token);
		String email=TokenManager.tokenIssuedId(token);
		if(jsondata.getEmail().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please enter all fields correctly","",""));
		}
		
	     String name=AccountHelper.getName(email);
	     
		try {
			 token = TokenManager.createToken(comp_code, jsondata.getEmail(), "", "comp_user");
			 String path=PathUrl.getBillerAppPath("biller");
				String url= path+"account-opening/activate-account"+"?token=" + token;
				  String subject="Ebiller Account";
				  VelocityContext vc = new VelocityContext();
		          vc.put("login_link", url);
		          vc.put("name", name);
		          String email_template = EmailManager.email_message_template(vc,"account_temp.vm");
					EmailManager.send_mail(jsondata.getEmail(),email_template,subject,email,"Company user resend invitation");
			 
		}catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("01","User could not be saved","",""));
		}
		 return ResponseEntity.ok(new AuthProduceJson("00","Email sent successfully","",""));	    
		
	}
	
	
	
	@RequestMapping(value = "/v1/getCompanyUsers", method = RequestMethod.POST)
	public ResponseEntity<?> getEslipReports(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CompanyUsersReqModel jsondata) {
		List<CompanyUsersRespModel> model = new ArrayList<CompanyUsersRespModel>();
		
		String comp_code=TokenManager.tokenIssuedCompCode(token);
		try {
			String sql = "SELECT *,"
					+ "(SELECT name FROM `user_groups` B WHERE B.id=A.group_id )group_description \n" + 
					"FROM `ebiller_auth` A WHERE comp_code=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				CompanyUsersRespModel data = new CompanyUsersRespModel();
				data.email=rs.getString("email");
				data.personel_l_name=rs.getString("personel_l_name");
				data.personel_f_name=rs.getString("personel_f_name");
				data.GroupDescription=rs.getString("group_description");
				data.groupId=rs.getString("group_id");
				data.updated_at=rs.getString("updated_at");
				data.selected=true;
				data.id=rs.getString("id");
				String dbfrozen = rs.getString("frozen");
				if(dbfrozen.equalsIgnoreCase("1")) {
					data.frozen = "Inactive";
				}
				else {
					data.frozen = "Active";
				}
				data.comp_code = rs.getString("comp_code");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	@RequestMapping(value = "/v1/updateMyTeam", method = RequestMethod.POST)
	public ResponseEntity<?> updateMyTeam(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CompanyUsersUpdateReqModel jsondata) {
		if(jsondata.getEmail().isEmpty() || jsondata.getPersonel_f_name().isEmpty() || jsondata.getPersonel_l_name().isEmpty() || jsondata.getGroup_id().isEmpty() || jsondata.getId().isEmpty() || jsondata.getGroup_id().equalsIgnoreCase("")) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please enter all fields correctly","",""));
		}		
		
		String comp_code=TokenManager.tokenIssuedCompCode(token);

	    String emailOfCurrentUser=TokenManager.tokenIssuedId(token);
	    System.out.println(emailOfCurrentUser+" wants to delete "+jsondata.getEmail());
			if(emailOfCurrentUser.equalsIgnoreCase(jsondata.getEmail())){
				return ResponseEntity.ok(new AuthProduceJson("05","Action Denied","",""));
		}
	   
		try {
			String sql = "UPDATE ebiller_auth SET personel_f_name=?,personel_l_name=?,group_id=?,email=? WHERE id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getPersonel_f_name());
			prep.setObject(2, jsondata.getPersonel_l_name());
			prep.setObject(3, jsondata.getGroup_id());
			prep.setObject(4, jsondata.getEmail());
			prep.setObject(5,jsondata.getId());
			prep.execute();
			 conn.close();
			 return ResponseEntity.ok(new AuthProduceJson("00","User records updated successfully","",""));
		}catch(Exception n) {
			System.out.println(n.getMessage());
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
			     
	}
	
	@RequestMapping(value = "/v1/freezeUser", method = RequestMethod.POST)
	public ResponseEntity<?> deleteUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CompanyUsersUpdateReqModel jsondata){
		
		String comp_code=TokenManager.tokenIssuedCompCode(token);
		 
		if(jsondata.getEmail().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please enter valid email address","",""));
		}
		
		if (userFrozen(jsondata.getEmail())) {
			return ResponseEntity.ok(new AuthProduceJson("06","User Already frozen","",""));
		}
		
		String emailOfCurrentUser=TokenManager.tokenIssuedId(token);
		if(emailOfCurrentUser.equalsIgnoreCase(jsondata.getEmail())){
			return ResponseEntity.ok(new AuthProduceJson("05","Action Denied","",""));
		}
		
		try {
			String sql = "UPDATE ebiller_auth SET frozen=? WHERE email=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "1");
			prep.setObject(2, jsondata.getEmail());
			prep.execute();
			conn.close();
			return ResponseEntity.ok(new AuthProduceJson("00","User is now Frozen","",""));
		}catch(Exception e) {
			return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
	}
	
	@RequestMapping(value = "/v1/restoreUser", method = RequestMethod.POST)
	public ResponseEntity<?> restoreUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CompanyUsersUpdateReqModel jsondata){
		String comp_code=TokenManager.tokenIssuedCompCode(token);
		
		if(jsondata.getEmail().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please enter valid email address","",""));
		}
		
		String emailOfCurrentUser=TokenManager.tokenIssuedId(token);
		if(emailOfCurrentUser.equalsIgnoreCase(jsondata.getEmail())){
			return ResponseEntity.ok(new AuthProduceJson("05","Action Denied","",""));
		}
		
		if (!userFrozen(jsondata.getEmail())) {
			return ResponseEntity.ok(new AuthProduceJson("06","User is not frozen","",""));
		}
		
		try {
			String sql = "UPDATE ebiller_auth SET frozen=? WHERE email=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, 0);
			prep.setObject(2, jsondata.getEmail());
			prep.execute();
			conn.close();
			return ResponseEntity.ok(new AuthProduceJson("00","User has been restored successfully","",""));
		}catch(Exception e) {
			return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
	}
	
	@RequestMapping(value = "/v1/deleteUserGroup", method = RequestMethod.POST)
	public ResponseEntity<?> deleteUserGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody UserGroupModel jsondata){
		
		String comp_code=TokenManager.tokenIssuedCompCode(token);
		if(jsondata.getId().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please enter valid email address","",""));
		}
		Connection conn = null;
		
		try {
			String sql = "DELETE FROM `user_groups` WHERE id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getId());
			prep.execute();
			conn.close();
			return ResponseEntity.ok(new AuthProduceJson("00","Deleted successfully","",""));
		}catch(Exception e) {
			return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
	}	
	

	public boolean userFrozen(String email) {
		boolean frozen = false;
		try {
			String sql = "SELECT frozen FROM ebiller_auth WHERE email=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, email);
			ResultSet rs = prep.executeQuery();
			if(rs.next()) {
				frozen = rs.getString("frozen").equals("1");
			}
			conn.close();
		}catch(Exception e) {
			System.out.println("reached here");
		}
		return frozen;
	}
	

	
}