package com.stanbic.Team;

public class CompanyUsersRespModel {

	String name=null;
	String id=null;
	String groupId=null;
	String GroupDescription=null;
    String comp_code=null;
    String updated_at=null;
    String email=null;
    boolean selected;
    String frozen = null;
    String personel_f_name=null;
    String personel_l_name=null;
    
    
    
	public String getPersonel_f_name() {
		return personel_f_name;
	}
	public void setPersonel_f_name(String personel_f_name) {
		this.personel_f_name = personel_f_name;
	}
	public String getPersonel_l_name() {
		return personel_l_name;
	}
	public void setPersonel_l_name(String personel_l_name) {
		this.personel_l_name = personel_l_name;
	}
	public String getFrozen() {
		return frozen;
	}
	public void setFrozen(String frozen) {
		this.frozen = frozen;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGroupDescription() {
		return GroupDescription;
	}
	public void setGroupDescription(String groupDescription) {
		GroupDescription = groupDescription;
	}
	public String getComp_code() {
		return comp_code;
	}
	public void setComp_code(String comp_code) {
		this.comp_code = comp_code;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
    
    
}
