package com.stanbic.Team;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.stanbic.comm.DbManager;



public class Users {

	Connection conn=null;
	public void addCompanyUsers(String personel_f_name,String personel_l_name,String group_id,String comp_code,String email,String employeeCode,String national_id,String phone,String created_by,String password) {
		
		try {
			String sql = "INSERT INTO ebiller_auth(personel_f_name,personel_l_name,group_id,comp_code,email,employee_code,national_id,phone,created_by,password)VALUES(?,?,?,?,?,?,?,?,?,?)";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, personel_f_name);
			prep.setObject(2, personel_l_name);
			prep.setObject(3, group_id);
			prep.setObject(4, comp_code);
			prep.setObject(5, email);
			prep.setObject(6, employeeCode);
			prep.setObject(7, national_id);
			prep.setObject(8, phone);
			prep.setObject(9, created_by);
			prep.setObject(10, password);
			prep.execute();
			conn.close();
		}catch(Exception n) {
			System.out.println(n.getMessage());
			
		}
	}
	
}
