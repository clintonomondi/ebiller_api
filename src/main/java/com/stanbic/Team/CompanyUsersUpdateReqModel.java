package com.stanbic.Team;

public class CompanyUsersUpdateReqModel {
	
	String employeeCode="";
	String name="";
	String email="";
	String group_id="";
	String phone="";
	String national_id="";
	String token="";
	String id="";
	String personel_f_name=null;
	String personel_l_name=null;
	
	
	
	public String getPersonel_f_name() {
		return personel_f_name;
	}
	public void setPersonel_f_name(String personel_f_name) {
		this.personel_f_name = personel_f_name;
	}
	public String getPersonel_l_name() {
		return personel_l_name;
	}
	public void setPersonel_l_name(String personel_l_name) {
		this.personel_l_name = personel_l_name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNational_id() {
		return national_id;
	}
	public void setNational_id(String national_id) {
		this.national_id = national_id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGroup_id() {
		return group_id;
	}
	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	
}
