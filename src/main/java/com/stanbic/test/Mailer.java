package com.stanbic.test;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.ini4j.Ini;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.comm.GeneralCodes;

public class Mailer {
	static String FROM = null;
	static String FROMNAME = null;
	static String SMTP_USERNAME = null;
	static String SMTP_PASSWORD = null;
	static String HOST = null;
	static String PORT = null;
	static String SUBJECT = null;
	static String ebiller_base_url = null;
	static String message = null;
	
	@Autowired
	 private JavaMailSender sender;
	
	@Autowired
	JavaMailSenderImpl sender2;
	
	 static Connection conn;
	
	public static String MailerTest(String emailTo, String mess,String subject) {
		try {
			Ini ini = new Ini(new File(GeneralCodes.configFilePath()));
			FROM = ini.get("header", "email_from");
			FROMNAME = ini.get("header", "email_from_description");
			SMTP_USERNAME = ini.get("header", "email_smtp_username");
			SMTP_PASSWORD = ini.get("header", "email_smtp_password");
			HOST = ini.get("header", "email_host");
			PORT = ini.get("header", "email_port");
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtp");
		      props.put("mail.debug", "true");
			props.put("mail.smtp.port", PORT);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.EnableSSL.enable", "true"); 
			
			Session session = Session.getDefaultInstance(props);
			try {
				MimeMessage msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(FROM, FROMNAME));
				msg.setRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
				msg.setSubject(subject);
				msg.setContent(mess,"text/html");
				Transport transport = session.getTransport();
				transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
				transport.sendMessage(msg, msg.getAllRecipients());
				message = "200";
				transport.close();
				return message;
			} catch (IOException e) { 
				e.printStackTrace();
				message="250";
				return message;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			message="250";
			return message;
		}
			
	}
	
}
