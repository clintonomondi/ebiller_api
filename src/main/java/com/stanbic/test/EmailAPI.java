package com.stanbic.test;


import org.apache.velocity.VelocityContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Responses.Response;

import com.stanbic.comm.TimeManager;
import com.stanbic.ebiller.auth.AuthConsumeJson;
import com.stanbic.ebiller.auth.BillerAuthconsumeJson;


@SpringBootApplication
@RequestMapping("/api")
public class EmailAPI {

	 @RequestMapping(value = "/v1/sendGiktekEmail", method = RequestMethod.POST)
		public ResponseEntity<?> sendGiktekEmail(@RequestBody TestModel jsondata) {
			 
		 System.out.println("Email->"+jsondata.getEmail());
		 System.out.println("Subject->"+jsondata.getSubject());
		 System.out.println("Message->"+jsondata.getMessage());
		 if(jsondata.getEmail().isEmpty() || jsondata.getMessage().isEmpty() || jsondata.getSubject().isEmpty()) {
			 return ResponseEntity.ok(new Response("06", "Please provide all fields correctly"));
		 }
		try {
			String date =TimeManager.getEmailTime();
		  String subject=jsondata.getSubject();
		  VelocityContext vc = new VelocityContext();
          vc.put("date", date);
//          String email_template = EmailManager.email_message_template(vc,"welcome_self.vm");
			Mailer.MailerTest(jsondata.getEmail(),jsondata.getMessage(),subject);
			
			return ResponseEntity.ok(new Response("00", "Email sent wsuccessfully"));
		}catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
	}
}
