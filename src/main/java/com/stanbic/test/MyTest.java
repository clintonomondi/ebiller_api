package com.stanbic.test;

import java.util.Hashtable;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.poifs.crypt.Decryptor;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Payers.PayerDetailsReqModel;
import com.stanbic.comm.EncryptionManager;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

@SpringBootApplication
@RequestMapping("/api")
public class MyTest {

	private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    byte[] arrayBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;
    
	@RequestMapping(value = "/v1/passwordGen", method = RequestMethod.POST)
	  public ResponseEntity<?> genPassword(@RequestBody String jsondata) {
//		String encrypt=new EncryptionManager().encryptedPassword("test", "100");
//		String decryp=new EncryptionManager().decryptPassword(encrypt, "100");
//		System.out.println("Password is here::"+encrypt);
//		System.out.println("Password decrypted::"+decryp);
		
		try {
			
			JSONObject obj = new JSONObject(jsondata);
			String  providedPassword=obj.getString("password");
			String salt=obj.getString("salt"); 

			myEncryptionKey = "hjfdhf$34556029hjfuur%)8839399qkksklk(()q8wj&*9w9kemmm8q8q0nm372901928";
			
	        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;

	        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
	        ks = new DESedeKeySpec(arrayBytes);
	        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
	        cipher = Cipher.getInstance(myEncryptionScheme);
	        key = skf.generateSecret(ks);
	        
			String encryptedPass=encrypt(providedPassword);
			
	        System.out.println("Encrypted:::  "+encryptedPass );
	        
	        System.out.println("Decrypted : "+decrypt(encryptedPass));
			
			}catch(Exception e) {
				e.printStackTrace();
			}
		Hashtable<String,String> map=new Hashtable<String,String>();
		map.put("responseCode", "00");
		return ResponseEntity.ok(map);
	}
	public String encrypt(String unencryptedString) {
        String encryptedString = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(encryptedText));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encryptedString;
    }


    public String decrypt(String encryptedString) {
        String decryptedText=null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedText= new String(plainText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }
}
