package com.stanbic.dropdown.sector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.ebiller.auth.AuthConsumeJson;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class Sector {

	Connection conn=null;
	 @RequestMapping(value = "/v1/addSector", method = RequestMethod.POST)
	 public ResponseEntity<?> addSector(@RequestBody AuthConsumeJson jsondata) {
		 
		 return null;
	 }
	 
	 @RequestMapping(value = "/v1/getSectors", method = RequestMethod.POST)
	 public ResponseEntity<?> getAllSector() {
		 
		 List<SectorModel> model= new ArrayList<SectorModel>();
		 try {
			String sql="SELECT SECTOR_CODE,SECTOR_NAME FROM `biller_sector` "; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				SectorModel r=new SectorModel();
				r.sectorCode=rs.getString("SECTOR_CODE");
				r.sectorName=rs.getString("SECTOR_NAME");
				model.add(r);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(model);
		 }
		
		 
	 }
	 
}
