package com.stanbic.dropdown.country;

public class CountryProduceJson {
	String messageCode = "";
	String message = "";
	String token="";
	
	public CountryProduceJson(String messageCode, String message,String token) {
		this.messageCode = messageCode;
		this.message = message;
		this.token=token;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
