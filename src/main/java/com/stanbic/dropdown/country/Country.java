package com.stanbic.dropdown.country;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;

import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.onboarding.OnboadingBillerConsumerJson;



@SpringBootApplication
@RequestMapping("/api")
public class Country {

	Connection conn=null;
	 @RequestMapping(value = "/v1/addCountry", method = RequestMethod.POST)
	 public ResponseEntity<?> addCountry(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CountryModel jsondata) {
		 if(jsondata.getCountryCode().isEmpty() || jsondata.getCountryName().isEmpty()) {
			 return ResponseEntity.ok(new CountryProduceJson("06","Please enter both name and code",""));
		 }
		 String username=TokenManager.tokenIssuedCompCode(token);
		 try {
			 String sql="INSERT INTO countries (country_code,country_name,created_by)VALUES(?,?,?)";
			  conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, jsondata.getCountryCode());
				 prep.setObject(2, jsondata.getCountryName());
				 prep.setObject(3, username);
				 prep.execute();
				 conn.close();
				 return ResponseEntity.ok(new CountryProduceJson("00","Country added successfully",""));
		 }catch(Exception n) {
			 n.printStackTrace();
			 return ResponseEntity.ok(new CountryProduceJson("02","System technical error",""));
		 }
	 }
	 
	 @RequestMapping(value = "/v1/editCountry", method = RequestMethod.POST)
	 public ResponseEntity<?> editCountry(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CountryModel jsondata) {
		 if(jsondata.getCountryCode().isEmpty() || jsondata.getCountryName().isEmpty() || jsondata.getCountry_id().isEmpty()) {
			 return ResponseEntity.ok(new CountryProduceJson("06","Please enter both name and code and id",""));
		 }
		 String username=TokenManager.tokenIssuedCompCode(token);
		 try {
			 String sql="UPDATE  countries SET  country_code=?,country_name=?,updated_by=? WHERE id=?";
			  conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, jsondata.getCountryCode());
				 prep.setObject(2, jsondata.getCountryName());
				 prep.setObject(3, username);
				 prep.setObject(4, jsondata.getCountry_id());
				 prep.execute();
				 conn.close();
				 System_Logs.log_system(username, "", "User editing  country:", "Success", "");
				 return ResponseEntity.ok(new CountryProduceJson("00","Country updated successfully",""));
		 }catch(Exception n) {
			 n.printStackTrace();
			 return ResponseEntity.ok(new CountryProduceJson("02","System technical error",""));
		 }
	 }
	 
	 @RequestMapping(value = "/v1/deleteCountry", method = RequestMethod.POST)
	 public ResponseEntity<?> deleteCountry(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CountryModel jsondata) {
		 if(jsondata.getCountryCode().isEmpty()) {
			 return ResponseEntity.ok(new CountryProduceJson("06","Please enter  code",""));
		 }
		 String username=TokenManager.tokenIssuedCompCode(token);
		 try {
			 String sql="DELETE FROM countries WHERE country_code=?";
			  conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, jsondata.getCountryCode());
				 prep.execute();
				 conn.close();
				 System_Logs.log_system(username, "", "User deleted  country:", "Success", "");
				 return ResponseEntity.ok(new CountryProduceJson("00","Country deleted successfully",""));
		 }catch(Exception n) {
			 n.printStackTrace();
			 return ResponseEntity.ok(new CountryProduceJson("02","System technical error",""));
		 }
	 }
	 
	 @RequestMapping(value = "/v1/addBrach", method = RequestMethod.POST)
	 public ResponseEntity<?> addBrach(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CountryModel jsondata) {
		 if(jsondata.getBrach_code().isEmpty() || jsondata.getBranch_name().isEmpty() || jsondata.getCountry_id().isEmpty()) {
			 return ResponseEntity.ok(new CountryProduceJson("06","Please enter both name and code and country id",""));
		 }
		 String username=TokenManager.tokenIssuedCompCode(token);
		 try {
			 String sql="INSERT INTO country_branches (branch_code,branch_name,country_id,created_by)VALUES(?,?,?,?)";
			  conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1,jsondata.getBrach_code() );
				 prep.setObject(2, jsondata.getBranch_name());
				 prep.setObject(3, jsondata.getCountry_id());
				 prep.setObject(4, username);
				 prep.execute();
				 conn.close();
				 return ResponseEntity.ok(new CountryProduceJson("00","Branch added successfully",""));
		 }catch(Exception n) {
			 n.printStackTrace();
			 return ResponseEntity.ok(new CountryProduceJson("02","System technical error",""));
		 }
	 }
	 
	 @RequestMapping(value = "/v1/getCountry", method = RequestMethod.GET)
	 public ResponseEntity<?> getAllCountry(HttpServletRequest request, @RequestHeader(name = "Authorization") String token) {
		 
		 List<CountryModel> model= new ArrayList<CountryModel>();
		 try {
			String sql="SELECT COUNTRY_CODE,COUNTRY_NAME,id FROM `countries` "; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				CountryModel r=new CountryModel();
				r.countryCode=rs.getString("COUNTRY_CODE");
				r.countryName=rs.getString("COUNTRY_NAME");
				r.country_id=rs.getString("id");
				model.add(r);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(model);
		 }
		
		 
	 }
	 
	 
	 @RequestMapping(value = "/v1/getCountryBranches", method = RequestMethod.POST)
	 public ResponseEntity<?> getCountryBranches(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CountryBranchesModel jsondata) {
		 List<CountryBranchesModel> model= new ArrayList<CountryBranchesModel>();
		 try {
			String sql="SELECT *,\r\n" + 
					"(SELECT country_name FROM countries B WHERE B.id=A.country_id)country\r\n" + 
					" FROM `country_branches` A"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				CountryBranchesModel r=new CountryBranchesModel();
				r.country_id=rs.getString("country_id");
				r.branchCode=rs.getString("BRANCH_CODE");
				r.branchName=rs.getString("BRANCH_NAME");
				r.branch_id=rs.getString("id");
				r.country=rs.getString("country");
				model.add(r);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(model);
		 }

	 }
	 
	 @RequestMapping(value = "/v1/getBranchesPerCountry", method = RequestMethod.POST)
	 public ResponseEntity<?> getBranchesPerCountry(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CountryBranchesModel jsondata) {
		 List<CountryBranchesModel> model= new ArrayList<CountryBranchesModel>();
		 try {
			String sql="SELECT *,\r\n" + 
					"(SELECT country_name FROM countries B WHERE B.id=A.country_id)country\r\n" + 
					" FROM `country_branches` A WHERE country_id=?"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getCountry_id());
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				CountryBranchesModel r=new CountryBranchesModel();
				r.country_id=rs.getString("country_id");
				r.branchCode=rs.getString("BRANCH_CODE");
				r.branchName=rs.getString("BRANCH_NAME");
				r.branch_id=rs.getString("id");
				r.country=rs.getString("country");
				model.add(r);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(model);
		 }

	 }
	 
	 @RequestMapping(value = "/v1/editCountryBranches", method = RequestMethod.POST)
	 public ResponseEntity<?> editCountryBranches(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CountryModel jsondata) {
		 if(jsondata.getBrach_code().isEmpty() || jsondata.getBranch_name().isEmpty() || jsondata.getCountry_id().isEmpty()) {
			 return ResponseEntity.ok(new CountryProduceJson("06","Please enter both name and code and country id",""));
		 }
		 String username=TokenManager.tokenIssuedCompCode(token);
		 try {
			 String sql="UPDATE country_branches SET branch_code=?,branch_name=?,country_id=?,updated_by=? WHERE id=?";
			  conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1,jsondata.getBrach_code() );
				 prep.setObject(2, jsondata.getBranch_name());
				 prep.setObject(3, jsondata.getCountry_id());
				 prep.setObject(4, username);
				 prep.setObject(5, jsondata.getBranch_id());
				 prep.execute();
				 conn.close();
				 System_Logs.log_system(username, "", "User updated  branch:", "Success", "");
				 return ResponseEntity.ok(new CountryProduceJson("00","Branch updated successfully",""));
		 }catch(Exception n) {
			 n.printStackTrace();
			 return ResponseEntity.ok(new CountryProduceJson("02","System technical error",""));
		 }

	 }
	 
	 @RequestMapping(value = "/v1/deleteBranch", method = RequestMethod.POST)
	 public ResponseEntity<?> deleteBranch(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CountryModel jsondata) {
		 if(jsondata.getBrach_code().isEmpty()) {
			 return ResponseEntity.ok(new CountryProduceJson("06","Please enter  code",""));
		 }
		 String username=TokenManager.tokenIssuedCompCode(token);
		 try {
			 String sql="DELETE FROM country_branches WHERE branch_code=?";
			  conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, jsondata.getBrach_code());
				 prep.execute();
				 conn.close();
				 System_Logs.log_system(username, "", "User deleted  branch:", "Success", "");
				 return ResponseEntity.ok(new CountryProduceJson("00","Branch deleted successfully",""));
		 }catch(Exception n) {
			 n.printStackTrace();
			 return ResponseEntity.ok(new CountryProduceJson("02","System technical error",""));
		 }
	 }
	 
	 @RequestMapping(value = "/v1/getLocations", method = RequestMethod.GET)
	 public ResponseEntity<?> getAllSector() {
		 
		 List<LocationModel> model= new ArrayList<LocationModel>();
		 try {
			String sql="SELECT LOCATION_CODE,LOCATION_NAME FROM location"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				LocationModel r=new LocationModel();
				r.locationCode=rs.getString("LOCATION_CODE");
				r.locationName=rs.getString("LOCATION_NAME");
				model.add(r);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(model);
		 }
		
		 
	 }
}
