package com.stanbic.dropdown.country;

public class CountryModel {

	String countryCode=null;
	String countryName=null;
	String country_id=null;
	String brach_code=null;
	String branch_name=null;
	String branch_id="";
	
	
	
	public String getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}
	public String getBrach_code() {
		return brach_code;
	}
	public void setBrach_code(String brach_code) {
		this.brach_code = brach_code;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public String getCountry_id() {
		return country_id;
	}
	public void setCountry_id(String country_id) {
		this.country_id = country_id;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
}
