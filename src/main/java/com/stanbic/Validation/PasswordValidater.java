package com.stanbic.Validation;

import java.sql.Connection;
import java.util.regex.Pattern;

public class PasswordValidater {
	static Connection conn;
	
	
	public static boolean isPassword8(String password) {
		boolean result=true;
		Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
	    Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
	    Pattern lowerCasePatten = Pattern.compile("[a-z ]");
	    Pattern digitCasePatten = Pattern.compile("[0-9 ]");
	    
	    if (password.length() < 8) {
	    	result=false;
	    }
	    return result;
	}
	
	public static boolean isPasswordHasSpecial(String password) {
		boolean result=true;
		Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
	    Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
	    Pattern lowerCasePatten = Pattern.compile("[a-z ]");
	    Pattern digitCasePatten = Pattern.compile("[0-9 ]");
	    
	    if (!specailCharPatten.matcher(password).find()) {
	    	result=false;
	    }
	    return result;
	}
	
	public static boolean isPasswordHasUpperCase(String password) {
		boolean result=true;
		Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
	    Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
	    Pattern lowerCasePatten = Pattern.compile("[a-z ]");
	    Pattern digitCasePatten = Pattern.compile("[0-9 ]");
	    
	    if (!UpperCasePatten.matcher(password).find()) {
	    	result=false;
	    }
	    return result;
	}
	
	public static boolean isPasswordHasLowerCase(String password) {
		boolean result=true;
		Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
	    Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
	    Pattern lowerCasePatten = Pattern.compile("[a-z ]");
	    Pattern digitCasePatten = Pattern.compile("[0-9 ]");
	    
	    if (!lowerCasePatten.matcher(password).find()) {
	    	result=false;
	    }
	    return result;
	}
	
	public static boolean isPasswordHasLowerDigit(String password) {
		boolean result=true;
		Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
	    Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
	    Pattern lowerCasePatten = Pattern.compile("[a-z ]");
	    Pattern digitCasePatten = Pattern.compile("[0-9 ]");
	    
	    if (!digitCasePatten.matcher(password).find()) {
	    	result=false;
	    }
	    return result;
	}

}
