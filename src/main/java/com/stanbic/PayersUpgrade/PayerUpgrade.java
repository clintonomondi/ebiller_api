package com.stanbic.PayersUpgrade;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
@SpringBootApplication
@RequestMapping("/api")
public class PayerUpgrade {

	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	Date date = new Date();
	String todayDate = formatter.format(date);
	Connection conn = null;

	String FROM = null;
	String FROMNAME = null;
	String SMTP_USERNAME = null;
	String SMTP_PASSWORD = null;
	String HOST = null;
	String PORT = null;
	String SUBJECT = null;
	String ebiller_base_url = null;
	String message = null;
	
	@Autowired
	private JavaMailSender sender;
	@RequestMapping(value = "/v1/flagPayerToBiller", method = RequestMethod.POST)
	public ResponseEntity<?> flagPayerToBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody FlagPayerToBeBillerReqModel jsondata) {

		//String companyCode = jsondata.getCompanyCode();
		String companyCode = TokenManager.tokenIssuedCompCode(token);
		String email = TokenManager.tokenIssuedId(token);

		System.out.println("Username"+email);
		
		System.out.println("companyCode"+companyCode);
		try {
			
			String sql = "UPDATE biller_profile SET STATUS=?,BILLER_CREATED_BY=?,BILLER_CREATED_DATE=?,REG_FORM=? WHERE COMP_CODE='"+companyCode+"'";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "pending");
			prep.setObject(2, email);
			prep.setObject(3, todayDate);
			prep.setObject(4, "CONVERT_PAYER");
			prep.executeUpdate();
			conn.close();
			Hashtable<String, String> map = new Hashtable<String, String>();
			map.put("responseCode", "00");
			map.put("message", "Payer has been flagged as biller waiting approval");
			map.put("flag", "1");
			return ResponseEntity.ok(map);
		} catch (Exception e) {
			Hashtable<String, String> map = new Hashtable<String, String>();
			map.put("responseCode", "01");
			map.put("message", "Could not flag Payer to Biller");
			return ResponseEntity.ok(map);
		}

	}

	@RequestMapping(value = "/v1/listOfPendingBillersApproval", method = RequestMethod.POST)
	public ResponseEntity<?> listOfBillersPendingApproval(@RequestBody FlagPayerToBeBillerReqModel jsondata) {

		String compCode = "";
		String Location = "";
		String phone = "";
		String branch = "";
		String businessDescription = "";
		String companyName = "";
		String country = "";
		String email = "";
		String status = "";
		String personelName = "";
		String accountno = "";
		String accountName = "";
		String sector = "";
		Hashtable<String, Object> map = new Hashtable<String, Object>();
		try {
			String sql = "SELECT COMP_CODE,BILLER_LOCATION,BILLER_PHONE,BILLER_MONTH,BRANCH,BUSINESS_DESCRIPTION,COMPANY_NAME,COUNTRY,EMAIL,STATUS,PERSONEL_L_NAME,STB_ACC_NAME,SECTOR\r\n"
					+ "USER_TYPE FROM `biller_profile` \r\n" + "WHERE COMP_CODE=? AND STATUS='PENDING'";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getCompanyCode());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				compCode = rs.getString("COMP_CODE");
				Location = rs.getString("BILLER_LOCATION");
				phone = rs.getString("BILLER_PHONE");
				branch = rs.getString("BRANCH");
				businessDescription = rs.getString("BUSINESS_DESCRIPTION");
				companyName = rs.getString("COMPANY_NAME");
				country = rs.getString("COUNTRY");
				email = rs.getString("EMAIL");
				status = rs.getString("STATUS");
				personelName = rs.getString("PERSONEL_L_NAME");
				// accountno=rs.getString("");
				accountName = rs.getString("STB_ACC_NAME");
				sector = rs.getString("SECTOR");
			}
			
			conn.close();
			map.put("responseCode", "00");
			map.put("message", "List of payer pending approval");
			map.put("companyCode", compCode);
			map.put("location", Location);
			map.put("phoneNo", phone);
			map.put("branch", branch);
			map.put("businessDescription", businessDescription);
			map.put("companyName", companyName);
			map.put("country", country);
			map.put("email", email);
			map.put("status", status);
			map.put("personelName", personelName);
			map.put("accountName", accountName);
			map.put("accountNo", accountno);
			map.put("sector", sector);
			return ResponseEntity.ok(map);
		} catch (Exception e) {
			map.put("responseCode", "02");
			map.put("message", "Could not query database for pending approval list for billers");
			return ResponseEntity.ok(map);
		}
		
	}
	
	@RequestMapping(value = "/v1/approvePendingBillers", method = RequestMethod.POST)
	public ResponseEntity<?> approveBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody ApprovePayerReqModel jsondata) {
	 
		String system_username = TokenManager.tokenIssuedCompCode(token);
		
		String comp_code=jsondata.getCompanyCode();
		int v_count=checkBillerCreatedBy(system_username);
		Hashtable<String,String> map=new Hashtable<String,String>();
	 
		System.out.println("jkjkejejkejjekkeek"+comp_code);
	 if(v_count==0) {
		 
		 try {
			 
			 String sql="UPDATE biller_profile SET STATUS=?,APPROVED_BY=?,APPROVAL_DATE=?,APPROVED_YN=? ,user_type=?,\r\n" + 
			 		"BILLER_LOCATION=?,BILLER_PHONE=?,BILLER_MONTH=?,BRANCH=?,BUSINESS_DESCRIPTION=?,COMPANY_NAME=?,COUNTRY=?,SECTOR=?,STB_ACC_NAME=?,BILLER_TYPE=?\r\n" + 
			 		"WHERE COMP_CODE='"+comp_code+"'";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, "Active");
			 prep.setObject(2, system_username);
			 prep.setObject(3, todayDate);
			 prep.setObject(4, "Y");
			 prep.setObject(5, "biller");
			 prep.setObject(6, jsondata.getLocation());
			 prep.setObject(7, jsondata.getPhoneNo());
			 prep.setObject(8, jsondata.getMonthlyPayer());
			 prep.setObject(9, jsondata.getBranch());
			 prep.setObject(10, jsondata.getBusinessDescription());
			 prep.setObject(11, jsondata.getCompanyName());
			 prep.setObject(12, jsondata.getCountry());
			 prep.setObject(13, jsondata.getIndustry());
			 prep.setObject(14, jsondata.getAccounName());
			 prep.setObject(15, jsondata.getBillerType());
			 prep.executeUpdate();
			 conn.close();
			 map.put("responseCode", "00");
			 map.put("message", "Biller successfully approved");
			
			}catch(Exception e) {
				e.printStackTrace();
			}
		 
		    String eamil_of_company="";

		 try {
			 String sql="SELECT EMAIL FROM `biller_profile` WHERE COMP_CODE=?";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, comp_code);
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 eamil_of_company=rs.getString("EMAIL"); 
			 }
			 conn.close();
		 }catch(Exception e) {
			 e.printStackTrace();
		 }
		 
		 String path=PathUrl.getBillerAppPath("biller");
		// String token=TokenManager.generateJwtToken(comp_code, eamil_of_company, ExternalFile.getSecretKey(),"");
		  token = TokenManager.createToken(comp_code, eamil_of_company, "", "");
	     String emailMessage= path+"account-opening/activate-account" + "?token=" + token;
		
		 new EmailManager().send_mail(eamil_of_company,emailMessage,"",system_username,"Payer approved to be biller");
		 
		 MimeMessage message = sender.createMimeMessage();
		 MimeMessageHelper msHelper = new MimeMessageHelper(message);
			
			System.out.println("Sending Email...");
			try {
				
				msHelper.setTo(eamil_of_company);
				msHelper.setText(emailMessage);
				msHelper.setSubject("Mail From ebiller on account opening");
			} catch (MessagingException e) {
				System.out.println(e.getMessage());
				  return ResponseEntity.ok(new AuthProduceJson("02","An error occured while sending email try again","",""));
			}
			sender.send(message);
			System.out.println("Email Sending Done");
		 
			 return ResponseEntity.ok(map);
	 }else {
		 map.put("responseCode", "11");
		 map.put("message", "User created record and cannot approve");
		 return ResponseEntity.ok(map);
	 }
		
	}
	
	
	public int checkBillerCreatedBy(String username) {
		int v_count=0;
		try {
			String sql="SELECT COUNT(*) AS V_COUNT FROM `biller_profile` WHERE CREATED_BILLER_BY=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, username);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				v_count=rs.getInt("");
			}
			conn.close();
			return v_count;
		}catch(Exception e) {
			return v_count;
		}
	}
}
