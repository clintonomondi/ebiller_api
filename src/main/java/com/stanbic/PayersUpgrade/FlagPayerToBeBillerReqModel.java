package com.stanbic.PayersUpgrade;

public class FlagPayerToBeBillerReqModel {
	String token="";
	String companyCode="";
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

}
