package com.stanbic.PayersUpgrade;

public class ApprovePayerReqModel {
  String companyCode="";
  String token="";
  String companyName="";
  String location="";
  String phoneNo="";
  String industry="";
  String noOfEmployees="";
  String accounName="";
  String accountNumber="";
  String county="";
  String country="";
  String billerType="";
  String monthlyPayer="";
  String businessDescription="";
  String branch="";
  String sector="";
  
  
public String getSector() {
	return sector;
}
public void setSector(String sector) {
	this.sector = sector;
}
public String getBusinessDescription() {
	return businessDescription;
}
public void setBusinessDescription(String businessDescription) {
	this.businessDescription = businessDescription;
}
public String getBranch() {
	return branch;
}
public void setBranch(String branch) {
	this.branch = branch;
}
public String getCompanyCode() {
	return companyCode;
}
public void setCompanyCode(String companyCode) {
	this.companyCode = companyCode;
}
public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getCompanyName() {
	return companyName;
}
public void setCompanyName(String companyName) {
	this.companyName = companyName;
}
public String getLocation() {
	return location;
}
public void setLocation(String location) {
	this.location = location;
}
public String getPhoneNo() {
	return phoneNo;
}
public void setPhoneNo(String phoneNo) {
	this.phoneNo = phoneNo;
}
public String getIndustry() {
	return industry;
}
public void setIndustry(String industry) {
	this.industry = industry;
}
public String getNoOfEmployees() {
	return noOfEmployees;
}
public void setNoOfEmployees(String noOfEmployees) {
	this.noOfEmployees = noOfEmployees;
}
public String getAccounName() {
	return accounName;
}
public void setAccounName(String accounName) {
	this.accounName = accounName;
}
public String getAccountNumber() {
	return accountNumber;
}
public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
}
public String getCounty() {
	return county;
}
public void setCounty(String county) {
	this.county = county;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getBillerType() {
	return billerType;
}
public void setBillerType(String billerType) {
	this.billerType = billerType;
}
public String getMonthlyPayer() {
	return monthlyPayer;
}
public void setMonthlyPayer(String monthlyPayer) {
	this.monthlyPayer = monthlyPayer;
}
  
}
