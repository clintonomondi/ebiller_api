package com.stanbic.backendUsers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.stanbic.comm.DbManager;

public class BankUserHelper {
static Connection conn;

public static int checkBankUser(String username,String email) {
	int vcount=0;
	try {
		String sql="SELECT COUNT(*)vcount FROM bank_users WHERE username=? OR email=?";
		conn = DbManager.getConnection();
		PreparedStatement prep = conn.prepareStatement(sql);
		prep.setObject(1,username );
		prep.setObject(2,email );
		ResultSet rs=prep.executeQuery();
		while(rs.next()) {
			vcount=rs.getInt("vcount");
		}
		
	}catch(Exception e) {
		e.printStackTrace();
	}
	return vcount;
}

public static String getbankuserEmail(String username) {
	String email="";
	try {
		String sql="SELECT email FROM bank_users WHERE username=?";
		conn = DbManager.getConnection();
		PreparedStatement prep = conn.prepareStatement(sql);
		prep.setObject(1,username );
		ResultSet rs=prep.executeQuery();
		while(rs.next()) {
			email=rs.getString("email");
		}
		
	}catch(Exception e) {
		e.printStackTrace();
	}
	return email;
}
}
