package com.stanbic.backendUsers;

public class BankUsersRespModel {
	
	String otherName="";
	String phone="";
	String email="";
	String userGroup="";
	String username="";
	String group_id="";
	String status="";
	String id="";
	String notification="";
	String created_at="";
	String datemodified="";
	String datelastlogin="";
	String delete_user="";
	String edit_user="";
	
	
	
	
	
	public String getEdit_user() {
		return edit_user;
	}
	public void setEdit_user(String edit_user) {
		this.edit_user = edit_user;
	}
	public String getDelete_user() {
		return delete_user;
	}
	public void setDelete_user(String delete_user) {
		this.delete_user = delete_user;
	}
	public String getDatemodified() {
		return datemodified;
	}
	public void setDatemodified(String datemodified) {
		this.datemodified = datemodified;
	}
	public String getDatelastlogin() {
		return datelastlogin;
	}
	public void setDatelastlogin(String datelastlogin) {
		this.datelastlogin = datelastlogin;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getNotification() {
		return notification;
	}
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroup_id() {
		return group_id;
	}
	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOtherName() {
		return otherName;
	}
	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
}
