package com.stanbic.backendUsers;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;
import com.stanbic.ebiller.auth.LoginHelper;


@SpringBootApplication
@RequestMapping("/api")
public class BankUsers {
static Connection conn;
static Logger log = Logger.getLogger(BankUsers.class.getName());
	@RequestMapping(value = "/v1/createBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> createBankUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankUsersReqModel jsondata) {
		String username = TokenManager.tokenIssuedCompCode(token);
		if(jsondata.getUsername().isEmpty() || jsondata.getEmail().isEmpty() || jsondata.getGroup_id().isEmpty()
				|| jsondata.getOtherName().isEmpty() || jsondata.getSurname().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide all fields", "", ""));
		}
		
		int vcount=BankUserHelper.checkBankUser(jsondata.getUsername(),jsondata.getEmail());
		if(vcount>0) {
			return ResponseEntity.ok(new BankUsersResp("08", "The email address or username already exist in the local system", ""));
		}else {
			System_Logs.log_system(username, "", "Created bank user ID:"+jsondata.getUsername(), "success", "");
       try {
			conn = DbManager.getConnection();
			String sql="INSERT INTO bank_users(USERNAME,EMAIL,SURNAME,OTHER_NAMES,group_id,PHONE,CREATED_BY,SMS_EMAIL_NOTIF_YN) VALUES(?,?,?,?,?,?,?,?)";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getUsername().toUpperCase());
			prep.setObject(2, jsondata.getEmail().toUpperCase());
			prep.setObject(3, jsondata.getSurname().toUpperCase());
			prep.setObject(4, jsondata.getOtherName().toUpperCase());
			prep.setObject(5, jsondata.getGroup_id());
			prep.setObject(6, jsondata.getPhone());
			prep.setObject(7, username);
			prep.setObject(8, jsondata.getNotification());
			prep.execute();
			conn.close();
			EmailAlert.logger(username, "Created bank user id:"+jsondata.getEmail(), log);
			 System_Logs.log_system(username, "", "User added bank user:"+jsondata.getEmail(), "Success", "");
			return ResponseEntity.ok(new BankUsersResp("00", "bank user successfully created", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "System technical error", ""));
		}
		}
	}
	@RequestMapping(value = "/v1/rejectCreatedBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> rejectCreatedBankUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
		if(jsondata.getUsername().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide username", "", ""));
		}
    String system_username=TokenManager.tokenIssuedCompCode(token);   
    String getCreatedBy=LoginHelper.getUpdatedBy(jsondata.getUsername());
    if(system_username.equalsIgnoreCase(getCreatedBy)) {
    	return ResponseEntity.ok(new AuthProduceJson("08", "You are not authorized to reject this user", "", ""));
    }
       try {
			conn = DbManager.getConnection();
			String sql="DELETE FROM bank_users  WHERE USERNAME=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getUsername());
			prep.execute();
			conn.close();
			EmailAlert.logger(system_username, "Rejected new bank user id:"+jsondata.getUsername(), log);
			System_Logs.log_system(system_username, "", "User rejected new bank user:"+jsondata.getUsername(), "queued", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User queued successfully for approval", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "System Error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/getBankUsers", method = RequestMethod.POST)
	public ResponseEntity<?> getBankUser(@RequestBody BankGetUsersReqModel jsondata) {
	String compCode=companyCode();
	  List<BankUsersRespModel> model=new ArrayList<BankUsersRespModel>();
       try {
			conn = DbManager.getConnection();
			String sql="SELECT *,\r\n" + 
					"(SELECT NAME FROM bank_groups B WHERE B.id=A.group_id)GROUP_NAME,\r\n" + 
					"(SELECT created_at FROM `system_logs` B WHERE B.created_by=A.username ORDER BY ID DESC LIMIT 1)datelastlogin\r\n" + 
					" FROM bank_users A";
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				BankUsersRespModel m=new BankUsersRespModel();
				m.email=rs.getString("email");
				m.otherName=rs.getString("other_names");
				m.phone=rs.getString("phone");
				m.userGroup=rs.getString("GROUP_NAME");
				m.username=rs.getString("USERNAME");
				m.group_id=rs.getString("group_id");
				m.status=rs.getString("status");
				m.id=rs.getString("id");
				m.notification=rs.getString("sms_email_notif_yn");
				m.created_at=rs.getString("created_date");
				m.datelastlogin=rs.getString("datelastlogin");
				m.datemodified=rs.getString("modified_date");
				m.delete_user=rs.getString("delete_user");
				m.edit_user=rs.getString("edit_user");
				model.add(m);
			}
			conn.close();
			return ResponseEntity.ok(model);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(model);
		}
	}
	
	
	
	@RequestMapping(value = "/v1/editBankUsers", method = RequestMethod.POST)
	public ResponseEntity<?> editBankUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
    String system_username=TokenManager.tokenIssuedCompCode(token);
    
	if(jsondata.getUsername().isEmpty() || jsondata.getGroup_id().isEmpty() || jsondata.getId().isEmpty() || jsondata.getOther_names().isEmpty() || jsondata.getNotification().isEmpty()) {
		return ResponseEntity.ok(new AuthProduceJson("06", "Please provide all fields", "", ""));
	} 
	if(jsondata.getId().isEmpty()) {
		return ResponseEntity.ok(new AuthProduceJson("06", "Please provide id", "", ""));
	}
       try {
			conn = DbManager.getConnection();
			String sql="INSERT INTO edit_bank_user(email,username,other_names,group_id,modified_by,sms_email_notif_yn,user_id)VALUES(?,?,?,?,?,?,?) ON DUPLICATE "
					+ "KEY UPDATE email=?,username=?,other_names=?,group_id=?,modified_by=?,sms_email_notif_yn=?,user_id=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEmail().toUpperCase());
			prep.setObject(2, jsondata.getUsername().toUpperCase());
			prep.setObject(3, jsondata.getOther_names().toUpperCase());
			prep.setObject(4, jsondata.getGroup_id());
			prep.setObject(5, system_username);
			prep.setObject(6, jsondata.getNotification());
			prep.setObject(7, jsondata.getId());
			
			prep.setObject(8, jsondata.getEmail().toUpperCase());
			prep.setObject(9, jsondata.getUsername().toUpperCase());
			prep.setObject(10, jsondata.getOther_names().toUpperCase());
			prep.setObject(11, jsondata.getGroup_id());
			prep.setObject(12, system_username);
			prep.setObject(13, jsondata.getNotification());
			prep.setObject(14, jsondata.getId());
			prep.executeUpdate();
			
			String sql2="UPDATE bank_users SET edit_user=?,modified_by=? WHERE id=?";
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep2.setObject(1, "yes");
			prep2.setObject(2, system_username);
			prep2.setObject(3, jsondata.getId());
			prep2.execute();
			conn.close();
			
			EmailAlert.logger(system_username, "Edited queued bank user id:"+jsondata.getEmail(), log);
			 System_Logs.log_system(system_username, "", "User edited bank user:"+jsondata.getEmail(), "queued", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User details queued  successfully for update approval", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
	}
	
	
	
	@RequestMapping(value = "/v1/ApproveEditedBankUsers", method = RequestMethod.POST)
	public ResponseEntity<?> ApproveEditedBankUsers(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
    String system_username=TokenManager.tokenIssuedCompCode(token);
    
	if(jsondata.getId().isEmpty() || jsondata.getUsername().isEmpty()) {
		return ResponseEntity.ok(new AuthProduceJson("06", "Please provide id and username", "", ""));
	} 
	
	String getCreatedBy=LoginHelper.getUpdatedBy(jsondata.getUsername());
	if(system_username.equalsIgnoreCase(getCreatedBy)) {
		return ResponseEntity.ok(new AuthProduceJson("06", "You are not authorised to approve this editing", "", ""));
	}
	
	System_Logs.log_system(system_username, "", "Approve Edited bank user ID:"+jsondata.getUsername(), "Success", "");
	String username="";
	String surname="";
	String other_name="";
	String email="";
	String phone="";
	String group_id="";
	String sms_email_notif_yn="";
	String edit_user="";
	String modified_by="";
       try {
			conn = DbManager.getConnection();
			String sql="SELECT * FROM `edit_bank_user` WHERE user_id=? ";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getId());
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				username=rs.getString("username");
				surname=rs.getString("surname");
				other_name=rs.getString("other_names");
				email=rs.getString("email");
				phone=rs.getString("phone");
				sms_email_notif_yn=rs.getString("sms_email_notif_yn");
				edit_user=rs.getString("edit_user");
				group_id=rs.getString("group_id");
			}
			
			
			String sql2="UPDATE bank_users SET email=?,username=?,other_names=?,group_id=?,modified_by=?,sms_email_notif_yn=?,edit_user=? WHERE id=?";
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep2.setObject(1, email);
			prep2.setObject(2, username);
			prep2.setObject(3, other_name);
			prep2.setObject(4, group_id);
			prep2.setObject(5, modified_by);
			prep2.setObject(6, sms_email_notif_yn);
			prep2.setObject(7, "no");
			prep2.setObject(8, jsondata.getId());
			prep2.executeUpdate();
			
			conn.close();
			EmailAlert.logger(system_username, "Approved Edited bank user id:"+jsondata.getEmail(), log);
			 System_Logs.log_system(system_username, "", "Appr User edited bank user:"+jsondata.getEmail(), "Success", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User details updated successfully after approval", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
	}
	@RequestMapping(value = "/v1/RejectEditedBankUsers", method = RequestMethod.POST)
	public ResponseEntity<?> RejectEditedBankUsers(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
    String system_username=TokenManager.tokenIssuedCompCode(token);
    
	if(jsondata.getUsername().isEmpty()) {
		return ResponseEntity.ok(new AuthProduceJson("06", "Please provide id and username", "", ""));
	} 
	
	String getCreatedBy=LoginHelper.getUpdatedBy(jsondata.getUsername());
	if(system_username.equalsIgnoreCase(getCreatedBy)) {
		return ResponseEntity.ok(new AuthProduceJson("06", "You are not authorised to approve this editing", "", ""));
	}
	
       try {
			conn = DbManager.getConnection();
			String sql="DELETE FROM edit_bank_user WHERE username=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getUsername());
			prep.execute();
			
			String sql2="UPDATE bank_users SET edit_user=? WHERE username=?";
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep2.setObject(1, "no");
			prep2.setObject(2, jsondata.getUsername());
			prep2.execute();
			
			conn.close();
			EmailAlert.logger(system_username, "Rejected  Edited bank user id:"+jsondata.getUsername(), log);
			 System_Logs.log_system(system_username, "", "Rejected   edited bank user:"+jsondata.getUsername(), "Success", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User details updated successfully", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/freezeBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> freezeBankUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
		if(jsondata.getUsername().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide username", "", ""));
		}
		
    String system_username=TokenManager.tokenIssuedCompCode(token);
	if(system_username.equalsIgnoreCase(jsondata.getUsername())) {
    	return ResponseEntity.ok(new AuthProduceJson("08", "You are not authorized to deactivate this user", "", ""));
    }
	System_Logs.log_system(system_username, "", "Freeze bank user ID:"+jsondata.getUsername(), "Success", "");
       try {
			conn = DbManager.getConnection();
			String sql="UPDATE bank_users SET status=?,CREATED_BY=? WHERE USERNAME=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Inactive");
			prep.setObject(2, system_username);
			prep.setObject(3, jsondata.getUsername());
			prep.executeUpdate();
			conn.close();
			EmailAlert.logger(system_username, "Freeze bank user id:"+jsondata.getEmail(), log);
			System_Logs.log_system(system_username, "", "User froze bank user:"+jsondata.getEmail(), "Success", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User dectivated successfully", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/restoreBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> restoreBankUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
		if(jsondata.getUsername().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide username", "", ""));
		}
    String system_username=TokenManager.tokenIssuedCompCode(token);
   
    if(system_username.equalsIgnoreCase(jsondata.getUsername())) {
    	return ResponseEntity.ok(new AuthProduceJson("08", "You are not authorized to activate this user", "", ""));
    }
    if(!LoginHelper.isBankUserFrozen(jsondata.getEmail())) {
    	return ResponseEntity.ok(new AuthProduceJson("07", "The user is already active ", "", "",""));
    }
    System_Logs.log_system(system_username, "", "Restored bank user ID:"+jsondata.getUsername(), "Success", "");
       try {
			conn = DbManager.getConnection();
			String sql="UPDATE bank_users SET status=?,CREATED_BY=? WHERE USERNAME=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Active");
			prep.setObject(2, system_username);
			prep.setObject(3, jsondata.getUsername());
			prep.executeUpdate();
			conn.close();
			EmailAlert.logger(system_username, "freeze bank user id:"+jsondata.getEmail(), log);
			System_Logs.log_system(system_username, "", "User restored bank user:"+jsondata.getEmail(), "Success", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User restored successfully", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
	}
	
	
	@RequestMapping(value = "/v1/approveBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> approveBankUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
		if(jsondata.getUsername().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide username", "", ""));
		}
    String system_username=TokenManager.tokenIssuedCompCode(token);
    
     String getCreatedBy=LoginHelper.getCreatedBy(jsondata.getUsername());
     if(getCreatedBy.equalsIgnoreCase(system_username)) {
    	 return ResponseEntity.ok(new BankUsersResp("07", "You are not authorised to approve this user", ""));
     }
     String email=BankUserHelper.getbankuserEmail(jsondata.getUsername());
     String path=PathUrl.getBillerAppPath("bank");
		String url= path; 
		String date=TimeManager.getEmailTime();
		System_Logs.log_system(system_username, "", "Approved bank user ID:"+jsondata.getUsername(), "Success", "");
       try {
			conn = DbManager.getConnection();
			String sql="UPDATE bank_users SET status=?,approved_by=? WHERE USERNAME=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Active");
			prep.setObject(2, system_username);
			prep.setObject(3, jsondata.getUsername());
			prep.executeUpdate();
			conn.close();
			
			 String subject="E-biller";
			  VelocityContext vc = new VelocityContext();
	          vc.put("login_link", url);
	          vc.put("date", date);
	          vc.put("invited", jsondata.getSurname().toUpperCase());
	          vc.put("inviter", getBankName(system_username));
	          String email_template = EmailManager.email_message_template(vc,"welcome_bank.vm");
				EmailManager.send_mail(email,email_template,subject,system_username,"Bank user approval");
				EmailAlert.logger(system_username, "Approved bank user id:"+jsondata.getEmail(), log);
				System_Logs.log_system(system_username, "", "User approved bank user:"+jsondata.getUsername(), "Success", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User approved successfully", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/deleteBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> deleteBankUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
		if(jsondata.getUsername().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide username", "", ""));
		}
    String system_username=TokenManager.tokenIssuedCompCode(token);   
    if(system_username.equalsIgnoreCase(jsondata.getUsername())) {
    	return ResponseEntity.ok(new AuthProduceJson("08", "You are not authorized to delete this user", "", ""));
    }
		System_Logs.log_system(system_username, "", "Deleted bank user ID:"+jsondata.getUsername(), "Success", "");
       try {
			conn = DbManager.getConnection();
			String sql="UPDATE bank_users SET delete_user=?,modified_by=?  WHERE USERNAME=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "yes");
			prep.setObject(2, system_username);
			prep.setObject(3, jsondata.getUsername());
			prep.execute();
			conn.close();
			EmailAlert.logger(system_username, "Deleted bank user id:"+jsondata.getEmail(), log);
			System_Logs.log_system(system_username, "", "User deleted bank user:"+jsondata.getUsername(), "queued", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User queued successfully for approval", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "System Error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/ApproveDeleteBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> ApproveDeleteBankUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
		if(jsondata.getUsername().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide username", "", ""));
		}
    String system_username=TokenManager.tokenIssuedCompCode(token);   
    if(system_username.equalsIgnoreCase(jsondata.getUsername())) {
    	return ResponseEntity.ok(new AuthProduceJson("08", "You are not authorized to approve this delete this user", "", ""));
    }
    String getCreatedBy=LoginHelper.getUpdatedBy(jsondata.getUsername());
		if(system_username.equalsIgnoreCase(getCreatedBy)) {
			return ResponseEntity.ok(new AuthProduceJson("06", "You are not authorised to approve this delete", "", ""));
		}
		
       try {
			conn = DbManager.getConnection();
			String sql="DELETE FROM bank_users  WHERE USERNAME=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getUsername());
			prep.execute();
			conn.close();
			EmailAlert.logger(system_username, "approve Deleted bank user id:"+jsondata.getEmail(), log);
			System_Logs.log_system(system_username, "", "User approved deleted bank user:"+jsondata.getUsername(), "Success", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User deleted successfully", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "System Error", ""));
		}
	}
	@RequestMapping(value = "/v1/RejectdeleteBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> RejectdeleteBankUser(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BankEditUsersReqModel jsondata) {
		if(jsondata.getUsername().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide username", "", ""));
		}
    String system_username=TokenManager.tokenIssuedCompCode(token);   
    if(system_username.equalsIgnoreCase(jsondata.getUsername())) {
    	return ResponseEntity.ok(new AuthProduceJson("08", "You are not authorized to approve this delete this user", "", ""));
    }
    String getCreatedBy=LoginHelper.getUpdatedBy(jsondata.getUsername());
		if(system_username.equalsIgnoreCase(getCreatedBy)) {
			return ResponseEntity.ok(new AuthProduceJson("06", "You are not authorised to approve this delete", "", ""));
		}
       try {
			conn = DbManager.getConnection();
			String sql="UPDATE bank_users SET delete_user=?,modified_by=?  WHERE USERNAME=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "no");
			prep.setObject(2, system_username);
			prep.setObject(3, jsondata.getUsername());
			prep.execute();
			conn.close();
			EmailAlert.logger(system_username, "Rejected Deleted bank user id:"+jsondata.getEmail(), log);
			System_Logs.log_system(system_username, "", "User rejecteddeleted bank user:"+jsondata.getUsername(), "success", "");
			return ResponseEntity.ok(new BankUsersResp("00", "User queued successfully for approval", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "System Error", ""));
		}
	}
	
	
	public int checkIfGroupExist(String companyCode,String groupId) {
		int v_count=0;
		try {
			String sql="SELECT COUNT(*) AS V_COUNT FROM `user_groups` WHERE comp_code=? AND id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, companyCode);
			prep.setObject(2, groupId);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				
			}
			return v_count;
		}catch(Exception e) {
			return v_count;
		}
	}
	
	public String companyCode() {
		String bankCode="";
		try {
			String sql="SELECT BANK_CODE FROM `general_settings` WHERE ID=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, 1);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				bankCode=rs.getString("BANK_CODE");
			}
			conn.close();
			return bankCode;
		}catch(Exception e) {
			return bankCode;
		}
	}
	
	public static String getBankName(String username) {
		String name="";
		try {
			String sql="SELECT other_names FROM `bank_users` WHERE username=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1,username);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				name=rs.getString("other_names");
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return name;
	}
	
	
	
}
