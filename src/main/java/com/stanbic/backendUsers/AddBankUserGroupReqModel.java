package com.stanbic.backendUsers;

public class AddBankUserGroupReqModel {
String token="";
String groupName="";
String id="";

public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getGroupName() {
	return groupName;
}
public void setGroupName(String groupName) {
	this.groupName = groupName;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}

}
