package com.stanbic.backendUsers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Eslip.EslipConsumeJson;
import com.stanbic.Eslip.EslipHelper;
import com.stanbic.Eslip.MyESlipsProduceJSon;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class EslipReportsBank {
	Connection conn = null;
	
	@RequestMapping(value = "/v1/getAllBillerEslipsBank", method = RequestMethod.POST)
	public ResponseEntity<?> getAllBillerEslips(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipReportModel jsondata) {

		List<EslipReportModel> model = new ArrayList<EslipReportModel>();
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
		 
		try {
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay"
					+ ",approved_by,created_at,status,bank_ref_no,(SELECT COUNT(*) AS V_COUNT FROM eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n" 
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `eslip` b GROUP BY id,eslip_no,amount_due,expiry_date,payer_code,biller_code,status, amount_to_pay,approved_by,created_at,bank_ref_no ORDER BY id DESC"; 
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			//prep.setObject(1, comp_code);
			//prep.setObject(2, jsondata.getBiller_code());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				EslipReportModel data = new EslipReportModel();
				data.id=rs.getString("id"); 
				data.messageCode="00";
				data.message="success";
				data.eslip_no=rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.expiry_date=rs.getString("expiry_date");
				data.clients_name=rs.getString("client_name");
				data.amount_due=rs.getString("amount_due");
				data.amount_to_pay=rs.getString("amount_to_pay");
				data.status=rs.getString("status");
				data.account_no=rs.getString("account_no");
				data.accounts=rs.getString("accounts");
				data.biller_code=rs.getString("biller_code");
				data.payer_code=rs.getString("payer_code");
				data.approved_by=rs.getString("approved_by");
				data.created_at=rs.getString("created_at");
				data.bank_ref_no=rs.getString("bank_ref_no");
				data.created_by=EslipHelper.getPayerName(rs.getString("payer_code"));
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
}
