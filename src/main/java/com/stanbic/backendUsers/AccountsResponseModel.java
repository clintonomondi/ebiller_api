package com.stanbic.backendUsers;

public class AccountsResponseModel {
	String messageCode="00";
	String message="success";
	String token=null;
	String biller_code=null;
	String payer_code=null;
	String account_no=null;
	String accounts=null;
	String amount_due=null;
	String due_date=null;
	String alias=null;
	String account_name=null;
	String name=null;
	String status=null;
	String user_type=null;
	String firstName=null;
	String lastName=null;
	String payer_company_name=null;
	String personel_f_name = null;
	String personel_l_name = null;
	String comp_code = null;
	String biller_type = null;
	String company_name = null;
	String no_of_accounts = null;
	String sector = null;
	String delete_user="";
	String edit_user="";
	
	
	
	
	public String getDelete_user() {
		return delete_user;
	}
	public void setDelete_user(String delete_user) {
		this.delete_user = delete_user;
	}
	public String getEdit_user() {
		return edit_user;
	}
	public void setEdit_user(String edit_user) {
		this.edit_user = edit_user;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	String branch = null;
	String email = null;
	public String getPersonel_f_name() {
		return personel_f_name;
	}
	public void setPersonel_f_name(String personel_f_name) {
		this.personel_f_name = personel_f_name;
	}
	public String getPersonel_l_name() {
		return personel_l_name;
	}
	public void setPersonel_l_name(String personel_l_name) {
		this.personel_l_name = personel_l_name;
	}
	public String getComp_code() {
		return comp_code;
	}
	public void setComp_code(String comp_code) {
		this.comp_code = comp_code;
	}
	public String getBiller_type() {
		return biller_type;
	}
	public void setBiller_type(String biller_type) {
		this.biller_type = biller_type;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public String getNo_of_accounts() {
		return no_of_accounts;
	}
	public void setNo_of_accounts(String no_of_accounts) {
		this.no_of_accounts = no_of_accounts;
	}
	public String getPayer_company_name() {
		return payer_company_name;
	}
	public void setPayer_company_name(String payer_company_name) {
		this.payer_company_name = payer_company_name;
	}
	public String getBiller_company_name() {
		return biller_company_name;
	}
	public void setBiller_company_name(String biller_company_name) {
		this.biller_company_name = biller_company_name;
	}
	String biller_company_name=null;
	String id=null;
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getBiller_code() {
		return biller_code;
	}
	public void setBiller_code(String biller_code) {
		this.biller_code = biller_code;
	}
	public String getPayer_code() {
		return payer_code;
	}
	public void setPayer_code(String payer_code) {
		this.payer_code = payer_code;
	}
	public String getAccount_no() {
		return account_no;
	}
	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}
	public String getAccounts() {
		return accounts;
	}
	public void setAccounts(String accounts) {
		this.accounts = accounts;
	}
	public String getAmount_due() {
		return amount_due;
	}
	public void setAmount_due(String amount_due) {
		this.amount_due = amount_due;
	}
	public String getDue_date() {
		return due_date;
	}
	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getAccount_name() {
		return account_name;
	}
	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
