package com.stanbic.backendUsers;

public class BankUsersReqModel {

String otherName="";
String surname="";
String phone="";
String email="";
String group_id="";
String username="";
String token="";
String notification="";




public String getNotification() {
	return notification;
}
public void setNotification(String notification) {
	this.notification = notification;
}
public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getOtherName() {
	return otherName;
}
public void setOtherName(String otherName) {
	this.otherName = otherName;
}

public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getSurname() {
	return surname;
}
public void setSurname(String surname) {
	this.surname = surname;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getGroup_id() {
	return group_id;
}
public void setGroup_id(String group_id) {
	this.group_id = group_id;
}


}
