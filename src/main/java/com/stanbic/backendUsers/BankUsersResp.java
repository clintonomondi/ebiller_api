package com.stanbic.backendUsers;

public class BankUsersResp {
String messageCode="";
String message="";
String description="";

public BankUsersResp(String messageCode, String message, String description) {
	this.messageCode = messageCode;
	this.message = message;
	this.description = description;
}

public String getMessageCode() {
	return messageCode;
}
public void setMessageCode(String messageCode) {
	this.messageCode = messageCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}

}
