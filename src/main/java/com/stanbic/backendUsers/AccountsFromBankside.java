package com.stanbic.backendUsers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Billers.BillersAccountProduceJson;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class AccountsFromBankside {
	
	Connection conn=null;
	static Logger log = Logger.getLogger(AccountsFromBankside.class.getName());
	
	@RequestMapping(value = "/v1/getPayerAccountsFromBankside", method = RequestMethod.POST)
	public ResponseEntity<?> getPayerAccounts(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
	    if(jsondata.getBiller_code().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Empty Fields","",""));
		}
	     
		List<AccountsResponseModel> model= new ArrayList<AccountsResponseModel>();
		try {
		if(jsondata.getPayer_code().isEmpty()) {
			String sql="SELECT biller_code,account_no,payer_code,amount_due,due_date,alias,account_name,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.payer_code)payer_name,\r\n" + 
					"(SELECT company_name FROM biller_profile C WHERE C.comp_code=A.biller_code)biller_name\r\n" + 
					" FROM `account` A WHERE biller_code=?"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getBiller_code());
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				AccountsResponseModel data=new AccountsResponseModel();
				data.messageCode="00";
				data.message="Success";
				data.biller_code=rs.getString("biller_code");
				data.account_no=rs.getString("account_no");
				data.payer_code=rs.getString("payer_code");
				data.amount_due=rs.getString("amount_due");
				data.due_date=rs.getString("due_date");
				data.alias=rs.getString("alias");
				data.account_name=rs.getString("account_name");
				data.payer_company_name=rs.getString("payer_name");
				data.biller_company_name=rs.getString("biller_name");
				model.add(data);
			}
			
		}else {
		
			String sql="SELECT biller_code,account_no,payer_code,amount_due,due_date,alias,account_name,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.payer_code)payer_name,\r\n" + 
					"(SELECT company_name FROM biller_profile C WHERE C.comp_code=A.biller_code)biller_name\r\n" + 
					" FROM `account` A WHERE biller_code=? AND payer_code=?"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getBiller_code());
			prep.setObject(2, jsondata.getPayer_code());
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				AccountsResponseModel data=new AccountsResponseModel();
				data.messageCode="00";
				data.message="Success";
				data.biller_code=rs.getString("biller_code");
				data.account_no=rs.getString("account_no");
				data.payer_code=rs.getString("payer_code");
				data.amount_due=rs.getString("amount_due");
				data.due_date=rs.getString("due_date");
				data.alias=rs.getString("alias");
				data.account_name=rs.getString("account_name");
				data.payer_company_name=rs.getString("payer_name");
				data.biller_company_name=rs.getString("biller_name");
				model.add(data);
			}
		}
			conn.close();
			return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
	}
	
	@RequestMapping(value = "/v1/getAllUsersFromBankside", method = RequestMethod.POST)
	public ResponseEntity<?> getAllUsers(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
		
		List<AccountsResponseModel> model= new ArrayList<AccountsResponseModel>();
		try {
			String sql="SELECT *,\r\n" + 
					"(SELECT COUNT(*) FROM `account` B WHERE A.comp_code=B.payer_code)no_of_accounts \r\n" + 
					"FROM biller_profile A";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				AccountsResponseModel data=new AccountsResponseModel();
				data.personel_f_name = rs.getString("personel_f_name");
				data.personel_l_name = rs.getString("personel_l_name");
				data.comp_code = rs.getString("comp_code");
				data.user_type = rs.getString("user_type");
				data.biller_type = rs.getString("user_type");
				data.company_name = rs.getString("company_name");
				data.no_of_accounts = rs.getString("no_of_accounts");
				data.status=rs.getString("status");
				data.sector=rs.getString("sector");
				data.email=rs.getString("email");
				data.alias=rs.getString("alias");
				data.edit_user=rs.getString("edit_user");
				data.delete_user=rs.getString("delete_user");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		 }catch(Exception e) {
//			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
		
	}
	
	@RequestMapping(value = "/v1/getAllPayersFromBankside", method = RequestMethod.POST)
	public ResponseEntity<?> getAllPayersFromBankside(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
		
		List<AccountsResponseModel> model= new ArrayList<AccountsResponseModel>();
		String biller_code=jsondata.getBiller_code();
		try {
			if(jsondata.getBiller_code().isEmpty()) {
			String sql="SELECT personel_f_name,status,sector, email, personel_l_name, comp_code, user_type, alias, biller_type, company_name,\r\n" + 
					"(SELECT COUNT(*) FROM `account` B WHERE A.comp_code=B.payer_code)no_of_accounts \r\n" + 
					"FROM biller_profile A WHERE user_type=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, "payer");
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				AccountsResponseModel data=new AccountsResponseModel();
				data.personel_f_name = rs.getString("personel_f_name");
				data.personel_l_name = rs.getString("personel_l_name");
				data.comp_code = rs.getString("comp_code");
				data.user_type = rs.getString("user_type");
				data.biller_type = rs.getString("user_type");
				data.company_name = rs.getString("company_name");
				data.no_of_accounts = rs.getString("no_of_accounts");
				data.status=rs.getString("status");
				data.sector=rs.getString("sector");
				data.email=rs.getString("email");
				data.alias=rs.getString("alias");
				model.add(data);
			}
			}else {
				String sql="SELECT payer_code AS comp_code,\r\n" + 
						"(SELECT personel_f_name FROM biller_profile B WHERE B.comp_code=A.payer_code)personel_f_name,\r\n" + 
						"(SELECT personel_l_name FROM biller_profile C WHERE C.comp_code=A.payer_code)personel_l_name,\r\n" + 
						"(SELECT user_type FROM biller_profile D WHERE D.comp_code=A.payer_code)user_type,\r\n" + 
						"(SELECT company_name FROM biller_profile E WHERE E.comp_code=A.payer_code)company_name,\r\n" + 
						"(SELECT COUNT(*) FROM `account` F WHERE F.payer_code=A.payer_code)no_of_accounts,\r\n" + 
						"(SELECT STATUS FROM biller_profile G WHERE G.comp_code=A.payer_code)STATUS,\r\n" + 
						"(SELECT sector FROM biller_profile H WHERE H.comp_code=A.payer_code)sector,\r\n" + 
						"(SELECT email FROM biller_profile I WHERE I.comp_code=A.payer_code)email,\r\n" + 
						"(SELECT alias FROM biller_profile J WHERE J.comp_code=A.payer_code)alias\r\n" + 
						"FROM billers A WHERE biller_code=? \r\n" + 
						"";
				conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				prep.setObject(1, biller_code);
				ResultSet  rs=prep.executeQuery();
				while(rs.next()) {
					AccountsResponseModel data=new AccountsResponseModel();
					data.personel_f_name = rs.getString("personel_f_name");
					data.personel_l_name = rs.getString("personel_l_name");
					data.comp_code = rs.getString("comp_code");
					data.user_type = rs.getString("user_type");
					data.biller_type = rs.getString("user_type");
					data.company_name = rs.getString("company_name");
					data.no_of_accounts = rs.getString("no_of_accounts");
					data.status=rs.getString("status");
					data.sector=rs.getString("sector");
					data.email=rs.getString("email");
					data.alias=rs.getString("alias");
					model.add(data);
				}
			}
			conn.close();
			return ResponseEntity.ok(model);
		 }catch(Exception e) {
//			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
		
	}
	
	@RequestMapping(value = "/v1/getAllPayersByBiller", method = RequestMethod.POST)
	public ResponseEntity<?> getAllPayersByBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
		if(jsondata.getBiller_code().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Where is biller code","",""));
		}
		List<AccountsResponseModel> model= new ArrayList<AccountsResponseModel>();
		try {
			String sql="SELECT payer_code AS comp_code,\r\n" + 
					"(SELECT personel_f_name FROM biller_profile B WHERE B.comp_code=A.payer_code)personel_f_name,\r\n" + 
					"(SELECT personel_l_name FROM biller_profile C WHERE C.comp_code=A.payer_code)personel_l_name,\r\n" + 
					"(SELECT user_type FROM biller_profile D WHERE D.comp_code=A.payer_code)user_type,\r\n" + 
					"(SELECT company_name FROM biller_profile E WHERE E.comp_code=A.payer_code)company_name,\r\n" + 
					"(SELECT COUNT(*) FROM `account` F WHERE F.payer_code=A.payer_code)no_of_accounts,\r\n" + 
					"(SELECT STATUS FROM biller_profile G WHERE G.comp_code=A.payer_code)STATUS,\r\n" + 
					"(SELECT sector FROM biller_profile H WHERE H.comp_code=A.payer_code)sector,\r\n" + 
					"(SELECT email FROM biller_profile I WHERE I.comp_code=A.payer_code)email,\r\n" + 
					"(SELECT alias FROM biller_profile J WHERE J.comp_code=A.payer_code)alias\r\n" + 
					"FROM billers A WHERE biller_code=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getBiller_code());
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				AccountsResponseModel data=new AccountsResponseModel();
				data.personel_f_name = rs.getString("personel_f_name");
				data.personel_l_name = rs.getString("personel_l_name");
				data.comp_code = rs.getString("comp_code");
				data.user_type = rs.getString("user_type");
				data.company_name = rs.getString("company_name");
				data.no_of_accounts = rs.getString("no_of_accounts");
				data.status=rs.getString("status");
				data.sector=rs.getString("sector");
				data.email=rs.getString("email");
				data.alias=rs.getString("alias");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
		
	}
	
	public boolean isUserABankUser(String email) {
		boolean bankValue = false;
		try {
			String sql = "SELECT * FROM `bank_users` WHERE email=?";
			conn=DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet  rs=prep.executeQuery();
			if(rs.next()) {
				bankValue = true;
			}
		}catch(Exception e) {
			
		}
		return bankValue;
	}
	
}
