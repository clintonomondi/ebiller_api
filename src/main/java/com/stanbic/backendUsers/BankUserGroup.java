package com.stanbic.backendUsers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;

@SpringBootApplication
@RequestMapping("/api")
public class BankUserGroup {
	static Logger log = Logger.getLogger(BankUserGroup.class.getName());
	Connection conn=null;
	
	@RequestMapping(value = "/v1/getBankUserGroup", method = RequestMethod.POST)
	public ResponseEntity<?> getBankUsersGroups(HttpServletRequest request, @RequestHeader(name = "Authorization") String token) {
		List<GetBankUserGroupRespModel> model=new ArrayList<GetBankUserGroupRespModel>();
		String companyCode=companyCode();
		try {	
		String sql="SELECT id,name FROM `user_groups` WHERE comp_code=?";
		conn=DbManager.getConnection();
		PreparedStatement prep=conn.prepareStatement(sql);
		prep.setObject(1, companyCode);
		ResultSet rs=prep.executeQuery();
		while(rs.next()) {
			GetBankUserGroupRespModel m=new GetBankUserGroupRespModel();
			m.groupCode=rs.getString("id");
			m.groupName=rs.getString("name");
			model.add(m);
		}
		conn.close();
		return ResponseEntity.ok(model);
		}catch(Exception e) {
		e.printStackTrace();
		return ResponseEntity.ok(model);
		}
	}

	@RequestMapping(value = "/v1/addBankUserGroup", method = RequestMethod.POST)
	public ResponseEntity<?> addBankUsersGroups(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AddBankUserGroupReqModel jsondata) {
		String companyCode=companyCode();
		String userName=TokenManager.tokenIssuedCompCode(token);
			
		try {	
		String sql="INSERT INTO user_groups(name,comp_code,created_by) VALUES(?,?,?)";
		conn=DbManager.getConnection();
		PreparedStatement prep=conn.prepareStatement(sql);
		prep.setObject(1, jsondata.getGroupName());
		prep.setObject(2, companyCode);
		prep.setObject(3, userName);
		prep.execute();
		conn.close();
		return ResponseEntity.ok(new BankUsersResp("00", "Success", ""));
		}catch(Exception e) {
		e.printStackTrace();
		EmailAlert.logger(companyCode, "Added bank user group", log);
		 System_Logs.log_system(companyCode, "", "User created bank group:", "Success", "");
		return ResponseEntity.ok(new BankUsersResp("02", "Systeem Error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/editBankUserGroup", method = RequestMethod.POST)
	public ResponseEntity<?> editBankUsersGroups(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AddBankUserGroupReqModel jsondata) {
		String companyCode=companyCode();
		if(jsondata.getGroupName().equalsIgnoreCase("Admin")) {
			return ResponseEntity.ok(new BankUsersResp("06", "You cannot edit this user group", ""));
		}
		try {	
		String sql="UPDATE user_groups SET name=? WHERE comp_code=? AND id=? ";
		conn=DbManager.getConnection();
		PreparedStatement prep=conn.prepareStatement(sql);
		prep.setObject(1, jsondata.getGroupName());
		prep.setObject(2, companyCode);
		prep.setObject(3, jsondata.getId());
		prep.executeUpdate();
		conn.close();
		EmailAlert.logger(companyCode, "edited bank user group", log);
		 System_Logs.log_system(companyCode, "", "User edited bank group:", "Success", "");
		return ResponseEntity.ok(new BankUsersResp("00", "Success", ""));
		}catch(Exception e) {
		e.printStackTrace();
		return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
		
	}
	public String companyCode() {
		String bankCode="";
		try {
			String sql="SELECT BANK_CODE FROM `general_settings` WHERE ID=1";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				bankCode=rs.getString("BANK_CODE");
			}
			conn.close();
			return bankCode;
		}catch(Exception e) {
			return bankCode;
		}
	}
}
