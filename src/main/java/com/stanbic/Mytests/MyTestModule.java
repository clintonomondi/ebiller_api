package com.stanbic.Mytests;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import org.apache.log4j.Logger;

import org.json.simple.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.*;
import java.util.*;

import javax.net.ssl.HttpsURLConnection;

@SpringBootApplication
@RequestMapping("/api")
public class MyTestModule {
	
	static Logger log = Logger.getLogger(MyTestModule.class.getName());
	public static String postData(Hashtable<String, String> data, String url, String apiMethod) {
		try {
			@SuppressWarnings("rawtypes")
			Enumeration datesc;
			URL myurl = new URL(url);	
			if ((myurl.getProtocol()).equals("htpps")){
				HttpsURLConnection conn = null;
				conn = (HttpsURLConnection)myurl.openConnection();
				OutputStream os;
				InputStream in = null;
				List<NameValuePair> list;
				String str;
				JSONObject parent = new JSONObject();
				String d = "";
				conn.setConnectTimeout(10000);
				conn.setRequestMethod("POST");
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
				datesc = data.keys();
				list = new ArrayList<NameValuePair>();

				while (datesc.hasMoreElements()) {
					str = (String) datesc.nextElement();
					list.add(new BasicNameValuePair(str, "" + data.get(str)));
					parent.put(str, data.get(str));
				}
				
				os = conn.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				switch (apiMethod) {
				case "POST":
					writer.write(list.toString());
					break;
				case "JSON":
					writer.write(parent.toString());
					break;
				default:
					break;
				}
				
				writer.flush();
				writer.close();
				os.close();

				int status = conn.getResponseCode();
				if (status == 200) {
					in = conn.getInputStream();
				} else if (status == 404) {
					in = conn.getErrorStream();
				}

				java.io.InputStreamReader isr = new java.io.InputStreamReader(in);
				java.io.BufferedReader inn = new java.io.BufferedReader(isr);

				String inputLine;

				while ((inputLine = inn.readLine()) != null) {
					System.out.println(inputLine);
					d = "" + inputLine;
				}
				return d.toString();
			}
			else {
				HttpURLConnection conc = null;
				conc = (HttpURLConnection) myurl.openConnection();
				OutputStream os = null;
				InputStream ins = null;
				List<NameValuePair> params1 = null;
				JSONObject parent = new JSONObject();
				String d = null;

				conc.setConnectTimeout(15000);
				conc.setRequestMethod("POST");
				conc.setDoInput(true);
				conc.setDoOutput(true);
				conc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

				String str;
				datesc = data.keys();
				params1 = new ArrayList<NameValuePair>();

				while (datesc.hasMoreElements()) {
					str = (String) datesc.nextElement();
					params1.add(new BasicNameValuePair(str, "" + data.get(str)));
					parent.put(str, data.get(str));
				}
				
				os = conc.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				switch (apiMethod) {
				case "POST":
					writer.write(params1.toString());
					break;
				case "JSON":
					writer.write(parent.toString());
					break;
				default:
					break;
				}
				writer.flush();
				writer.close();
				os.close();
				int status = conc.getResponseCode();
				if (status == 200) {
					ins = conc.getInputStream();
				} else if (status == 404) {
					ins = conc.getErrorStream();
				} else if (status == 503) {
					ins = conc.getErrorStream();
				} else {
					ins = conc.getErrorStream();
				}
				System.out.println("step 2");
				java.io.InputStreamReader isr = new java.io.InputStreamReader(ins);
				java.io.BufferedReader in = new java.io.BufferedReader(isr);
				System.out.println("step 11");
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					System.out.println(inputLine);
					d = "" + inputLine;
				}
				System.out.println("step 3");
				return d.toString();
			}
		} catch (Exception n) {
			System.out.println(n.getMessage());
			return null;
		} 
	}
	
	@RequestMapping(value = "/v1/testLogger", method = RequestMethod.GET)
	public void perform() {
		log.warn("Logging an error");
		Hashtable<String, String> hash = new Hashtable<>();
//		hash.put("name", "Collins");
//		postData(hash, "http://localhost:8080/api/customerInfo", "POST");
	}
}
