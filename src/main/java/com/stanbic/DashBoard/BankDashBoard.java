package com.stanbic.DashBoard;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Profile.PayerProfileProduceJson;
import com.stanbic.comm.DbManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class BankDashBoard {
	Connection conn=null;
	
	@RequestMapping(value = "/v1/getDashBoardDataBank", method = RequestMethod.POST)
	public ResponseEntity<?>getDashBoardDataBank(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata){
		 String active_billers="";
		 String invited_billers="";
		 String pending_billers="";
		 String active_payers="";
		 String pending_payers="";
		 String accounts="";
		 String amount_charged_pending="";
		 String amount_charged_paid="";
		 String eslip_pending="";
		 String eslip_paid="";
		 String inactive_payers="";
		 String exceptions="";
		 Hashtable<String,String> map=new Hashtable<String,String>();
		 try {
			 String sql="SELECT (SELECT COUNT(*) FROM biller_profile WHERE STATUS='Active' AND user_type='biller')active_billers,\r\n" + 
			 		"(SELECT COUNT(*) FROM biller_profile WHERE STATUS='Invited' AND user_type='biller')invited_billers,\r\n" + 
			 		"(SELECT COUNT(*) FROM `biller_profile` WHERE STATUS='Pending' AND user_type='biller')pending_billers,\r\n" + 
			 		"(SELECT COUNT(*) FROM `biller_profile` WHERE STATUS='Active' AND user_type='payer')active_payers,\r\n" + 
			 		"(SELECT COUNT(*) FROM `biller_profile` WHERE STATUS='Pending' AND user_type='payer')pending_payers,\r\n" + 
			 		"(SELECT COUNT(*) FROM `biller_profile` WHERE STATUS='Inactive' AND user_type='payer')inactive_payers,\r\n" + 
			 		"(SELECT COUNT(*) FROM `account`)accounts,\r\n" + 
			 		"(SELECT COUNT(*) FROM `exception_logs`)exceptions,\r\n" +
			 		"(SELECT if(SUM(amount_charged) is null,'0',SUM(amount_charged)) FROM `eslip` WHERE charge_status='Pending')amount_charged_pending,\r\n" + 
			 		"(SELECT if(SUM(amount_charged) is null,'0',SUM(amount_charged)) FROM `eslip` WHERE charge_status='Paid')amount_charged_paid,\r\n" + 
			 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM `eslip` WHERE STATUS='Pending')eslip_pending,\r\n" + 
			 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM `eslip` WHERE STATUS='Paid')eslip_paid\r\n" + 
			 		" FROM DUAL";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 active_billers=rs.getString("active_billers");
				 invited_billers=rs.getString("invited_billers");
				 pending_billers=rs.getString("pending_billers");
				 active_payers=rs.getString("active_payers");
				 accounts=rs.getString("accounts");
				 amount_charged_pending=rs.getString("amount_charged_pending");
				 amount_charged_paid=rs.getString("amount_charged_paid");
				 eslip_pending=rs.getString("eslip_pending");
				 eslip_paid=rs.getString("eslip_paid");
				 pending_payers=rs.getString("pending_payers");
				 inactive_payers=rs.getString("inactive_payers");
				 exceptions=rs.getString("exceptions");
			 }
			 conn.close();
			 map.put("active_billers", active_billers);
			 map.put("invited_billers", invited_billers);
			 map.put("pending_billers", pending_billers);
			 map.put("active_payers", active_payers);
			 map.put("accounts", accounts);
			 map.put("amount_charged_pending", amount_charged_pending);
			 map.put("amount_charged_paid", amount_charged_paid);
			 map.put("eslip_pending", eslip_pending);
			 map.put("eslip_paid", eslip_paid);
			 map.put("pending_payers",pending_payers);
			 map.put("inactive_payers",inactive_payers);
			 map.put("exceptions",exceptions);
			 return ResponseEntity.ok(map);
		 }
		 catch(Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
	
	}
}
