package com.stanbic.DashBoard;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Year;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Profile.PayerProfileProduceJson;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class BillerDashBoard {
	Connection conn=null;
	
	@RequestMapping(value = "/v1/getDashBoardDataBiller", method = RequestMethod.POST)
	public ResponseEntity<?>getDashBoardDataBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata){
	String biller_code=TokenManager.tokenIssuedCompCode(token);
	 List<DashBoardModel> model= new ArrayList<DashBoardModel>();
	     int time = Year.now().getValue();
	     String year=time+"";
	 try {
		 String sql="SELECT (SELECT COUNT(*) FROM account WHERE  biller_code='"+biller_code+"')total_accounts,\r\n" + 
		 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM account WHERE  biller_code='"+biller_code+"')total_amount_due,\r\n" + 
		 		"(SELECT COUNT(*) FROM billers WHERE  biller_code='"+biller_code+"')payers,\r\n" + 
		 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE biller_code='"+biller_code+"' AND STATUS='Pending')total_balance_due,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 1 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')january,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 2 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')february,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 3 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')march,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 4 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')april,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 5 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')may,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 6 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')june,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 7 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')july,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 8 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')augast,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 9 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')september,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 10 AND YEAR(created_at) = '"+year+"' AND  biller_code='"+biller_code+"')october,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 11 AND YEAR(created_at) = '"+year+"' AND  biller_code='"+biller_code+"')november,\r\n" + 
		 		"(SELECT if(SUM(amount_to_pay) is null,'0',SUM(amount_to_pay)) FROM eslip WHERE MONTH(created_at) = 12 AND YEAR(created_at) = '"+year+"' AND  biller_code='"+biller_code+"')december\r\n" + 
		 		"FROM DUAL";
		 conn=DbManager.getConnection();
		 PreparedStatement prep=conn.prepareStatement(sql);
		 ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				DashBoardModel data=new DashBoardModel();
				data.total_accounts=rs.getString("total_accounts");
				data.total_amount_due=rs.getDouble("total_amount_due");
				data.total_balance_due=rs.getDouble("total_balance_due");
				data.payers=rs.getString("payers");
				data.year=year;
				LinkedHashMap<String, Object> table=new LinkedHashMap<String,Object>();
				
				table.put("January",rs.getString("january"));
				table.put("February",rs.getString("february"));
				table.put("March",rs.getString("march"));
				table.put("April",rs.getString("april"));
				table.put("May",rs.getString("may"));
				table.put("June",rs.getString("june"));
				table.put("July",rs.getString("july"));
				table.put("August",rs.getString("augast"));
				table.put("September",rs.getString("september"));
				table.put("October",rs.getString("october"));
				table.put("November",rs.getString("november"));
				table.put("December",rs.getString("december"));
				
				data.eslip_raised=table;
				data.eslip_pending=bankAccessMenu(biller_code,year);
				data.service_charge=bankAccessMenu(biller_code,year);
				 model.add(data);
			}
			conn.close();
			 return ResponseEntity.ok(model);
	 }catch(Exception n) {
		 n.printStackTrace();
		 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
	 }
	 
	 
 }
 
	public LinkedHashMap<String,Object> bankAccessMenu(String biller_code,String year){
		LinkedHashMap<String, Object> table=new LinkedHashMap<String,Object>();
		try {
			 String sql="SELECT (SELECT COUNT(*) FROM account WHERE  biller_code='"+biller_code+"')total_accounts,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 1 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')january,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 2 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')february,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 3 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')march,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 4 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')april,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 5 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')may,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 6 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')june,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 7 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')july,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 8 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')augast,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 9 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')september,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 10 AND YEAR(created_at) = '"+year+"' AND  biller_code='"+biller_code+"')october,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 11 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')november,\r\n" + 
				 		"(SELECT if(SUM(amount_due) is null,'0',SUM(amount_due)) FROM eslip WHERE STATUS='Paid' AND MONTH(created_at) = 12 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')december\r\n" + 
				 		"FROM DUAL";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 ResultSet  rs=prep.executeQuery();
				while(rs.next()) {
					
					table.put("January",rs.getString("january"));
					table.put("February",rs.getString("february"));
					table.put("March",rs.getString("march"));
					table.put("April",rs.getString("april"));
					table.put("May",rs.getString("may"));
					table.put("June",rs.getString("june"));
					table.put("July",rs.getString("july"));
					table.put("August",rs.getString("augast"));
					table.put("September",rs.getString("september"));
					table.put("October",rs.getString("october"));
					table.put("November",rs.getString("november"));
					table.put("December",rs.getString("december"));
				}
				conn.close();
		 }catch(Exception n) {
			 n.printStackTrace();
		 }
		return table;
	
	}
	
	public LinkedHashMap<String,Object> getServiceCharge(String biller_code,String year){
		LinkedHashMap<String, Object> table=new LinkedHashMap<String,Object>();
		try {
			 String sql="SELECT (SELECT COUNT(*) FROM account WHERE  biller_code='"+biller_code+"')total_accounts,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 1 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')january,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE MONTH(created_at) = 2 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')february,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE   MONTH(created_at) = 3 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')march,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 4 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')april,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 5 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')may,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 6 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')june,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 7 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')july,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 8 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')augast,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 9 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')september,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 10 AND YEAR(created_at) = '"+year+"' AND  biller_code='"+biller_code+"')october,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 11 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')november,\r\n" + 
				 		"(SELECT SUM(amount_charged) FROM eslip WHERE  MONTH(created_at) = 12 AND YEAR(created_at) = '"+year+"' AND   biller_code='"+biller_code+"')december\r\n" + 
				 		"FROM DUAL";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 ResultSet  rs=prep.executeQuery();
				while(rs.next()) {
					
					table.put("January",rs.getString("january"));
					table.put("February",rs.getString("february"));
					table.put("March",rs.getString("march"));
					table.put("April",rs.getString("april"));
					table.put("May",rs.getString("may"));
					table.put("June",rs.getString("june"));
					table.put("July",rs.getString("july"));
					table.put("Augast",rs.getString("augast"));
					table.put("September",rs.getString("september"));
					table.put("October",rs.getString("october"));
					table.put("November",rs.getString("november"));
					table.put("December",rs.getString("december"));
				}
				conn.close();
		 }catch(Exception n) {
			 n.printStackTrace();
		 }
		return table;
	
	}
}
