package com.stanbic.DashBoard;

import java.util.LinkedHashMap;

public class DashBoardModel {
	String messageCode="00";
	String message="success";
String total_accounts=null;
double total_amount_due=0.0;
double total_balance_due=0.0;
String payers="";
String year=null;
Object eslip_raised=null;
LinkedHashMap eslip_pending=null;
LinkedHashMap service_charge=null;
String eslip_on_paid=null;
String eslip_on_pending=null;






public String getEslip_on_paid() {
	return eslip_on_paid;
}


public void setEslip_on_paid(String eslip_on_paid) {
	this.eslip_on_paid = eslip_on_paid;
}


public String getEslip_on_pending() {
	return eslip_on_pending;
}


public void setEslip_on_pending(String eslip_on_pending) {
	this.eslip_on_pending = eslip_on_pending;
}


public LinkedHashMap getEslip_pending() {
	return eslip_pending;
}


public void setEslip_pending(LinkedHashMap eslip_pending) {
	this.eslip_pending = eslip_pending;
}


public LinkedHashMap getService_charge() {
	return service_charge;
}


public void setService_charge(LinkedHashMap service_charge) {
	this.service_charge = service_charge;
}


public String getPayers() {
	return payers;
}


public void setPayers(String payers) {
	this.payers = payers;
}


public String getYear() {
	return year;
}


public Object getEslip_raised() {
	return eslip_raised;
}


public void setEslip_raised(Object eslip_raised) {
	this.eslip_raised = eslip_raised;
}


public void setYear(String year) {
	this.year = year;
}

public String getMessageCode() {
	return messageCode;
}
public void setMessageCode(String messageCode) {
	this.messageCode = messageCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getTotal_accounts() {
	return total_accounts;
}
public void setTotal_accounts(String total_accounts) {
	this.total_accounts = total_accounts;
}


public double getTotal_amount_due() {
	return total_amount_due;
}


public void setTotal_amount_due(double total_amount_due) {
	this.total_amount_due = total_amount_due;
}


public double getTotal_balance_due() {
	return total_balance_due;
}


public void setTotal_balance_due(double total_balance_due) {
	this.total_balance_due = total_balance_due;
}



}
