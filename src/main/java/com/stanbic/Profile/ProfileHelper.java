package com.stanbic.Profile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.stanbic.comm.DbManager;

public class ProfileHelper {
	static Connection conn=null;
	
	public static String  getInvoiceTheme(String comp_code) {
		String data=null;
		try {  
			 String sql="SELECT invoice_color FROM interface_settings WHERE comp_code=?";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, comp_code);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  data=rs.getString("invoice_color");
				}
			  conn.close();
	    }catch(Exception n) {
		n.printStackTrace();
	    }
	return data;
}
	
	public static String getGroupName(String group_id) {
		String name="";
		try {  
			 String sql="SELECT name FROM user_groups WHERE id=?";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, group_id);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  name=rs.getString("name");
				}
			  
			  conn.close();
	    }catch(Exception n) {
		n.printStackTrace();
	    }
		
		return name;
	}

	
	public static String getForeStatus(String comp_code) {
		String data="0%";
		int vcount=0;
		try {  
			 String sql="SELECT COUNT(*)vcount FROM forex WHERE comp_code=?";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, comp_code);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				 if(rs.getInt("vcount")>0) {
					 data="100%";
				 }
				}
			  conn.close();
	    }catch(Exception n) {
		n.printStackTrace();
	    }
		
		return data;
	}
	public static String getCommissionStatus(String comp_code) {
		String data="0%";
		int vcount=0;
		try {  
			 String sql="SELECT COUNT(*)vcount FROM commission WHERE biller_code=? AND type=?";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, comp_code);
			 prep.setObject(2, "all");
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				 if(rs.getInt("vcount")>0) {
					 data="100%";
				 }
				}
			  conn.close();
	    }catch(Exception n) {
		n.printStackTrace();
	    }
		
		return data;
	}
	
	public static String file_setting(String comp_code) {
		String data="0%";
		int vcount=0;
		try {  
			 String sql="SELECT COUNT(*)vcount FROM invoice_file_setting WHERE comp_code=?";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, comp_code);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				 if(rs.getInt("vcount")>0) {
					 data="100%";
				 }
				}
			  conn.close();
	    }catch(Exception n) {
		n.printStackTrace();
	    }
		
		return data;
	}
	
	public static String  isForexDateExpire(String comp_code) {
		String value="0";
		try {
			 String sql="SELECT COUNT(*)vcount FROM forex WHERE  comp_code=? AND end_date > NOW()";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, comp_code);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				if(rs.getInt("vcount")>0) {
					value="1";
				}
			}
			 conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return value;
	}
}
