package com.stanbic.Profile;

public class InvoiceSettingProduceJson {
	String comp_code=null;
	String invoice_color=null;
	String invoice_name=null;
	String token=null;
	String terms=null;
	String tax="";
	String commission="";
	
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getTerms() {
		return terms;
	}
	public void setTerms(String terms) {
		this.terms = terms;
	}
	public String getComp_code() {
		return comp_code;
	}
	public void setComp_code(String comp_code) {
		this.comp_code = comp_code;
	}
	public String getInvoice_color() {
		return invoice_color;
	}
	public void setInvoice_color(String invoice_color) {
		this.invoice_color = invoice_color;
	}
	public String getInvoice_name() {
		return invoice_name;
	}
	public void setInvoice_name(String invoice_name) {
		this.invoice_name = invoice_name;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
}
