package com.stanbic.Profile;


public class PayerProfileProduceJson {
String alias=null;
String id=null;
String comp_code=null;
String biller_location=null;
String biller_phone=null;
String biller_month=null;
String branch=null;
String business_description=null;
String company_name=null;
String country=null;
String email=null;
String employee_no=null;
String personel_f_name=null;
String personel_l_name=null;
String sector=null;
String stb_acc_name=null;
String stb_acc_no=null;
String token=null;
String status=null;
String biller_type=null;
String year=null;
String user_type=null;
String biller_code=null;
String payer_code=null;
String brand_theme=null;
String base64Logo=null;
String test_invoice=null;
String brandpercentage=null;
String companypercentage=null;
String notif_value="";
String forex_percentage="";
String forex_value="";
String commission_percentage="";
String file_setting_percentage="";
String paybill="";





public String getPaybill() {
	return paybill;
}
public void setPaybill(String paybill) {
	this.paybill = paybill;
}
public String getFile_setting_percentage() {
	return file_setting_percentage;
}
public void setFile_setting_percentage(String file_setting_percentage) {
	this.file_setting_percentage = file_setting_percentage;
}
public String getCommission_percentage() {
	return commission_percentage;
}
public void setCommission_percentage(String commission_percentage) {
	this.commission_percentage = commission_percentage;
}
public String getForex_value() {
	return forex_value;
}
public void setForex_value(String forex_value) {
	this.forex_value = forex_value;
}
public String getForex_percentage() {
	return forex_percentage;
}
public void setForex_percentage(String forex_percentage) {
	this.forex_percentage = forex_percentage;
}
public String getNotif_value() {
	return notif_value;
}
public void setNotif_value(String notif_value) {
	this.notif_value = notif_value;
}
public String getAlias() {
	return alias;
}
public void setAlias(String alias) {
	this.alias = alias;
}
public String getCompanypercentage() {
	return companypercentage;
}
public void setCompanypercentage(String companypercentage) {
	this.companypercentage = companypercentage;
}
public String getBrandpercentage() {
	return brandpercentage;
}
public void setBrandpercentage(String brandpercentage) {
	this.brandpercentage = brandpercentage;
}
public String getTest_invoice() {
	return test_invoice;
}
public void setTest_invoice(String test_invoice) {
	this.test_invoice = test_invoice;
}
public String getBase64Logo() {
	return base64Logo;
}
public void setBase64Logo(String base64Logo) {
	this.base64Logo = base64Logo;
}
public String getBrand_theme() {
	return brand_theme;
}
public void setBrand_theme(String brand_theme) {
	this.brand_theme = brand_theme;
}
public String getPayer_code() {
	return payer_code;
}
public void setPayer_code(String payer_code) {
	this.payer_code = payer_code;
}
public String getBiller_code() {
	return biller_code;
}
public void setBiller_code(String biller_code) {
	this.biller_code = biller_code;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getBiller_type() {
	return biller_type;
}
public void setBiller_type(String biller_type) {
	this.biller_type = biller_type;
}
public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getComp_code() {
	return comp_code;
}
public void setComp_code(String comp_code) {
	this.comp_code = comp_code;
}
public String getBiller_location() {
	return biller_location;
}
public void setBiller_location(String biller_location) {
	this.biller_location = biller_location;
}
public String getBiller_phone() {
	return biller_phone;
}
public void setBiller_phone(String biller_phone) {
	this.biller_phone = biller_phone;
}
public String getBiller_month() {
	return biller_month;
}
public void setBiller_month(String biller_month) {
	this.biller_month = biller_month;
}
public String getBranch() {
	return branch;
}
public void setBranch(String branch) {
	this.branch = branch;
}
public String getBusiness_description() {
	return business_description;
}
public void setBusiness_description(String business_description) {
	this.business_description = business_description;
}
public String getCompany_name() {
	return company_name;
}
public void setCompany_name(String company_name) {
	this.company_name = company_name;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getEmployee_no() {
	return employee_no;
}
public void setEmployee_no(String employee_no) {
	this.employee_no = employee_no;
}
public String getPersonel_f_name() {
	return personel_f_name;
}
public void setPersonel_f_name(String personel_f_name) {
	this.personel_f_name = personel_f_name;
}
public String getPersonel_l_name() {
	return personel_l_name;
}
public void setPersonel_l_name(String personel_l_name) {
	this.personel_l_name = personel_l_name;
}
public String getSector() {
	return sector;
}
public void setSector(String sector) {
	this.sector = sector;
}
public String getStb_acc_name() {
	return stb_acc_name;
}
public void setStb_acc_name(String stb_acc_name) {
	this.stb_acc_name = stb_acc_name;
}
public String getStb_acc_no() {
	return stb_acc_no;
}
public void setStb_acc_no(String stb_acc_no) {
	this.stb_acc_no = stb_acc_no;
}
public String getYear() {
	return year;
}
public void setYear(String year) {
	this.year = year;
}
public String getUser_type() {
	return user_type;
}
public void setUser_type(String user_type) {
	this.user_type = user_type;
}



}
