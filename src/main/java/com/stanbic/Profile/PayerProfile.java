package com.stanbic.Profile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;
import java.sql.Connection;

import com.stanbic.Logo.InvoiceLogo;
import com.stanbic.Responses.Response;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class PayerProfile {
	Connection conn=null;
	
	 @RequestMapping(value = "/v1/getPayerProfile", method = RequestMethod.POST)
	public ResponseEntity<?>getPayerProfile(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata){
		 List<PayerProfileProduceJson> model= new ArrayList<PayerProfileProduceJson>();
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
		 String email=TokenManager.tokenIssuedId(token);
		 String group_id="";
	     String path=InvoiceLogo.getLogo(comp_code);
		 String base64=null;
		 String forex_percentage=ProfileHelper.getForeStatus(comp_code);
		 String commission_percentage=ProfileHelper.getCommissionStatus(comp_code);
		 String forex_value=ProfileHelper.isForexDateExpire(comp_code);
		 String file_setting_percentage=ProfileHelper.file_setting(comp_code);
	     if(path==null) {
	    	 base64=null;
	     }
	     else {
	    	 File file=new File(path);
	    	 base64=FileManager.encodeFileToBase64Binary(file);
	     }
		 try {
			 String sql="SELECT id,paybill,comp_code,biller_location,biller_phone,business_description,company_name,email,employee_no,\r\n" + 
			 		"STATUS,sector,stb_acc_no,YEAR,user_type,biller_type,prefix,alias,test_invoice,biller_month,stb_acc_name,\r\n" + 
			 		"(SELECT invoice_color FROM interface_settings B WHERE B.comp_code=A.comp_code)invoice_color,\r\n" + 
			 		"(SELECT personel_f_name FROM ebiller_auth C WHERE email=?)personel_f_name,\r\n" + 
			 		"(SELECT personel_l_name FROM ebiller_auth D WHERE email=?)personel_l_name,\r\n" + 
			 		"(SELECT group_id FROM ebiller_auth E WHERE email=?)group_id \r\n"+
			 		"FROM biller_profile A WHERE comp_code=?";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, email);
			 prep.setObject(2, email);
			 prep.setObject(3, email);
			 prep.setObject(4, comp_code);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  PayerProfileProduceJson data=new PayerProfileProduceJson();
				  String brandpercentage="0%";
				  String companypercentage="0%";
				  if(rs.getString("invoice_color")==null || base64==null) {
					  brandpercentage="50%";
				  }else {
					  brandpercentage="100%";
				  }
				  
				  if(rs.getString("personel_f_name")==null || rs.getString("biller_location")==null) {
					  companypercentage="50%";
				  }
				  else {
					  companypercentage="100%";
				  }
				  group_id=rs.getString("group_id");
				  data.id=rs.getString("id");
				  data.biller_location=rs.getString("biller_location");
				  data.biller_month=rs.getString("biller_month");
				  data.biller_phone=rs.getString("biller_phone");
				  data.business_description=rs.getString("business_description");
				  data.comp_code=rs.getString("comp_code");
				  data.company_name=rs.getString("company_name");
				  data.email=rs.getString("email");
				  data.employee_no=rs.getString("employee_no");
				  data.personel_f_name=rs.getString("personel_f_name");
				  data.personel_l_name=rs.getString("personel_l_name");
				  data.sector=rs.getString("sector"); 
				  data.stb_acc_name=rs.getString("stb_acc_name");
				  data.stb_acc_no=rs.getString("stb_acc_no");
				  data.user_type=rs.getString("user_type");
				  data.status=rs.getString("status");
				  data.biller_type=rs.getString("biller_type");
				  data.test_invoice=rs.getString("test_invoice");
				  data.brand_theme=rs.getString("invoice_color");
				  data.brandpercentage=brandpercentage;
				  data.companypercentage=companypercentage;
				  data.base64Logo=base64;
				  data.alias=rs.getString("alias");
				  data.forex_percentage=forex_percentage;
				  data.forex_value=forex_value;
				  data.commission_percentage=commission_percentage;
				  data.file_setting_percentage=file_setting_percentage;
				  data.paybill=rs.getString("paybill");
				  model.add(data);
				}
				conn.close();
				 return ResponseEntity.ok(model);
		 }catch(Exception n) {
			 System.out.println(n.getMessage());
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
	}
	 
	 @RequestMapping(value = "/v1/getInvoiceSettings", method = RequestMethod.POST)
		public ResponseEntity<?>getInvoiceSettings(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceSettingProduceJson jsondata){
			 List<InvoiceSettingProduceJson> model= new ArrayList<InvoiceSettingProduceJson>();
			 String comp_code=TokenManager.tokenIssuedCompCode(token);
			
			 try {  
				 String sql="SELECT * FROM interface_settings WHERE comp_code=?";
				 conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, comp_code); 
				  ResultSet rs=prep.executeQuery();
				  while(rs.next()) {
					  InvoiceSettingProduceJson data=new InvoiceSettingProduceJson();
					  data.comp_code=rs.getString("comp_code");
					  data.invoice_color=rs.getString("invoice_color");
					  data.invoice_name=rs.getString("invoice_name");
					  data.terms=rs.getString("terms");
					  data.tax=rs.getString("tax");
					  data.commission=rs.getString("commission");
					  model.add(data);
					}
					conn.close();
					 return ResponseEntity.ok(model);
			 }catch(Exception n) {
				 System.out.println(n.getMessage());
				 return ResponseEntity.ok(new AuthProduceJson("04","System technical error","",""));
			 }
			 
		}
	 @RequestMapping(value = "/v1/updateInvoice", method = RequestMethod.POST)
		public ResponseEntity<?>updateInvoice(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceSettingProduceJson jsondata){
		 if(jsondata.getInvoice_color().isEmpty() || jsondata.getTax().isEmpty() || jsondata.getCommission().isEmpty() || jsondata.getTerms().isEmpty()) {
			 return ResponseEntity.ok(new Response("06", "Please provide all fields")); 
		 }
			 String comp_code=TokenManager.tokenIssuedCompCode(token);
			 
			 try {
				 String sql="UPDATE interface_settings SET invoice_color=?,terms=?,tax=?,commission=? WHERE comp_code=?";
				   conn = DbManager.getConnection();
					PreparedStatement prep = conn.prepareStatement(sql);
					prep.setObject(1, jsondata.getInvoice_color());
					prep.setObject(2, jsondata.getTerms());
					prep.setObject(3, jsondata.getTax());
					prep.setObject(4, jsondata.getCommission());
					prep.setObject(5, comp_code);
					prep.executeUpdate();
					conn.close();
					return ResponseEntity.ok(new Response("00", "Invoice settings updated successfully"));
			 }catch(Exception n) {
				 System.out.println(n.getMessage());
				 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
			 }
			 
		}
	 
	 @RequestMapping(value = "/v1/updateInvoiceSettings", method = RequestMethod.POST)
		public ResponseEntity<?>updateInvoiceSettings(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceSettingProduceJson jsondata){
		 if(jsondata.getInvoice_color().isEmpty() || jsondata.getInvoice_name().isEmpty()) {
			 
		 }
			 String comp_code=TokenManager.tokenIssuedCompCode(token);
			 try {
				 String sql="UPDATE interface_settings SET invoice_color=?,invoice_name=? WHERE comp_code=?";
				   conn = DbManager.getConnection();
					PreparedStatement prep = conn.prepareStatement(sql);
					prep.setObject(1, jsondata.getInvoice_color());
					prep.setObject(2, jsondata.getInvoice_name());
					prep.setObject(3, comp_code);
					prep.executeUpdate();
					conn.close();
					return ResponseEntity.ok(new Response("00", "Invoice settings updated successfully"));
			 }catch(Exception n) {
				 System.out.println(n.getMessage());
				 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
			 }
			 
		}
	 
	 @RequestMapping(value = "/v1/updateInvoiceTerms", method = RequestMethod.POST)
		public ResponseEntity<?>updateInvoiceTerms(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceSettingProduceJson jsondata){
		 if(jsondata.getTerms().isEmpty()) {

		 }
			 String comp_code=TokenManager.tokenIssuedCompCode(token);
			 if(comp_code.equalsIgnoreCase("03")){
				 return ResponseEntity.ok(new AuthProduceJson("03","Page has expired","",""));
			}
		     if(comp_code.equalsIgnoreCase("01")) {
				 return ResponseEntity.ok(new AuthProduceJson("01","An error occured,page might expired","",""));
			}
			 
			 try {
				 String sql="UPDATE interface_settings SET terms=? WHERE comp_code=?";
				   conn = DbManager.getConnection();
					PreparedStatement prep = conn.prepareStatement(sql);
					prep.setObject(1, jsondata.getTerms());
					prep.setObject(2, comp_code);
					prep.executeUpdate();
					conn.close();
					return ResponseEntity.ok(new Response("00", "Invoice settings updated successfully"));
			 }catch(Exception n) {
				 n.printStackTrace();
				 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
			 }
			 
		}
	 
	 
	 @RequestMapping(value = "/v1/updatePayerProfile", method = RequestMethod.POST)
		public ResponseEntity<?>updatePayerProfile(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata){
		 if(jsondata.getCompany_name().isEmpty() || jsondata.getPersonel_f_name().isEmpty() || jsondata.getPersonel_l_name().isEmpty()
				 || jsondata.getEmail().isEmpty()  || jsondata.getBiller_phone().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please enter all fields.","",""));
		 }
			 String comp_code=TokenManager.tokenIssuedCompCode(token);
			 String email=TokenManager.tokenIssuedId(token);
			 if(jsondata.getBiller_phone().substring(1).length()!=12) {
				 return ResponseEntity.ok(new Response("06","Phone number must be of this format= 254712****28"));
			 }
			 
			 try {
				 String sql="UPDATE biller_profile SET personel_f_name=?,personel_l_name=?,email=?,company_name=?,biller_phone=?,paybill=?  WHERE comp_code=?";
				   conn = DbManager.getConnection();
					PreparedStatement prep = conn.prepareStatement(sql);
					prep.setObject(1, jsondata.getPersonel_f_name());
					prep.setObject(2, jsondata.getPersonel_l_name());
					prep.setObject(3,jsondata.getEmail());
					prep.setObject(4, jsondata.getCompany_name());
					prep.setObject(5, jsondata.getBiller_phone().substring(1));
					prep.setObject(6, jsondata.getPaybill());
					prep.setObject(7, comp_code);
					prep.executeUpdate();
					conn.close();
			 }catch(Exception n) {
				 n.printStackTrace();
				 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
			 }

	 try {
		 String sql="UPDATE ebiller_auth SET email=?,personel_f_name=?,personel_l_name=?,phone=? WHERE email=?";
		   conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEmail());
			prep.setObject(2, jsondata.getPersonel_f_name());
			prep.setObject(3, jsondata.getPersonel_l_name());
			prep.setObject(4, jsondata.getBiller_phone().substring(1));
			prep.setObject(5, email);
			prep.executeUpdate();
			conn.close();
			 return ResponseEntity.ok(new AuthProduceJson("00","Payer profile updated successfully","",""));
	 }catch(Exception e) {
		 System.out.println(e.getMessage());
		 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
	 }
	 
	 }
	 
	 
	 @RequestMapping(value = "/v1/updateNotificationSettings", method = RequestMethod.POST)
		public ResponseEntity<?>updateNotificationSettings(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata){
		 if(jsondata.getNotif_value().isEmpty() || jsondata.getBiller_code().isEmpty()) {
			 return ResponseEntity.ok(new Response("06","Please provide biller code and a value"));
		 }
		 String payer_code=TokenManager.tokenIssuedCompCode(token);
		 try {
			 String sql="UPDATE billers SET email_notif=? WHERE payer_code=? AND biller_code=?";
			  conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, jsondata.getNotif_value());
				prep.setObject(2, payer_code);
				prep.setObject(3, jsondata.getBiller_code());
				prep.execute();
				conn.close();
				 return ResponseEntity.ok(new Response("00","Email notification settings changed successfully"));
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new Response("02","System technical error"));
		 }
		 
	 }

}
