package com.stanbic.Invoice;

public class InvoiceEslipBillsProduceJson {
	String amount_due="";
	String amount_to_pay="";
	String due_date="";
	String status="";
	String eslip_no="";
	String invoice="";
	String account_name="";
	public String getAmount_due() {
		return amount_due;
	}
	public void setAmount_due(String amount_due) {
		this.amount_due = amount_due;
	}
	public String getAmount_to_pay() {
		return amount_to_pay;
	}
	public void setAmount_to_pay(String amount_to_pay) {
		this.amount_to_pay = amount_to_pay;
	}
	public String getDue_date() {
		return due_date;
	}
	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEslip_no() {
		return eslip_no;
	}
	public void setEslip_no(String eslip_no) {
		this.eslip_no = eslip_no;
	}
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public String getAccount_name() {
		return account_name;
	}
	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}
	
	
}
