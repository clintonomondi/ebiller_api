package com.stanbic.Invoice;

public class InvoiceBillsModel {
String file_id="";
String invoice="";
String service="";
String amount="";
String commission="";
String tax="";
String status="";
String biller_code="";
String payer_code="";
String due_date="";
String payer_name="";
String biller_name="";
String email="";


public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getBiller_name() {
	return biller_name;
}
public void setBiller_name(String biller_name) {
	this.biller_name = biller_name;
}
public String getFile_id() {
	return file_id;
}
public void setFile_id(String file_id) {
	this.file_id = file_id;
}
public String getInvoice() {
	return invoice;
}
public void setInvoice(String invoice) {
	this.invoice = invoice;
}
public String getService() {
	return service;
}
public void setService(String service) {
	this.service = service;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}
public String getCommission() {
	return commission;
}
public void setCommission(String commission) {
	this.commission = commission;
}
public String getTax() {
	return tax;
}
public void setTax(String tax) {
	this.tax = tax;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getBiller_code() {
	return biller_code;
}
public void setBiller_code(String biller_code) {
	this.biller_code = biller_code;
}
public String getPayer_code() {
	return payer_code;
}
public void setPayer_code(String payer_code) {
	this.payer_code = payer_code;
}
public String getDue_date() {
	return due_date;
}
public void setDue_date(String due_date) {
	this.due_date = due_date;
}
public String getPayer_name() {
	return payer_name;
}
public void setPayer_name(String payer_name) {
	this.payer_name = payer_name;
}


}
