package com.stanbic.Invoice;

public class InvoiceModel {
	String base64Excel="";
	String file_name="";
	String file_id="";
	String biller_code="";
	String successrecords="";
	String failedrecords="";
	String created_at="";
	String created_by="";
	String email="";
	String payer_code="";
	
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPayer_code() {
		return payer_code;
	}

	public void setPayer_code(String payer_code) {
		this.payer_code = payer_code;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getFile_id() {
		return file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}

	public String getBiller_code() {
		return biller_code;
	}

	public void setBiller_code(String biller_code) {
		this.biller_code = biller_code;
	}

	public String getSuccessrecords() {
		return successrecords;
	}

	public void setSuccessrecords(String successrecords) {
		this.successrecords = successrecords;
	}

	public String getFailedrecords() {
		return failedrecords;
	}

	public void setFailedrecords(String failedrecords) {
		this.failedrecords = failedrecords;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getBase64Excel() {
		return base64Excel;
	}

	public void setBase64Excel(String base64Excel) {
		this.base64Excel = base64Excel;
	}
	
}
