package com.stanbic.Invoice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Eslip.EslipConsumeJson;
import com.stanbic.Eslip.MyESlipsProduceJSon;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;
@SpringBootApplication
@RequestMapping("/api")
public class InvoiceEslipReport {
	Connection conn = null;
	
	
	@RequestMapping(value = "/v1/invoiceEslipReconcileReportBiller", method = RequestMethod.POST)
    public ResponseEntity<?> invoiceEslipReconcileReportBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata){
		if(jsondata.getDatefrom().isEmpty() || jsondata.getTodate().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide all fields","",""));
		}
		String biller_code=TokenManager.tokenIssuedCompCode(token);
		List<MyInvoiceESlipsProduceJSon> model = new ArrayList<MyInvoiceESlipsProduceJSon>();
		try {
			String sql="SELECT *,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name,\r\n" + 
					"(SELECT company_name FROM biller_profile C WHERE C.comp_code=A.payer_code)payer_name,\r\n" + 
					"(SELECT approved_by FROM invoice_eslip D WHERE D.eslip_no=A.eslip_no)approved_by,\r\n" + 
					"(SELECT created_by FROM invoice_eslip E WHERE E.eslip_no=A.eslip_no)created_by,\r\n" + 
					"(SELECT bank_ref_no FROM invoice_eslip F WHERE F.eslip_no=A.eslip_no)bank_ref_no,\r\n" + 
					"(SELECT amount_to_pay FROM invoice_eslip G WHERE G.eslip_no=A.eslip_no)eslip_amount,\r\n" + 
					"(SELECT COUNT(*)vcount FROM invoice_eslip_bills H WHERE H.eslip_no=A.eslip_no)accounts\r\n" + 
					" FROM invoice_eslip_bills A WHERE biller_code=? AND created_at  BETWEEN ? AND ?";
			  conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, biller_code);
				 prep.setObject(2, jsondata.getDatefrom());
				 prep.setObject(3, jsondata.getTodate());
				  ResultSet rs=prep.executeQuery();
				  while(rs.next()) {
					  MyInvoiceESlipsProduceJSon data = new MyInvoiceESlipsProduceJSon();
						data.id=rs.getString("id");
						data.eslip_no=rs.getString("eslip_no");
						data.amount_due = rs.getString("amount_due");
						data.amount_to_pay = rs.getString("amount_to_pay");
						data.expiry_date=rs.getString("due_date");
						data.payer_name=rs.getString("payer_name");
						data.biller_name=rs.getString("biller_name");
						data.status=rs.getString("status");
						data.account_no=rs.getString("invoice");
						data.biller_code=rs.getString("biller_code");
						data.payer_code=rs.getString("payer_code");
						data.approved_by=rs.getString("approved_by");
						data.created_at=rs.getString("created_at");
						data.created_by=rs.getString("created_by");
						data.bank_ref_no=rs.getString("bank_ref_no");
						data.payment_date=rs.getString("payment_date");
						data.biller_payment_ref=rs.getString("biller_payment_ref");
						data.accounts=rs.getString("accounts");
						data.eslip_amount=rs.getString("eslip_amount");
						data.account_name=rs.getString("account_name");
						model.add(data);
				  }
				  conn.close();
				  return ResponseEntity.ok(model);	
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	
	}
}
