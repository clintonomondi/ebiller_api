package com.stanbic.Invoice;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Eslip.EslipAccInfoRespModel;
import com.stanbic.Eslip.EslipAccountsReq;
import com.stanbic.Eslip.EslipBillsProduceJson;
import com.stanbic.Eslip.EslipConsumeJson;
import com.stanbic.Eslip.EslipResponse;
import com.stanbic.Eslip.EslipService;
import com.stanbic.Eslip.MyESlipsProduceJSon;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class InvoiceEslip {
	 Connection conn = null;
	  String username="";
	
	@RequestMapping(value = "/v1/generateInvoiceeslip", method = RequestMethod.POST)
	  public ResponseEntity<?> generateInvoiceeslip(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
			Vector vector = new Vector();
			try {
				// extract e slip from
				JSONObject obj = new JSONObject(jsondata);
				System.out.println(obj);
				String biller_code = obj.getString("biller_code");
				String total_amount_due = obj.getString("total_amount_due");
				String total_amount_to_pay = obj.getString("total_amount_to_pay");
				String account_no="";
				String amount_to_pay = "";
				String amount_due = "";
				String eslip_no = "";
				String due_date = "";
				String account_name="";
				String payer_code = TokenManager.tokenIssuedCompCode(token);
				String user_email=TokenManager.tokenIssuedId( token);
				

				// generate e slip number
				eslip_no =EslipService.eslipNoGen(biller_code);
				due_date=EslipService.expiryDate();
				JSONArray slipInfoArray = obj.getJSONArray("eslipInfo");
				int count=1;
//				boolean flag=true;
				double amt=0.0;
				for (int i = 0; i < slipInfoArray.length(); i++) {
					JSONObject object = slipInfoArray.getJSONObject(i);
					account_no = object.getString("account_no");
					amount_to_pay = object.getString("amount_to_pay");
					amount_due = object.getString("amount_due");
					account_name=object.getString("account_name");
					
					amt=Double.parseDouble(amount_to_pay);
					if(amt<=0.0) {
						 return ResponseEntity.ok(new EslipResponse("07","Some account numbers have zero amount to pay",""));
					}
					Vector holder = new Vector();
					holder.addElement(eslip_no);
					holder.addElement(amount_due);
					holder.addElement(amount_to_pay);
					holder.addElement(due_date);
					holder.addElement(account_no);
					holder.addElement(count);
					holder.addElement(account_name);
					vector.addElement(holder);
					count++;
				}
				String sql = "INSERT INTO invoice_eslip_bills (eslip_no,amount_due,amount_to_pay,due_date,invoice,count,account_name,biller_code,payer_code)VALUES(?,?,?,?,?,?,?,?,?)";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				conn.setAutoCommit(false);
				int ii=0;
				for (int i = 0; i < vector.size(); i++) {
					Vector data = (Vector) vector.elementAt(i);
					prep.setObject(1, data.toArray()[0].toString()); 
					prep.setObject(2, data.toArray()[1].toString());
					prep.setObject(3, data.toArray()[2].toString());
					prep.setObject(4, data.toArray()[3].toString());
					prep.setObject(5, data.toArray()[4].toString());
					prep.setObject(6, data.toArray()[5].toString());
					prep.setObject(7, data.toArray()[6].toString());
					prep.setObject(8, biller_code);
					prep.setObject(9, payer_code);
					prep.addBatch();
					ii++;
					if (ii % 1000 == 0 || ii == vector.size()) {
						System.out.println(">Creating eslip accounts at initial stage="+ii);
						prep.executeBatch(); // Execute every 1000 items
						conn.commit();
			            prep.clearBatch();
					}
				}
				conn.setAutoCommit(true);
				conn.close();
				vector.clear();
				insertEslip(eslip_no , due_date, total_amount_due,total_amount_to_pay,biller_code, payer_code,"",user_email);
				 return ResponseEntity.ok(new EslipResponse("00","Eslip generated successfully",eslip_no));	
			}catch(Exception e) {
				System.out.println(e.getMessage());
				return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
			}
		 
	}
	
	
	
	@RequestMapping(value = "/v1/getMyInvoiceEslips", method = RequestMethod.POST)
	public ResponseEntity<?> getMyInvoiceEslips(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipConsumeJson jsondata) {
		if(jsondata.getBiller_code().isEmpty() || jsondata.getBiller_code().equalsIgnoreCase("")) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide biller_code","",""));
		}
		List<MyInvoiceESlipsProduceJSon> model = new ArrayList<MyInvoiceESlipsProduceJSon>();
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
	
		try {
			conn = DbManager.getConnection();
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay"
					+ ",approved_by,created_at,status,created_by,bank_ref_no,(SELECT COUNT(*) AS V_COUNT FROM invoice_eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name AS client_name FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `invoice_eslip` b WHERE payer_code=? AND biller_code=?  GROUP BY id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay,"
					+ "approved_by,created_at,status,created_by,bank_ref_no ORDER BY id DESC"; 
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			prep.setObject(2, jsondata.getBiller_code());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				MyInvoiceESlipsProduceJSon data = new MyInvoiceESlipsProduceJSon();
				data.id=rs.getString("id");
				data.eslip_no=rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.expiry_date=rs.getString("expiry_date");
				data.clients_name=rs.getString("client_name");
				data.amount_to_pay=rs.getString("amount_to_pay");
				data.status=rs.getString("status");
				data.account_no=rs.getString("account_no");
				data.accounts=rs.getString("accounts");
				data.biller_code=rs.getString("biller_code");
				data.payer_code=rs.getString("payer_code");
				data.approved_by=rs.getString("approved_by");
				data.created_at=rs.getString("created_at");
				data.created_by=rs.getString("created_by");
				data.bank_ref_no=rs.getString("bank_ref_no");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
//			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	@RequestMapping(value = "/v1/getInvoiceEslipBills", method = RequestMethod.POST)
	public ResponseEntity<?> getEslipBills(@RequestBody EslipAccountsReq jsondata) {
		if(jsondata.getEslip_no().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Provide eslip_no", "", ""));
		}
		List<InvoiceEslipBillsProduceJson> model = new ArrayList<InvoiceEslipBillsProduceJson>();
		try {
			String sql = "SELECT * FROM `invoice_eslip_bills` WHERE eslip_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEslip_no());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				InvoiceEslipBillsProduceJson data = new InvoiceEslipBillsProduceJson();
				data.amount_due = rs.getString("amount_due");
				data.amount_to_pay = rs.getString("amount_to_pay");
				data.eslip_no = rs.getString("eslip_no");
				data.status = rs.getString("status");
				data.due_date=rs.getString("due_date");
				data.invoice=rs.getString("invoice");
				data.account_name=rs.getString("account_name");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	
	@RequestMapping(value = "/v1/getInvoiceEslipInfo", method = RequestMethod.POST)
	public ResponseEntity<?> getEslipInfo(@RequestBody EslipConsumeJson jsondata) {
		if(jsondata.getEslip_no().isEmpty() || jsondata.getEslip_no().equalsIgnoreCase("")) {
			return ResponseEntity.ok(new AuthProduceJson("02", "Please provide eslip number", "", ""));
		}
		String amount_to_pay="";
		String eslip_no="";
		String status="";
		String due_date="";
		String account_no="";
		String amount_due="";
		String client_name="";
		String expiry_date="";
		String eslip_date="";
		String payer_code="";
		String biller_code="";
		String created_by="";
		 String base64="";
		 String company_website="55";
		 String company_customer_care="55";
		 String email="";
		 String biller_phone="";
		
		try {
			String sql="SELECT id,eslip_no,amount_due,expiry_date,payer_code,biller_code, amount_to_pay"
					+ ",approved_by,created_at,status,created_by,(SELECT COUNT(*) AS V_COUNT FROM invoice_eslip_bills A WHERE A.eslip_no=b.eslip_no) accounts ,\n"
					+ "(SELECT company_name  FROM biller_profile C WHERE C.comp_code=b.biller_code) client_name, \n"
					+ "(SELECT email  FROM biller_profile Z WHERE Z.comp_code=b.biller_code) email, \n"
					+ "(SELECT if(company_website is null,'',company_website)  FROM biller_profile S WHERE S.comp_code=b.biller_code) company_website, \n"
					+ "(SELECT if(company_customer_care is null,'',company_customer_care)  FROM biller_profile L WHERE L.comp_code=b.biller_code) company_customer_care, \n"
					+ "(SELECT if(biller_phone is null,'',biller_phone)  FROM biller_profile Q WHERE Q.comp_code=b.biller_code) biller_phone, \n"
					+ "(SELECT logo_url  FROM interface_settings T WHERE T.comp_code=b.biller_code) logo_url, \n"
					+ "(SELECT stb_acc_no AS account_no FROM biller_profile C WHERE C.comp_code=b.biller_code) account_no \n" + 
					"FROM `invoice_eslip` b WHERE eslip_no=? ORDER BY id DESC"; 
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEslip_no());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				 if(rs.getString("logo_url")==null) { 
					 base64="no image";
				 }else {
					 File file=new File(rs.getString("logo_url"));
		    	 base64=FileManager.encodeFileToBase64Binary(file); 
				 }
				
				amount_due = rs.getString("amount_due");
				amount_to_pay = rs.getString("amount_to_pay");
				eslip_no = rs.getString("eslip_no");
				client_name = rs.getString("client_name");
				expiry_date = rs.getString("expiry_date");
				account_no = rs.getString("account_no");
				eslip_date = rs.getString("created_at");
				status = rs.getString("status");
				payer_code = rs.getString("payer_code");
				biller_code = rs.getString("biller_code");
				created_by=rs.getString("created_by");
				company_customer_care=rs.getString("company_customer_care");
				company_website=rs.getString("company_website");
				email=rs.getString("email");
				biller_phone=rs.getString("biller_phone");
				
			}
			conn.close();
			Hashtable<String, Object> map=new Hashtable<String,Object>();
			map.put("amount_due", amount_due);
			map.put("amount_to_pay", amount_to_pay);
			map.put("eslip_no", eslip_no);
			map.put("status", status);
			map.put("due_date", due_date);
			map.put("account_no", account_no);
			map.put("client_name", client_name);
			map.put("expiry_date", expiry_date);
			map.put("eslip_date", eslip_date);
			map.put("payer_code", payer_code);
			map.put("biller_code", biller_code);
			map.put("created_by",created_by);
			map.put("accountDetails",  accountsForEslip(jsondata.getEslip_no()));
			map.put("billerlogo", base64);
			map.put("company_customer_care",company_customer_care);
			map.put("company_website",company_website);
			map.put("email",email);
			map.put("biller_phone", biller_phone);
			return ResponseEntity.ok(map);
		} catch (Exception e) {
//			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	 
	@RequestMapping(value = "/v1/InvoiceEslipAccounts", method = RequestMethod.POST)
	public ResponseEntity<?> getEslipAccount(@RequestBody EslipAccountsReq jsondata) {
		String eslipNo=jsondata.getEslip_no();
		if(jsondata.getEslip_no().isEmpty() || jsondata.getEslip_no().equals("")) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide eslip no", "", ""));
		}
		List<InvoiceEslipBillsProduceJson> model = new ArrayList<InvoiceEslipBillsProduceJson>();
		try {
			String sql = "SELECT eslip_no,invoice,amount_due,amount_to_pay,STATUS,due_date,account_name FROM `invoice_eslip_bills` WHERE eslip_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, eslipNo);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				InvoiceEslipBillsProduceJson data = new InvoiceEslipBillsProduceJson();
				data.amount_due = rs.getString("amount_due");
				data.amount_to_pay = rs.getString("amount_to_pay");
				data.eslip_no = rs.getString("eslip_no");
				data.status = rs.getString("status");
				data.due_date=rs.getString("due_date");
				data.invoice=rs.getString("invoice");
				data.account_name=rs.getString("account_name");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	
public List<InvoiceEslipAccInfoRespModel> accountsForEslip(String eslipNo) {
		
		List<InvoiceEslipAccInfoRespModel> model=new ArrayList<InvoiceEslipAccInfoRespModel>();
		try {
			String sql="SELECT invoice,AMOUNT_TO_PAY,account_name,biller_payment_ref,count FROM invoice_eslip_bills where eslip_no=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, eslipNo);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				InvoiceEslipAccInfoRespModel c=new InvoiceEslipAccInfoRespModel();
				c.accountAmount=rs.getString("AMOUNT_TO_PAY");
				c.accountNo=rs.getString("invoice");
				c.account_name=rs.getString("account_name");
				c.biller_payment_ref=rs.getString("biller_payment_ref");
				c.count=rs.getString("count");
				model.add(c);
			}
			conn.close();
		}catch(Exception e) {
//			e.printStackTrace();
		}
		return model;
	}
	 
	 public  void insertEslip(String eslip_no, String due_date, String amount_due,String amount_to_pay,
				String biller_code, String payer_code,String file_id,String user_email) {
			username=AccountHelper.getName(user_email);
			String date=TimeManager.getEmailTime();
			try {
				String sql = "INSERT INTO invoice_eslip (eslip_no,expiry_date,amount_due,amount_to_pay,biller_code,payer_code,file_id,created_by)VALUES(?,?,?,?,?,?,?,?)";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, eslip_no);
				prep.setObject(2, due_date);
				prep.setObject(3, amount_due);
				prep.setObject(4, amount_to_pay);
				prep.setObject(5, biller_code);
				prep.setObject(6, payer_code);
				prep.setObject(7, file_id);
				prep.setObject(8, user_email);
				prep.execute();
				conn.close();
				Thread t = new Thread() {
					public void run() {
						String subject="Stanbic E-biller";
						  VelocityContext vc = new VelocityContext();
				          vc.put("e_slip_no", eslip_no);
				          vc.put("user_name", username);
				          vc.put("amount", amount_to_pay);
				          vc.put("date", date);
				          vc.put("time",AccountHelper.getCurrentTime());
				          String email_template = EmailManager.email_message_template(vc,"eslipGenerated.vm");
							EmailManager.send_mail(user_email,email_template,subject,username,"eslip generated="+eslip_no);
					}
				};
				t.start(); 
			} catch (Exception n) {
				System.out.println(n.getMessage());
			}
		}
}
