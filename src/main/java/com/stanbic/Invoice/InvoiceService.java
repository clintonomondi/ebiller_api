package com.stanbic.Invoice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Eslip.MyESlipsProduceJSon;
import com.stanbic.Forex.ForexHelper;
import com.stanbic.KPLC.QueryBulkBillsService;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;
import com.stanbic.ebiller.auth.BillerAuthconsumeJson;
import com.stanbic.ebiller.auth.LoginHelper;

@SpringBootApplication
@RequestMapping("/api")
public class InvoiceService {
	 Connection conn = null;
	 
	
	
	@RequestMapping(value = "/v1/uploadInvoice", method = RequestMethod.POST)
	  public ResponseEntity<?> uploadInvoice(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceModel jsondata) {
		 if(jsondata.getBase64Excel().isEmpty() || jsondata.getFile_name().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide excel and file name","",""));
		 }
		 String biller_code = TokenManager.tokenIssuedCompCode(token);
		 if(ForexHelper.checkFileSettings(biller_code) !=1) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please make file settings before upload of invoices","",""));
		 }
		 if(ForexHelper.checkAll(biller_code) !=1) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please make commission for all settings before upload of invoices","",""));
		 }
		 
		 try {
		 String email = TokenManager.tokenIssuedId(token);
		 String name=AccountHelper.getName(email);
		 
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			LocalDateTime now = LocalDateTime.now();
			String time = dtf.format(now); 
			String filename = time.replace("/", "_") + biller_code;
			String path = ExternalFile.getExcelPath()+AccountHelper.generateRandomInt(4) + filename + ".xlsx";
			FileManager.saveImage(jsondata.getBase64Excel(), path);
			String file_id = AccountHelper.generateRandomInt(99999) + time;
			
			
			 Vector invoice=QueryBulkBillsService.read(path);
			 Thread t = new Thread() {
					public void run() {
						InvoiceHelper.processInvoice(invoice,biller_code,name,file_id,path,jsondata.getFile_name());
					}
				};
				t.start(); 
				return ResponseEntity.ok(new AuthProduceJson("00", "Invoice  uploaded successfully", "", ""));
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		 }
		 
	 }
	
	@RequestMapping(value = "/v1/uploadInvoiceIndividual", method = RequestMethod.POST)
	  public ResponseEntity<?> uploadInvoiceIndividual(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceModel jsondata) {
		 if(jsondata.getBase64Excel().isEmpty() || jsondata.getFile_name().isEmpty() || jsondata.getEmail().isEmpty()  || jsondata.getPayer_code().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provideall fields","","")); 
		 }
		 String biller_code = TokenManager.tokenIssuedCompCode(token);
		 if(ForexHelper.checkFileSettings(biller_code) !=1) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please make file settings before upload of invoices","",""));
		 }
		 if(ForexHelper.checkAll(biller_code) !=1) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please make commission for all settings before upload of invoices","",""));
		 }
		 try {
		 String email = TokenManager.tokenIssuedId(token);
		 String name=AccountHelper.getName(email);
		 
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			LocalDateTime now = LocalDateTime.now();
			String time = dtf.format(now); 
			String filename = time.replace("/", "_") + biller_code;
			String path = ExternalFile.getExcelPath()+AccountHelper.generateRandomInt(4) + filename + ".xlsx";
			FileManager.saveImage(jsondata.getBase64Excel(), path);
			String file_id = AccountHelper.generateRandomInt(99999) + time;
			
			 Vector invoice=QueryBulkBillsService.read(path);
			 Thread t = new Thread() {
					public void run() {
						InvoiceHelper.processInvoiceIndividual(invoice,biller_code,name,file_id,path,jsondata.getFile_name(),jsondata.getPayer_code(),jsondata.getEmail());
					}
				};
				t.start(); 
				return ResponseEntity.ok(new AuthProduceJson("00", "Invoice  uploaded successfully", "", ""));
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		 }
		 
		 
		 
		 
	}
	
	@RequestMapping(value = "/v1/getUploadedInvoiceFiles", method = RequestMethod.POST)
	  public ResponseEntity<?> getUploadedInvoiceFiles(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceModel jsondata) {
		String biller_code=TokenManager.tokenIssuedCompCode(token);
		List<InvoiceModel> model = new ArrayList<InvoiceModel>();
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT *,\r\n" + 
					"(SELECT COUNT(*) FROM invoice_bills B WHERE B.file_id=A.file_id AND STATUS='Found')successrecords,\r\n" + 
					"(SELECT COUNT(*) FROM invoice_bills C WHERE C.file_id=A.file_id AND STATUS='Not Found')failedrecords\r\n" + 
					" FROM `invoice_files` A WHERE biller_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, biller_code);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				InvoiceModel data=new InvoiceModel();
				data.biller_code=rs.getString("biller_code");
				data.failedrecords=rs.getString("failedrecords");
				data.successrecords=rs.getString("successrecords");
				data.file_id=rs.getString("file_id");
				data.file_name=rs.getString("file_name");
				data.created_at=rs.getString("created_at");
				data.created_by=rs.getString("created_by");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
			
		}catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		 
	}
	
	@RequestMapping(value = "/v1/getInvoiceFileRecords", method = RequestMethod.POST)
	  public ResponseEntity<?> getInvoiceFileRecords(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceModel jsondata) {
		if(jsondata.getFile_id().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06", "Provide file_id", "", ""));
		}
		List<InvoiceBillsModel> model = new ArrayList<InvoiceBillsModel>();
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT *,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.payer_code)payer_name\r\n" + 
					" FROM `invoice_bills` A WHERE file_id=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getFile_id());
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				InvoiceBillsModel data=new InvoiceBillsModel();
				data.biller_code=rs.getString("biller_code");
				data.status=rs.getString("status");
				data.due_date=rs.getString("due_date");
				data.file_id=rs.getString("file_id");
				data.payer_name=rs.getString("payer_name");
				data.invoice=rs.getString("invoice");
				data.tax=rs.getString("tax");
				data.commission=rs.getString("commission");
				data.amount=rs.getString("amount");
				data.service=rs.getString("service");
				data.payer_code=rs.getString("payer_code");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
			
		}catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		 
	}
	
	
	@RequestMapping(value = "/v1/getInvoices", method = RequestMethod.POST)
	  public ResponseEntity<?> getInvoices(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceModel jsondata) {
		String biller_code=TokenManager.tokenIssuedCompCode(token);
		List<InvoiceBillsModel> model = new ArrayList<InvoiceBillsModel>();
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT *,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.payer_code)payer_name\r\n" + 
					" FROM `invoice` A WHERE biller_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, biller_code);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				InvoiceBillsModel data=new InvoiceBillsModel();
				data.biller_code=rs.getString("biller_code");
				data.status=rs.getString("status");
				data.due_date=rs.getString("due_date");
				data.file_id=rs.getString("file_id");
				data.payer_name=rs.getString("payer_name");
				data.invoice=rs.getString("invoice");
				data.tax=rs.getString("tax");
				data.commission=rs.getString("commission");
				data.amount=rs.getString("amount");
				data.service=rs.getString("service");
				data.payer_code=rs.getString("payer_code");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
			
		}catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		 
	}
}
