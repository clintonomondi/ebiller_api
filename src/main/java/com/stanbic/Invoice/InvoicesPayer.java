package com.stanbic.Invoice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Eslip.EslipResponse;
import com.stanbic.Eslip.EslipService;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class InvoicesPayer {
	 Connection conn = null;
	  String username="";
	 @RequestMapping(value = "/v1/getInvoicesPayer", method = RequestMethod.POST)
	  public ResponseEntity<?> getInvoicesPayer(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceModel jsondata) {
		if(jsondata.getBiller_code().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please enter biller code", "", ""));
		}
		 String payer_code=TokenManager.tokenIssuedCompCode(token);
		List<InvoiceBillsModel> model = new ArrayList<InvoiceBillsModel>();
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT *,\r\n" + 
					"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name\r\n" + 
					" FROM `invoice` A WHERE biller_code=? AND payer_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getBiller_code());
			prep.setObject(2, payer_code);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				InvoiceBillsModel data=new InvoiceBillsModel();
				data.biller_code=rs.getString("biller_code");
				data.status=rs.getString("status");
				data.due_date=rs.getString("due_date");
				data.file_id=rs.getString("file_id");
				data.biller_name=rs.getString("biller_name");
				data.invoice=rs.getString("invoice");
				data.tax=rs.getString("tax");
				data.commission=rs.getString("commission");
				data.amount=rs.getString("amount");
				data.service=rs.getString("service");
				data.payer_code=rs.getString("payer_code");
				data.email=rs.getString("email");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
			
		}catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		 
	}
	 
	 
	 @RequestMapping(value = "/v1/disputeInvoice", method = RequestMethod.POST)
	  public ResponseEntity<?> disputeInvoice(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceBillsModel jsondata) {
		if(jsondata.getInvoice().isEmpty() || jsondata.getBiller_code().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please enter biller code and invoice", "", ""));
		}
		 String payer_code=TokenManager.tokenIssuedCompCode(token);
		
		try {
			conn = DbManager.getConnection();
			String sql = "UPDATE invoice SET status=? WHERE biller_code=? AND payer_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Disputed");
			prep.setObject(2, jsondata.getBiller_code());
			prep.setObject(3, payer_code);
			prep.execute();
			conn.close();
			 return ResponseEntity.ok(new AuthProduceJson("00", "Invoice disputed successfully", "", ""));
			
		}catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		 
	}
	 
	 
	 
}
