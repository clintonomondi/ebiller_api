package com.stanbic.Invoice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.apache.velocity.VelocityContext;

import com.stanbic.Billers.BillerHelper;
import com.stanbic.Forex.FileSettingModel;
import com.stanbic.Forex.ForexHelper;
import com.stanbic.comm.CheckUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.LoginHelper;

public class InvoiceHelper {
	static Connection conn;
	
	public static void processInvoice(Vector holder,String biller_code,String biller_name,String file_id,String file_path,String file_name) {
		saveFile( file_path,  file_name,  biller_code,file_id,  biller_name);
		int invoice_val=0;
		int service_val=0;
		int amount_val=0;
		int due_date_val=0;
		int email_val=0;
		try {
			conn=DbManager.getConnection();
			 String sql="SELECT * FROM invoice_file_setting WHERE comp_code=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, biller_code);
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 amount_val=rs.getInt("amount")-1;
				 email_val=rs.getInt("email")-1;
				 invoice_val=rs.getInt("invoice_no")-1;
				 due_date_val=rs.getInt("due_date")-1;
				 service_val=rs.getInt("service")-1;
			 }
			 conn.close();
			
			
			
			
		Vector dataholder=new Vector();
		Vector dataholder2=new Vector();
		for (int i=0;i<holder.size(); i++){
			Vector cellStoreVector=(Vector)holder.elementAt(i);
			String invoice=cellStoreVector.toArray()[invoice_val].toString();
			String service=cellStoreVector.toArray()[service_val].toString();
			double amount=Double.parseDouble(cellStoreVector.toArray()[amount_val].toString());
			String due_date=cellStoreVector.toArray()[due_date_val].toString();
			String email=cellStoreVector.toArray()[email_val].toString();
			
			String tax="0";
			String status="Not Found";
			String payer_code="NA";
			if(amount>0) {
				String commission="0";
				int v_count= CheckUsers.checkUser(email);
				if(v_count>0) {
				status="Found";
				payer_code=LoginHelper.getComp_Code(email);
			    commission=ForexHelper.getIndividualCommission(biller_code, payer_code);
				Vector data=new Vector();
				data.addElement(email);
				data.addElement(biller_code);
				data.addElement(biller_name);
				data.addElement(tax);
				data.add(commission);
				data.add(payer_code);
				data.add(due_date);
				data.add(amount);
				data.add(invoice);
				data.add(service);
				data.add(file_id);
				dataholder.addElement(data);
				}else {
					status="Not Found";
					 payer_code="NA";
				}
				
				Vector data2=new Vector();
				data2.addElement(email);
				data2.addElement(biller_code);
				data2.addElement(tax);
				data2.add(commission);
				data2.add(payer_code);
				data2.add(due_date);
				data2.add(amount);
				data2.add(invoice);
				data2.add(service);
				data2.add(status);
				data2.add(file_id);
				dataholder2.addElement(data2);
			}else {
				
			}
			
		}
		saveToInvoiceBills(dataholder2);
		sendInvoice(dataholder);
		
	}catch(Exception e) {
		e.printStackTrace();
	}
		
	}
	
	
	public static void processInvoiceIndividual(Vector holder,String biller_code,String biller_name,String file_id,String file_path,String file_name,String payer_code,String email) {
		saveFile( file_path,  file_name,  biller_code,file_id,  biller_name);
		int invoice_val=0;
		int service_val=0;
		int amount_val=0;
		int due_date_val=0;
		try {
			conn=DbManager.getConnection();
			 String sql="SELECT * FROM invoice_file_setting WHERE comp_code=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, biller_code);
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 amount_val=rs.getInt("amount")-1;
				 invoice_val=rs.getInt("invoice_no")-1;
				 due_date_val=rs.getInt("due_date")-1;
				 service_val=rs.getInt("service")-1;
			 }
			 conn.close();
			
			
			
			
		Vector dataholder=new Vector();
		Vector dataholder2=new Vector();
		String commission="0";
		  commission=ForexHelper.getIndividualCommission(biller_code, payer_code);
		for (int i=0;i<holder.size(); i++){
			Vector cellStoreVector=(Vector)holder.elementAt(i);
			String invoice=cellStoreVector.toArray()[invoice_val].toString();
			String service=cellStoreVector.toArray()[service_val].toString();
			double amount=Double.parseDouble(cellStoreVector.toArray()[amount_val].toString());
			String due_date=cellStoreVector.toArray()[due_date_val].toString();
			
			String tax="0";
			String status="Not Found";
			if(amount>0) {
				status="Found";
				Vector data=new Vector();
				data.addElement(email);
				data.addElement(biller_code);
				data.addElement(biller_name);
				data.addElement(tax);
				data.add(commission);
				data.add(payer_code);
				data.add(due_date);
				data.add(amount);
				data.add(invoice);
				data.add(service);
				data.add(file_id);
				dataholder.addElement(data);
				
				
				Vector data2=new Vector();
				data2.addElement(email);
				data2.addElement(biller_code);
				data2.addElement(tax);
				data2.add(commission);
				data2.add(payer_code);
				data2.add(due_date);
				data2.add(amount);
				data2.add(invoice);
				data2.add(service);
				data2.add(status);
				data2.add(file_id);
				dataholder2.addElement(data2);
			}else {
				
			}
			
		}
		saveToInvoiceBills(dataholder2);
		sendInvoice(dataholder);
		
	}catch(Exception e) {
		e.printStackTrace();
	}
		
	}
	
	public static  void saveToInvoiceBills(Vector holder ) {
		try {
			conn = DbManager.getConnection();
			String sql = "INSERT INTO invoice_bills(email,biller_code,tax,commission,payer_code,due_date,amount,invoice,service,status,file_id)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement prep = conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			int ii=0;
			for (int i=0;i<holder.size(); i++){
				Vector cellStoreVector=(Vector)holder.elementAt(i);
				    prep.setObject(1, cellStoreVector.toArray()[0].toString());
					prep.setObject(2, cellStoreVector.toArray()[1].toString());
					prep.setObject(3, cellStoreVector.toArray()[2].toString());
					prep.setObject(4, cellStoreVector.toArray()[3].toString());
					prep.setObject(5, cellStoreVector.toArray()[4].toString());
					prep.setObject(6, cellStoreVector.toArray()[5].toString());
					prep.setObject(7, cellStoreVector.toArray()[6].toString());
					prep.setObject(8,cellStoreVector.toArray()[7].toString());
					prep.setObject(9,cellStoreVector.toArray()[8].toString());
					prep.setObject(10,cellStoreVector.toArray()[9].toString());
					prep.setObject(11,cellStoreVector.toArray()[10].toString());
					prep.addBatch();
					
					ii++; 
					if (ii % 1000 == 0 || ii == holder.size()) {
						System.out.println(">Inserting file invoice in inoice_bills table at initial stage="+ii);
		                prep.executeBatch(); // Execute every 1000 items.
		                conn.commit();
		                prep.clearBatch();
					}
			}
			 conn.setAutoCommit(true);
				conn.close();	
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static  void sendInvoice(Vector holder ) {
		  String date=TimeManager.getEmailTime();  
		try {
			conn = DbManager.getConnection();
			String sql = "INSERT INTO invoice(email,biller_code,tax,commission,payer_code,due_date,amount,invoice,service,file_id)VALUES(?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement prep = conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			int ii=0;
			 String subject="Ebiller Account";
			for (int i=0;i<holder.size(); i++){
				Vector cellStoreVector=(Vector)holder.elementAt(i);
				    prep.setObject(1, cellStoreVector.toArray()[0].toString());
					prep.setObject(2, cellStoreVector.toArray()[1].toString());
					prep.setObject(3, cellStoreVector.toArray()[3].toString());
					prep.setObject(4, cellStoreVector.toArray()[4].toString());
					prep.setObject(5, cellStoreVector.toArray()[5].toString());
					prep.setObject(6, cellStoreVector.toArray()[6].toString());
					prep.setObject(7, cellStoreVector.toArray()[7].toString());
					prep.setObject(8,cellStoreVector.toArray()[8].toString());
					prep.setObject(9,cellStoreVector.toArray()[9].toString());
					prep.setObject(10,cellStoreVector.toArray()[10].toString());
					prep.addBatch();
					
                    double amount=Double.parseDouble(cellStoreVector.toArray()[7].toString());
                    double commission=Double.parseDouble(cellStoreVector.toArray()[4].toString());
                    double tax=Double.parseDouble(cellStoreVector.toArray()[3].toString());
                    double comm_amount=(commission * amount)/100;
                    double invoice_amount=amount-comm_amount;
					
					VelocityContext vc = new VelocityContext();
					vc.put("invoice_no",cellStoreVector.toArray()[8].toString()  );
					vc.put("biller_name",cellStoreVector.toArray()[2].toString());
					vc.put("commission",cellStoreVector.toArray()[4].toString() +"%" );
					vc.put("tax",cellStoreVector.toArray()[3].toString()  +"%");
					vc.put("invoice_amount",invoice_amount );
					vc.put("date",date );
					vc.put("total",cellStoreVector.toArray()[7].toString());
					vc.put("due_date",cellStoreVector.toArray()[6].toString()  );
			         String email_template = EmailManager.email_message_template(vc,"invoice_generated.vm");
						EmailManager.send_mail(cellStoreVector.toArray()[0].toString(),email_template,subject,cellStoreVector.toArray()[2].toString(),"Invoicing");
					
					
					ii++;
					if (ii % 1000 == 0 || ii == holder.size()) {
						System.out.println(">Inserting file invoice in inoice table at initial stage="+ii);
		                prep.executeBatch(); // Execute every 1000 items.
		                conn.commit();
		                prep.clearBatch();
					}
			}
			 conn.setAutoCommit(true);
				conn.close();	
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	public static void saveFile(String file_path, String file_name, String biller_code,
			String file_id, String created_by) {
		try {
			String sql = "INSERT INTO invoice_files (file_path,file_name,biller_code,file_id,created_by)VALUES(?,?,?,?,?)";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, file_path);
			prep.setObject(2, file_name);
			prep.setObject(3, biller_code);
			prep.setObject(4, file_id);
			prep.setObject(5, created_by);
			prep.execute();
			conn.close();
		} catch (Exception n) {
			n.printStackTrace();
		}
	}
}
