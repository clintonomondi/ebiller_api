package com.stanbic.Forex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Department.DepartmentModel;
import com.stanbic.Responses.Response;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;
import com.stanbic.ebiller.onboarding.OnboadingBillerConsumerJson;

@SpringBootApplication
@RequestMapping("/api")
public class BillerForex {
	Connection conn=null;
	
	 @RequestMapping(value = "/v1/setForex", method = RequestMethod.POST)
	 public ResponseEntity<?> setForex(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody ForexModel jsondata) {
		if(jsondata.getCurrenty().isEmpty() || jsondata.getEnd_date().isEmpty() || jsondata.getRate().isEmpty() || jsondata.getStart_date().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter all fields"));
		}
		try {
			 String comp_code=TokenManager.tokenIssuedCompCode(token);
			conn=DbManager.getConnection();
			 String sql="INSERT INTO forex(comp_code,currency,rate,start_date,end_date)VALUES(?,?,?,?,?) ON DUPLICATE KEY UPDATE currency=?,rate=?,start_date=?,end_date=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, comp_code);
			 prep.setObject(2, jsondata.getCurrenty());
			 prep.setObject(3, jsondata.getRate());
			 prep.setObject(4, jsondata.getStart_date());
			 prep.setObject(5, jsondata.getEnd_date());
			 prep.setObject(6, jsondata.getCurrenty());
			 prep.setObject(7, jsondata.getRate());
			 prep.setObject(8, jsondata.getStart_date());
			 prep.setObject(9, jsondata.getEnd_date());
			 prep.execute();
			 conn.close();
			 return ResponseEntity.ok(new Response("00", "Invoice Forex updated successfully"));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
		 
	 }
	 
	 @RequestMapping(value = "/v1/getForex", method = RequestMethod.POST)
	 public ResponseEntity<?> getForex(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody ForexModel jsondata) {
		 List<ForexModel> model= new ArrayList<ForexModel>();
		try {
			 String comp_code=TokenManager.tokenIssuedCompCode(token);
			conn=DbManager.getConnection();
			 String sql="SELECT * FROM forex WHERE comp_code=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, comp_code);
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 ForexModel data= new ForexModel();
				 data.currenty=rs.getString("currency");
				 data.end_date=rs.getString("end_date");
				 data.rate=rs.getString("rate");
				 data.start_date=rs.getString("start_date");
				 model.add(data);
			 }
			 conn.close();
			 return ResponseEntity.ok(model);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
		 
	 }
}
