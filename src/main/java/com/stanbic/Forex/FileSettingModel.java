package com.stanbic.Forex;

public class FileSettingModel {
String email="";
String invoice_no="";
String amount="";
String due_date="";
String service="";


public String getDue_date() {
	return due_date;
}
public void setDue_date(String due_date) {
	this.due_date = due_date;
}
public String getService() {
	return service;
}
public void setService(String service) {
	this.service = service;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getInvoice_no() {
	return invoice_no;
}
public void setInvoice_no(String invoice_no) {
	this.invoice_no = invoice_no;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}


}
