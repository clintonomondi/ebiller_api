package com.stanbic.Forex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.stanbic.comm.DbManager;

public class ForexHelper {
	static Connection conn=null;
	
//	public static boolean checkSpecificPayer(String biller_code,String payer_code) {
//		 
//		 try {
//			 String sql="SELECT COUNT(*) AS V_COUNT FROM `commission` WHERE biller_code=? AND payer_code=?";
//			 conn=DbManager.getConnection();
//			 PreparedStatement prep=conn.prepareStatement(sql);
//			 prep.setObject(1, biller_code);
//			 prep.setObject(2,payer_code);
//			 ResultSet rs=prep.executeQuery();
//			 while(rs.next()) {
//				if(rs.getInt("V_COUNT")>0) {
//					return true;
//				}else {
//					return false;
//				}
//			 }
//			 conn.close();
//		 }
//		 catch(Exception e) {
//			 e.printStackTrace();
//			 return true;
//		 }
//		 return true;
//		 
//	 }
	
	
	public static int checkAll(String biller_code) {
		 int val=0;
		 try {
			 String sql="SELECT COUNT(*) AS V_COUNT FROM `commission` WHERE biller_code=? AND type=?  AND end_date > NOW()";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, biller_code);
			 prep.setObject(2,"all");
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
					val=rs.getInt("V_COUNT");
				 }
			 conn.close();
		 }
		 catch(Exception e) {
			 e.printStackTrace();
		 }
		 return val;
		 
	 }
	
	
	public static int checkFileSettings(String comp_code) {
	 int val=0;
	 try {
		 String sql="SELECT COUNT(*) AS V_COUNT FROM `invoice_file_setting` WHERE comp_code=?";
		 conn=DbManager.getConnection();
		 PreparedStatement prep=conn.prepareStatement(sql);
		 prep.setObject(1, comp_code);
		 ResultSet rs=prep.executeQuery();
		 while(rs.next()) {
			val=rs.getInt("V_COUNT");
		 }
		 conn.close();
	 }
	 catch(Exception e) {
		 e.printStackTrace();
	 }
	 return val;
	 
}
	
	public static String getIndividualCommission(String biller_code,String payer_code) {
		 String commission="0";
		 try {
			 String sql="SELECT rate FROM  commission  WHERE biller_code=? AND payer_code=?  AND end_date > NOW()";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, biller_code);
			 prep.setObject(2, payer_code);
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				commission=rs.getString("rate");
			 }
			 conn.close();
			 
			 if(commission.isEmpty() || commission==null || commission.equals("0")) {
				 String sql2="SELECT rate FROM  commission  WHERE biller_code=? AND type=?";
				 conn=DbManager.getConnection();
				 PreparedStatement prep2=conn.prepareStatement(sql2);
				 prep2.setObject(1, biller_code);
				 prep2.setObject(2, "all");
				 ResultSet rs2=prep2.executeQuery();
				 while(rs2.next()) {
					commission=rs2.getString("rate");
				 } 
				 conn.close();
			 }
		 }
		 catch(Exception e) {
			 e.printStackTrace();
		 }
		 System.out.println("compcode=>>>>>>>"+payer_code +"commission=====>>>>>>>"+commission);
		 return commission;
		 
	}
	
	 public static  boolean checkSameColumn(int[] arr){
	        for (int i = 0; i < arr.length-1; i++) {
	        for (int j = i+1; j < arr.length; j++) {
	             if (arr[i] == arr[j]) {
	                 return true;
	             }
	        }
	    } 
	        
	    return false;    
	    }
}
