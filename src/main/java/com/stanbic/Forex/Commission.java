package com.stanbic.Forex;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Responses.Response;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;

@SpringBootApplication
@RequestMapping("/api")
public class Commission {
	Connection conn=null;
	
	 @RequestMapping(value = "/v1/setCommission", method = RequestMethod.POST)
	 public ResponseEntity<?> setCommission(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CommissionModel jsondata) {
		 String biller_code=TokenManager.tokenIssuedCompCode(token);
		 if(jsondata.getName().isEmpty() || jsondata.getEnd_date().isEmpty() || jsondata.getRate().isEmpty() || jsondata.getStart_date().isEmpty() || jsondata.getType().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter all fields"));
		 }
		
		if(jsondata.getType().equalsIgnoreCase("specific")) {
//			if(ForexHelper.checkSpecificPayer(biller_code, jsondata.getPayer_code())) {
//				return ResponseEntity.ok(new Response("06", "The payer already exist"));
//			}
			if(jsondata.getPayer_code().isEmpty()) {
				return ResponseEntity.ok(new Response("06", "Please please select payer"));	
			}
			
		}
		else if(jsondata.getType().equalsIgnoreCase("all")) {
//        	if(ForexHelper.checkAll(biller_code)) {
//				return ResponseEntity.ok(new Response("06", "The biller has similar settings already exist"));
//			}
//			
		}else {
			return ResponseEntity.ok(new Response("06", "Please  select commission type"));
		}
		
		try {
			
			conn=DbManager.getConnection();
			 String sql="INSERT INTO commission(biller_code,name,rate,start_date,end_date,type,payer_code)"
			 		+ "VALUES(?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE name=?,rate=?,start_date=?,end_date=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, biller_code);
			 prep.setObject(2, jsondata.getName());
			 prep.setObject(3, jsondata.getRate());
			 prep.setObject(4, jsondata.getStart_date());
			 prep.setObject(5, jsondata.getEnd_date());
			 prep.setObject(6, jsondata.getType());
			 prep.setObject(7, jsondata.getPayer_code());
			 
			 prep.setObject(8, jsondata.getName());
			 prep.setObject(9, jsondata.getRate());
			 prep.setObject(10, jsondata.getStart_date());
			 prep.setObject(11, jsondata.getEnd_date());
			 prep.execute();
			 conn.close();
			 return ResponseEntity.ok(new Response("00", "Invoice commission saved successfully"));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
		 
	 }
	 
	 
	 
	 @RequestMapping(value = "/v1/getCommission", method = RequestMethod.POST)
	 public ResponseEntity<?> getCommission(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CommissionModel jsondata) {
		 List<CommissionModel> model= new ArrayList<CommissionModel>();
		try {
			 String biller_code=TokenManager.tokenIssuedCompCode(token);
			conn=DbManager.getConnection();
			 String sql="SELECT id,type, name,rate,payer_name,start_date,end_date,payer_code,IF(STATUS IS NULL,'expired','active') STATUS FROM (\r\n" + 
			 		"SELECT id,name,rate,start_date,end_date,payer_code,type,\r\n" + 
			 		"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.payer_code)payer_name,\r\n" + 
			 		"(SELECT id  FROM commission C WHERE C.payer_code=A.payer_code  AND end_date > NOW()) STATUS\r\n" + 
			 		"FROM commission A WHERE biller_code=?\r\n" + 
			 		")Q1";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, biller_code);
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 CommissionModel data= new CommissionModel();
				 data.type=rs.getString("type");
				 data.end_date=rs.getString("end_date");
				 data.rate=rs.getString("rate");
				 data.start_date=rs.getString("start_date");
				 data.payer_name=rs.getString("payer_name");
				 data.payer_code=rs.getString("payer_code");
				 data.id=rs.getString("id");
				 data.status=rs.getString("status");
				 model.add(data);
			 }
			 conn.close();
			 return ResponseEntity.ok(model);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
		 
	 }

}
