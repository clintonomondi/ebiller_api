package com.stanbic.ebiller.auth;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.apache.velocity.VelocityContext;
import org.springframework.http.ResponseEntity;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Billers.BillerHelper;
import com.stanbic.comm.CheckUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TokenManager;


public class LoginHelper {
	static Connection conn;
	
	public static String getUserType(String comp_code) {
		String user_type=null;
		try {
			  conn=DbManager.getConnection();
			  String sql="SELECT user_type FROM biller_profile WHERE comp_code=?";
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, comp_code);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  user_type=rs.getString("user_type");
			  }
			  conn.close();
		  }catch(Exception n) {
			  System.out.println(n.getMessage());
		  }
		return user_type;
	}
	
	public static String getComp_Code(String email) {
		String code="";
		try {
			  conn=DbManager.getConnection();
			  String sql="SELECT comp_code FROM biller_profile WHERE email=?";
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  code=rs.getString("comp_code");
			  }
			  conn.close();
		  }catch(Exception n) {
			 n.printStackTrace();
		  }
		return code;
	}
	
	public static String getGroupName(String id) {
		String name=null;
		try {
			  conn=DbManager.getConnection();
			  String sql="SELECT name FROM user_groups WHERE id=?";
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, id);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  name=rs.getString("name");
			  }
			  conn.close();
		  }catch(Exception n) {
			 n.printStackTrace();
		  }
		return name;
	}
	
	public static String getGroupId(String email) {
		String group_no="";
		try {
			 String sql="SELECT IF(group_id IS NULL,'',group_id)group_id FROM ebiller_auth WHERE email=?";
			  conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  group_no=rs.getString("group_id");
			  }
			  conn.close();
		  }catch(Exception e) {
			 e.printStackTrace();
		  }
		return group_no;
	}
	
	public static String saveInvitedPayer(String email,String biller_code,String department_id) {
	String comp_code="";
	int v_count= CheckUsers.checkPayer(email);
	if(v_count>0) {
		comp_code=LoginHelper.getComp_Code(email);
		BillerHelper.linkPayerToBiller2(biller_code, comp_code,department_id,"Invited");
	}else {
	 comp_code = Long.toString(new GeneralCodes().cust_code());
	 BillerHelper.linkPayerToBiller2(biller_code, comp_code,department_id,"Invited");
	 try {
		 String sql="INSERT INTO biller_profile (comp_code,email,status)VALUES(?,?,?) ON DUPLICATE KEY UPDATE status=?";
		 conn=DbManager.getConnection();
		  PreparedStatement prep=conn.prepareStatement(sql);
		  prep.setObject(1, comp_code);
		  prep.setObject(2, email);
		  prep.setObject(3, "Invited");
		  prep.setObject(4, "Invited");
		  prep.execute();
		  conn.close();
	 }catch(Exception e) {
		 e.printStackTrace();
	 }
		
	}
	return comp_code;
		
	}
	
	public static void updatePayer(String personel_f_name,String personel_l_name,String biller_phone,String company_name,String email ) {
		try {
			String sql="UPDATE biller_profile SET personel_f_name=?,personel_l_name=?,biller_phone=?,user_type=?,company_name=?,status=? WHERE email=?";
			 conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, personel_f_name);
			  prep.setObject(2, personel_l_name);
			  prep.setObject(3, biller_phone);
			  prep.setObject(4, "payer");
			  prep.setObject(5, company_name);
			  prep.setObject(6, "Active");
			  prep.setObject(7, email);
			  prep.execute();
			  conn.close();
		}catch(Exception n) {
			n.printStackTrace();
		}
	}
	
	
	public static void  invitePayers(Vector emailsHolder,String biller_code,String user_email,String department_id) {
		 String name=AccountHelper.getName(user_email);
		 String department=LoginHelper.getDepartmentName(department_id);
		 try {
				for (int i=0;i<emailsHolder.size(); i++){
					Vector cellStoreVector=(Vector)emailsHolder.elementAt(i);
					String email=cellStoreVector.toArray()[0].toString();
					
					int v_count= CheckUsers.checkUser(email);
					if(v_count>0) {
						BillerHelper.linkPayerToBiller2(biller_code, LoginHelper.getComp_Code(email),department_id,"Invited");
						String token=TokenManager.createToken2(department_id, email, "", biller_code);
						String path=PathUrl.getBillerAppPath("biller");
							String url= path+"login/payer-approval"+"?token=" + token;
							  String subject="Stanbic E-biller";
							  VelocityContext vc = new VelocityContext();
					         vc.put("login_link", url);
					         vc.put("name", name);
					         vc.put("department",department);
					         String email_template = EmailManager.email_message_template(vc,"account_temp.vm");
								EmailManager.send_mail(email,email_template,subject,name,"bank invited user");
					  }
					else {
					String payer_code=LoginHelper.saveInvitedPayer(email, biller_code, department_id);
					String token=TokenManager.createToken2(payer_code, email, biller_code, "account_principal");
					String path=PathUrl.getBillerAppPath("biller");
					String url= path+"login/register"+"?token=" + token;
					  String subject="Ebiller Account";
					  VelocityContext vc = new VelocityContext();
			         vc.put("login_link", url);
			         vc.put("name", name);
			         vc.put("department",department );
			         String email_template = EmailManager.email_message_template(vc,"account_temp.vm");
						EmailManager.send_mail(email,email_template,subject,name,"bank invited user");
					}
				}
				
		 }catch(Exception e) {
			 e.printStackTrace();
		 }
	}
	
	public static boolean isUserFrozen(String email) {
		try {
			String sql="SELECT frozen FROM ebiller_auth WHERE email=?";
			 conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  if(rs.getString("frozen").equals("0")) {
					  return true;
				  }else {
					  return false;
				  }
			  }
			  conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	public static boolean isPasswordSame(String email,String password) {
		try {
			String sql="SELECT * FROM ebiller_auth WHERE email=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, email);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				if(password.equalsIgnoreCase(rs.getString("password"))) {
					return true;
				}else {
					return false;
				}
			}
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static String getDepartmentName(String id) {
		String name="";
		try {
			String sql="SELECT name FROM department WHERE id=?";
			 conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, id);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  name=rs.getString("name");
			  }
			  conn.close();
		}catch(Exception n) {
			n.printStackTrace();
		}
		return name;
		
	}
	
	public static boolean isBankUserFrozen(String username) {
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT status FROM bank_users WHERE username=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, username);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				if(rs.getString("status").equalsIgnoreCase("Active")) {
					return false;
				}else {
					return true;
				}
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public static String getCreatedBy(String username) {
		String created_by="";
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT created_by FROM bank_users WHERE username=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, username);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				created_by=rs.getString("created_by");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return created_by;
	}
	
	public static String getUpdatedBy(String username) {
		String created_by="";
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT modified_by FROM bank_users WHERE username=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, username);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				created_by=rs.getString("modified_by");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return created_by;
	}
	
	public static int isPhoneNumberExist(String phone) {
		int vcount=0;
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT COUNT(*)vcount FROM ebiller_auth WHERE phone=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, phone);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				vcount=rs.getInt("vcount");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vcount;
	}
	
	public static int isPrefixExist(String prefix) {
		int vcount=0;
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT COUNT(*)vcount FROM biller_profile WHERE prefix=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, prefix);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				vcount=rs.getInt("vcount");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vcount;
	}
	
	public static String getuserapprovedStatus(String comp_code) {
		String status="";
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT status FROM biller_profile WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				status=rs.getString("status");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
	}

}
