package com.stanbic.ebiller.auth;

public class AuthProduceJson {
	String messageCode = "";
	String message = "";
	String token="";
	String fname="";
	String user_group="";
	
	public AuthProduceJson(String messageCode, String message,String token,String fname) {
		this.messageCode = messageCode;
		this.message = message;
		this.token=token;
		this.fname=fname;
	}
	

	public AuthProduceJson(String messageCode, String message, String token, String fname, String user_group) {
		this.messageCode = messageCode;
		this.message = message;
		this.token = token;
		this.fname = fname;
		this.user_group = user_group;
	}


	public String getUser_group() {
		return user_group;
	}


	public void setUser_group(String user_group) {
		this.user_group = user_group;
	}


	public String getToken() {
		return token;
	}


	public String getFname() {
		return fname;
	}


	public void setFname(String fname) {
		this.fname = fname;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
