package com.stanbic.ebiller.auth;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.stanbic.comm.DbManager;

public class GenerateAutoGroupId {

	Connection conn=null;

	public String getLastId() {
		String group_id="";
		try {
			conn=DbManager.getConnection();
			String sql="SELECT id+1 as VALUE FROM `user_groups` ORDER BY id DESC LIMIT 1";
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				group_id=rs.getString("VALUE");
				System.out.println("The group id added"+group_id);
			}
			conn.close();
			return group_id;
		}catch(Exception e) {
			e.printStackTrace();
			return group_id;
		}
	}
	
	
}
