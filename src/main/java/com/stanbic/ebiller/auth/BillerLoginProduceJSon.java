package com.stanbic.ebiller.auth;

public class BillerLoginProduceJSon {
	String messageCode = "";
	String message = "";
	String token = "";
	String user_type="";
	String biller_type=null;
	String loginvalue=null;
	String group_id=null;
	String group_name=null;
	
	public BillerLoginProduceJSon(String messageCode,String message,String token,String user_type,String biller_type,String loginvalue) {
		this.messageCode = messageCode;
		this.message = message;
		this.token = token;
		this.user_type=user_type;
		this.biller_type=biller_type;
		this.loginvalue=loginvalue;
	}
	
	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getGroup_name() {
		return group_name;
	}



	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}



	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	public String getBiller_type() {
		return biller_type;
	}
	public void setBiller_type(String biller_type) {
		this.biller_type = biller_type;
	}
	public String getLoginvalue() {
		return loginvalue;
	}
	public void setLoginvalue(String loginvalue) {
		this.loginvalue = loginvalue;
	}
}
