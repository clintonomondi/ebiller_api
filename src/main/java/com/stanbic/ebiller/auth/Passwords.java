package com.stanbic.ebiller.auth;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Logs.System_Logs;
import com.stanbic.Validation.PasswordValidater;
import com.stanbic.comm.CheckUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.PasswordUtils;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;

@SpringBootApplication
@RequestMapping("/api")
public class Passwords {
	Connection conn = null;
	String username="";
	
	@RequestMapping(value = "/v1/sendEmailResetPassword", method = RequestMethod.POST)
	  public ResponseEntity<?> sendEmailResetPassword(@RequestBody BillerAuthconsumeJson jsondata) {
		if(jsondata.getEmail().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please enter email address","",""));
		}
		try {
		int v_count= CheckUsers.checkUser(jsondata.getEmail());
		if(v_count==0) {
			  return ResponseEntity.ok(new AuthProduceJson("07","Sorry,we do not have this email address in our record","",""));
		  }
		//String token=TokenManager.generateJwtToken("", jsondata.getEmail(), ExternalFile.getSecretKey(),"account_principal");
		String token = TokenManager.createToken("", jsondata.getEmail(), "",  "account_principal");
		 String path=PathUrl.getBillerAppPath("biller");
		String url= path+"login/reset-password"+"?token="+token;
		  String subject="Account Reset password";
		  String date=TimeManager.getEmailTime();
		  VelocityContext vc = new VelocityContext();
          vc.put("login_link", url);
          vc.put("date", date);
          String email_template = EmailManager.email_message_template(vc,"resetpassword_temp.vm");
    
			EmailManager.send_mail(jsondata.getEmail(),email_template,subject,jsondata.getEmail(),"Forgot password"); 
		
		}catch(Exception e) {
			e.printStackTrace();
			  return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
		System_Logs.log_system(jsondata.getEmail(), "", "User sent email to reset password:", "Success", "");
		return ResponseEntity.ok(new AuthProduceJson("00","An email has been successfully sent to your email:"+jsondata.getEmail(),"",""));
	}
	
	
	@RequestMapping(value = "/v1/resetPassword", method = RequestMethod.POST)
	  public ResponseEntity<?> resetPassword(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillerAuthconsumeJson jsondata) {
		if(jsondata.getPassword().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please enter password","",""));
		}
		String email = TokenManager.tokenIssuedId(token);
		String pass=jsondata.getPassword();
		
		if (!PasswordValidater.isPassword8(pass)) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must be atleast 8 characters", "", ""));
		}
		if (!PasswordValidater.isPasswordHasLowerCase(pass)) {
			return ResponseEntity
					.ok(new AuthProduceJson("08", "Passsword must have atleast one lower case character", "", ""));
		}
		
		if (!PasswordValidater.isPasswordHasLowerDigit(pass)) {
			return ResponseEntity
					.ok(new AuthProduceJson("08", "Passsword must have atleast a digit character", "", ""));
		}
		
		if (!PasswordValidater.isPasswordHasSpecial(pass)) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must have a special character", "", ""));
		}
		
		if (!PasswordValidater.isPasswordHasUpperCase(pass)) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must have a upper case character", "", ""));
		}
		String newpassword = PasswordUtils.generateSecurePassword(pass, ExternalFile.getSecretKey());
		try {
			String sql="UPDATE ebiller_auth SET password=? WHERE email=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, newpassword);
			prep.setObject(2, email);
			prep.executeUpdate();
			conn.close();
			System_Logs.log_system(email, "", "User reset	 password:", "Success", "");
			return ResponseEntity.ok(new AuthProduceJson("00", "Password reset successfully,please login with your new password", "", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		
	}
	
	
	@RequestMapping(value = "/v1/changePassword", method = RequestMethod.POST)
	  public ResponseEntity<?> changePassword(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillerAuthconsumeJson jsondata) {
		if(jsondata.getCurrentPassword().isEmpty() || jsondata.getNewPassword().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please enter password","",""));
		}
		String email = TokenManager.tokenIssuedId(token);
		String payer_code = TokenManager.tokenIssuedCompCode(token);
		
	
		
		if (!PasswordValidater.isPassword8(jsondata.getNewPassword())) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must be atleast 8 characters", "", ""));
		}
		if (!PasswordValidater.isPasswordHasLowerCase(jsondata.getNewPassword())) {
			return ResponseEntity
					.ok(new AuthProduceJson("08", "Passsword must have atleast one lower case character", "", ""));
		}
		
		if (!PasswordValidater.isPasswordHasLowerDigit(jsondata.getNewPassword())) {
			return ResponseEntity
					.ok(new AuthProduceJson("08", "Passsword must have atleast a digit character", "", ""));
		}
		
		if (!PasswordValidater.isPasswordHasSpecial(jsondata.getNewPassword())) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must have a special character", "", ""));
		}
		
		if (!PasswordValidater.isPasswordHasUpperCase(jsondata.getNewPassword())) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must have a upper case character", "", ""));
		}
		String currentPassword = PasswordUtils.generateSecurePassword(jsondata.getCurrentPassword(), ExternalFile.getSecretKey());
		String newPassword=PasswordUtils.generateSecurePassword(jsondata.getNewPassword(), ExternalFile.getSecretKey());
		if(LoginHelper.isPasswordSame(email, currentPassword)) {
			try {
				String sql="UPDATE ebiller_auth SET password=? WHERE email=?";
				conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				prep.setObject(1, newPassword);
				prep.setObject(2, email);
				prep.executeUpdate();
				conn.close();
						String subject="Password Changed";
						  VelocityContext vc = new VelocityContext();
				          vc.put("user_name", AccountHelper.getName(email));
				          vc.put("time", AccountHelper.getCurrentTime());
				          String email_template = EmailManager.email_message_template(vc,"passwordChanged.vm");
							EmailManager.send_mail(email,email_template,subject,jsondata.getEmail(),"Change password");
				return ResponseEntity.ok(new AuthProduceJson("00", "Password changed successfully", "", ""));
			}catch(Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
			}
		}else {
			System_Logs.log_system(email, "", "User changed password:", "Success", "");
			return ResponseEntity.ok(new AuthProduceJson("06", "The current password is incorrect", "", ""));
		}
		
		
		
	}
}
