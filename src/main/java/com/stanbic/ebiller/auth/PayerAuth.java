package com.stanbic.ebiller.auth;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.json.JSONArray;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;

import com.stanbic.Billers.BillerHelper;
import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.Logs.System_Logs;
import com.stanbic.Mytests.MyTestModule;
import com.stanbic.Responses.Response;
import com.stanbic.Roles.RoleHelper;
import com.stanbic.Roles.UserGroupAndMenu;
import com.stanbic.Validation.PasswordValidater;
import com.stanbic.comm.CheckUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.PasswordUtils;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthConsumeJson;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class PayerAuth {

	static Connection conn = null;
	static Logger log = Logger.getLogger(PayerAuth.class.getName());
 
	@PostMapping(value = "/v1/sendEmailPayer")
	public ResponseEntity<?> billerCreatePass(@ModelAttribute  BillerAuthconsumeJson jsondata) {
		if(jsondata.getEmail().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Enter a valid email address", "", ""));
		}
	   String date=TimeManager.getEmailTime();  
		try {
			int v_count = CheckUsers.checkPayer(jsondata.getEmail());
			if (v_count > 0) {
				return ResponseEntity.ok(new AuthProduceJson("02", "The email is already in use", "", ""));
			}
			String token = TokenManager.createToken2("", jsondata.getEmail(), "", "account_principal");
			String path = PathUrl.getBillerAppPath("biller");
			String url = path + "login/register" + "?token=" + token;

			String subject = "Stanbic E-biller";
			VelocityContext vc = new VelocityContext();
			vc.put("login_link", url);
			vc.put("date", date);
			String email_template = EmailManager.email_message_template(vc, "welcome_self.vm");
			EmailManager.send_mail(jsondata.getEmail(), email_template, subject,jsondata.getEmail(),"Payer signup email");

		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		System_Logs.log_system(jsondata.getEmail(), "", "User sent email to create account:", "Success", "");
		return ResponseEntity.ok(new AuthProduceJson("00","An email has been successfully sent to your email:" + jsondata.getEmail(), "", ""));
	}
	@PostMapping(value = "/v1/submitContact")
	public ResponseEntity<?> submitContact(@ModelAttribute  BillerAuthconsumeJson jsondata) {
		if(jsondata.getEmail().isEmpty() || jsondata.getFname().isEmpty() || jsondata.getSubject().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide all fields"));
		}
		try {
			String sql="INSERT INTO contact(fname,lname,subject,email,message)VALUES(?,?,?,?,?)";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, jsondata.getFname());
			 prep.setObject(2, jsondata.getLname());
			 prep.setObject(3, jsondata.getSubject());
			 prep.setObject(4, jsondata.getEmail());
			 prep.setObject(5, jsondata.getMessage());
			 prep.execute();
			 conn.close();
			 return ResponseEntity.ok(new Response("00", "Your message has been submitted successfully"));
		}catch(Exception e) {
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
	}

	@RequestMapping(value = "/v1/billerLogin", method = RequestMethod.POST)
	public ResponseEntity<?> payerLogin(@RequestBody AuthConsumeJson jsondata) {
		EmailAlert.logger(jsondata.getEmail(), "accessed system", log);
		if (LoginHelper.isUserFrozen(jsondata.getEmail())) {
			String passwordIn = jsondata.getPassword();
			String password = PasswordUtils.generateSecurePassword(passwordIn, ExternalFile.getSecretKey());
			String user_type = "";
			String group_id = "";
			String group_name = "";
			String email_no = "";
			String switchedToBillerYN = "";
			String comp_code = "";
			String biller_type = "";
			String loginvalue="";
			HashMap<String, Object> map = new HashMap<String, Object>();
			int v_count = checkIfEmailExist(jsondata.getEmail());
			
			if (v_count > 0) {
				try {
					String sql = "SELECT email,comp_code,group_id,loginvalue,(SELECT user_type FROM biller_profile B WHERE B.COMP_CODE=A.COMP_CODE) user_type ,\n"
							+ "(SELECT biller_type FROM biller_profile B WHERE B.COMP_CODE=A.COMP_CODE) biller_type ,\n"
							+ "(SELECT IF(APPROVED_YN IS NULL,'N',APPROVED_YN) FROM biller_profile B WHERE B.COMP_CODE=A.COMP_CODE) APPROVED_YN, \n"
							+ "(SELECT name FROM user_groups C WHERE C.id=A.group_id) group_name \n"
							+ "FROM `ebiller_auth` A WHERE email=?";
					conn = DbManager.getConnection();
					PreparedStatement prep = conn.prepareStatement(sql);
					prep.setObject(1, jsondata.getEmail());
					ResultSet rs = prep.executeQuery();
					while (rs.next()) {
						comp_code = rs.getString("comp_code");
						user_type = rs.getString("USER_TYPE");
						group_id = rs.getString("group_id");
						email_no = rs.getString("email");
						group_name = rs.getString("group_name");
						switchedToBillerYN = rs.getString("APPROVED_YN");
						biller_type = rs.getString("biller_type");
						loginvalue=rs.getString("loginvalue");
					}
					conn.close();
				} catch (Exception e) {
				}
				
				if (switchedToBillerYN.equalsIgnoreCase("N")) {
					int v_count_2 = passwordFromDb(jsondata.getEmail(), password);
					if (v_count_2 > 0) {
						String token = TokenManager.createToken(comp_code, jsondata.getEmail(), "", group_id);
						System_Logs.log_system(jsondata.getEmail(), "", "System User attempt login:", "success", "");
						map.put("messageCode", "00");
						map.put("message", "Correct username and password for Biller");
						map.put("user_type", user_type);
						map.put("biller_type", biller_type);
						map.put("loginvalue", loginvalue);
						map.put("group_id", group_id);
						map.put("group_name", group_name);
						map.put("companyCode", comp_code);
						if (group_name.equalsIgnoreCase("Super Admin")) {
							map.put("menus", new UserGroupAndMenu().getAdminmenu());
						} else {
							map.put("default", "DashBoard");
							map.put("menus", new UserGroupAndMenu().userAccessMenu(comp_code, group_id));
						}
						System_Logs.log_system(jsondata.getEmail(), "", "User logged into the system:", "Success", "");
						return ResponseEntity.ok().header("Authorization", token).body(map);
					} else {
						System_Logs.log_system(jsondata.getEmail(), "", "System User attempt login:", "Failed", "");
						map.put("messageCode", "09");
						map.put("message", "Wrong credentials");
						return ResponseEntity.ok(map);
					}

				} else {

					try {
						String sql = "SELECT email,comp_code,group_id,loginvalue,(SELECT USER_TYPE FROM biller_profile B WHERE B.COMP_CODE=A.COMP_CODE) USER_TYPE ,\n"
								+ "(SELECT biller_type FROM biller_profile B WHERE B.COMP_CODE=A.COMP_CODE) biller_type ,\n"
								+ "(SELECT IF(APPROVED_YN IS NULL,'N',APPROVED_YN) FROM biller_profile B WHERE B.COMP_CODE=A.COMP_CODE) APPROVED_YN \n"
								+ "FROM `ebiller_auth` A WHERE email=?";
						conn = DbManager.getConnection();
						PreparedStatement prep = conn.prepareStatement(sql);
						prep.setObject(1, jsondata.getEmail());
						ResultSet rs = prep.executeQuery();
						while (rs.next()) {
							comp_code = rs.getString("comp_code");
							user_type = rs.getString("USER_TYPE");
							group_id = rs.getString("group_id");
							email_no = rs.getString("email");
							biller_type = rs.getString("biller_type");
							switchedToBillerYN = rs.getString("APPROVED_YN");
							loginvalue=rs.getString("loginvalue");
						}
						conn.close();
					} catch (Exception e) {
						e.printStackTrace();
					}

					String token = TokenManager.createToken(comp_code, jsondata.getEmail(), "", group_id);
					System_Logs.log_system(jsondata.getEmail(), "", "System User attempt login:", "success", "");
					map.put("messageCode", "12");
					map.put("message", "User have been upgraded to BILLER.");
					map.put("user_type", user_type);
					map.put("biller_type", biller_type);
					map.put("loginvalue", loginvalue);
					map.put("group_id", group_id); 
					map.put("group_name", group_name);
					map.put("companyCode", comp_code);
					if (group_name.equalsIgnoreCase("Super Admin")) {
						map.put("adminMenu", new UserGroupAndMenu().getAdminmenu());
					} else {
						map.put("default", "DashBoard");
						map.put("menus", new UserGroupAndMenu().userAccessMenu(comp_code, group_id));
					}
					System_Logs.log_system(jsondata.getEmail(), "", "User logged into the system:", "Success", "");
					return ResponseEntity.ok().header("Authorization", token).body(map);
				}

			} else {
				System_Logs.log_system(jsondata.getEmail(), "", "System User attempt login:", "Failed", "");
				map.put("messageCode", "07");
				map.put("message", "Payer does not exist in the system");
				return ResponseEntity.ok(map);
			}
		} else {
			System_Logs.log_system(jsondata.getEmail(), "", "System User attempt login:", "Failed", "");
			return ResponseEntity.ok(new AuthProduceJson("06", "The email address does not exist or inactive account", "", ""));
		}

	}

	@RequestMapping(value = "/v1/payerSignup", method = RequestMethod.POST)
	public ResponseEntity<?> registerPayer(HttpServletRequest request,
			@RequestHeader(name = "Authorization") String token, @RequestBody BillerAuthconsumeJson jsondata) {
		String pass = jsondata.getPassword();
		String email = TokenManager.tokenIssuedId(token);
		String comp_code = "";

		String tokenIssuer = TokenManager.tokenIssuer(token);
		String password = PasswordUtils.generateSecurePassword(pass, ExternalFile.getSecretKey());
      
		if (jsondata.getLname().isEmpty() || jsondata.getFname().isEmpty() || jsondata.getCompany_name().isEmpty()
				|| jsondata.getPassword().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please all fields must be entered!"));
		}
		if(jsondata.getBiller_phone().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter valid phone number"));
		}
		 int checkphone=LoginHelper.isPhoneNumberExist(jsondata.getBiller_phone());
		 if(checkphone>0) {
				return ResponseEntity.ok(new Response("06", "The phone number is already in use"));
		 }
		 if(jsondata.getBiller_phone().length()!=12) {
				return ResponseEntity.ok(new Response("06", "The phone number must be 12 characters"));
		 }

		if (!PasswordValidater.isPassword8(pass)) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must be atleast 8 characters", "", ""));
		}
		if (!PasswordValidater.isPasswordHasLowerCase(pass)) {
			return ResponseEntity
					.ok(new AuthProduceJson("08", "Passsword must have atleast one lower case character", "", ""));
		}

		if (!PasswordValidater.isPasswordHasLowerDigit(pass)) {
			return ResponseEntity
					.ok(new AuthProduceJson("08", "Passsword must have atleast a digit character", "", ""));
		}

		if (!PasswordValidater.isPasswordHasSpecial(pass)) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must have a special character", "", ""));
		}

		if (!PasswordValidater.isPasswordHasUpperCase(pass)) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must have a upper case character", "", ""));
		}

		int v_count = CheckUsers.checkPayer(email);
		String group_id = LoginHelper.getGroupId(email);

		if (tokenIssuer.equalsIgnoreCase("comp_user")) {
			comp_code = TokenManager.tokenIssuedCompCode(token);
			token = TokenManager.createToken(comp_code, email, "", "");
			String user_type = "payer";
			try {
				String sql = "UPDATE ebiller_auth SET password=? WHERE email='" + email + "' and comp_code='"
						+ comp_code + "' ";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, password);
				prep.executeUpdate();
				prep.close();
				Hashtable<String, Object> map = new Hashtable<String, Object>();
				map.put("messageCode", "00");
				map.put("message", "Changed password as user");
				map.put("user_type", user_type);
				map.put("logIn_type", "company_user");
				map.put("companyCode", comp_code);
				map.put("default", "DashBoard");
				map.put("menus", new UserGroupAndMenu().userAccessMenu(comp_code, group_id));
				System_Logs.log_system(jsondata.getEmail(), "", "payer signed up into the system:", "Success", "");
				return ResponseEntity.ok().header("Authorization", token).body(map);

			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new Response("02", "Could not update password"));
			}
		} else if (tokenIssuer.equalsIgnoreCase("account_principal")) {
			if (v_count == 0) {
				comp_code = Long.toString(new GeneralCodes().cust_code());
				token = TokenManager.createToken(comp_code, email, "", "");
				String user_type = "payer";
				try {
					String sql = "INSERT INTO biller_profile (personel_f_name,personel_l_name,email,biller_phone,comp_code,user_type,company_name,status)VALUES(?,?,?,?,?,?,?,?)";
					conn = DbManager.getConnection();
					PreparedStatement prep = conn.prepareStatement(sql);
					prep.setObject(1, jsondata.getFname());
					prep.setObject(2, jsondata.getLname());
					prep.setObject(3, email);
					prep.setObject(4, jsondata.getBiller_phone());
					prep.setObject(5, comp_code);
					prep.setObject(6, "payer");
					prep.setObject(7, jsondata.getCompany_name());
					prep.setObject(8, "Active");
					prep.execute();
					conn.close();
				} catch (Exception n) {
					System.out.println(n.getMessage());
					return ResponseEntity.ok(new Response("01",
							"The data was not successfully created due to systemtechnical error"));
				}
			
				
				registerPayerAuth(jsondata.getFname(), jsondata.getLname(), comp_code, email, password, "1",
						RoleHelper.getInsertedGroupId("Super Admin", comp_code),jsondata.getBiller_phone());
				interfaceDeafult(comp_code,
						"This is the default invoice terms and condtions,kindly update the default");
				Hashtable<String, Object> map = new Hashtable<String, Object>();
				map.put("messageCode", "00");
				map.put("message", "You have successfully  registered as Payer");
				map.put("user_type", user_type);
				map.put("loginvalue", "0");
				map.put("logIn_type", "company_principal");
				map.put("companyCode", comp_code);
				map.put("adminMenu", new UserGroupAndMenu().getAdminmenu());
				return ResponseEntity.ok().header("Authorization", token).body(map);
			} else {
				comp_code = TokenManager.tokenIssuedCompCode(token);
				String biller_code=TokenManager.userGroup(token);
				LoginHelper.updatePayer(jsondata.getFname(), jsondata.getLname(), jsondata.getPhone(),
						jsondata.getCompany_name(), email);
				token = TokenManager.createToken(comp_code, email, "", "");
				String user_type = "payer";
				registerPayerAuth(jsondata.getFname(), jsondata.getLname(), comp_code, email, password, "1",
						RoleHelper.getInsertedGroupId("Super Admin", comp_code),jsondata.getBiller_phone());
				interfaceDeafult(comp_code,
						"This is the default invoice terms and condtions,kindly update the default");
				Hashtable<String, Object> map = new Hashtable<String, Object>();
				map.put("messageCode", "00");
				map.put("message", "You have successfully  registered as invited Payer");
				map.put("user_type", user_type);
				map.put("loginvalue", "0");
				map.put("logIn_type", "company_principal");
				map.put("companyCode", comp_code);
				map.put("adminMenu", new UserGroupAndMenu().getAdminmenu());
				
				 BillerHelper.linkPayerToBiller2(biller_code, comp_code,"","Active");
				 System_Logs.log_system(jsondata.getEmail(), "", "User signed up into the system:", "Success", "");
				return ResponseEntity.ok().header("Authorization", token).body(map);
			}
		} else {
			Hashtable<String, Object> map = new Hashtable<String, Object>();
			map.put("messageCode", "05");
			map.put("message", "Unathorized user");
			return ResponseEntity.ok(map);
		}

	}

	@RequestMapping(value = "/v1/requestTobeBiller", method = RequestMethod.POST)
	public ResponseEntity<?> requestTobeBiller(HttpServletRequest request,
			@RequestHeader(name = "Authorization") String token, @RequestBody BillerAuthconsumeJson jsondata) {
		String comp_code = TokenManager.tokenIssuedCompCode(token);
		if (comp_code.equalsIgnoreCase("03")) {
			return ResponseEntity.ok(new AuthProduceJson("01", "Page has expired", "", ""));
		} else if (comp_code.equalsIgnoreCase("01")) {
			return ResponseEntity.ok(new AuthProduceJson("03", "An error occured,page might expired", "", ""));
		}
		try {
			conn = DbManager.getConnection();
			String sql = "UPDATE biller_profile SET status=? WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Pending");
			prep.setObject(2, comp_code);
			prep.execute();
			conn.close();
			return ResponseEntity.ok(new BillerLoginProduceJSon("00", "Your request to be a biller has been submitted successfully",token, "payer", "", ""));
		} catch (Exception n) {
			System.out.println(n.getMessage());
			return ResponseEntity.ok(new Response("01", "System technical error"));
		}

	}

	public void registerPayerAuth(String personel_f_name, String personel_l_name, String comp_code, String email,
			String password, String loginvalue, String group_id,String phone) {
		try {
			String sql = "INSERT INTO ebiller_auth (comp_code,email,password,loginvalue,group_id,personel_f_name,personel_l_name,phone)VALUES(?,?,?,?,?,?,?,?)";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			prep.setObject(2, email);
			prep.setObject(3, password);
			prep.setObject(4, loginvalue);
			prep.setObject(5, group_id);
			prep.setObject(6, personel_f_name);
			prep.setObject(7, personel_l_name);
			prep.setObject(8, phone);
			prep.execute();
			conn.close();
		} catch (Exception n) {
			n.printStackTrace();
		}
	}

	public static void interfaceDeafult(String comp_code, String terms) {
		try {
			String sql = "INSERT INTO interface_settings (comp_code,terms)VALUES(?,?) ON DUPLICATE KEY UPDATE terms=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			prep.setObject(2, terms);
			prep.setObject(3, terms);
			prep.execute();
			conn.close();
		} catch (Exception n) {
			n.printStackTrace();
		}
	}

	public void updateBillerStatus(String comp_code, String status) {
		try {
			conn = DbManager.getConnection();
			String sql = "UPDATE biller_profile SET status=? WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, status);
			prep.setObject(2, comp_code);
			prep.execute();
			conn.close();
		} catch (Exception n) {
			n.printStackTrace();
		}
	}

	public int checkIfEmailExist(String email) {
		int v_count = 0;
		try {
			String sql = "SELECT COUNT(*) AS V_COUNT FROM `ebiller_auth` WHERE email=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, email);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				v_count = rs.getInt("V_COUNT");
			}
			conn.close();
			return v_count;
		} catch (Exception e) {
			e.printStackTrace();
			return v_count;
		}
	}

	public int passwordFromDb(String username, String password) {
		int v_count = 0;
		try {
			String sql = "SELECT COUNT(*) V_COUNT FROM `ebiller_auth` WHERE email=? AND PASSWORD=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, username);
			prep.setObject(2, password);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				v_count = rs.getInt("V_COUNT");
			}
			conn.close();
			return v_count;
		} catch (Exception e) {
			e.printStackTrace();
			return v_count;
		}

	}

	public void updatePassowrd(String companyNo, String email, String password) {
		try {
			String sql = "UPDATE ebiller_auth SET password=? WHERE email='" + email + "' and comp_code='" + companyNo
					+ "' ";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, password);
			prep.executeUpdate();
			prep.close();
			prep.close();
		} catch (Exception e) {

		}
	}

}
