package com.stanbic.ebiller.auth;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.ini4j.Ini;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.sql.Connection;

import com.stanbic.Logs.System_Logs;
import com.stanbic.Roles.RoleHelper;
import com.stanbic.Roles.UserGroupAndMenu;
import com.stanbic.Team.Users;
import com.stanbic.Validation.PasswordValidater;
import com.stanbic.comm.CheckUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.EmailProduceJson;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.PasswordUtils;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class BillerAuth {

	Connection conn = null;

	@RequestMapping(value = "/v1/biller/setPassword", method = RequestMethod.POST)
	public ResponseEntity<?> billerCreatePass(HttpServletRequest request, @RequestHeader(name = "Authorization") String token, @RequestBody BillerAuthconsumeJson jsondata) {

		if (jsondata.getPassword().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("02", "Please enter all fields correctly", "", ""));
		}

		if (!PasswordValidater.isPassword8(jsondata.getPassword())) {
			return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must be atleast 8 characters", "", ""));
		}

		if (!PasswordValidater.isPasswordHasLowerCase(jsondata.getPassword())) {
			return ResponseEntity.ok(
					new AuthProduceJson("08", "Passsword must have atleast one lower case character [a-z ]", "", ""));
		}

		if (!PasswordValidater.isPasswordHasLowerDigit(jsondata.getPassword())) {
			return ResponseEntity
					.ok(new AuthProduceJson("08", "Passsword must have atleast a digit character [0-9 ]", "", ""));
		}

		if (!PasswordValidater.isPasswordHasSpecial(jsondata.getPassword())) {
			return ResponseEntity
					.ok(new AuthProduceJson("08", "Passsword must have a special character [^a-z0-9 ]", "", ""));
		}

		if (!PasswordValidater.isPasswordHasUpperCase(jsondata.getPassword())) {
			return ResponseEntity
					.ok(new AuthProduceJson("08", "Passsword must have a upper case character [A-Z ]", "", ""));
		}
		
		
		String email = TokenManager.tokenIssuedId(token);
		String identity=TokenManager.tokenIssuer(token);
		String comp_no = TokenManager.tokenIssuedCompCode(token);
		String group_id = LoginHelper.getGroupId(email);
		String password = PasswordUtils.generateSecurePassword(jsondata.getPassword(), ExternalFile.getSecretKey());
		String user_type = null;
		String biller_type = null;
		
		if(LoginHelper.getuserapprovedStatus(comp_no).equalsIgnoreCase("unapproved")) {
			return ResponseEntity.ok(new AuthProduceJson("06", "You have not been approved", "", ""));
		}
		
		Hashtable<String, Object> map = new Hashtable<String, Object>();
		int checkIfExist=checkIfbillerExist(email,comp_no);
		
		if(identity.equalsIgnoreCase("comp_user")) {
			try {
			String sql="UPDATE ebiller_auth set password=? WHERE email=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, password);
			prep.setObject(2, email); 
			prep.execute();
			conn.close();

			token = TokenManager.createToken(comp_no, email, "",  "comp_user");
			map.put("messageCode", "00");
			map.put("message", "You have successfully  registered as a team member");
			map.put("user_type", LoginHelper.getUserType(comp_no));
			map.put("loginvalue", "0");
			map.put("logIn_type","comp_user" );
			map.put("companyCode", comp_no);
			map.put("menus", new UserGroupAndMenu().userAccessMenu(comp_no, group_id));
			System_Logs.log_system(email, "", "User set password:", "Success", "");
			}catch(Exception n) {
				n.printStackTrace();
			}
			return ResponseEntity.ok().header("Authorization", token) .body(map);
			
		}else {
		
		if(checkIfExist==1) {
			
			System.out.println("Password step 2");
			
				try {
					String sql = "SELECT * FROM biller_profile WHERE email=?";
					conn = DbManager.getConnection();
					PreparedStatement prep = conn.prepareStatement(sql);
					prep.setObject(1, email);
					ResultSet rs = prep.executeQuery();
					while (rs.next()) {
						user_type = rs.getString("user_type");
						biller_type = rs.getString("biller_type");
						comp_no=rs.getString("comp_code");
					}
					conn.close();
					
					String sql2="UPDATE ebiller_auth set password=? WHERE email=?";
					conn = DbManager.getConnection();
					PreparedStatement prep2 = conn.prepareStatement(sql2);
					prep2.setObject(1, password);
					prep2.setObject(2, email); 
					prep2.execute();
					conn.close();
					
					new PayerAuth().updateBillerStatus(comp_no, "Active");
				 token = TokenManager.createToken(comp_no, email, "", group_id);
					map.put("messageCode", "00");
					map.put("message", "You have been successfully  registered as Biller");
					map.put("user_type", user_type);
					map.put("loginvalue", "0");
					map.put("biller_type", biller_type);
					map.put("adminMenu",new UserGroupAndMenu().getAdminmenu() );
					System_Logs.log_system(email, "", "User set password:", "Success", "");
					return ResponseEntity.ok().header("Authorization", token) .body(map);
				} catch (Exception n) {
					n.printStackTrace();
					map.put("messageCode", "02");
					map.put("message", "Could not log user.Error from the system");
					return ResponseEntity.ok(map);
				}	
				
		}else {
			
			return ResponseEntity.ok(new AuthProduceJson("07", "User not found in the system", "", ""));
		}
		}
	}
	
	
	
	@RequestMapping(value = "/v1/sendEmailBiller", method = RequestMethod.POST)
	  public ResponseEntity<?> sendEmailBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillerAuthconsumeJson jsondata) {
		 String id=TokenManager.tokenIssuedCompCode(token);
		try {
			if(jsondata.getEmail().isEmpty()) {
				 return ResponseEntity.ok(new AuthProduceJson("06","Email address cannot be empty","",""));
			}
			 String comp_code= CheckUsers.getBillerCode(jsondata.getEmail());
			 if(comp_code==null) {
				 return ResponseEntity.ok(new AuthProduceJson("07","The email address does not exist","",""));
			 }
			 sendEmailForBiller(jsondata.getEmail(),comp_code,id);
		}catch(Exception e) {
			e.printStackTrace();
			  return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
		System_Logs.log_system(jsondata.getEmail(), "", "User sent account creation email:", "Success", "");
		return ResponseEntity.ok(new AuthProduceJson("00","An email has been successfully sent to the email:"+jsondata.getEmail(),"",""));
	}
	
	public static void sendEmailForBiller(String email,String comp_code,String id) {
		try {
			String date =TimeManager.getEmailTime();
			String token = TokenManager.createToken2(comp_code, email, "",  "account_principal");
			 String path=PathUrl.getBillerAppPath("biller");
			 String url=path +"account-opening/activate-account" + "?token=" + token;
		  String subject="Stanbic E-biller";
		  VelocityContext vc = new VelocityContext();
          vc.put("login_link", url);
          vc.put("date", date);
          String email_template = EmailManager.email_message_template(vc,"welcome_self.vm");
			EmailManager.send_mail(email,email_template,subject,id,"Biller onboarding");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping(value = "/v1/getBillerFirstName", method = RequestMethod.POST)
	  public ResponseEntity<?> getBillerFirstName(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillerAuthconsumeJson jsondata) {
		 String firstName="";
		String email=TokenManager.tokenIssuedId(token);
		
		String identity=TokenManager.tokenIssuer(token);
		if(email.equalsIgnoreCase("03")){
			 return ResponseEntity.ok(new AuthProduceJson("02","Page has expired","",""));
		}
		else if(email.equalsIgnoreCase("01")) {
			 return ResponseEntity.ok(new AuthProduceJson("02","An error occured,page might expired","",""));
		}
		String teamName=getTeamFirstName(email);
		try {
			 if(identity.equalsIgnoreCase("comp_user")) {
				  firstName=teamName;
			  }else {
			  conn=DbManager.getConnection();
			String sql="SELECT personel_f_name FROM biller_profile WHERE email=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  firstName=rs.getString("personel_f_name");
			  }
			  conn.close();
			  }
			  return ResponseEntity.ok(new EmailProduceJson("00","First name found","",firstName,email));
		}catch(Exception n) {
			n.printStackTrace();
			  return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
		
		
	}

	public int confirmPassword(String username, String password) {
		int v_count = 0;
		try {
			String sql = "SELECT COUNT(*) AS V_COUNT FROM `ebiller_auth` WHERE email=? AND PASSWORD=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, username);
			prep.setObject(2, password);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				v_count = rs.getInt("");
			}
			conn.close();
			return v_count;
		} catch (Exception e) {
			e.printStackTrace();
			return v_count;
		}

	}
	
	public  String getTeamFirstName(String email) {
		String name="";
		try {
			  conn=DbManager.getConnection();
			String sql="SELECT personel_f_name FROM ebiller_auth WHERE email=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  name=rs.getString("personel_f_name");
			  }
			  conn.close();
		}catch(Exception n) {
			n.printStackTrace();
		}
		return name;
	}
	
	public void updatePassowrd(String companyNo,String email,String password) {
		try {
			String sql="UPDATE ebiller_auth SET password=? WHERE email='"+email+"' and comp_code='"+companyNo+"' ";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, password);
			prep.executeUpdate();
			prep.close();
			prep.close();
		}catch(Exception e) {
			
		}
	}
	
	public int checkIfbillerExist(String email,String companyCode) {
		int v_count=0;
		try {
			String sql="SELECT COUNT(*) AS V_COUNT FROM `biller_profile` WHERE  email=? AND comp_code=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, email);
			prep.setObject(2, companyCode);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				v_count=rs.getInt("V_COUNT");
			}
			conn.close();
			return v_count;
		}catch(Exception e) {
			e.printStackTrace();
			return v_count;
		}
	}
	
	

}
