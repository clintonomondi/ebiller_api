package com.stanbic.ebiller.auth;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Billers.BillerHelper;
import com.stanbic.KPLC.QueryBulkBillsService;
import com.stanbic.comm.CheckUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;

@SpringBootApplication
@RequestMapping("/api")
public class ClosedBillerAuth {
	 Connection conn = null;
	 
	 @RequestMapping(value = "/v1/billerSendEmail", method = RequestMethod.POST)
	  public ResponseEntity<?> billerSendEmail(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillerAuthconsumeJson jsondata) {
		 if(jsondata.getEmail().isEmpty() || jsondata.getDepartment_id().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("00","Please provide all email and department","",""));
		 }
		 String department=LoginHelper.getDepartmentName(jsondata.getDepartment_id());
		 String biller_code = TokenManager.tokenIssuedCompCode(token);
		 String email = TokenManager.tokenIssuedId(token);
		 String name=AccountHelper.getName(email);
		 String date=TimeManager.getEmailTime();
		int v_count= CheckUsers.checkUser(jsondata.getEmail());
		if(v_count>0) {
			BillerHelper.linkPayerToBiller2(biller_code, LoginHelper.getComp_Code(jsondata.getEmail()),jsondata.getDepartment_id(),"Invited");
			token=TokenManager.createToken2(jsondata.getDepartment_id(), jsondata.getEmail(), "", biller_code);
			String path=PathUrl.getBillerAppPath("biller");
				String url= path+"login/payer-approval"+"?token=" + token;
				  String subject="Stanbic E-biller";
				  VelocityContext vc = new VelocityContext();
		         vc.put("login_link", url);
		         vc.put("name", name);
		         vc.put("date", date);
		         vc.put("department",department);
		         String email_template = EmailManager.email_message_template(vc,"account_temp.vm");
					EmailManager.send_mail(jsondata.getEmail(),email_template,subject,name,"Ebiller invitation");
					return ResponseEntity.ok(new AuthProduceJson("00","An email has been successfully sent to  email:"+jsondata.getEmail(),"",""));
		  }
		else {
		String payer_code=LoginHelper.saveInvitedPayer(jsondata.getEmail(), biller_code, jsondata.getDepartment_id());
		token=TokenManager.createToken2(payer_code, jsondata.getEmail(), biller_code, "account_principal");
		String path=PathUrl.getBillerAppPath("biller");
		String url= path+"login/register"+"?token=" + token;
		  String subject="Ebiller Account";
		  VelocityContext vc = new VelocityContext();
         vc.put("login_link", url);
         vc.put("name", name);
         vc.put("department",department );
         vc.put("date", date);
         String email_template = EmailManager.email_message_template(vc,"account_temp.vm");
			EmailManager.send_mail(jsondata.getEmail(),email_template,subject,name,"Ebiller invitation");
			return ResponseEntity.ok(new AuthProduceJson("00","An email has been successfully sent to :"+jsondata.getEmail(),"",""));
		}
		
	}
	 
	 @RequestMapping(value = "/v1/acceptTobePayer", method = RequestMethod.POST)
	  public ResponseEntity<?> acceptTobePayer(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillerAuthconsumeJson jsondata) {
		 String biller_code = TokenManager.tokenIssuer(token);
		 String payer_email=TokenManager.tokenIssuedId(token);
		 String department_id=TokenManager.tokenIssuedCompCode(token);
		 String payer_code="";
		 	 
		 try {
			 String sql="SELECT comp_code FROM biller_profile WHERE email=?";
			 conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, payer_email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  payer_code=rs.getString("comp_code");
			  }
			  conn.close();
			 
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		 }
		 BillerHelper.linkPayerToBiller2(biller_code, payer_code,department_id,"Active");
		 return ResponseEntity.ok(new AuthProduceJson("00", "Your are now linked to your invited biller", "", ""));
	 }
	 
	 
	 @RequestMapping(value = "/v1/inviteMultiple", method = RequestMethod.POST)
	  public ResponseEntity<?> inviteMultiple(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillerAuthconsumeJson jsondata) {
		 if(jsondata.getBase64Excel().isEmpty() || jsondata.getDepartment_id().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide  email and department","",""));
		 }
		 try {
		 String biller_code = TokenManager.tokenIssuedCompCode(token);
		 String email = TokenManager.tokenIssuedId(token);
		 String name=AccountHelper.getName(biller_code);
		 
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			LocalDateTime now = LocalDateTime.now();
			String time = dtf.format(now);
			String filename = time.replace("/", "_") + biller_code;
			String path = ExternalFile.getExcelPath()+AccountHelper.generateRandomInt(4) + filename + ".xlsx";
			FileManager.saveImage(jsondata.getBase64Excel(), path);
			
			 Vector emailsHolder=QueryBulkBillsService.read(path);
			 Thread t = new Thread() {
					public void run() {
						LoginHelper.invitePayers(emailsHolder,biller_code,email,jsondata.getDepartment_id());
					}
				};
				t.start(); 
				return ResponseEntity.ok(new AuthProduceJson("00", "Email uploaded successfully", "", ""));
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		 }
		 
	 }
	 
}
