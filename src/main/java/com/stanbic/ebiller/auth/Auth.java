package com.stanbic.ebiller.auth;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;
import org.ini4j.Ini;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;

import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.Logs.System_Logs;
import com.stanbic.Roles.UserGroupAndMenu;
import com.stanbic.Roles.UsersAccessMenuRespModel;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.PasswordUtils;
import com.stanbic.comm.TokenManager;

@SpringBootApplication
@RequestMapping("/api")
public class Auth {

	Connection conn = null;
	static Logger log = Logger.getLogger(Auth.class.getName());

	@RequestMapping(value = "/v1/bankLogin", method = RequestMethod.POST)
	public ResponseEntity<?> newOrder(@RequestBody AuthConsumeJson jsondata) {
		
		
       
		if(LoginHelper.isBankUserFrozen(jsondata.getEmail())) {
			EmailAlert.logger(jsondata.getEmail(), "Access to system failed", log);
        	return ResponseEntity.ok(new AuthProduceJson("07", "Username not found  on ebiller system ", "", "",""));
        }
        
		String userExist = null;
		String username = jsondata.getEmail();
		String password = jsondata.getPassword();
		String ldapPath = "";
		String name="";
		String group_id="";
		String user_group="";
		String email="";
		HashMap<String, Object> map2 = new HashMap<String, Object>();
		
		//**********Production************
//		
		try {
			Ini ini;
			ini = new Ini(new File(GeneralCodes.configFilePath()));
			ldapPath = ini.get("header", "ldapPath");
		// userExist = Ldap.AuthLDAP(username, password, ldapPath);
			Hashtable<String, String> map = new Hashtable<String, String>();
			map.put("username", username);
		map.put("password", password);
			map.put("ldapDomain", ldapPath);
			String response = GeneralCodes.encryptedPostRequest(map,ExternalFile.getUsbNtken(), "JSON");
			JSONObject obj = new JSONObject(response);

			userExist = obj.getString("responseCode");
	} catch (Exception e) {
			e.printStackTrace();
		}

		if (userExist.equalsIgnoreCase("00")) {
			int v_localExist = checkIfExistInLocal(username);
			if (v_localExist == 1) {
				loginCheck(username,"LOGIN","SUCCESSFULL LOGGED IN","BANK","BANK");
				try {
					String sql="SELECT OTHER_NAMES,USERNAME,ID,SURNAME,EMAIL,group_id,\r\n" + 
							"(SELECT NAME FROM bank_groups B WHERE B.id=A.group_id)user_group\r\n" + 
							" FROM bank_users A WHERE  USERNAME=?";
					conn=DbManager.getConnection();
					PreparedStatement prep=conn.prepareStatement(sql);
					prep.setObject(1, username);
					ResultSet rs=prep.executeQuery();
					while(rs.next()) {
						name=rs.getString("OTHER_NAMES");
						user_group=rs.getString("user_group");
						email=rs.getString("email");
						group_id=rs.getString("group_id");
					}
					conn.close();
				}catch(Exception e) {
					e.printStackTrace();
				}
				System_Logs.log_system(username, "", "Bank User Logged in successfully:", "Success", "");

				String token = TokenManager.createToken(username, user_group, "", email);
				map2.put("messageCode", "00");
				map2.put("message", "Correct username and password");
				map2.put("username", username);
				map2.put("name", name);
				map2.put("user_group", user_group);
				if(user_group.equalsIgnoreCase("Admin")) {
					map2.put("menus", new UserGroupAndMenu().bankAccessAllMenu( ));
				}else {
				map2.put("menus", new UserGroupAndMenu().bankAccessMenu( group_id));
				}
				return ResponseEntity.ok().header("Authorization", token) .body(map2);
			} else {
				EmailAlert.logger(jsondata.getEmail(), "Access to system failed", log);
				loginCheck(username,"LOGIN","USER DOES NOT REGISTERED IN THE ONBORDING SYSTEM","BANK","BANK") ;
				System_Logs.log_system(username, "", "Bank User attempt login:", "Failed", "");
				return ResponseEntity.ok(new AuthProduceJson("07", "User not found in the ebiller system", "", "",""));
			}
			
		} else {
			EmailAlert.logger(jsondata.getEmail(), "Access to system failed", log);
			loginCheck(username,"LOGIN","USER CREDENTIALS ARE INCORRECT","BANK","BANK") ;
			System_Logs.log_system(username, "", "Bank User attempt login:", "Failed", "");
			return ResponseEntity.ok(new AuthProduceJson("02", "User entered wrong login details", "", "",""));
		}
//	
		
	}

	public int checkIfExistInLocal(String username) {
		int v_count = 0;
		try {
			String sql = "SELECT COUNT(*) AS V_COUNT FROM `bank_users` WHERE USERNAME=? AND status=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, username); 
			prep.setObject(2, "Active");
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				v_count = rs.getInt("V_COUNT");
			}
			conn.close();
			return v_count;
		} catch (Exception e) {
			e.printStackTrace();
			return v_count;
		}
		
	
	}

	public String checkIfBankUserExistBackEnd(String email, String password) {
		String name = null;
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT * FROM bank_auth WHERE email=? AND password=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, email);
			prep.setObject(2, password);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				name = rs.getString("name");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return name;
	}

	public String checkIfEmailExist(String email) {
		String name = null;
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT * FROM bank_auth WHERE email=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, email);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				name = rs.getString("name");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return name;
	}
	
	public void loginCheck(String username,String section,String description,String user_type,String companycode) {
		try {
			String sql="INSERT INTO login_log(USERNAME,SECTION,DESCRIPTION,USER_TYPE,COMPANY_CODE) VALUES(?,?,?,?,?)";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, username);
			prep.setObject(2, section);
			prep.setObject(3, description); 
			prep.setObject(4, user_type);
			prep.setObject(5, companycode);
			prep.execute();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
