package com.stanbic.ebiller.auth;

public class BankAuthProduceJSon {
	String messageCode = "";
	String message = "";
	String token="";
	String name="";
	String title=null;
	
	
	
	public BankAuthProduceJSon(String messageCode, String message, String token, String name,String title) {
	
		this.messageCode = messageCode;
		this.message = message;
		this.token = token;
		this.name = name;
		this.title=title;
	}
	
	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
