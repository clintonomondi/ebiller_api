package com.stanbic.ebiller.fetchBills;

public class BillerInfoConsumeJson {
String messageCode=null;
String message=null;
String token=null;
String biller_code=null;
String account_length=null;



public String getMessageCode() {
	return messageCode;
}
public void setMessageCode(String messageCode) {
	this.messageCode = messageCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getAccount_length() {
	return account_length;
}
public void setAccount_length(String account_length) {
	this.account_length = account_length;
}
public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getBiller_code() {
	return biller_code;
}
public void setBiller_code(String biller_code) {
	this.biller_code = biller_code;
}

}
