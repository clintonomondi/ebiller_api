package com.stanbic.ebiller.fetchBills;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.ini4j.Ini;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;
import com.stanbic.ebiller.auth.PayerAuth;

@SpringBootApplication
@RequestMapping("/api")
public class FetchBillsService {
	static Logger log = Logger.getLogger(FetchBillsService.class.getName());
	Connection conn;
	@RequestMapping(value = "/v1/fetchBills", method = RequestMethod.POST)
	 public ResponseEntity<?> fetchBills(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillsModel jsondata) {
		if(jsondata.getBillerCode().isEmpty() || jsondata.getBillerCode().equalsIgnoreCase("")) {
			 return ResponseEntity.ok(new FetchBillsResponse("","","","","","06","Please provide all fields"));
		}
		
		String user=TokenManager.tokenIssuedCompCode(token);
		try {
		 Hashtable<String,String>map=new Hashtable<String,String>();
		 map.put("AccountRefNo",jsondata.getAccountRefNo());
		 map.put("BillerCode",jsondata.getBillerCode());
		 map.put("BillerUrl","KPLC_URL");
		 EmailAlert.loggAccountValidation("REQUEST",user, map.toString(), log);
		 String response=GeneralCodes.encryptedPostRequest(map, ExternalFile.getUsbBaseUrl(), "JSON");
		
		 JSONObject obj = new JSONObject(response);
         String jsonResponse = obj.toString();
       Gson gson = new GsonBuilder().create();
       FetchBillsResponse res = gson.fromJson(jsonResponse, FetchBillsResponse.class);
     	EmailAlert.loggAccountValidation("RESPONSE",user, response, log);
       
		 if(res.getResponseCode().isEmpty() || res.getResponseCode().equalsIgnoreCase("")) {
			 return ResponseEntity.ok(new FetchBillsResponse(""," ","","","","02","Invalid Account Number"));
		 }
		 else if(res.getResponseCode().equalsIgnoreCase("0")) {
			 return ResponseEntity.ok(new FetchBillsResponse(res.getAccountRefNo(), res.getBillDescription(), res.getBillBalance().replaceAll(",", ""), res.getDueDate(),
						res.getOtherInfomation(), res.getResponseCode(), res.getResponseMessage()));
		 }
		 else {
			 return ResponseEntity.ok(new FetchBillsResponse("","","","","","02",res.getResponseMessage()));
		 }
		 
	 }catch(Exception n) {
		 n.printStackTrace();
			 EmailAlert.loggAccountValidation("RESPONSE",user, "Error Code 500 occured", log);
		 return ResponseEntity.ok(new FetchBillsResponse("","","","","","02","System technical error"));
	 }
	
		//return ResponseEntity.ok(new FetchBillsResponse(jsondata.getAccountRefNo(),"Oliver Opanga ","500.36","12/12/2019","KPLC Biils","0","Success"));
	}
}
