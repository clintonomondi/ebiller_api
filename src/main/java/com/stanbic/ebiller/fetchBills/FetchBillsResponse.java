package com.stanbic.ebiller.fetchBills;

public class FetchBillsResponse {

	String AccountRefNo = null;
	String BillDescription = null;
	String BillBalance = null;
	String DueDate = null;
	String OtherInfomation = null;
	String ResponseCode = null;
	String ResponseMessage = null;
	
	
	
	public FetchBillsResponse(String accountRefNo, String billDescription, String billBalance, String dueDate,
			String otherInfomation, String responseCode, String responseMessage) {
		AccountRefNo = accountRefNo;
		BillDescription = billDescription;
		BillBalance = billBalance;
		DueDate = dueDate;
		OtherInfomation = otherInfomation;
		ResponseCode = responseCode;
		ResponseMessage = responseMessage;
	}
	public String getAccountRefNo() {
		return AccountRefNo;
	}
	public void setAccountRefNo(String accountRefNo) {
		AccountRefNo = accountRefNo;
	}
	public String getBillDescription() {
		return BillDescription;
	}
	public void setBillDescription(String billDescription) {
		BillDescription = billDescription;
	}
	public String getBillBalance() {
		return BillBalance;
	}
	public void setBillBalance(String billBalance) {
		BillBalance = billBalance;
	}
	public String getDueDate() {
		return DueDate;
	}
	public void setDueDate(String dueDate) {
		DueDate = dueDate;
	}
	public String getOtherInfomation() {
		return OtherInfomation;
	}
	public void setOtherInfomation(String otherInfomation) {
		OtherInfomation = otherInfomation;
	}
	public String getResponseCode() {
		return ResponseCode;
	}
	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}
	public String getResponseMessage() {
		return ResponseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		ResponseMessage = responseMessage;
	}
	
	
	

}
