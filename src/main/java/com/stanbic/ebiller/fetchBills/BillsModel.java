package com.stanbic.ebiller.fetchBills;

public class BillsModel {
	String accountRefNo=null;
	String billerCode=null;
	String billerUrl=null;
	public String getAccountRefNo() {
		return accountRefNo;
	}
	public void setAccountRefNo(String accountRefNo) {
		this.accountRefNo = accountRefNo;
	}
	public String getBillerCode() {
		return billerCode;
	}
	public void setBillerCode(String billerCode) {
		this.billerCode = billerCode;
	}
	public String getBillerUrl() {
		return billerUrl;
	}
	public void setBillerUrl(String billerUrl) {
		this.billerUrl = billerUrl;
	}
	
	
	
}
