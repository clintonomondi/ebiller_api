package com.stanbic.ebiller.onboarding;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Eslip.EslipService;
import com.stanbic.Eslip.MyESlipsProduceJSon;
import com.stanbic.Logs.System_Logs;
import com.stanbic.Responses.Response;
import com.stanbic.Roles.RoleHelper;
import com.stanbic.Team.Users;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.TokenManager;
import com.stanbic.dropdown.country.CountryModel;
import com.stanbic.ebiller.auth.AuthConsumeJson;
import com.stanbic.ebiller.auth.AuthProduceJson;
import com.stanbic.ebiller.auth.BillerAuth;
import com.stanbic.ebiller.auth.LoginHelper;
import com.stanbic.ebiller.auth.PayerAuth;

@SpringBootApplication
@RequestMapping("/api")
public class OnboardingBiller {
	Connection conn = null;

	@RequestMapping(value = "/v1/onboardOpenBiller", method = RequestMethod.POST)
	public ResponseEntity<?> consolidatedBillerBoarding(HttpServletRequest request,
			@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		if (jsondata.getBiller_location().isEmpty() || jsondata.getCompany_name().isEmpty()
				|| jsondata.getPersonelFirstname().isEmpty() || jsondata.getPersonelLastname().isEmpty()
				|| jsondata.getEmail().isEmpty() || jsondata.getBranch_id().isEmpty()
				|| jsondata.getCharge_type().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide all fields", "", ""));

		}
		String user=TokenManager.tokenIssuedCompCode(token);
		if (jsondata.getPrefix().length() != 4) {
			return ResponseEntity.ok(new Response("06", "The prefix must be four characters"));
		}
		if (LoginHelper.isPrefixExist(jsondata.getPrefix()) > 0) {
			return ResponseEntity.ok(new Response("06", "The prefix is already in use by another biller"));
		}
		if (jsondata.getBiller_phone().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter valid phone number"));
		}
		if (jsondata.getBiller_phone().substring(1).length() != 12) {
			return ResponseEntity.ok(new Response("06", "The phone number must be 12 characters"));
		}
		int checkphone = LoginHelper.isPhoneNumberExist(jsondata.getBiller_phone().substring(1));
		if (checkphone > 0) {
			return ResponseEntity.ok(new Response("06", "The phone number is already in use"));
		}
		
		if (jsondata.getCharge_type().equalsIgnoreCase("TRANSACTIONAL")
				&& jsondata.getAmount_to_charge().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter amount to charge"));
		}
		if (jsondata.getCharge_type().equalsIgnoreCase("PERCENTAGE") && jsondata.getPercentage().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter percentage to charge"));
		}

		String id = TokenManager.tokenIssuedCompCode(token);
		int checkBillerExist = checkIfBillerEmilExist(jsondata.getEmail());
		if (checkBillerExist == 0) {
			String companyCode = Long.toString(new GeneralCodes().cust_code());

			try {
				conn = DbManager.getConnection();
				String sql = "INSERT INTO `biller_profile`(CREATED_BY,UPDATED_BY,BILLER_LOCATION,BILLER_PHONE,COMPANY_NAME,PERSONEL_F_NAME,PERSONEL_L_NAME,EMAIL,\r\n"
						+ "COMP_CODE,USER_TYPE,STATUS,alias,sector,business_description,employee_no,stb_acc_name,stb_acc_no,branch_id,biller_month,\r\n"
						+ "charge_type,prefix,biller_type, company_website, company_customer_care,percentage,amount_to_charge,currency,vat,kra,paybill) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, id);
				prep.setObject(2, id);
				prep.setObject(3, jsondata.getBiller_location());
				prep.setObject(4, jsondata.getBiller_phone().substring(1));
				prep.setObject(5, jsondata.getCompany_name());
				prep.setObject(6, jsondata.getPersonelFirstname());
				prep.setObject(7, jsondata.getPersonelLastname());
				prep.setObject(8, jsondata.getEmail());
				prep.setObject(9, companyCode);
				prep.setObject(10, "biller");
				prep.setObject(11, "unapproved");
				prep.setObject(12, jsondata.getAlias());
				prep.setObject(13, jsondata.getSector());
				prep.setObject(14, jsondata.getBusiness_description());
				prep.setObject(15, jsondata.getEmployee_no());
				prep.setObject(16, jsondata.getStb_acc_name());
				prep.setObject(17, jsondata.getStb_acc_no());
				prep.setObject(18, jsondata.getBranch_id());
				prep.setObject(19, jsondata.getBiller_month());
				prep.setObject(20, jsondata.getCharge_type());
				prep.setObject(21, jsondata.getPrefix());
				prep.setObject(22, "Open");
				prep.setObject(23, jsondata.getWebsite());
				prep.setObject(24, jsondata.getCustomer_care());
				prep.setObject(25, jsondata.getPercentage());
				prep.setObject(26, jsondata.getAmount_to_charge());
				prep.setObject(27, jsondata.getCurrency());
				prep.setObject(28, jsondata.getVat());
				prep.setObject(29, jsondata.getKra());
				prep.setObject(30, jsondata.getPaybill());
				prep.execute();
				conn.close();
				new Users().addCompanyUsers(jsondata.getPersonelFirstname(), jsondata.getPersonelLastname(),
						RoleHelper.getInsertedGroupId("Super Admin", companyCode), companyCode, jsondata.getEmail(), "",
						"", jsondata.getBiller_phone().substring(1), "SELF", "");
				new PayerAuth().interfaceDeafult(companyCode,
						"This is the default invoice terms and conditions,kindly update the default");
//				BillerAuth.sendEmailForBiller(jsondata.getEmail(), companyCode, id);
				System_Logs.log_system(user, companyCode, "Bank User added open biller:", "Success", "");
				return ResponseEntity.ok(new Response("00", "Biller registered  successfully"));
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new Response("02", "System technical error"));
			}

		} else {

			return ResponseEntity.ok(new Response("04", "The Biller already exist in the system"));

		}

	}

	@RequestMapping(value = "/v1/onboardClosedBiller", method = RequestMethod.POST)
	public ResponseEntity<?> onboardClosedBiller(HttpServletRequest request,
			@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		String user=TokenManager.tokenIssuedCompCode(token);
		try {
			if (jsondata.getCompany_name().isEmpty() || jsondata.getPersonelFirstname().isEmpty()
					|| jsondata.getPersonelLastname().isEmpty() || jsondata.getEmail().isEmpty()
					|| jsondata.getBranch_id().isEmpty() || jsondata.getCustomerNumber().isEmpty()) {
				return ResponseEntity.ok(new AuthProduceJson("06", "Please provide all fields", "", ""));
			}

			if (jsondata.getPrefix().length() != 4) {
				return ResponseEntity.ok(new Response("06", "The prefix must be four characters"));
			}
			if (LoginHelper.isPrefixExist(jsondata.getPrefix()) > 0) {
				return ResponseEntity.ok(new Response("06", "The prefix is already in use by another biller"));
			}

			if (jsondata.getBiller_phone().isEmpty()) {
				return ResponseEntity.ok(new Response("06", "Please enter valid phone number"));
			}
			if (jsondata.getBiller_phone().substring(1).length() != 12) {
				return ResponseEntity.ok(new Response("06", "The phone number must be 12 characters"));
			}
			int checkphone = LoginHelper.isPhoneNumberExist(jsondata.getBiller_phone().substring(1));
			if (checkphone > 0) {
				return ResponseEntity.ok(new Response("06", "The phone number is already in use"));
			}

			String id = TokenManager.tokenIssuedCompCode(token);
			int checkBillerExist = checkIfBillerEmilExist(jsondata.getEmail());
			if (checkBillerExist == 0) {
				String companyCode = Long.toString(new GeneralCodes().cust_code());

				JSONObject data = new JSONObject(jsondata);

				conn = DbManager.getConnection();
				String sql = "INSERT INTO `biller_profile`(CREATED_BY,UPDATED_BY,BILLER_LOCATION,BILLER_PHONE,COMPANY_NAME,PERSONEL_F_NAME,PERSONEL_L_NAME,EMAIL,\r\n"
						+ "COMP_CODE,USER_TYPE,STATUS,alias,sector,business_description,employee_no,stb_acc_name,stb_acc_no,branch_id,biller_month,\r\n"
						+ "charge_type,prefix,biller_type, company_website, company_customer_care,percentage,amount_to_charge,currency,vat,kra,phone,physical_address,postal_address,company_email,customerNumber,paybill) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, id);
				prep.setObject(2, id);
				prep.setObject(3, jsondata.getBiller_location());
				prep.setObject(4, jsondata.getBiller_phone().substring(1));
				prep.setObject(5, jsondata.getCompany_name());
				prep.setObject(6, jsondata.getPersonelFirstname());
				prep.setObject(7, jsondata.getPersonelLastname());
				prep.setObject(8, jsondata.getEmail());
				prep.setObject(9, companyCode);
				prep.setObject(10, "biller");
				prep.setObject(11, "unapproved");
				prep.setObject(12, jsondata.getAlias());
				prep.setObject(13, jsondata.getSector());
				prep.setObject(14, jsondata.getBusiness_description());
				prep.setObject(15, jsondata.getEmployee_no());
				prep.setObject(16, jsondata.getStb_acc_name());
				prep.setObject(17, jsondata.getStb_acc_no());
				prep.setObject(18, jsondata.getBranch_id());
				prep.setObject(19, jsondata.getBiller_month());
				prep.setObject(20, jsondata.getCharge_type());
				prep.setObject(21, jsondata.getPrefix());
				prep.setObject(22, "Closed");
				prep.setObject(23, jsondata.getWebsite());
				prep.setObject(24, jsondata.getCustomer_care());
				prep.setObject(25, jsondata.getPercentage());
				prep.setObject(26, jsondata.getAmount_to_charge());
				prep.setObject(27, jsondata.getCurrency());
				prep.setObject(28, jsondata.getVat());
				prep.setObject(29, jsondata.getKra());
				prep.setObject(30, jsondata.getPhone());
				prep.setObject(31, jsondata.getPhysical_address());
				prep.setObject(32, jsondata.getPostal_address());
				prep.setObject(33, jsondata.getCompany_email());
				prep.setObject(34, jsondata.getCustomerNumber());
				prep.setObject(35, jsondata.getPaybill());
				prep.execute();
				conn.close();

				Thread t = new Thread() {
					public void run() {
						new Users().addCompanyUsers(jsondata.getPersonelFirstname(), jsondata.getPersonelLastname(),
								RoleHelper.getInsertedGroupId("Super Admin", companyCode), companyCode,
								jsondata.getEmail(), "", "", jsondata.getBiller_phone().substring(1), "SELF", "");
						new PayerAuth().interfaceDeafult(companyCode,
								"This is the default invoice terms and conditions,kindly update the default");
						SaveData(data.toString(), companyCode);
//						BillerAuth.sendEmailForBiller(jsondata.getEmail(), companyCode, id);
					}
				};
				t.start();
				System_Logs.log_system(user, companyCode, "Bank User added open biller:", "Success", "");
				return ResponseEntity.ok(new Response("00", "Biller registered  successfully"));
			} else {

				return ResponseEntity.ok(new Response("04", "The Biller already exist in the system"));

			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}

	}

	public void SaveData(String jsondata, String comp_code) {
		String service_line = "";
		String account = "";
		String email = "";
		String currency = "";
		String service_code = "";
		Vector billing_line_vec = new Vector();
		Vector billing_product_vec = new Vector();

		try {
			JSONObject billing_line_obj = new JSONObject(jsondata);
			JSONArray billing_line = billing_line_obj.getJSONArray("billing_line");
			for (int i = 0; i < billing_line.length(); i++) {
				JSONObject billing_object = billing_line.getJSONObject(i);
				Vector holder = new Vector();
				service_code = comp_code + "_" + EslipService.genRandom();
				holder.addElement(billing_object.getString("service_line"));
				holder.addElement(billing_object.getString("account"));
				holder.addElement(billing_object.getString("email"));
				holder.addElement(billing_object.getString("currency"));
				holder.addElement(comp_code);
				holder.addElement(service_code);
				billing_line_vec.addElement(holder);

				JSONArray billing_product = billing_object.getJSONArray("product_details");
				for (int j = 0; j < billing_product.length(); j++) {
					JSONObject poduct_object = billing_product.getJSONObject(j);
					Vector holder2 = new Vector();
					holder2.addElement(poduct_object.getString("product_name"));
					holder2.addElement(poduct_object.getString("product_code"));
					holder2.addElement(service_code);
					billing_product_vec.addElement(holder2);
				}
			}

			if (OboardingHelper.SaveBillingLine(billing_line_vec)) {
				OboardingHelper.SaveBillingProduct(billing_product_vec);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@RequestMapping(value = "/v1/getBiller", method = RequestMethod.POST)
	public ResponseEntity<?> getBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,
			@RequestBody OnboadingBillerConsumerJson jsondata) {
		if (jsondata.getEmail().isEmpty() || jsondata.getComp_code().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide both company code and email address"));
		}
		List<OnboadingBillerConsumerJson> model = new ArrayList<OnboadingBillerConsumerJson>();
		try {
			String sql = "SELECT *,\r\n"
					+ "(SELECT branch_name FROM `country_branches` B WHERE B.id=A.branch_id)branch,\r\n"
					+ "(SELECT country_name FROM `countries` C WHERE C.id=(SELECT country_id FROM `country_branches` D WHERE D.id=A.branch_id))country\r\n"
					+ "FROM `biller_profile` A WHERE comp_code=? AND email=?\r\n" + "";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getComp_code());
			prep.setObject(2, jsondata.getEmail());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				OnboadingBillerConsumerJson data = new OnboadingBillerConsumerJson();
				data.alias = rs.getString("alias");
				data.amount_to_charge = rs.getString("amount_to_charge");
				data.company_name = rs.getString("company_name");
				data.biller_location = rs.getString("biller_location");
				data.email = rs.getString("email");
				data.phone = rs.getString("phone");
				data.personelFirstname = rs.getString("personel_f_name");
				data.personelLastname = rs.getString("personel_l_name");
				data.biller_type = rs.getString("biller_type");
				data.user_type = rs.getString("user_type");
				data.created_by = rs.getString("created_by");
				data.updated_by = rs.getString("updated_by");
				data.sector = rs.getString("sector");
				data.business_description = rs.getString("business_description");
				data.employee_no = rs.getString("employee_no");
				data.stb_acc_name = rs.getString("stb_acc_name");
				data.stb_acc_no = rs.getString("stb_acc_no");
				data.biller_month = rs.getString("biller_month");
				data.prefix = rs.getString("prefix");
				data.website = rs.getString("company_website");
				data.customer_care = rs.getString("company_customer_care");
				data.charge_type = rs.getString("charge_type");
				data.amount_to_charge = rs.getString("amount_to_charge");
				data.percentage = rs.getString("percentage");
				data.currency = rs.getString("currency");
				data.comp_code = rs.getString("comp_code");
				data.country = rs.getString("country");
				data.branch = rs.getString("branch");
				data.branch_id = rs.getString("branch_id");
				data.paybill=rs.getString("paybill");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}

	@RequestMapping(value = "/v1/updateBiller", method = RequestMethod.POST)
	public ResponseEntity<?> updateBiller(HttpServletRequest request,
			@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		if (jsondata.getBiller_location().isEmpty() || jsondata.getPhone().isEmpty()
				|| jsondata.getCompany_name().isEmpty()|| jsondata.getWebsite().isEmpty()
				|| jsondata.getBranch_id().isEmpty() || jsondata.getCharge_type().isEmpty()
				|| jsondata.getAlias().isEmpty() || jsondata.getSector().isEmpty()
				|| jsondata.getPrefix().isEmpty() || jsondata.getCustomer_care().isEmpty()
				|| jsondata.getCurrency().isEmpty() || jsondata.getPaybill().isEmpty()
				|| jsondata.getEmail().isEmpty() || jsondata.getComp_code().isEmpty()
				) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please all fields are required", "", ""));

		} 
        String user=TokenManager.tokenIssuedCompCode(token);
		if (jsondata.getPrefix().length() != 4) {
			return ResponseEntity.ok(new Response("06", "The prefix must be four characters"));
		}
		if (jsondata.getCharge_type().equalsIgnoreCase("TRANSACTIONAL")
				&& jsondata.getAmount_to_charge().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter amount to charge"));
		}
		if (jsondata.getCharge_type().equalsIgnoreCase("PERCENTAGE") && jsondata.getPercentage().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter percentage to charge"));
		}
		String id = TokenManager.tokenIssuedCompCode(token);
		try {
			
			
			conn = DbManager.getConnection();
			String sql1="DELETE FROM edit_biller WHERE email=? AND comp_code=?";
			PreparedStatement prep1 = conn.prepareStatement(sql1);
			prep1.setObject(1, jsondata.getEmail());
			prep1.setObject(2, jsondata.getComp_code());
			prep1.execute();
			
			
			String sql = "INSERT INTO  `edit_biller` (updated_by,comp_code,email,BILLER_LOCATION,phone,COMPANY_NAME,\r\n"
					+ "alias,sector,branch_id,paybill,company_customer_care,percentage,\r\n"
					+ "charge_type,prefix, company_website,amount_to_charge,currency)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, id);
			prep.setObject(2, jsondata.getComp_code());
			prep.setObject(3, jsondata.getEmail());
			prep.setObject(4, jsondata.getBiller_location());
			prep.setObject(5, jsondata.getPhone());
			prep.setObject(6, jsondata.getCompany_name());
			prep.setObject(7, jsondata.getAlias());
			prep.setObject(8, jsondata.getSector());
			prep.setObject(9, jsondata.getBranch_id());
			prep.setObject(10, jsondata.getPaybill());
			prep.setObject(11, jsondata.getCustomer_care());
			prep.setObject(12, jsondata.getPercentage());
			prep.setObject(13, jsondata.getCharge_type());
			prep.setObject(14, jsondata.getPrefix());
			prep.setObject(15, jsondata.getWebsite());
			prep.setObject(16, jsondata.getAmount_to_charge());
			prep.setObject(17, jsondata.getCurrency());
			prep.execute();
			
			String sql2="UPDATE biller_profile SET edit_user=?,updated_by=? WHERE comp_code=? AND email=?";
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep2.setObject(1, "yes");
			prep2.setObject(2, user);
			prep2.setObject(3, jsondata.getComp_code());
			prep2.setObject(4, jsondata.getEmail());
			prep2.execute();
			
			conn.close();
			System_Logs.log_system(user, jsondata.getComp_code(), "Bank User edited open biller:", "queued", "");
			return ResponseEntity.ok(new Response("00", "Biller information queued successfully and need a approval"));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "Sorry!,we are not able to make this request"));
		}

	}
	
	
	@RequestMapping(value = "/v1/approveEditeBiller", method = RequestMethod.POST)
	public ResponseEntity<?> approveEditeBiller(HttpServletRequest request,
			@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		if (jsondata.getEmail().isEmpty() || jsondata.getComp_code().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please peovide email and company code", "", ""));

		} 
		
        String user=TokenManager.tokenIssuedCompCode(token);
        String updated_by="";
        
		String biller_location="";
		String phone="";
		String company_name="";
		String alias="";
		String secttor="";
		String branch_id="";
		String charge_type="";
		String prefix="";
		String website="";
		String cutomercare="";
		String percentage="";
		String amount="";
		String currency="";
		String paybill="";
		
		try {
			conn = DbManager.getConnection();
			String sql1="SELECT * FROM edit_biller WHERE comp_code=? AND email=?";
			PreparedStatement prep1 = conn.prepareStatement(sql1);
			prep1.setObject(1, jsondata.getComp_code());
			prep1.setObject(2, jsondata.getEmail());
			ResultSet rs=prep1.executeQuery();
			while(rs.next()) {
				
				 biller_location=rs.getString("biller_location");
				 company_name=rs.getString("company_name");
				 alias=rs.getString("alias");
				 secttor=rs.getString("sector");
				 branch_id=rs.getString("branch_id");
				 charge_type=rs.getString("charge_type");
				 prefix=rs.getString("prefix");
				 website=rs.getString("company_website");
				 cutomercare=rs.getString("company_customer_care");
				 percentage=rs.getString("percentage");
				 amount=rs.getString("amount_to_charge");
				 currency=rs.getString("currency");
				 paybill=rs.getString("paybill");
				 phone=rs.getString("phone");
				 
				 updated_by=rs.getString("updated_by");
			}
			if(updated_by.equalsIgnoreCase(user)) {
				return ResponseEntity.ok(new AuthProduceJson("06", "You are not authorised to approve this biller", "", ""));	
			}
			
			String sql = "UPDATE  `biller_profile`SET approved_by=?,BILLER_LOCATION=?,phone=?,COMPANY_NAME=?,\r\n" + 
					"					alias=?,sector=?,branch_id=?,paybill=?,company_customer_care=?,percentage=?,\r\n" + 
					"					charge_type=?,prefix=?,company_website=?,amount_to_charge=?,currency=?,edit_user=? WHERE email=? AND comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, user);
			prep.setObject(2, biller_location);
			prep.setObject(3, phone);
			prep.setObject(4, company_name);
			prep.setObject(5, alias);
			prep.setObject(6, secttor);
			prep.setObject(7, branch_id);
			prep.setObject(8, paybill);
			prep.setObject(9, cutomercare);
			prep.setObject(10, percentage);
			prep.setObject(11, charge_type);
			prep.setObject(12, prefix);
			prep.setObject(13, website);
			prep.setObject(14, amount);
			prep.setObject(15, currency);
			prep.setObject(16, "no");
			prep.setObject(17, jsondata.getEmail());
			prep.setObject(18, jsondata.getComp_code());
			prep.execute();
			
			String sql2="DELETE FROM edit_biller WHERE email=? AND comp_code=?";
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep2.setObject(1, jsondata.getEmail());
			prep2.setObject(2, jsondata.getComp_code());
			prep2.execute();
			
			conn.close();
			System_Logs.log_system(user, jsondata.getComp_code(), "Bank User approved  edited open biller:", "Success", "");
			return ResponseEntity.ok(new Response("00", "Biller information updated successfully"));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}

	}
	@RequestMapping(value = "/v1/RejectEditedBiller", method = RequestMethod.POST)
	public ResponseEntity<?> RejectEditedBiller(HttpServletRequest request,@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		
		if(jsondata.getComp_code().isEmpty() || jsondata.getEmail().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide company code and email"));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		String updater=OboardingHelper.getUpdater(jsondata.getComp_code());
		if(username.equalsIgnoreCase(updater)) {
			return ResponseEntity.ok(new Response("06", "You are not authorised to reject this user"));
		}
		try {
			conn = DbManager.getConnection();
			String sql = "DELETE FROM edit_biller WHERE comp_code=? AND email=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getComp_code());
			prep.setObject(2, jsondata.getEmail());
			prep.execute();
			
			String sql2 = "UPDATE biller_profile set edit_user=? WHERE comp_code=? AND email=?";
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep2.setObject(1, "no");
			prep2.setObject(2, jsondata.getComp_code());
			prep2.setObject(3, jsondata.getEmail());
			prep2.execute();
			
			conn.close();
			System_Logs.log_system(username, jsondata.getComp_code(), "Bank User approved deletion of  biller:", "Success", "");
			return ResponseEntity.ok(new Response("00", "User removed from the system successfully"));
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
	}
	
	
	
	@RequestMapping(value = "/v1/approveBiller", method = RequestMethod.POST)
	public ResponseEntity<?> approveBiller(HttpServletRequest request,@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		
		if(jsondata.getComp_code().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide comp_code"));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		String creater= OboardingHelper.getCreater(jsondata.getComp_code());
		String billeremail=OboardingHelper.getBillerEmail(jsondata.getComp_code());
		
		if(creater.equalsIgnoreCase(username)) {
			return ResponseEntity.ok(new Response("06", "You are not authorised to approve this Biller"));
		}
		try {
			conn = DbManager.getConnection();
			String sql = "UPDATE biller_profile SET status=? ,approved_by=? WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Invited");
			prep.setObject(2, username);
			prep.setObject(3, jsondata.getComp_code());
			prep.execute();
			conn.close();
			BillerAuth.sendEmailForBiller(billeremail,jsondata.getComp_code() , creater);
			System_Logs.log_system(username, jsondata.getComp_code(), "Bank User approved  biller creation:", "Success", "");
			return ResponseEntity.ok(new Response("00", "Biller approved successfully"));
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
	}
	
	@RequestMapping(value = "/v1/rejectNewBiller", method = RequestMethod.POST)
	public ResponseEntity<?> rejectNewBiller(HttpServletRequest request,@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		
		if(jsondata.getComp_code().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide comp_code"));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		String creater= OboardingHelper.getCreater(jsondata.getComp_code());
		String billeremail=OboardingHelper.getBillerEmail(jsondata.getComp_code());
		
		if(creater.equalsIgnoreCase(username)) {
			return ResponseEntity.ok(new Response("06", "You are not authorised to reject this Biller"));
		}
		try {
			conn = DbManager.getConnection();
			
			
			String sql = "DELETE FROM biller_profile WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getComp_code());
			prep.execute();
				
			conn.close();
			System_Logs.log_system(username, jsondata.getComp_code(), "Bank User rejected new   biller:", "Success", "");
			return ResponseEntity.ok(new Response("00", "User removed from the system successfully"));
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
	}
	
	@RequestMapping(value = "/v1/deleteBiller", method = RequestMethod.POST)
	public ResponseEntity<?> deleteBiller(HttpServletRequest request,@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		
		if(jsondata.getComp_code().isEmpty() || jsondata.getEmail().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide company code and email"));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		try {
			conn = DbManager.getConnection();
			String sql = "UPDATE biller_profile SET delete_user=?,updated_by=? WHERE comp_code=? AND email=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "yes");
			prep.setObject(2, username);
			prep.setObject(3, jsondata.getComp_code());
			prep.setObject(4, jsondata.getEmail());
			prep.execute();
			conn.close();
			System_Logs.log_system(username, jsondata.getComp_code(), "Bank User deleted open biller:", "queued for approval", "");
			return ResponseEntity.ok(new Response("00", "Biller will be removed from ebiller after approval"));
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
	}
	@RequestMapping(value = "/v1/ApprovedeleteBiller", method = RequestMethod.POST)
	public ResponseEntity<?> ApprovedeleteBiller(HttpServletRequest request,@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		
		if(jsondata.getComp_code().isEmpty() || jsondata.getEmail().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide company code and email"));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		String updater=OboardingHelper.getUpdater(jsondata.getComp_code());
		if(username.equalsIgnoreCase(updater)) {
			return ResponseEntity.ok(new Response("06", "You are not authorised to approve this user"));
		}
		try {
			conn = DbManager.getConnection();
			
			
			String sql = "DELETE FROM biller_profile WHERE comp_code=? AND email=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getComp_code());
			prep.setObject(2, jsondata.getEmail());
			prep.execute();
			
			String sql2 = "DELETE FROM ebiller_auth WHERE comp_code=? AND email=?";
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep2.setObject(1, jsondata.getComp_code());
			prep2.setObject(2, jsondata.getEmail());
			prep2.execute();
			
			conn.close();
			System_Logs.log_system(username, jsondata.getComp_code(), "Bank User approved deletion of  biller:", "Success", "");
			return ResponseEntity.ok(new Response("00", "User removed from the system successfully"));
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
	}
	
	@RequestMapping(value = "/v1/RejectdeleteBiller", method = RequestMethod.POST)
	public ResponseEntity<?> RejectdeleteBiller(HttpServletRequest request,@RequestHeader(name = "Authorization") String token, @RequestBody OnboadingBillerConsumerJson jsondata) {
		
		if(jsondata.getComp_code().isEmpty() || jsondata.getEmail().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide company code and email"));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		String updater=OboardingHelper.getUpdater(jsondata.getComp_code());
		if(username.equalsIgnoreCase(updater)) {
			return ResponseEntity.ok(new Response("06", "You are not authorised to reject this user"));
		}
		try {
			conn = DbManager.getConnection();
			String sql = "UPDATE biller_profile SET delete_user=?,updated_by=? WHERE comp_code=? AND email=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "no");
			prep.setObject(2, username);
			prep.setObject(3, jsondata.getComp_code());
			prep.setObject(4, jsondata.getEmail());
			prep.execute();
			conn.close();
			
			
			conn.close();
			System_Logs.log_system(username, jsondata.getComp_code(), "Bank User rejected deletion of  biller:", "Success", "");
			return ResponseEntity.ok(new Response("00", "User removed from the system successfully"));
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
	}
	
	public void updateAuth(String email, String comp_code, String personel_f_name, String personel_l_name) {
		try {
			conn = DbManager.getConnection();
			String sql = "UPDATE ebiller_auth SET PERSONEL_F_NAME=?,PERSONEL_L_NAME=? WHERE email=? AND comp_code=? ";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, personel_f_name);
			prep.setObject(2, personel_l_name);
			prep.setObject(3, email);
			prep.setObject(4, comp_code);
			prep.execute();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int checkIfBillerEmilExist(String email) {
		int v_count = 0;
		try {
			String sql = "SELECT COUNT(*) AS V_COUNT FROM `biller_profile` WHERE email=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, email);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				v_count = rs.getInt("V_COUNT");
			}
			conn.close();
			return v_count;
		} catch (Exception e) {
			e.printStackTrace();
			return v_count;
		}

	}

}
