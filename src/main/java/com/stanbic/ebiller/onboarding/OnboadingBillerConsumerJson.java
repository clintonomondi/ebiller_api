package com.stanbic.ebiller.onboarding;

public class OnboadingBillerConsumerJson {
    String messageCode="00";
    String message="success";
	String company_name="";
	String biller_location="";
	String email="";
	String biller_phone="";
	String personelFirstname="";
	String personelLastname="";
	String biller_type=null;
	String user_type="";
	String alias="";
	String created_by="";
	String updated_by="";
	String sector="";
	String business_description="";
	String employee_no="";
	String stb_acc_name="";
	String stb_acc_no="";
	String branch_id="";
	String biller_month="";
	String prefix="";
	String website="";
	String customer_care="";
	String charge_type="";
	String amount_to_charge="";
	String percentage="";
	String currency="";
	String comp_code="";
	String country="";
	String branch="";
	String vat="";
	String kra="";
	String phone="";
	String physical_address="";
	String postal_address="";
	String company_email="";
	String customerNumber="";
	Object billing_line="";
	String paybill="";
	
	
	

	public String getPaybill() {
		return paybill;
	}

	public void setPaybill(String paybill) {
		this.paybill = paybill;
	}

	public Object getBilling_line() {
		return billing_line;
	}

	public void setBilling_line(Object billing_line) {
		this.billing_line = billing_line;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCompany_email() {
		return company_email;
	}

	public void setCompany_email(String company_email) {
		this.company_email = company_email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhysical_address() {
		return physical_address;
	}

	public void setPhysical_address(String physical_address) {
		this.physical_address = physical_address;
	}

	public String getPostal_address() {
		return postal_address;
	}

	public void setPostal_address(String postal_address) {
		this.postal_address = postal_address;
	}

	public String getKra() {
		return kra;
	}

	public void setKra(String kra) {
		this.kra = kra;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public String getComp_code() {
		return comp_code;
	}

	public void setComp_code(String comp_code) {
		this.comp_code = comp_code;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCharge_type() {
		return charge_type;
	}

	public void setCharge_type(String charge_type) {
		this.charge_type = charge_type;
	}

	public String getAmount_to_charge() {
		return amount_to_charge;
	}

	public void setAmount_to_charge(String amount_to_charge) {
		this.amount_to_charge = amount_to_charge;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getCustomer_care() {
		return customer_care;
	}

	public void setCustomer_care(String customer_care) {
		this.customer_care = customer_care;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getBusiness_description() {
		return business_description;
	}

	public void setBusiness_description(String business_description) {
		this.business_description = business_description;
	}

	public String getEmployee_no() {
		return employee_no;
	}

	public void setEmployee_no(String employee_no) {
		this.employee_no = employee_no;
	}

	public String getStb_acc_name() {
		return stb_acc_name;
	}

	public void setStb_acc_name(String stb_acc_name) {
		this.stb_acc_name = stb_acc_name;
	}

	public String getStb_acc_no() {
		return stb_acc_no;
	}

	public void setStb_acc_no(String stb_acc_no) {
		this.stb_acc_no = stb_acc_no;
	}

	

	public String getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}

	public String getBiller_month() {
		return biller_month;
	}

	public void setBiller_month(String biller_month) {
		this.biller_month = biller_month;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getBiller_type() {
		return biller_type;
	}

	public void setBiller_type(String biller_type) {
		this.biller_type = biller_type;
	}
	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getBiller_location() {
		return biller_location;
	}

	public void setBiller_location(String biller_location) {
		this.biller_location = biller_location;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBiller_phone() {
		return biller_phone;
	}

	public void setBiller_phone(String biller_phone) {
		this.biller_phone = biller_phone;
	}

	public String getPersonelFirstname() {
		return personelFirstname;
	}

	public void setPersonelFirstname(String personelFirstname) {
		this.personelFirstname = personelFirstname;
	}

	public String getPersonelLastname() {
		return personelLastname;
	}

	public void setPersonelLastname(String personelLastname) {
		this.personelLastname = personelLastname;
	}


}
