package com.stanbic.ebiller.onboarding;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import com.stanbic.comm.DbManager;

public class OboardingHelper {
	static Connection conn = null;
	
	public static boolean SaveBillingLine(Vector vector) {
		try {
		String sql = "INSERT INTO billing_line(service_line,account,email,currency,comp_code,service_code)VALUES(?,?,?,?,?,?)";
		conn = DbManager.getConnection(); 
		PreparedStatement prep = conn.prepareStatement(sql);
		conn.setAutoCommit(false);
		int ii=0;
		for (int i = 0; i < vector.size(); i++) {
			Vector data = (Vector) vector.elementAt(i);
			prep.setObject(1, data.toArray()[0].toString());
			prep.setObject(2, data.toArray()[1].toString());
			prep.setObject(3, data.toArray()[2].toString());
			prep.setObject(4, data.toArray()[3].toString());
			prep.setObject(5, data.toArray()[4].toString());
			prep.setObject(6, data.toArray()[5].toString());
			prep.addBatch();
			ii++;
			if (ii % 1000 == 0 || ii == vector.size()) {
				System.out.println(">inserting service lines at initial stage="+ii);
				prep.executeBatch(); // Execute every 1000 items
				conn.commit();
	            prep.clearBatch();
			}
			
		}
		
		conn.setAutoCommit(true);
		conn.close();
		vector.clear();
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	public static void SaveBillingProduct(Vector vector) {
		try {
		String sql = "INSERT INTO billing_product(product_name,product_code,service_code)VALUES(?,?,?)";
		conn = DbManager.getConnection(); 
		PreparedStatement prep = conn.prepareStatement(sql);
		conn.setAutoCommit(false);
		int ii=0;
		for (int i = 0; i < vector.size(); i++) {
			Vector data = (Vector) vector.elementAt(i);
			prep.setObject(1, data.toArray()[0].toString());
			prep.setObject(2, data.toArray()[1].toString());
			prep.setObject(3, data.toArray()[2].toString());
			prep.addBatch();
			ii++;
			if (ii % 1000 == 0 || ii == vector.size()) {
				System.out.println(">inserting service products at initial stage="+ii);
				prep.executeBatch(); // Execute every 1000 items
				conn.commit();
	            prep.clearBatch();
			}
			
		}
		
		conn.setAutoCommit(true);
		conn.close();
		vector.clear();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getCreater(String comp_code) {
		String status="";
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT created_by FROM biller_profile WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				status=rs.getString("created_by");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
	}
	public static String getUpdater(String comp_code) {
		String status="";
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT updated_by FROM biller_profile WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				status=rs.getString("updated_by");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
	}
	public static String getBillerEmail(String comp_code) {
		String status="";
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT email FROM biller_profile WHERE comp_code=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				status=rs.getString("email");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
	}
	
}
