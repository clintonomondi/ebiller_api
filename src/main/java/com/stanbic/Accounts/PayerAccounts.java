package com.stanbic.Accounts;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Billers.BillersAccountProduceJson;
import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class PayerAccounts {
	Connection conn = null;
	static Logger log = Logger.getLogger(PayerAccounts.class.getName());
	@RequestMapping(value = "/v1/payerDeleteAccount", method = RequestMethod.POST)
	public ResponseEntity<?> payerDeleteAccount(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountProduceJson jsondata) {
		if(jsondata.getAccount_no().isEmpty() || jsondata.getBiller_code().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide biller code", "", ""));
		}
		
		String comp_code = TokenManager.tokenIssuedCompCode(token);
		
		
		try {
			String sql="DELETE FROM account WHERE payer_code=? AND biller_code=? AND account_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			prep.setObject(2, jsondata.getBiller_code());
			prep.setObject(3, jsondata.account_no);
			prep.execute();
			conn.close();
			EmailAlert.logger(comp_code, "User deleeted accounts", log);
			 System_Logs.log_system(comp_code, "", "User deleted accounts:", "Success", "");
			return ResponseEntity.ok(new AuthProduceJson("00", "Account deleted successfully", "", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
		
	}
	
	
	
	@RequestMapping(value = "/v1/updateAccountAlias", method = RequestMethod.POST)
	 public ResponseEntity<?> updateAccountAlias(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
	
		String payer_code = TokenManager.tokenIssuedCompCode(token);

		
		try {
			String sql="UPDATE account SET alias=? WHERE payer_code=? and account_no=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getAlias());
			prep.setObject(2, payer_code);
			prep.setObject(3, jsondata.getAccount_no());
			
			prep.execute();
			conn.close();
			EmailAlert.logger(payer_code, "User updated accounts alias", log);
			 System_Logs.log_system(payer_code, "", "User updated alias for  accounts:", "Success", "");
			return ResponseEntity.ok(new AuthProduceJson("00","Alias updated successfully","",""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
		
	}
}
