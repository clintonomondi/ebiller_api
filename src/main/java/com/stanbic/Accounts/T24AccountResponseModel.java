package com.stanbic.Accounts;

public class T24AccountResponseModel {
	String ResponseCode="";
	String ResponseMessage="";
	String AccountName="";
	String AccountNumber="";
	String CustomerId="";
	String CurrencyCode="";
	String Email="";
	String Phone="";
	String ReferenceId="";
	String KRAPin="";
	String Street="";
	String PostCode="";
	String TownCountry="";
	String IncorporationDate="";
	String BranchCode="";
	
	public String getAccountName() {
		return AccountName;
	}
	public void setAccountName(String accountName) {
		AccountName = accountName;
	}
	public String getResponseCode() {
		return ResponseCode;
	}
	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}
	public String getResponseMessage() {
		return ResponseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		ResponseMessage = responseMessage;
	}

	public String getAccountNumber() {
		return AccountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		AccountNumber = accountNumber;
	}
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getCurrencyCode() {
		return CurrencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public String getReferenceId() {
		return ReferenceId;
	}
	public void setReferenceId(String referenceId) {
		ReferenceId = referenceId;
	}
	public String getKRAPin() {
		return KRAPin;
	}
	public void setKRAPin(String kRAPin) {
		KRAPin = kRAPin;
	}
	public String getStreet() {
		return Street;
	}
	public void setStreet(String street) {
		Street = street;
	}
	public String getPostCode() {
		return PostCode;
	}
	public void setPostCode(String postCode) {
		PostCode = postCode;
	}
	public String getTownCountry() {
		return TownCountry;
	}
	public void setTownCountry(String townCountry) {
		TownCountry = townCountry;
	}
	public String getIncorporationDate() {
		return IncorporationDate;
	}
	public void setIncorporationDate(String incorporationDate) {
		IncorporationDate = incorporationDate;
	}
	public String getBranchCode() {
		return BranchCode;
	}
	public void setBranchCode(String branchCode) {
		BranchCode = branchCode;
	}

	
}
