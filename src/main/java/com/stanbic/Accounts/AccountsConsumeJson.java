package com.stanbic.Accounts;

public class AccountsConsumeJson {
	String token="";
	String base64Excel="";
	String biller_code="";
	String typeOfupload="";
	String file_name="";
	String payer_code="";
	String file_id="";
	
	
	
	public String getFile_id() {
		return file_id;
	}
	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}
	public String getPayer_code() {
		return payer_code;
	}
	public void setPayer_code(String payer_code) {
		this.payer_code = payer_code;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getBiller_code() {
		return biller_code;
	}
	public void setBiller_code(String biller_code) {
		this.biller_code = biller_code;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getBase64Excel() {
		return base64Excel;
	}
	public void setBase64Excel(String base64Excel) {
		this.base64Excel = base64Excel;
	}
	public String getTypeOfupload() {
		return typeOfupload;
	}
	public void setTypeOfupload(String typeOfupload) {
		this.typeOfupload = typeOfupload;
	}
	
	
	
	
}
