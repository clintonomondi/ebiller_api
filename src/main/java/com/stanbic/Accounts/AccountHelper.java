package com.stanbic.Accounts;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.velocity.VelocityContext;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.sql.Connection;

import com.google.gson.JsonObject;
import com.stanbic.backendUsers.BankUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;

public class AccountHelper {
	static Connection conn = null;
	 static String username="";

	public static void saveFile(String file_path, String file_name, String biller_code, String payer_code,
			String file_id, String email, String uploadType) {
		try {
			String sql = "INSERT INTO files (file_path,file_name,biller_code,payer_code,file_id,created_by,upload_type)VALUES(?,?,?,?,?,?,?)";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, file_path);
			prep.setObject(2, file_name);
			prep.setObject(3, biller_code);
			prep.setObject(4, payer_code);
			prep.setObject(5, file_id);
			prep.setObject(6, email);
			prep.setObject(7, uploadType);
			prep.execute();
			conn.close();
		} catch (Exception n) {
			n.printStackTrace();
		}
	}

	public static int generateRandomInt(int upperRange) {

		Random random = new Random();
		return random.nextInt(upperRange);

	}
	
	public static boolean isFileOk(String path,String uploadType) {
		if(uploadType.equalsIgnoreCase("auto_eslip")){
		try {
			Vector cellVectorHolder = new Vector();
			Vector data = null;
				FileInputStream myInput = new FileInputStream(path);
				// POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
				XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);
				XSSFSheet mySheet = myWorkBook.getSheetAt(0);
				Iterator<Row> rowIter = mySheet.rowIterator();
	            int count=0;
				while (rowIter.hasNext()) {         
					XSSFRow myRow = (XSSFRow) rowIter.next();
					Iterator<Cell> cellIter = myRow.iterator();
					Vector cellStoreVector = new Vector();
					 if(count>0) {
					while (cellIter.hasNext()) {
						XSSFCell myCell = (XSSFCell) cellIter.next();
						myCell.setCellType(Cell.CELL_TYPE_STRING);
						cellStoreVector.addElement(myCell);
					}
					cellVectorHolder.addElement(cellStoreVector);
		          }
		          count++;
				}
				for (int i = 0; i < cellVectorHolder.size(); i++) {
					data = (Vector) cellVectorHolder.elementAt(i);
					if(data.toArray()[1].toString().isEmpty() || data.toArray()[1].toString().equalsIgnoreCase("")) {
						return false;
					}
				}
				
				
		}catch(Exception n) {
			return false;
		}
		
		
		}else {
			
			try {
				Vector cellVectorHolder = new Vector();
				Vector data = null;
					FileInputStream myInput = new FileInputStream(path);
					// POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
					XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);
					XSSFSheet mySheet = myWorkBook.getSheetAt(0);
					Iterator<Row> rowIter = mySheet.rowIterator();
		            int count=0;
					while (rowIter.hasNext()) {         
						XSSFRow myRow = (XSSFRow) rowIter.next();
						Iterator<Cell> cellIter = myRow.iterator();
						Vector cellStoreVector = new Vector();
						 if(count>0) {
						while (cellIter.hasNext()) {
							XSSFCell myCell = (XSSFCell) cellIter.next();
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							cellStoreVector.addElement(myCell);
						}
						cellVectorHolder.addElement(cellStoreVector);
			          }
			          count++;
					}
//					System.out.println(cellVectorHolder);
			
			}catch(Exception n) {
				return false;
			}
			
		}
		
		return true;
	}


	public static  JSONObject doesAccountExist(String path, String payer_code,String biller_code) {
		JSONArray array = new JSONArray();
		JSONObject object = new JSONObject();
		JSONObject main = new JSONObject();
		String res = "";
		List<AccountListModel> model2 = new ArrayList<AccountListModel>();
		Vector cellVectorHolder = new Vector();
		 float time1=System.currentTimeMillis();
		String account_built = "";
		Vector data = null;
		try {
			FileInputStream myInput = new FileInputStream(path);
			// POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
			XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);
			Iterator<Row> rowIter = mySheet.rowIterator();
            int count=0;
			while (rowIter.hasNext()) {         
				XSSFRow myRow = (XSSFRow) rowIter.next();
				Iterator<Cell> cellIter = myRow.iterator();
				Vector cellStoreVector = new Vector();
				 if(count>0) {
				while (cellIter.hasNext()) {
					XSSFCell myCell = (XSSFCell) cellIter.next();
					myCell.setCellType(Cell.CELL_TYPE_STRING);
					cellStoreVector.addElement(myCell);
				}
				cellVectorHolder.addElement(cellStoreVector);
	          }
	          count++;
			}
			System.out.println("Building started>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			for (int i = 0; i < cellVectorHolder.size(); i++) {
					data = (Vector) cellVectorHolder.elementAt(i);
							account_built = data.toArray()[0].toString() + "," + account_built;
						
						cellVectorHolder.removeElement(i);
					}

			 
			System.out.println("Buiding finished>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>=");
			if (account_built.endsWith(",")) {

				account_built = account_built.substring(0, account_built.length() - 1);
			}

			ArrayList<String> model = new ArrayList<String>();
			try {
				String sql = "SELECT *  \r\n" + "FROM `account`\r\n" + "WHERE FIND_IN_SET(account_no, '" + account_built
						+ "') AND payer_code=? AND biller_code=?  ";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, payer_code);
				prep.setObject(2, biller_code);
				ResultSet rs = prep.executeQuery();
				while (rs.next()) {
					model.add(rs.getString("account_no"));
				}

				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean flag = true;

			List<String> excelAccountList = new ArrayList<String>(Arrays.asList(account_built.split(",")));
			System.out.println("Comparing two arrays and building report started.>>>>>>>>>>>");
			for (int i = 0; i < excelAccountList.size(); i++) {
				String status = "";
				String account_no = "";
				String amount_status="PASS";
				if(isNumeric((String)((Vector) cellVectorHolder.get(i)).get(1).toString())) {
					
				}else {
					amount_status="FAIL";
					flag=false;
				}
				  
				if (model.contains(excelAccountList.get(i).toString())) {
					status = "FOUND";
					account_no = excelAccountList.get(i).toString();
				} else {

					status = "NOT FOUND";
					account_no = excelAccountList.get(i).toString();
					flag = false;
				}
				float amount=Float.parseFloat((String)((Vector) cellVectorHolder.get(i)).get(1).toString());
				if(amount>0) {
				AccountListModel t = new AccountListModel();
				t.setAccountNo(account_no);
				t.setStatus(status);
				t.setAmount((String)((Vector) cellVectorHolder.get(i)).get(1).toString());
				t.setAmount_status(amount_status);
				model2.add(t);
				}else {
					
				}
				
				
			}

			System.out.println("Comparing two arrays and building report finished.>>>>>>>>>>>");
			if (flag) {
				array.put(model2);
				main.put("messageCode", "00");
				main.put("message", "Success all accounts are validated");
				main.put("account_records", array);
				main.put("flag", flag);
				main.put("path", path);
				System.out.println("Flag true,building jsondata.>>>>>>>>>>>");
			} else {
				array.put(model2);
				main.put("messageCode", "07");
				main.put("message", "Some accounts cannot be validated or invalid amount");
				main.put("account_records", array);
				main.put("flag", flag);
				System.out.println("Flag false,building jsondata.>>>>>>>>>>>");
			}
			System.out.println("Response Returned now!.>>>>>>>>>>>");
			return main;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return main;
	}
	
	public static boolean isNumeric(String number) {
		try {
			float num=Float.parseFloat(number);
		}catch(Exception e) {
			return false;
		}
		return true;
	}

	public static void sendEmail(String file_id, String file_name, String emailto,String payer_code) {
		username=AccountHelper.getName(emailto);
		 String time=getCurrentTime();
		 String validaccounts=getNumberOdAccountValid(file_id);
		 String invalidaccounts=getNumberOdAccountInValid(file_id);
		 String total=getTotalUploaded(file_id);
		try {
			String sql = "UPDATE files SET email_state=? WHERE file_id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "1");
			prep.setObject(2, file_id);
			prep.execute();
			conn.close();

			 String subject="Stanbic E-biller";
			  VelocityContext vc = new VelocityContext();
	          vc.put("file_name", file_name);
	          vc.put("user_name", username);
	          vc.put("time", time);
	          vc.put("valid_accounts", validaccounts);
	          vc.put("invalid_accounts", invalidaccounts);
	          vc.put("total_accounts", total);
	          String email_template = EmailManager.email_message_template(vc,"file_upload.vm");
				EmailManager.send_mail(emailto,email_template,subject,username,"File Upload finished");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public static void sendEmailBank(String file_id, String file_name, String emailto,String payer_code,String bankusername) {
		username=BankUsers.getBankName(bankusername);
		 String time=getCurrentTime();
		 String validaccounts=getNumberOdAccountValid(file_id);
		 String invalidaccounts=getNumberOdAccountInValid(file_id);
		 String total=getTotalUploaded(file_id);
		try {
			String sql = "UPDATE files SET email_state=? WHERE file_id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "1");
			prep.setObject(2, file_id);
			prep.execute();
			conn.close();

			 String subject="Stanbic E-biller";
			  VelocityContext vc = new VelocityContext();
	          vc.put("file_name", file_name);
	          vc.put("user_name", username);
	          vc.put("time", time);
	          vc.put("valid_accounts", validaccounts);
	          vc.put("invalid_accounts", invalidaccounts);
	          vc.put("total_accounts", total);
	          String email_template = EmailManager.email_message_template(vc,"file_upload.vm");
				EmailManager.send_mail(emailto,email_template,subject,username,"File upload done");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String  getNumberOdAccountValid(String file_id) {
		String valid="0";
		try {
			String sql="SELECT COUNT(*)vcount FROM file_data WHERE file_id=? AND validation=?";
			conn = DbManager.getConnection(); 
			PreparedStatement prepquery = conn.prepareStatement(sql);
			prepquery.setObject(1,file_id);
			prepquery.setObject(2,"true");
			ResultSet rs=prepquery.executeQuery();
			while(rs.next()) {
				valid=rs.getString("vcount");
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return valid;
	}
	
	public static String  getNumberOdAccountInValid(String file_id) {
		String invalid="0";
		try {
			String sql="SELECT COUNT(*)vcount FROM file_data WHERE file_id=? AND validation=?";
			conn = DbManager.getConnection(); 
			PreparedStatement prepquery = conn.prepareStatement(sql);
			prepquery.setObject(1,file_id);
			prepquery.setObject(2,"false");
			ResultSet rs=prepquery.executeQuery();
			while(rs.next()) {
				invalid=rs.getString("vcount");
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return invalid;
	}
	
	public static String  getTotalUploaded(String file_id) {
		String total="0";
		try {
			String sql="SELECT COUNT(*)vcount FROM file_data WHERE file_id=?";
			conn = DbManager.getConnection(); 
			PreparedStatement prepquery = conn.prepareStatement(sql);
			prepquery.setObject(1,file_id);
			ResultSet rs=prepquery.executeQuery();
			while(rs.next()) {
				total=rs.getString("vcount");
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return total;
	}
	public static String getName(String email) {
		String name="";
		try {
			
			String query="SELECT personel_f_name,personel_l_name FROM ebiller_auth WHERE email=?";
			conn = DbManager.getConnection(); 
			PreparedStatement prepquery = conn.prepareStatement(query);
			prepquery.setObject(1,email);
			ResultSet rs=prepquery.executeQuery();
			while(rs.next()) {
				 name=rs.getString("personel_f_name")+"    "+rs.getString("personel_l_name");
			}
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return name;
	}
	
	public static String getCurrentTime() {
		String time=null;
		  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		   LocalDateTime now = LocalDateTime.now();  
		   time=dtf.format(now);
		   return time;
	}
	
	
	
	public static String getEmail(String comp_code) {
		String name="";
		try {
			
			String query="SELECT email FROM biller_profile WHERE comp_code=?";
			conn = DbManager.getConnection(); 
			PreparedStatement prepquery = conn.prepareStatement(query);
			prepquery.setObject(1,comp_code);
			ResultSet rs=prepquery.executeQuery();
			while(rs.next()) {
				 name=rs.getString("email");
			}
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return name;
	}
	
	
	
	public static int isAccountexist(String account) {
		int vcount=0;
		try {
			String sql="SELECT COUNT(*)vcount FROM biller_profile WHERE stb_acc_no=?";
			conn = DbManager.getConnection(); 
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1,account);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				vcount=rs.getInt("vcount");
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return vcount;
	}
	public static int isCustomerNumbertexist(String account) {
		int vcount=0;
		try {
			String sql="SELECT COUNT(*)vcount FROM biller_profile WHERE customerNumber=?";
			conn = DbManager.getConnection(); 
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1,account);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				vcount=rs.getInt("vcount");
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return vcount;
	}
	
	public static int chekErrorAccounts(String file_id) {
		int vcount=0;
		try {
			String sql="SELECT COUNT(*)vcount FROM `file_data` WHERE file_id=? AND account_balance='Error'";
			conn = DbManager.getConnection(); 
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1,file_id);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				vcount=rs.getInt("vcount");
			}
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return vcount;
	}

}
