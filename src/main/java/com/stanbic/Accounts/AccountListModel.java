package com.stanbic.Accounts;

public class AccountListModel {

	String accountNo="";
	String status="";
	String amount="";
	String amount_status="";


	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmount_status() {
		return amount_status;
	}

	public void setAmount_status(String amount_status) {
		this.amount_status = amount_status;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	
}
