package com.stanbic.Accounts;

import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.ini4j.Ini;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stanbic.Responses.Response;
import com.stanbic.Roles.UsersAccessMenuRespModel;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class T24AccountValidation {

	Connection conn=null;
	String esbAccountValidationPath="";
	@RequestMapping(value = "/v1/validateAccount", method = RequestMethod.POST)
	public ResponseEntity<?> validateAccount(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountValidationReqModel jsondata) {
		if(jsondata.getAccountNumber().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter a account number"));	
		}
		if(AccountHelper.isAccountexist(jsondata.getAccountNumber())>0) {
			return ResponseEntity.ok(new Response("06", "The account number is already in use"));
		}
		
		
		String accountNumber=jsondata.getAccountNumber();
		String system_username=TokenManager.tokenIssuedCompCode(token);
		
		String referenceId="";
		
		try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
		       esbAccountValidationPath=ini.get("header", "esbAccountValidationPath");		
//	
		//Hashtable<String,String> hashTable=new Hashtable<String,String>();
		       
			System.out.println(accountNumber);
			referenceId=GeneralCodes.uuid_sys();
		
			Hashtable<String,String> map=new Hashtable<String,String>();
			map.put("AccountNumber", accountNumber);
			map.put("Channel", "Agent");
			map.put("ReferenceId", referenceId);
			
			String response=GeneralCodes.encryptedPostRequest(map, esbAccountValidationPath, "JSON");
			
			JSONObject obj = new JSONObject(response);
			String jsonResponse = obj.toString();
			
			Gson gson = new GsonBuilder().create();
			
			T24AccountResponseModel res = gson.fromJson(jsonResponse, T24AccountResponseModel.class);
			
			System.out.println("res.getResponseCode()::"+res.getResponseCode());
			System.out.println(" res.getAccountName()::"+ res.getAccountName());
			
			Hashtable<String,String> hashTable=new Hashtable<String,String>();
//			
	if(res.getResponseCode().equalsIgnoreCase("00")) {
				
				try {
					conn = DbManager.getConnection();
					CallableStatement prep = conn.prepareCall("{CALL ebiller.ACCOUNT_T24_VALIDATE(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
					prep.setObject(1, res.getAccountName());
					prep.setObject(2, res.getAccountNumber());
					prep.setObject(3, res.getCustomerId());
					prep.setObject(4, res.getCurrencyCode());
					prep.setObject(5, res.getEmail());
					prep.setObject(6, res.getPhone());
					prep.setObject(7, res.getReferenceId());
					prep.setObject(8, res.getKRAPin());
					prep.setObject(9, res.getStreet());
					prep.setObject(10, res.getPostCode());
					prep.setObject(11, res.getTownCountry());
					prep.setObject(12, res.getIncorporationDate());
					prep.setObject(13, system_username);
					prep.execute();
					conn.close();
					
					hashTable.put("messageCode","00");
					hashTable.put("message","Succeesfully fetched account information");
					hashTable.put("accountName",res.getAccountName());
					hashTable.put("currencyCode",res.getCurrencyCode());
					hashTable.put("accountNumber",res.getAccountNumber());
					hashTable.put("email",res.getEmail());
					hashTable.put("phone",res.getPhone());
					hashTable.put("kraPin",res.getKRAPin());
					hashTable.put("street",res.getStreet());
					hashTable.put("postalCode",res.getPostCode());
					hashTable.put("townCountry",res.getTownCountry());
					hashTable.put("incorporationDate",res.getIncorporationDate());
					return ResponseEntity.ok(hashTable);	
				}catch(Exception e) {
					e.printStackTrace();
				}
			}else {
				hashTable.put("messageCode","07");
				hashTable.put("message","Could not get account number");
				return ResponseEntity.ok(hashTable);
			}
		} catch (IOException e) {
			e.printStackTrace();
			Hashtable<String,String> hashTable=new Hashtable<String,String>();
			hashTable.put("messageCode","07");
			hashTable.put("message","Could not get account number");
			return ResponseEntity.ok(hashTable);
		}
		return null;
//			hashTable.put("messageCode","00");
//			hashTable.put("message","Succeesfully fetched account information");
//			hashTable.put("accountName","Opanga");
//			hashTable.put("currencyCode","Ksh");
//			hashTable.put("accountNumber","900900900");
//			hashTable.put("email","Opanga@gmail.com");
//			hashTable.put("phone","09758584");
//			hashTable.put("kraPin","KI87666T");
//			hashTable.put("street","Pala");
//			hashTable.put("postalCode","30400");
//			hashTable.put("townCountry","Ugenya");
//			hashTable.put("incorporationDate","12/12/2019");
//     		return ResponseEntity.ok(hashTable);
			
			
	}
	
	
	@RequestMapping(value = "/v1/validateCustomerNumber", method = RequestMethod.POST)
	public ResponseEntity<?> validateCustomerNumber(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountValidationReqModel jsondata) {
		if(jsondata.getCustomerNumber().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please enter customer number"));	
		}
		if(AccountHelper.isCustomerNumbertexist(jsondata.getCustomerNumber())>0) {
			return ResponseEntity.ok(new Response("06", "The customer number is already in use"));
		}
		
		String customerNumber=jsondata.getCustomerNumber();
		String system_username=TokenManager.tokenIssuedCompCode(token);
		
		String referenceId="";
		
//		try {
//		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
//		       esbAccountValidationPath=ini.get("header", "esbAccountValidationPath");		
//	
		Hashtable<String,Object> hashTable=new Hashtable<String,Object>();
//		       
//			System.out.println(accountNumber);
//			referenceId=GeneralCodes.uuid_sys();
//		
//			Hashtable<String,String> map=new Hashtable<String,String>();
//			map.put("AccountNumber", accountNumber);
//			map.put("Channel", "Agent");
//			map.put("ReferenceId", referenceId);
//			
//			String response=GeneralCodes.encryptedPostRequest(map, esbAccountValidationPath, "JSON");
//			
//			JSONObject obj = new JSONObject(response);
//			String jsonResponse = obj.toString();
//			
//			Gson gson = new GsonBuilder().create();
//			
//			T24AccountResponseModel res = gson.fromJson(jsonResponse, T24AccountResponseModel.class);
//			
//			System.out.println("res.getResponseCode()::"+res.getResponseCode());
//			System.out.println(" res.getAccountName()::"+ res.getAccountName());
//			
//			Hashtable<String,String> hashTable=new Hashtable<String,String>();
////			
//	if(res.getResponseCode().equalsIgnoreCase("00")) {
//				
//				try {
//					conn = DbManager.getConnection();
//					CallableStatement prep = conn.prepareCall("{CALL ebiller.ACCOUNT_T24_VALIDATE(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
//					prep.setObject(1, res.getAccountName());
//					prep.setObject(2, res.getAccountNumber());
//					prep.setObject(3, res.getCustomerId());
//					prep.setObject(4, res.getCurrencyCode());
//					prep.setObject(5, res.getEmail());
//					prep.setObject(6, res.getPhone());
//					prep.setObject(7, res.getReferenceId());
//					prep.setObject(8, res.getKRAPin());
//					prep.setObject(9, res.getStreet());
//					prep.setObject(10, res.getPostCode());
//					prep.setObject(11, res.getTownCountry());
//					prep.setObject(12, res.getIncorporationDate());
//					prep.setObject(13, system_username);
//					prep.execute();
//					conn.close();
//					
//					hashTable.put("messageCode","00");
//					hashTable.put("message","Succeesfully fetched account information");
//					hashTable.put("accountName",res.getAccountName());
//					hashTable.put("currencyCode",res.getCurrencyCode());
//					hashTable.put("accountNumber",res.getAccountNumber());
//					hashTable.put("email",res.getEmail());
//					hashTable.put("phone",res.getPhone());
//					hashTable.put("kraPin",res.getKRAPin());
//					hashTable.put("street",res.getStreet());
//					hashTable.put("postalCode",res.getPostCode());
//					hashTable.put("townCountry",res.getTownCountry());
//					hashTable.put("incorporationDate",res.getIncorporationDate());
//					return ResponseEntity.ok(hashTable);	
//				}catch(Exception e) {
//					e.printStackTrace();
//				}
//			}else {
//				hashTable.put("messageCode","07");
//				hashTable.put("message","Could not get account number");
//				return ResponseEntity.ok(hashTable);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//			Hashtable<String,String> hashTable=new Hashtable<String,String>();
//			hashTable.put("messageCode","07");
//			hashTable.put("message","Could not get account number");
//			return ResponseEntity.ok(hashTable);
//		}
//		return null;
		
		
			hashTable.put("messageCode","00");
			hashTable.put("message","Succeesfully fetched account information");
			hashTable.put("customerNumber",jsondata.getCustomerNumber());
			hashTable.put("accounts",this.accounts());
     		return ResponseEntity.ok(hashTable);
			
			
	}
	public List<TestModel> accounts(){
		List<TestModel> list=new ArrayList<TestModel>();
		try {
			String sql="SELECT  stb_acc_no,stb_acc_name FROM biller_profile WHERE stb_acc_no IS NOT NULL";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				TestModel model=new TestModel();
				model.account_no=rs.getString("stb_acc_no");
				model.account_name=rs.getString("stb_acc_name");
				list.add(model);
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
