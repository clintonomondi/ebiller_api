package com.stanbic.Accounts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class RefreshBalance {
	Connection conn = null;
	
	@RequestMapping(value = "/v1/refreshBalance", method = RequestMethod.POST)
	public ResponseEntity<?> refreshBalance(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountProduceJson jsondata) {
		String payer_code=TokenManager.tokenIssuedCompCode(token);
		
		Thread t = new Thread() {
			public void run() {
				updateBalance(payer_code);
			}
		};
		t.start(); 
		
		return ResponseEntity.ok(new AuthProduceJson("00", "Success", "", ""));
		
	}
	
	public void updateBalance(String payer_code) {
		try {
			Vector vector = new Vector();
			String sql="SELECT * FROM account WHERE payer_code=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, payer_code);
			ResultSet rs = prep.executeQuery();
			while(rs.next()) {
				Vector holder = new Vector();
				holder.addElement(rs.getString("account"));
				vector.addElement(holder);
				
			}
			
		}catch(Exception e) {
			
		}
		
		
	}
}
