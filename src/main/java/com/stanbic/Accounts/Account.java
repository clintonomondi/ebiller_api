package com.stanbic.Accounts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.Auth;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class Account {
	 String username="";
	Connection conn = null;
	static Logger log = Logger.getLogger(Account.class.getName());
	
	@RequestMapping(value = "/v1/deleteMultiple", method = RequestMethod.POST)
	public ResponseEntity<?> payerDeleteAccount(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
		Vector vector = new Vector();
		String date=TimeManager.getEmailTime();
		try {
			JSONObject obj = new JSONObject(jsondata);
			String biller_code = obj.getString("biller_code");
			String account_no=null;
			String payer_code = TokenManager.tokenIssuedCompCode(token);
			String email = TokenManager.tokenIssuedId(token);
			
			username=AccountHelper.getName(email);
			JSONArray accounts = obj.getJSONArray("accounts");
			for (int i = 0; i < accounts.length(); i++) {
				JSONObject object = accounts.getJSONObject(i);
				account_no = object.getString("account_no");
				Vector holder = new Vector();
				holder.addElement(account_no);
				holder.addElement(biller_code);
				holder.addElement(payer_code);
				vector.addElement(holder);
			}
			String sql = "DELETE FROM account WHERE account_no=? AND biller_code=? AND payer_code=?";
			conn = DbManager.getConnection(); 
			PreparedStatement prep = conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			int ii=0;
			for (int i = 0; i < vector.size(); i++) {
				Vector data = (Vector) vector.elementAt(i);
				prep.setObject(1, data.toArray()[0].toString());
				prep.setObject(2, data.toArray()[1].toString());
				prep.setObject(3, data.toArray()[2].toString());
				prep.addBatch();
				ii++;
				if (ii % 1000 == 0 || ii == vector.size()) {
					System.out.println(">Deleting accounts at initial stage="+ii);
					prep.executeBatch(); // Execute every 1000 items
					conn.commit();
		            prep.clearBatch();
		            
		            if(ii==vector.size()) {
								String num=String.valueOf(vector.size());
								 VelocityContext vc = new VelocityContext();
						          vc.put("total_account", num);
						          vc.put("user_name",username);
						          vc.put("date",date);
						          vc.put("time",AccountHelper.getCurrentTime());
						          String email_template = EmailManager.email_message_template(vc,"deleteAccount.vm");
									EmailManager.send_mail(email,email_template,"Stanbic E-biller",username,"Account deletion="+num);
		            }
		            
				}
				
			}
			conn.setAutoCommit(true);
			conn.close();
			vector.clear();
			EmailAlert.logger(email, "Account deletion successful", log);
			System_Logs.log_system(username, "", "Bank User deleted accounts:", "Success", "");
			return ResponseEntity.ok(new AuthProduceJson("00","Account(s) deleted successfully","",""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
	
	   
	}
	
}
