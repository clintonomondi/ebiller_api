package com.stanbic.Accounts;

import java.io.File;
import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;

import com.stanbic.Billers.BillerHelper;
import com.stanbic.Billers.BillersAccountProduceJson;
import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.KPLC.QueryBulkBillsService;
import com.stanbic.Logs.System_Logs;
import com.stanbic.Responses.Response;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.Auth;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class MultipleAccounts {
	Connection conn = null;
	static Logger log = Logger.getLogger(MultipleAccounts.class.getName());
	@RequestMapping(value = "/v1/getMyUploadedFiles", method = RequestMethod.POST)
	public ResponseEntity<?> getMyUploadedFiles(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountProduceJson jsondata) {
		if (jsondata.getBiller_code().isEmpty() || jsondata.getBiller_code().equalsIgnoreCase("")) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide biller code", "", ""));
		}
		String comp_code = TokenManager.tokenIssuedCompCode(token);
		String email = TokenManager.tokenIssuedId(token);
		

		List<AccountProduceJson> model = new ArrayList<AccountProduceJson>();
		Vector vector = new Vector();
		try {

			String sql = "SELECT created_at,biller_code,file_id,file_name,payer_code,biller_code,id,email_state, (SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id) total ,\n"
					+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND status='STARTED') STARTED,\n"
					+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND account_balance='Error') Errors,\n"
					+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND account_balance='Notfound') Notfound,\n"
					+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND status='PENDING') pending,\n"
					+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND status='VALIDATED') validated,\n"
					+ "(((SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND status='VALIDATED')/(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id))*100)percnt\n"
					+ "FROM `files` b WHERE payer_code=? AND upload_type=? AND biller_code=? GROUP BY payer_code,biller_code,file_name,file_id,created_at,id,email_state ORDER BY id DESC LIMIT 5";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			prep.setObject(2, "manual_eslip");
			prep.setObject(3, jsondata.getBiller_code());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				AccountProduceJson data = new AccountProduceJson();
				data.id = rs.getString("id");
				data.biller_code = rs.getString("biller_code");
				data.file_id = rs.getString("file_id");
				data.file_name = rs.getString("file_name");
				data.payer_code = rs.getString("payer_code");
				data.biller_code = rs.getString("biller_code");
				data.total = rs.getString("total");
				data.pending = rs.getString("PENDING");
				data.validated = rs.getString("validated");
				data.created_at = rs.getString("created_at");
				data.progress = rs.getInt("percnt") + "";
				data.email_state=rs.getString("email_state");
				data.errors=rs.getString("Errors");
				data.notFound=rs.getString("Notfound");
				model.add(data);
				Vector holder = new Vector();
				holder.addElement(rs.getString("file_id"));
				holder.addElement(rs.getInt("percnt")+"");
				holder.addElement(rs.getString("file_name"));
				holder.addElement(rs.getString("email_state"));
				vector.addElement(holder);
			}
			conn.close();
			if(vector.size()>0) {
				for (int i = 0; i < vector.size(); i++) {
					Vector data = (Vector) vector.elementAt(i);
				if(data.toArray()[3].toString().equalsIgnoreCase("0") && data.toArray()[1].toString().equalsIgnoreCase("100")) { 
					Thread t = new Thread() {
						public void run() {
							AccountHelper.sendEmail(data.toArray()[0].toString(),data.toArray()[2].toString(),email,comp_code);
						}
					};
					t.start(); 
				}else {
					
				}
				vector.clear();
			}
			}
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}

	
	@RequestMapping(value = "/v1/getUploadedFilesRecords", method = RequestMethod.POST)
	public ResponseEntity<?> getUploadedFilesRecords(@RequestBody AccountProduceJson jsondata) {

		if (jsondata.getFile_id().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide file id", "", ""));
		}
		List<AccountProduceJson> model = new ArrayList<AccountProduceJson>();
		try {
			String sql = "SELECT * FROM `file_data` WHERE file_id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getFile_id());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				AccountProduceJson data = new AccountProduceJson();
				data.account_name = rs.getString("account_name");
				data.account_no = rs.getString("account_no");
				data.status = rs.getString("validation");
				data.account_balance = rs.getString("account_balance");
				data.created_at = rs.getString("created_at");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}

	@RequestMapping(value = "/v1/uploadExcelAccounts", method = RequestMethod.POST)
	public ResponseEntity<?> uploadExcelAccounts(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountsConsumeJson jsondata) {

		if (jsondata.getBiller_code().isEmpty() || jsondata.getBase64Excel().isEmpty()
				|| jsondata.getBiller_code().equalsIgnoreCase("") || jsondata.getFile_name().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide all fields", "", ""));
		}
		String payer_code = TokenManager.tokenIssuedCompCode(token);
		String user_email = TokenManager.tokenIssuedId(token);
		String uploadType = jsondata.getTypeOfupload();
		
		try {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		String time = dtf.format(now);
		String filename = time.replace("/", "_") + payer_code;
		String path = ExternalFile.getExcelPath()+AccountHelper.generateRandomInt(4) + filename + ".xlsx";

		FileManager.saveImage(jsondata.getBase64Excel(), path);
		String file_id = AccountHelper.generateRandomInt(99999) + time;
		if(AccountHelper.isFileOk(path,uploadType)) {
		if (uploadType.equalsIgnoreCase("auto_eslip") ) {
			String data=AccountHelper.doesAccountExist(path, payer_code,jsondata.getBiller_code()).toString();
			return ResponseEntity.ok(data);
		}

		else if (uploadType.equalsIgnoreCase("manual_eslip") ) {
				AccountHelper.saveFile(path, jsondata.getFile_name(), jsondata.getBiller_code(), payer_code, file_id,
						user_email, uploadType);
				BillerHelper.linkPayerToBiller(jsondata.getBiller_code(), payer_code,"");
				Thread t = new Thread() {
					public void run() {
						QueryBulkBillsService.KPLCService(path, payer_code, jsondata.getBiller_code(), file_id,
								user_email, uploadType);
					}
				};
				t.start(); 
		
				return ResponseEntity.ok(new AuthProduceJson("00", "Excel uploaded successfully", "", ""));
			} else {
				return ResponseEntity.ok(new AuthProduceJson("07", "Please provide upload type", "", ""));
			}
		}else {
			return ResponseEntity.ok(new AuthProduceJson("08", "Invalid File format", "", ""));
		}
		
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", " Invalid file format or system technical error", "", ""));
		}
		
	}
	
	@RequestMapping(value = "/v1/reValidateAccounts", method = RequestMethod.POST)
	public ResponseEntity<?> reValidateAccounts(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountsConsumeJson jsondata) {
    String user=TokenManager.tokenIssuedCompCode(token);
		if(jsondata.getFile_id().isEmpty()) {
			return ResponseEntity.ok(new Response("06", "Please provide file id"));
		}
		int ac=AccountHelper.chekErrorAccounts(jsondata.getFile_id());
		if(ac==0) {
			return ResponseEntity.ok(new Response("06", "You do not have errors in accounts uploaded for this file,the rest are not valid accounts with this biller "));
		}
		try {
			conn = DbManager.getConnection();
			
			String sql="UPDATE `file_data` SET status=?  WHERE file_id=? AND account_balance='Error'";
        	PreparedStatement prep = conn.prepareStatement(sql);
        	prep.setObject(1, "Pending");
        	prep.setObject(2, jsondata.getFile_id());
        	prep.execute();
			
			String sql2="UPDATE files SET state=? WHERE file_id=?";
        	PreparedStatement prep2 = conn.prepareStatement(sql2);
        	prep2.setObject(1, 1);
        	prep2.setObject(2, jsondata.getFile_id());
        	prep2.execute();
        	
        	
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new Response("02", "System technical error"));
		}
		EmailAlert.logger(user, "User revalidated accounts", log);
		System_Logs.log_system(user, "", "User revalidated accounts:", "Success", "");
		return ResponseEntity.ok(new Response("00", ac +"    accounts were not validated,Re-validation started successfully"));
		
	}
}
