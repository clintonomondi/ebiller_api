package com.stanbic.Accounts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.Eslip.EslipHelper;
import com.stanbic.Eslip.EslipResponse;
import com.stanbic.Eslip.EslipService;
import com.stanbic.Eslip.MyESlipsProduceJSon;
import com.stanbic.KPLC.QueryBulkBillsService;
import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class MultipleAccountsAutoSlips {
	static Logger log = Logger.getLogger(MultipleAccountsAutoSlips.class.getName());
	static Connection conn = null;
	
	@RequestMapping(value = "/v1/generateEslipAuto", method = RequestMethod.POST)
	public ResponseEntity<?> generateEslipAuto(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody MyESlipsProduceJSon jsondata) {
String eslip_no=null;
String due_date=null;
if(jsondata.getBiller_code().isEmpty() || jsondata.getPath().isEmpty()) {
	 return ResponseEntity.ok(new AuthProduceJson("06","Please provide all fields","",""));
}
		String payer_code = TokenManager.tokenIssuedCompCode(token);
		String user_email=TokenManager.tokenIssuedId(token);

		// generate e slip number
		eslip_no = EslipService.eslipNoGen(jsondata.getBiller_code());
		due_date=EslipService.expiryDate();
		double oddcent=Double.parseDouble(jsondata.getOddcent());
		 Vector exceldata=QueryBulkBillsService.read(jsondata.getPath());
		 double total_amount_to_pay=EslipHelper.saveToFileData(exceldata,payer_code,jsondata.getBiller_code(),eslip_no,due_date,jsondata.getAccount_to_add(),oddcent);
		 if(total_amount_to_pay<=0.0) {
			 EslipHelper.deleteHistory(eslip_no);
			 return ResponseEntity.ok(new AuthProduceJson("07", "Some accounts have zero amount to pay", "", ""));
		 }else {
			 EslipService.insertEslip(eslip_no , due_date, "0",total_amount_to_pay,jsondata.getBiller_code(), payer_code,"",user_email);
			 EmailAlert.logger(user_email, "User generated eslip number "+eslip_no, log);
			 System_Logs.log_system(user_email, "", "User generated eslip:"+eslip_no, "Success", "");
			 return ResponseEntity.ok(new AuthProduceJson("00", "Eslip generated successfully", "", ""));	 
		 }
		
		 
	}
	
	
	@RequestMapping(value = "/v1/generateBankEslipAuto", method = RequestMethod.POST)
	public ResponseEntity<?> generateBankEslipAuto(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody MyESlipsProduceJSon jsondata) {
String eslip_no="";
String due_date="";
if(jsondata.getBiller_code().isEmpty() || jsondata.getPath().isEmpty() || jsondata.getPayer_code().isEmpty()) {
	 return ResponseEntity.ok(new AuthProduceJson("06","Please provide all fields","",""));
}
		String username = TokenManager.tokenIssuedCompCode(token);
		

		// generate e slip number
		eslip_no = EslipService.eslipNoGen(jsondata.getBiller_code());
		due_date=EslipService.expiryDate();
		double oddcent=Double.parseDouble(jsondata.getOddcent());
		 Vector exceldata=QueryBulkBillsService.read(jsondata.getPath());
		 double total_amount_to_pay=EslipHelper.saveToFileData(exceldata,jsondata.getPayer_code(),jsondata.getBiller_code(),eslip_no,due_date,jsondata.getAccount_to_add(),oddcent);
		 if(total_amount_to_pay<=0.0) {
			 EslipHelper.deleteHistory(eslip_no);
			 return ResponseEntity.ok(new AuthProduceJson("07", "Some accounts have zero amount to pay", "", ""));
		 }else {
			 EslipService.insertEslipBank(eslip_no , due_date, "0",total_amount_to_pay+"",jsondata.getBiller_code(), jsondata.getPayer_code(),"",username);
			 EmailAlert.logger(username, "User generated eslip number "+eslip_no, log);
			 System_Logs.log_system(username, "", "User generated eslip:"+eslip_no, "Success", "");
			 return ResponseEntity.ok(new AuthProduceJson("00", "Eslip generated successfully", "", ""));	 
		 }
		
		 
	}

	
	public static void InsertEslipBills(String eslip_no,String file_id) {
		try {
			conn = DbManager.getConnection();
			String sql2 = "INSERT INTO eslip_bills (eslip_no,amount_due,amount_to_pay,due_date,account_no,file_id,account_name) \r\n" + 
					"SELECT '"+eslip_no+"',account_balance,amount_to_pay,'',account_no,'"+file_id+"',account_name from file_data WHERE file_id='"+file_id+"'";
			PreparedStatement prep = conn.prepareStatement(sql2);
			prep.execute();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}

	}
	public static void deleteCurrentEslip(String file_id) {
		try {
			conn = DbManager.getConnection();
			String sql="DELETE FROM eslip_bills WHERE file_id=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, file_id);
			prep.execute();
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			conn = DbManager.getConnection();
			String sql="DELETE FROM eslip WHERE file_id=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, file_id);
			prep.execute();
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
