package com.stanbic.Accounts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Billers.BillerHelper;
import com.stanbic.Billers.BillersAccountProduceJson;
import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.KPLC.QueryBulkBillsService;
import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class BankAccounts {
	 Connection conn = null;
	 static Logger log = Logger.getLogger(BankAccounts.class.getName());
	 @RequestMapping(value = "/v1/getBankUploadedFiles", method = RequestMethod.POST)
		public ResponseEntity<?> getBankUploadedFiles(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountProduceJson jsondata) {

			String username = TokenManager.tokenIssuedCompCode(token);			

			List<AccountProduceJson> model = new ArrayList<AccountProduceJson>();
			Vector vector = new Vector();
			try {

				String sql = "SELECT created_at,biller_code,file_id,file_name,payer_code,biller_code,id,email_state, (SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id) total ,\n"
						+ "(SELECT email FROM biller_profile Z WHERE Z.comp_code=b.payer_code)payer_email,\n"
						+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND status='STARTED') STARTED,\n"
						+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND account_balance='Error') Errors,\n"
						+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND account_balance='Notfound') Notfound,\n"
						+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND status='PENDING') pending,\n"
						+ "(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND status='VALIDATED') validated,\n"
						+ "(((SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id AND status='VALIDATED')/(SELECT COUNT(*) AS V_COUNT FROM file_data A WHERE A.file_id=b.file_id))*100)percnt\n"
						+ "FROM `files` b WHERE created_by=? GROUP BY payer_code,biller_code,file_name,file_id,created_at,id,email_state ORDER BY id DESC LIMIT 5";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, username);
				ResultSet rs = prep.executeQuery();
				while (rs.next()) {
					AccountProduceJson data = new AccountProduceJson();
					data.id = rs.getString("id");
					data.biller_code = rs.getString("biller_code");
					data.file_id = rs.getString("file_id");
					data.file_name = rs.getString("file_name");
					data.payer_code = rs.getString("payer_code");
					data.biller_code = rs.getString("biller_code");
					data.total = rs.getString("total");
					data.pending = rs.getString("PENDING");
					data.validated = rs.getString("validated");
					data.created_at = rs.getString("created_at");
					data.progress = rs.getInt("percnt") + "";
					data.email_state=rs.getString("email_state");
					data.errors=rs.getString("Errors");
					data.notFound=rs.getString("Notfound");
					
					model.add(data);
					Vector holder = new Vector();
					holder.addElement(rs.getString("file_id"));
					holder.addElement(rs.getInt("percnt")+"");
					holder.addElement(rs.getString("file_name"));
					holder.addElement(rs.getString("email_state"));
					holder.addElement(rs.getString("payer_email"));
					holder.addElement(rs.getString("payer_code"));
					vector.addElement(holder);
				}
				conn.close();
				if(vector.size()>0) {
					for (int i = 0; i < vector.size(); i++) {
						Vector data = (Vector) vector.elementAt(i);
					if(data.toArray()[3].toString().equalsIgnoreCase("0") && data.toArray()[1].toString().equalsIgnoreCase("100")) { 
						Thread t = new Thread() {
							public void run() {
								AccountHelper.sendEmailBank(data.toArray()[0].toString(),data.toArray()[2].toString(),data.toArray()[4].toString(),data.toArray()[5].toString(),username);
							}
						};
						t.start(); 
					}else {
						
					}
					vector.clear();
				}
				}
				EmailAlert.logger(username, "user uploaded file for payer", log);
				return ResponseEntity.ok(model);
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
			}
		}
	
	@RequestMapping(value = "/v1/uploadExceBankAccounts", method = RequestMethod.POST)
	public ResponseEntity<?> uploadExcelAccounts(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountsConsumeJson jsondata) {

		if (jsondata.getBiller_code().isEmpty() || jsondata.getBase64Excel().isEmpty()
				|| jsondata.getBiller_code().equalsIgnoreCase("") || jsondata.getFile_name().isEmpty() || jsondata.getPayer_code().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide all fields", "", ""));
		}
		String username = TokenManager.tokenIssuedCompCode(token);
		String uploadType = jsondata.getTypeOfupload();
		try {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDateTime now = LocalDateTime.now();
		String time = dtf.format(now);
		String filename = time.replace("/", "_") + jsondata.getPayer_code();
		String path = ExternalFile.getExcelPath()+AccountHelper.generateRandomInt(4) + filename + ".xlsx";

		FileManager.saveImage(jsondata.getBase64Excel(), path);
		String file_id = AccountHelper.generateRandomInt(99999) + time;
		if(AccountHelper.isFileOk(path,uploadType)) {
		if (uploadType.equalsIgnoreCase("auto_eslip") ) {
			String data=AccountHelper.doesAccountExist(path, jsondata.getPayer_code(),jsondata.getBiller_code()).toString();
			return ResponseEntity.ok(data);
		}

		else if (uploadType.equalsIgnoreCase("manual_eslip") ) {
				AccountHelper.saveFile(path, jsondata.getFile_name(), jsondata.getBiller_code(), jsondata.getPayer_code(), file_id,
						username, uploadType);
				BillerHelper.linkPayerToBiller(jsondata.getBiller_code(), jsondata.getPayer_code(),"");
				Thread t = new Thread() {
					public void run() {
						QueryBulkBillsService.KPLCService(path, jsondata.getPayer_code(), jsondata.getBiller_code(), file_id,
								username, uploadType);
					}
				};
				t.start(); 
				EmailAlert.logger(username, "User uploaded file for payment", log);
				System_Logs.log_system(username, "", "Bank User uploaded file:", "Success", "");
				return ResponseEntity.ok(new AuthProduceJson("00", "Excel uploaded successfully", "", ""));
			} else {
				return ResponseEntity.ok(new AuthProduceJson("07", "Please provide upload type", "", ""));
			}
		}else {
			return ResponseEntity.ok(new AuthProduceJson("08", "Invalid File format", "", ""));
		}
		
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new AuthProduceJson("02", " Invalid file format or system technical error", "", ""));
		}
		
	}
	
	@RequestMapping(value = "/v1/addBillingAccountBank", method = RequestMethod.POST)
	 public ResponseEntity<?> addBillingAccount(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
		 try {
		String biller_code=jsondata.getBiller_code();
		String username=TokenManager.tokenIssuedCompCode(token);
		String due_date=jsondata.getDue_date();
		String account_no=jsondata.getAccount_no();
		String amount_due=jsondata.getAmount_due();
		String alias=jsondata.getAlias();
		String account_name=jsondata.getAccount_name();
		String payer_code=jsondata.getPayer_code();
		
	     
	     if(biller_code.isEmpty() || account_no.isEmpty() || payer_code.isEmpty()) {
	    	 return ResponseEntity.ok(new AuthProduceJson("06","Please provide all fields","",""));
	     }
	     
	     if(BillerHelper.isAccountExist(account_no,payer_code,biller_code)) {
	    	 String sql = "INSERT INTO account (biller_code,account_no,amount_due,payer_code,due_date,alias,account_name)VALUES(?,?,?,?,?,?,?) ON DUPLICATE KEY"
						+ " UPDATE amount_due=?,due_date=?,alias=?";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, biller_code);
				prep.setObject(2, account_no);
				prep.setObject(3, amount_due);
				prep.setObject(4, payer_code);
				prep.setObject(5, due_date);
				prep.setObject(6, alias);
				prep.setObject(7, account_name);
				prep.setObject(8, amount_due);
				prep.setObject(9, due_date);
				prep.setObject(10, alias);
				prep.execute();
				conn.close();
				BillerHelper.linkPayerToBiller(biller_code, payer_code,"");
	    		 return ResponseEntity.ok(new AuthProduceJson("00","Account already exist,updated  successfully","",""));
			}
			else {
				String sql = "INSERT INTO account (biller_code,account_no,amount_due,payer_code,due_date,alias,account_name)VALUES(?,?,?,?,?,?,?) ON DUPLICATE KEY"
						+ " UPDATE amount_due=?,due_date=?,alias=?";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, biller_code);
				prep.setObject(2, account_no);
				prep.setObject(3, amount_due);
				prep.setObject(4, payer_code);
				prep.setObject(5, due_date);
				prep.setObject(6, alias);
				prep.setObject(7, account_name);
				prep.setObject(8, amount_due);
				prep.setObject(9, due_date);
				prep.setObject(10, alias);
				prep.execute();
				conn.close();
				BillerHelper.linkPayerToBiller(biller_code, payer_code,"");
				EmailAlert.logger(username, "User addedbilling file for payer", log);
				System_Logs.log_system(username, "", "Bank User added accounts:", "Success", "");
	    		 return ResponseEntity.ok(new AuthProduceJson("00","Account saved  successfully","",""));
			}
				
			
	     }catch(Exception n) {
	    	 System.out.println(n.getMessage());
	    	 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
	     }
	     
	 }
}
