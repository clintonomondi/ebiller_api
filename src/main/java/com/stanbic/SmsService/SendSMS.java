package com.stanbic.SmsService;

import java.security.spec.KeySpec;
import java.util.Hashtable;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mysql.jdbc.Connection;
import com.stanbic.Accounts.AccountsConsumeJson;
import com.stanbic.Responses.Response;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;

@SpringBootApplication
@RequestMapping("/api")
public class SendSMS {

	private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    byte[] arrayBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;
    
	 @RequestMapping(value = "/v1/sendSMS", method = RequestMethod.POST)
	 public ResponseEntity<?> sendSMS(@RequestBody SMSConsumeJson jsondata){
		  Hashtable<String, String> configsleg = new Hashtable<String, String>();
          configsleg.put("msisdn", jsondata.getPhone_no());
          configsleg.put("message", jsondata.getSms_message());
          configsleg.put("sms_id", jsondata.getSms_code());

          // send and receive response from sms server
          String resposnes = GeneralCodes.postRequest1(configsleg, ExternalFile.getSMSBaseUrl(), "JSON");
          JSONObject obj = new JSONObject(resposnes);
          String jsonResponse = obj.toString();

          System.out.println("Response from sms server::" + jsonResponse);
          
          JsonParser parser = new JsonParser();
          JsonElement jsonTree = parser.parse(jsonResponse);
          com.google.gson.JsonObject jsonObject = jsonTree.getAsJsonObject();

          JsonElement status = jsonObject.get("status");
          JsonElement status_description = jsonObject.get("status_description");

          System.out.println("Status>>>>"+status.toString());
          System.out.println("Description>>>>"+status_description.toString());
	
          return ResponseEntity.ok(new Response("00","Success"));
	 }
	
	 @RequestMapping(value = "/v1/encrypt_password", method = RequestMethod.POST)
	 public ResponseEntity<?> passwordEncrypt(@RequestBody String jsondata){
		 String encryptedPass="";
		 try {
				
			 JSONObject obj = new JSONObject(jsondata);
				String  providedPassword=obj.getString("password");
				
			//	String  providedPassword=jsondata.getPassword();
			//	String salt=obj.getString("salt"); 

				myEncryptionKey = "hjfdhf$34556029hjfuur%)8839399qkksklk(()q8wj&*9w9kemmm8q8q0nm372901928";
				
		        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
		        
		        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
		        ks = new DESedeKeySpec(arrayBytes);
		        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
		        cipher = Cipher.getInstance(myEncryptionScheme);
		        key = skf.generateSecret(ks);
		        
		        System.out.println(providedPassword);
		        
				encryptedPass=encrypt(providedPassword);
				
		        System.out.println("Encrypted:::  "+encryptedPass );
		        
		        //"i3YNthm2lUo4jm9vK25vHTBcViJ/TPA6"
		        System.out.println("Decrypted : "+decrypt(encryptedPass));
				
				}catch(Exception e) {
					e.printStackTrace();
				}
				
		 return ResponseEntity.ok(new ResponseReqModel(encryptedPass));
	 }
	 
		public String encrypt(String unencryptedString) {
	        String encryptedString = null;
	        try {
	            cipher.init(Cipher.ENCRYPT_MODE, key);
	            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
	            byte[] encryptedText = cipher.doFinal(plainText);
	            encryptedString = new String(Base64.encodeBase64(encryptedText));
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return encryptedString;
	    }


	    public String decrypt(String encryptedString) {
	        String decryptedText=null;
	        try {
	            cipher.init(Cipher.DECRYPT_MODE, key);
	            byte[] encryptedText = Base64.decodeBase64(encryptedString);
	            byte[] plainText = cipher.doFinal(encryptedText);
	            decryptedText= new String(plainText);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return decryptedText;
	    }
}
