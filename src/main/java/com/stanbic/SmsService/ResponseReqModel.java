package com.stanbic.SmsService;

public class ResponseReqModel {
	String password="";
	String encrypted="";
	public ResponseReqModel(String encrypted) {
		this.encrypted=encrypted;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEncrypted() {
		return encrypted;
	}
	public void setEncrypted(String encrypted) {
		this.encrypted = encrypted;
	}
	
}
