package com.stanbic.SmsService;

public class SMSConsumeJson {

	String phone_no=null;
	String sms_message=null;
	String sms_code=null;
	
	public String getPhone_no() {
		return phone_no;
	}
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	public String getSms_message() {
		return sms_message;
	}
	public void setSms_message(String sms_message) {
		this.sms_message = sms_message;
	}
	public String getSms_code() {
		return sms_code;
	}
	public void setSms_code(String sms_code) {
		this.sms_code = sms_code;
	}
	
	
}
