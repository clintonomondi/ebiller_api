package com.stanbic.EmailAlert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Responses.Response;
import com.stanbic.comm.DbManager;
import com.stanbic.dropdown.country.CountryModel;
import com.stanbic.ebiller.auth.AuthConsumeJson;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class EmailAlert {
	static Connection conn=null;
	
	public static void saveAlert(String email,String description,String created_by) {
		try {
			 conn=DbManager.getConnection();
			 String sql="INSERT INTO email_alert(email,description,created_by)VALUES(?,?,?)";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, email);
			 prep.setObject(2, description);
			 prep.setObject(3, created_by);
			 prep.execute();
			 conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	 @RequestMapping(value = "/v1/emailAlerts", method = RequestMethod.POST)
	 public ResponseEntity<?> emailAlerts(HttpServletRequest request, @RequestHeader(name = "Authorization") String token) {
		 
		 List<EmailAlertModel> model= new ArrayList<EmailAlertModel>();
		 try {
			String sql="SELECT * FROM email_alert ORDER BY id DESC"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				EmailAlertModel r=new EmailAlertModel();
				r.id=rs.getString("id");
				r.created_by=rs.getString("created_by");
				r.description=rs.getString("description");
				r.created_at=rs.getString("created_at");
				r.email=rs.getString("email");
				model.add(r);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new Response("02", "System technical error"));
		 } 
	 }
	 
	 
	 @RequestMapping(value = "/v1/deleteEmailAlerts", method = RequestMethod.POST)
	 public ResponseEntity<?> deleteEmailAlerts(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
		 String id="";
		 try {
		 JSONObject obj = new JSONObject(jsondata);
			JSONArray slipInfoArray = obj.getJSONArray("alerts");
			String sql="DELETE FROM email_alert WHERE id=?"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			int ii=0;
			for (int i = 0; i < slipInfoArray.length(); i++) {
				JSONObject object = slipInfoArray.getJSONObject(i);
				id = object.getString("id");
				prep.setObject(1, id);
				prep.addBatch();
				ii++;
				if (ii % 1000 == 0 || ii == slipInfoArray.length()) {
					System.out.println(">deleting some alerts="+ii);
					prep.executeBatch(); // Execute every 1000 items
					conn.commit();
		            prep.clearBatch();
				}
			}
			return ResponseEntity.ok(new Response("00", "Record deleted successfully"));
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new Response("02", "System technical error"));
		 }
	 }
	 
	 public static void logger(String email, String description, Logger log) {
		 log.info(email + " " + description +  " at " + new java.util.Date());
		 
	 }
	 
	 public static void loggAccountValidation(String type,String email, String description, Logger log) {
		 log.info(type+" "+email + " " + description +  " at " + new java.util.Date());
		 
	 }
}


