package com.stanbic.KPLC;
import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.util.http.fileupload.UploadContext;
import org.springframework.http.ResponseEntity;

import java.sql.Connection;
import com.stanbic.Accounts.AccountHelper;
import com.stanbic.Accounts.AccountProduceJson;
import com.stanbic.comm.DbManager;
import com.stanbic.ebiller.auth.AuthProduceJson;


public class QueryBulkBillsService {
	static Connection conn = null;
	
	public static void KPLCService(String path,String payer_code,String biller_code,String file_id,String created_by,String uploadtype) {
		
		 Vector dataHolder=read(path);
	    	saveToDatabase(dataHolder,payer_code,biller_code,file_id,created_by,uploadtype);
	}
	
	 public static Vector read(String fileName)    {
	    	Vector cellVectorHolder = new Vector();
	    	try{
	    		FileInputStream myInput = new FileInputStream(fileName);
	   	    	//POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
	            XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);
	            XSSFSheet mySheet = myWorkBook.getSheetAt(0);
	           Iterator rowIter = mySheet.rowIterator(); 
	           int count=0;
	           while(rowIter.hasNext()){
	        	  XSSFRow myRow = (XSSFRow) rowIter.next();
	        	  Iterator cellIter = myRow.cellIterator();
	        	  Vector cellStoreVector=new Vector();
	        	  if(count>0) {
	        	  while(cellIter.hasNext()){
	        		  XSSFCell myCell = (XSSFCell) cellIter.next();
	        		  myCell.setCellType(Cell.CELL_TYPE_STRING);
	        		  cellStoreVector.addElement(myCell);
	        	  }   
	        	  cellVectorHolder.addElement(cellStoreVector);
	        	  }
	        	  count++;
	          }
	    	}catch (Exception e){
	    		e.printStackTrace();
	    		}
	    	return cellVectorHolder;
	    }
	  
	 private static void saveToDatabase(Vector dataHolder,String payer_code,String biller_code,String file_id,String created_by,String upload_type) {
		
		 try{ 
			conn = DbManager.getConnection();
					String sql = "INSERT INTO file_data (account_no,payer_code,biller_code,file_id,chuncode,created_by,upload_type,alias)VALUES(?,?,?,?,?,?,?,?)";
					PreparedStatement prep = conn.prepareStatement(sql);
					conn.setAutoCommit(false);
					int ii=0;
					int chuncode=1;
					for (int i=0;i<dataHolder.size(); i++){
						Vector cellStoreVector=(Vector)dataHolder.elementAt(i);
						
						String alias = "";
						
						if (cellStoreVector.toArray().length >= 2) {
							alias=cellStoreVector.toArray()[1].toString();
						}
		                	prep.setObject(1, cellStoreVector.toArray()[0].toString());
		   					prep.setObject(2, payer_code);
		   					prep.setObject(3, biller_code);
		   					prep.setObject(4, file_id);
		   					prep.setObject(5, chuncode);
		   					prep.setObject(6, created_by);
		   					prep.setObject(7, upload_type);
		   					prep.setObject(8,alias);
		   					prep.addBatch();	                   
		                
					ii++;
					if (ii % 1000 == 0 || ii == dataHolder.size()) {
						System.out.println(">Inserting file record in file_data table at initial stage="+ii);
		                prep.executeBatch(); // Execute every 1000 items.
		                chuncode++;
		                conn.commit();
		                prep.clearBatch();
		                
		                if(ii==dataHolder.size()){
		                	
		              	String sql2="UPDATE files SET state=? WHERE file_id=?";
		                	PreparedStatement prep2 = conn.prepareStatement(sql2);
		                	prep2.setObject(1, 1);
		                	prep2.setObject(2, file_id);
		                	prep2.execute();
						}
					}
					}
		            conn.setAutoCommit(true);
					conn.close();		
		 }
		catch(Exception e){
			e.printStackTrace();
			}
			
		}
	 
	
}
