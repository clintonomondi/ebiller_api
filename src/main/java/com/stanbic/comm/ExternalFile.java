package com.stanbic.comm;

import java.io.File;
import java.io.IOException;

import org.ini4j.Ini;


public class ExternalFile {
	public static String getSecretKey() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "SECRET_KEY");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	
	public static String getEmailUrl() {
		 String key=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  key=ini.get("header", "EmailUrl");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return key;
	 }
	
	
	
	public static String getUsbBaseUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "esb_account_fetch_url");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getPaymentUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "payment_base_url");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getLogoPath() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Logo_Path");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getExcelPath() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Excel_Path");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	public static String getExceptionURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "exception_file_url");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getSMSBaseUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "sms_url_link");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getDefaultImageUrl() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "DefaultLogo");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	public static String getUsbNtken() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Esb_NTKEN");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getExcelReportURL() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Excel_Report");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
	
	public static String getExcelReportURL_Team() {
		 String url=null;
		 try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			  url=ini.get("header", "Excel_Report_Team");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		 return url;
	 }
}
