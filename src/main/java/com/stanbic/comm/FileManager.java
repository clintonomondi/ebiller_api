package com.stanbic.comm;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.kafka.common.utils.Java;

public class FileManager {
static File f;
	public static String convertImageToString(String image_path) {

        f = new File(image_path);
        String imageString = encodeFileToBase64Binary(f);
        return imageString;
    }

    public static void saveImage(String imageString, String imagePath) {

        byte dearr[] = Base64.decodeBase64(imageString);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(new File(imagePath));
            fos.write(dearr);
            fos.close();
        } catch (Exception  e) {
            System.out.println(e.getMessage());
        }

    }
    
    public static void saveImageLogo(String imageString, String imagePath) {

    	 byte dearr[] = Base64.decodeBase64(imageString);
         FileOutputStream fos;
         try {
             fos = new FileOutputStream(new File(imagePath));
             fos.write(dearr);
             fos.close();
             
             
             File input = new File(imagePath);
             BufferedImage image = ImageIO.read(input);

             BufferedImage resized = resize(image, 175, 146);

             File output = new File(imagePath);
             ImageIO.write(resized, "png", output);
             
         } catch (Exception  e) {
           e.printStackTrace();
         }

    }
    
    private static BufferedImage resize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }
    

public void saveImagesForIndividualAccount(String fileLocationPath, String fileSavingPath) {
        
        try {
            File input = new File(fileLocationPath);
            BufferedImage image;
            image = ImageIO.read(input);
            BufferedImage resized = autoResize(image, 600, 650);
            File output = new File(fileSavingPath);
            ImageIO.write(resized, "png", output);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private static BufferedImage autoResize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }
    
    public static String encodeFileToBase64Binary(File file) {
        String encodedfile = null;
        try {
            @SuppressWarnings("resource")
			FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int) file.length()];
            fileInputStreamReader.read(bytes);
            encodedfile = new String (Base64.encodeBase64(bytes),"UTF-8");
            
        } catch (IOException e) {
          e.printStackTrace();
        }
        return encodedfile;
    }
    

}
