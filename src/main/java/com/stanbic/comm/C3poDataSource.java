//package com.stanbic.comm;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.mchange.v2.c3p0.ComboPooledDataSource;
//
//public class C3poDataSource {
//	 
//    private static ComboPooledDataSource cpds = new ComboPooledDataSource();
// 
//    static {
//        try {
//        	
//            cpds.setDriverClass("com.mysql.jdbc.Driver");
//            cpds.setJdbcUrl(DbManager.url());
//            cpds.setUser(DbManager.dbUserName());
//            cpds.setPassword(DbManager.password());
//        } catch (Exception e) {
//            // handle the exception
//        }
//    }
//     
//    public static Connection getConnection() throws SQLException {
//        return cpds.getConnection();
//    }
//     
//    private C3poDataSource(){}
//}
