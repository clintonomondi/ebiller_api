package com.stanbic.comm;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
public class Ldap {

	
	@SuppressWarnings("unchecked")
	public static boolean AuthLDAP(String usrname, String secret, String domain) {

		
		@SuppressWarnings("rawtypes")
		Hashtable env = new Hashtable(11);
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		// env.put(Context.PROVIDER_URL,"ldap://ke.sbicdirectory.com:389/DC=ke,DC=sbicdirectory,DC=com");
		env.put(Context.PROVIDER_URL, domain);

		//System.out.println(domain);
		// Authenticate as S. User and give incorrect password
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, usrname + "@ke.sbicdirectory.com");
		env.put(Context.SECURITY_CREDENTIALS, secret);

		try {
			// Create initial context
			DirContext ctx = new InitialDirContext(env);
			ctx.close();
			return true;
		} catch (NamingException e) {
			// e.printStackTrace();
			return false;
		}
	}
	
}
