package com.stanbic.comm;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.ini4j.Ini;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;

import com.stanbic.EmailAlert.EmailAlert;
import com.stanbic.ebiller.auth.AuthProduceJson;
import com.stanbic.ebiller.auth.BillerAuthconsumeJson;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@SpringBootApplication
@RequestMapping("/api")
public class EmailManager {

	static String FROM = null;
	static String FROMNAME = null;
	static String SMTP_USERNAME = null;
	static String SMTP_PASSWORD = null;
	static String HOST = null;
	static String PORT = null;
	static String SUBJECT = null;
	static String ebiller_base_url = null;
	static String message = null;
	
	@Autowired
	 private JavaMailSender sender;
	
	@Autowired
	JavaMailSenderImpl sender2;
	
	 static Connection conn;
	 

	public static String send_mail(String emailTo, String mess,String subject,String created_by,String description) {
		EmailAlert.saveAlert(emailTo, description, created_by);
//		try {
//			Ini ini = new Ini(new File(GeneralCodes.configFilePath()));
//			FROM = ini.get("header", "email_from");
//			FROMNAME = ini.get("header", "email_from_description");
//			SMTP_USERNAME = ini.get("header", "email_smtp_username");
//			SMTP_PASSWORD = ini.get("header", "email_smtp_password");
//			HOST = ini.get("header", "email_host");
//			PORT = ini.get("header", "email_port");
//			Properties props = System.getProperties();
//			props.put("mail.transport.protocol", "smtp");
//		      props.put("mail.debug", "true");
//			props.put("mail.smtp.port", PORT);
//			props.put("mail.smtp.starttls.enable", "true");
//			props.put("mail.smtp.auth", "true");
//			props.put("mail.smtp.EnableSSL.enable", "true"); 
//			
//			Session session = Session.getDefaultInstance(props);
//			try {
//				MimeMessage msg = new MimeMessage(session);
//				msg.setFrom(new InternetAddress(FROM, FROMNAME));
//				msg.setRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
//				msg.setSubject(subject);
//				msg.setContent(mess,"text/html");
//				Transport transport = session.getTransport();
//				transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
//				transport.sendMessage(msg, msg.getAllRecipients());
//				message = "200";
//				transport.close();
//				return message;
//			} catch (IOException e) { 
//				e.printStackTrace();
//				message="250";
//				return message;
//			}
//
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			message="250";
//			return message;
//		}
//		

	 Hashtable<String,String>map=new Hashtable<String,String>();
		 map.put("emailTo",emailTo);
		 map.put("emailMessage",mess);
		 map.put("emailHeader",subject);
		 String response=GeneralCodes.encryptedPostRequest(map, ExternalFile.getEmailUrl(), "JSON");
		 return response;	
	}
	
	
	public static String email_message_template(VelocityContext vc,String template_name) {
        StringWriter sw = null;//mail_temp.vm
        try {
            Properties p = new Properties();
            p.setProperty("resource.loader", "class");
            p.setProperty("class.resource.loader.class",
                    "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            Velocity.init(p);
            Template t = Velocity.getTemplate("templates/"+template_name);
            sw = new StringWriter();
            t.merge(vc, sw);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sw.toString();
    }

}
