package com.stanbic.comm;

public class EmailProduceJson {
	String messageCode = "";
	String message = "";
	String token="";
	String fname="";
	String email="";
	
	public EmailProduceJson(String messageCode, String message,String token,String fname,String email) {
		this.messageCode = messageCode;
		this.message = message;
		this.token=token;
		this.fname=fname;
		this.email=email;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
