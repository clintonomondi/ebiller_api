package com.stanbic.comm;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

public class TokenManager {
	public static final String TOKEN_SECRET = "s4T2zOIWHMM1sxq";

	public static String tokenIssuedCompCode(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getClaim("companyCode").asString();
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
			// log WRONG Encoding message
			return null;
		} catch (JWTVerificationException exception) {
			exception.printStackTrace();
			// log Token Verification Failed
			return null;
		}
	}

	public static String tokenIssuer(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getClaim("userType").asString();
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
			// log WRONG Encoding message
			return null;
		} catch (JWTVerificationException exception) {
			exception.printStackTrace();
			// log Token Verification Failed
			return null;
		}
	}

	public static String expiresAt(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getClaim("ExpiresAt").asString();
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
			// log WRONG Encoding message
			return null;
		} catch (JWTVerificationException exception) {
			exception.printStackTrace();
			// log Token Verification Failed
			return null;
		}
	}

	public static String createdAt(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getClaim("createdAt").asString();
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
			// log WRONG Encoding message
			return null;
		} catch (JWTVerificationException exception) {
			exception.printStackTrace();
			// log Token Verification Failed
			return null;
		}
	}

	public static String tokenIssuedId(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getClaim("emailCode").asString();
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
			// log WRONG Encoding message
			return null;
		} catch (JWTVerificationException exception) {
			exception.printStackTrace();
			// log Token Verification Failed
			return null;
		}
	}

	public static String userGroup(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getClaim("userGroup").asString();
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
			// log WRONG Encoding message
			return null;
		} catch (JWTVerificationException exception) {
			exception.printStackTrace();
			// log Token Verification Failed
			return null;
		}
	}

	public boolean isTokenValid(String token) {
		String userId = tokenIssuedCompCode(token);
		return userId != null;
	}

	// public static String generateJwtToken(String subject, String tokenId, String
	// SECRET_KEY, String issuer) {
	// Date now = new Date();
	// Date exp = new Date(System.currentTimeMillis() + (1000 * 3600)); // One hour
	//
	// try {
	// String token =
	// Jwts.builder().setSubject(subject).setExpiration(exp).setIssuedAt(now).setIssuer(issuer)
	// .setId(tokenId).signWith(SignatureAlgorithm.HS512, SECRET_KEY).compact();
	// return token;
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// return null;
	// }
	// }

	public static String createToken(String subject, String tokenId, String userGroup, String issuer) {
		Date now = new Date();
		Date exp = new Date(System.currentTimeMillis() + (1000 * 900 * 2)); // 30 mins
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		String expiry_date = formatter.format(exp);
		String now_date = formatter.format(now);

		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			String token = JWT.create().withClaim("companyCode", subject).withClaim("emailCode", tokenId)
					.withClaim("userType", issuer).withClaim("userGroup", userGroup).withClaim("createdAt", now_date)
					.withClaim("ExpiresAt", expiry_date).sign(algorithm);
			return token;
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
			// log WRONG Encoding message
		} catch (JWTCreationException exception) {
			exception.printStackTrace();
			// log Token Signing Failed
		}
		return null;
	}
	
	public static String createToken2(String subject, String tokenId, String userGroup, String issuer) {
		Date now = new Date();
		Date exp = new Date(System.currentTimeMillis() + (1000 * 900 * 672)); // 1 hour * 
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		String expiry_date = formatter.format(exp);
		String now_date = formatter.format(now);

		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			String token = JWT.create().withClaim("companyCode", subject).withClaim("emailCode", tokenId)
					.withClaim("userType", issuer).withClaim("userGroup", userGroup).withClaim("createdAt", now_date)
					.withClaim("ExpiresAt", expiry_date).sign(algorithm);
			return token;
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
			// log WRONG Encoding message
		} catch (JWTCreationException exception) {
			exception.printStackTrace();
			// log Token Signing Failed
		}
		return null;
	}


//	public static String generateJwtToken(String subject, String tokenId, String SECRET_KEY,String issuer) {
//		Date now = new Date();
//		Date exp = new Date(System.currentTimeMillis() + (1000 * 3600)); // One hour
//
//		try {
//			String token = Jwts.builder().setSubject(subject).setExpiration(exp).setIssuedAt(now).setIssuer(issuer)
//					.setId(tokenId).signWith(SignatureAlgorithm.HS512, SECRET_KEY).compact();
//			return token;
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	public static String tokenIssuedId(String security_key, String token) {
//
//		try {
//			String tokenIssuedId = null;
//			Claims body = Jwts.parser().setSigningKey(security_key).parseClaimsJws(token).getBody();
//			tokenIssuedId = body.getId();
//			Date now = new Date();
//			Date expiryDate = body.getExpiration();
//			if (expiryDate.after(now)) {
//				return tokenIssuedId;
//			} else {
//				return "03";
//			}
//
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			return "01";
//		}
//	}
//
//	public static String tokenIssuedCompCode(String security_key, String token) {
//
//		try {
//
//			String comp_code = null;
//			Claims body = Jwts.parser().setSigningKey(security_key).parseClaimsJws(token).getBody();
//			comp_code = body.getSubject();
//			Date now = new Date();
//			Date expiryDate = body.getExpiration();
//			if (expiryDate.after(now)) {
//				return comp_code;
//			} else {
//
//				return "03";
//			}
//
//		} catch (Exception e) {
//			return "01";
//		}
//	}
//	
//	public static String tokenIssuer(String security_key, String token) {
//
//		try {
//
//			String comp_code = null;
//			Claims body = Jwts.parser().setSigningKey(security_key).parseClaimsJws(token).getBody();
//			comp_code = body.getIssuer();
//			Date now = new Date();
//			Date expiryDate = body.getExpiration();
//			if (expiryDate.after(now)) {
//				return comp_code;
//			} else {
//
//				return "03";
//			}
//
//		} catch (Exception e) {
//			return "01";
//		}
//	}
}
