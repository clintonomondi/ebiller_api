package com.stanbic.comm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.Connection;

public class CheckUsers {
	static Connection conn=null;

	 public static int checkPayer(String email) {
		  int v_count=0;
		  try {
			  conn=DbManager.getConnection();
			  String sql="SELECT COUNT(*)V_COUNT FROM biller_profile WHERE email=?";
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  v_count=rs.getInt("V_COUNT");
			  }
			  conn.close();
		  }catch(Exception e) {
			  e.printStackTrace();
		  }
		  
		  return v_count;
	  }
	 
	 public static int checkUser(String email) {
		  int v_count=0;
		  try {
			  conn=DbManager.getConnection();
			  String sql="SELECT COUNT(*) AS V_COUNT FROM ebiller_auth WHERE email=?";
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  v_count=rs.getInt("V_COUNT");
			  }
			  conn.close();
		  }catch(Exception e) {
			  e.printStackTrace();
		  }
		  
		  return v_count;
	  }
	
	
	 public static String  checkBillerAuth(String email,String password) {
		 String  comp_code=null;
		  try {
			  conn=DbManager.getConnection();
			  String sql="SELECT comp_code FROM ebiller_auth WHERE email=? AND password=?";
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  prep.setObject(2, password);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  comp_code=rs.getString("comp_code");
			  }
			  conn.close();
		  }catch(Exception e) {
			  e.printStackTrace();
		  }
		  
		  return comp_code;
	 }
	  
	 public static String  getBillerCode(String email) {
		 String  comp_code=null;
		  try {
			  conn=DbManager.getConnection();
			  String sql="SELECT comp_code FROM biller_profile WHERE email=?";
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  comp_code=rs.getString("comp_code");
				  return comp_code;
			  }
			  conn.close();
		  }catch(Exception e) {
			 System.out.println(e.getMessage());
		  }
		  
		  return comp_code;
	 }
	 
	 
	 public static int checkBiller(String email) {
		  int v_count=0;
		  try {
			  conn=DbManager.getConnection();
			  String sql="SELECT COUNT(*) AS V_COUNT FROM ebiller_auth WHERE email=?";
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, email);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  v_count=rs.getInt("V_COUNT");
			  }
			  conn.close();
		  }catch(Exception e) {
			  e.printStackTrace();
		  }
		  
		  return v_count;
	  }


}
