package com.stanbic.comm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.Connection;

public class PathUrl {
	
	static Connection conn=null;
	
	 public static String  getBillerAppPath(String description) {
		 String  pathname=null;
		  try {
			  conn=DbManager.getConnection();
			  String sql="SELECT name FROM pathurl WHERE description=?";
			  PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, description);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  pathname=rs.getString("name");
			  }
			  conn.close();
			  return pathname;
			  
		  }catch(Exception e) {
			 System.out.println(e.getMessage());
		  }
		  
		  return pathname;
	 }
}
