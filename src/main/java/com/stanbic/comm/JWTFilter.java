package com.stanbic.comm;

import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.GenericFilterBean;

@Configuration
public class JWTFilter extends GenericFilterBean {
	private TokenManager tokenService;

	public JWTFilter() {
		this.tokenService = new TokenManager();
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) {

//		System.out.println("In the token filter");

		String token = "";
		String System_name = "";
		try {

			HttpServletRequest request = (HttpServletRequest) req;
			HttpServletResponse response = (HttpServletResponse) res;
			token = request.getHeader("Authorization");


			if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
				response.sendError(HttpServletResponse.SC_OK, "success");
				return;
			}

			if (allowRequestWithoutToken(request)) {
				response.setStatus(HttpServletResponse.SC_OK);

				filterChain.doFilter(req, res);
			} else {

				if (token == null || !tokenService.isTokenValid(token)) {
//					System.out.println("Token is invalid.The request rejected");
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				} else {

//					System.out.println("Token: " + token);
//					
//					System.out.println("The token has been validated successfully");

					SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy hh:mm"); // first example
					Date dateNow = new Date();
					Date ExpiryDate = format1.parse(new TokenManager().expiresAt(token));
					ExpiryDate.setHours(ExpiryDate.getHours() + 12);
//					System.out.println("Expiry date::" + ExpiryDate);
//					System.out.println("Date now" + dateNow);

					 if (ExpiryDate.before(dateNow)) {
					 response.sendError(HttpServletResponse.SC_REQUEST_TIMEOUT);
					 }

					String companyCode = TokenManager.tokenIssuedCompCode(token);
					String userName = TokenManager.tokenIssuedId(token);
					String userGroup = TokenManager.userGroup(token);
					String userType = TokenManager.tokenIssuer(token);

					if (request.getRequestURI().contains("v1/bankSignup")) {

					} else if (request.getRequestURI().contains("v1/bankLogin")) {

					} else if (request.getRequestURI().contains("/v1/payerSignup")) {

					} else if (request.getRequestURI().contains("/v1/sendEmailResetPassword")) {

					}
					else if (request.getRequestURI().contains("/v1/biller/setPassword")) {

					} else if (request.getRequestURI().contains("/v1/billerLogin")) {

					} else {
						token = TokenManager.createToken(companyCode, userName, userGroup, userType);
						HttpServletResponse httpServletResponse = (HttpServletResponse) response;
						httpServletResponse.setHeader("Authorization", token);
//						System.out.println("New header" + httpServletResponse.getHeaders("Authorization"));
					}
					filterChain.doFilter(req, res);
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
		}

	}

	public boolean allowRequestWithoutToken(HttpServletRequest request) {
		//encrypt_password
		
		if (request.getRequestURI().contains("/v1/encrypt_password")) {
			return true;
		}
		if (request.getRequestURI().contains("/v1/encrypt_password")) {
			return true;
		}
		if (request.getRequestURI().contains("/v1/eslipReconcileReport")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/bankLogin")) {
			return true;
		}//
		if (request.getRequestURI().contains("/v1/submitContact")) {
			return true;
		}
		if (request.getRequestURI().contains("/v1/testLogger")) {
			return true;
		}
		if (request.getRequestURI().contains("/v1/sendSMS")) {
			return true;
		}
		if (request.getRequestURI().contains("/v1/resetOtpPassword")) {
			return true;
		}
		if (request.getRequestURI().contains("/v1/verifyOtpCode")) {
			return true;
		}
		if (request.getRequestURI().contains("/v1/getOtpCode")) {
			return true;
		}
		if(request.getRequestURI().contains("/v1/sendEmailPayer")) {
			return true;
		}
		if(request.getRequestURI().contains("/v1/sendEmailResetPassword")) {
			return true;
		}
		if (request.getRequestURI().contains("/v1/bankSignup")) {
			return true;
		}
		if (request.getRequestURI().contains("/v1/payerSignup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/billerLogin")) {
			return true;
		}

		if (request.getRequestURI().contains("/v1/setPassword")) {
			return true;
		}

		if (request.getRequestURI().contains("v1/getBankUserGroup")) {
			return false;
		}

		if (request.getRequestURI().contains("/v1/eslipPdfReport")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getEslipBills")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getEslipInfo")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMyEslips")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/generateEslip")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getEslipReports")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/eslipPayment")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/eslipReport")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/saveInvoiceLogo")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getInvoiceLogo")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getPayerProfile")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/approvePendingBillers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/listOfPendingBillersApproval")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/flagPayerToBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getAllPayers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/payerDetails")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/verifyEslip")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/EslipAccounts")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/addMenuToGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/addMyMenues")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMenues")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/updateProfile")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/updateInvoiceTerms")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/updateInvoiceSettings")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getInvoiceSettings")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/createBankUser")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/validateAccount")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/generateEslipAuto")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getUploadedFilesRecordsAuto")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMyUploadedFilesAutoEslip")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/uploadExcelAccounts")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getUploadedFilesRecords")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMyUploadedFiles")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/editBankUserGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/addBankUserGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getBankUserGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/editBankUsers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getBankUsers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/addCountry")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/countAllBillers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/deleteInvitedBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getPendingBillers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/viewPayerProfile")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/viewBillerProfile")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getInvitedBillers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getActiveBillers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/listOfBillers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/addBillingAccount")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getPayerAccounts")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMyBillers")) {
			return false;
		}

		if (request.getRequestURI().contains("/v1/addSector")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getLocations")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getCountryBranches")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getCountry")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/addCompanyUser")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getCompanyUsers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/resendInvite")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMyGroups")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/addGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMenuForGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/billerBoarding")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/billerBillsMonth")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/billerNoOfEmployees")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/billerSector")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/billerBankInfo")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/fetchBills")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/requestTobeBiller")) {
			return false;
		}

		if (request.getRequestURI().contains("/v1/filterTest")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getBillerFirstName")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/sendTestInvoice")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/refreshBalance")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMyEslipsBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMyPayers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/updateBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/emailAlerts")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/deleteEmailAlerts")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getAllEslipService")) {
			return false;
		}
		//
		if (request.getRequestURI().contains("/v1/updateNotificationSettings")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/payEslip")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getExceptionLogs")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getExceptionOwners")) {
			return false;
		} 
		if (request.getRequestURI().contains("/v1/readExceptionLogs")) {
			return false;
		} 
		if (request.getRequestURI().contains("/v1/readExceptionLogs2")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/eslipReconcileReportBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/eslipReconcileReportPayer")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/ignoreExceptionLogs")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/validateEslip")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getAllPayersFromBankside")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/updatePayerProfile")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getPayerAccountsFromBankside")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/approveEslip")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/receiptPdf")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getDashBoardDataBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getDashBoardData")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getDashBoardDataBank")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/deleteInvitedBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getPendingBillers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/viewPayerProfile")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/viewBillerProfile")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getInvitedBillers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getActiveBillers")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/addbankMenuToGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getBankMenuForGroup")) {
			return false;
		}
		
		if (request.getRequestURI().contains("/v1/getMenues")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getBankMenues")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/addMyMenues")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/updateMyTeam")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/freezeUser")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/restoreUser")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/deleteUserGroup")) {
			return false;
		}
		
		if (request.getRequestURI().contains("/v1/getSystemMonitoring")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/resetPassword")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/changePassword")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMyOldReport")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getMyOldReportDeatils")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/addDepartment")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/editDepartment")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/rejectEditingGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/RejectdeleteBankGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/RejectaddBankGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/RejectEditedBankUsers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/RejectdeleteBankUser")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/rejectCreatedBankUser")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/RejectEditedBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/rejectNewBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/ApprovedeleteBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/RejectdeleteBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/deleteBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getDepartments")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/billerSendEmail")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/updateInvoice")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/inviteMultiple")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/uploadInvoice")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getUploadedInvoiceFiles")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getInvoiceFileRecords")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/disputeInvoice")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/generateInvoiceeslip")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getInvoiceEslipBills")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getInvoiceEslipBills")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getInvoiceEslipInfo")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/InvoiceEslipAccounts")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/invoiceEslipReconcileReportBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/InvoiceEslipPdfReport")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/validateCustomerNumber")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/onboardOpenBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/onboardClosedBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/Test")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/setForex")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getForex")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/setCommission")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getCommission")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/setFileSettings")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getFileSettings")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/uploadInvoiceIndividual")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/reValidateAccounts")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/TestExport")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/getPaidEslipsBank")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getExceptionLogsBiller")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/PayFailedPaidEslips")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/deleteBankUser")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/approveEditingGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/ProceedAproval")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/approveEditeBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/approveBiller")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/getSystemLogs")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/ApproveDeleteBankUser")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/editCountry")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/getBranchesPerCountry")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/deleteCountry")) {
			return false;
		}//
		if (request.getRequestURI().contains("/v1/ApproveaddBankGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/ApprovedeleteBankGroup")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/ApproveEditedBankUsers")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/editCountryBranches")) {
			return false;
		}
		if (request.getRequestURI().contains("/v1/deleteBranch")) {
			return false;
		}
		
		return false;
	}
}
