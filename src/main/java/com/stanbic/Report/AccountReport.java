package com.stanbic.Report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class AccountReport {
	Connection conn=null;
	
	@RequestMapping(value = "/v1/accountReport", method = RequestMethod.POST)
    public ResponseEntity<?> accountReport(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountReportModel jsondata){
		if(jsondata.getBiller_code().isEmpty() || jsondata.getAccount_no().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide both biller code and account no","",""));
		}
		 List<AccountReportModel> model= new ArrayList<AccountReportModel>();
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
		 if(comp_code.equalsIgnoreCase("03")){
			 return ResponseEntity.ok(new AuthProduceJson("01","Page has expired","",""));
		}
	     if(comp_code.equalsIgnoreCase("01")) {
			 return ResponseEntity.ok(new AuthProduceJson("03","An error occured,page might expired","",""));
		}
	     try {
	     String sql="SELECT eslip_no,amount_due,amount_to_pay,status,account_no,due_date,created_at,(SELECT amount_to_pay FROM eslip B  WHERE B.eslip_no=A.eslip_no)amountPay,\r\n" + 
	     		"(SELECT amount_due FROM eslip C  WHERE C.eslip_no=A.eslip_no)amountDue,\r\n" + 
	     		"(SELECT STATUS FROM eslip D  WHERE D.eslip_no=A.eslip_no)eslipStatus,\r\n" + 
	     		"(SELECT expiry_date FROM eslip E  WHERE E.eslip_no=A.eslip_no)expiryDate,\r\n" + 
	     		"(SELECT biller_code FROM eslip F  WHERE F.eslip_no=A.eslip_no)biller_code,\r\n" + 
	     		"(SELECT company_name FROM biller_profile G  WHERE G.comp_code=?)biller_name,\r\n" + 
	     		"(SELECT approved_by FROM eslip H WHERE H.eslip_no=A.eslip_no)approved_by,\r\n" + 
	     		"(SELECT bank_ref_no FROM eslip I WHERE I.eslip_no=A.eslip_no)bank_ref_no,\r\n" + 
	     		"(SELECT created_at FROM eslip J WHERE J.eslip_no=A.eslip_no)eslip_created_at,\r\n" + 
	     		"(SELECT created_by FROM eslip K WHERE K.eslip_no=A.eslip_no)created_by,\r\n" + 
	     		"(SELECT payment_date FROM eslip L WHERE L.eslip_no=A.eslip_no)payment_date\r\n" + 
	     		"FROM eslip_bills A WHERE account_no=?\r\n" + 
	     		" GROUP BY eslip_no,amount_due,amount_to_pay,STATUS,account_no,due_date,created_at";
	     conn=DbManager.getConnection();
		 PreparedStatement prep=conn.prepareStatement(sql);
		 prep.setObject(1, jsondata.getBiller_code());
		 prep.setObject(2, jsondata.getAccount_no());
		  ResultSet rs=prep.executeQuery();
		  while(rs.next()) {
			  AccountReportModel data=new AccountReportModel();
			    data.eslip_no = rs.getString("eslip_no");
				data.amount_due = rs.getString("amount_due");
				data.amount_to_pay = rs.getString("amount_to_pay");
				data.status = rs.getString("status");
				data.due_date=rs.getString("due_date");
				data.account_no=rs.getString("account_no");
				 data.eslipamountDue = rs.getString("amountDue");
					data.eslipStatus = rs.getString("eslipStatus");
					data.eslipexpiryDate = rs.getString("expiryDate");
					data.biller_code = rs.getString("biller_code");
					data.biller_name=rs.getString("biller_name");
					data.approved_by=rs.getString("approved_by");
					data.bank_ref_no = rs.getString("bank_ref_no");
					data.created_at = rs.getString("created_at");
					data.biller_name=rs.getString("biller_name");
					data.payment_date=rs.getString("payment_date");
					data.eslip_created_at=rs.getString("eslip_created_at");
				
				
				model.add(data);
		  }
		  conn.close();
		  return ResponseEntity.ok(model);
	     }catch(Exception e) {
	    	 e.printStackTrace();
	    	 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
	     }
		
	}

}
