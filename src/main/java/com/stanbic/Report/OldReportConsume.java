package com.stanbic.Report;

public class OldReportConsume {
	String messageCode="00";
	String message="success";
String fromdate="";
String todate="";

String id="";
String eslip_no="";
String payment_date="";
String amount="";
String payref="";
String created_at="";
String corporateid="";



public String getMessageCode() {
	return messageCode;
}
public void setMessageCode(String messageCode) {
	this.messageCode = messageCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getEslip_no() {
	return eslip_no;
}
public void setEslip_no(String eslip_no) {
	this.eslip_no = eslip_no;
}
public String getPayment_date() {
	return payment_date;
}
public void setPayment_date(String payment_date) {
	this.payment_date = payment_date;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}
public String getPayref() {
	return payref;
}
public void setPayref(String payref) {
	this.payref = payref;
}
public String getCreated_at() {
	return created_at;
}
public void setCreated_at(String created_at) {
	this.created_at = created_at;
}
public String getCorporateid() {
	return corporateid;
}
public void setCorporateid(String corporateid) {
	this.corporateid = corporateid;
}
public String getFromdate() {
	return fromdate;
}
public void setFromdate(String fromdate) {
	this.fromdate = fromdate;
}
public String getTodate() {
	return todate;
}
public void setTodate(String todate) {
	this.todate = todate;
}


}
