package com.stanbic.Report;

public class AccountReportModel {
    String messageCode="00";
    String message="success";
	String amount_due=null;
	String eslip_no=null;
	String amount_to_pay=null;
	String status=null;
	String due_date=null;
	String eslipamountPay=null;
	String eslipamountDue=null;
    String eslipStatus=null;
    String eslipexpiryDate=null;
    String biller_code=null;
    String biller_name=null;
    String approved_by=null;
    String bank_ref_no=null;
    String created_at=null;
    String created_by=null;
    String payment_ref_no=null;
    String payment_date=null;
    String account_no=null;
    String token=null;
    String eslip_created_at=null;
    
    
    
    
	public String getEslip_created_at() {
		return eslip_created_at;
	}
	public void setEslip_created_at(String eslip_created_at) {
		this.eslip_created_at = eslip_created_at;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAccount_no() {
		return account_no;
	}
	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}
	public String getAmount_due() {
		return amount_due;
	}
	public void setAmount_due(String amount_due) {
		this.amount_due = amount_due;
	}
	public String getEslip_no() {
		return eslip_no;
	}
	public void setEslip_no(String eslip_no) {
		this.eslip_no = eslip_no;
	}
	public String getAmount_to_pay() {
		return amount_to_pay;
	}
	public void setAmount_to_pay(String amount_to_pay) {
		this.amount_to_pay = amount_to_pay;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDue_date() {
		return due_date;
	}
	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}
	public String getEslipamountPay() {
		return eslipamountPay;
	}
	public void setEslipamountPay(String eslipamountPay) {
		this.eslipamountPay = eslipamountPay;
	}
	public String getEslipamountDue() {
		return eslipamountDue;
	}
	public void setEslipamountDue(String eslipamountDue) {
		this.eslipamountDue = eslipamountDue;
	}
	public String getEslipStatus() {
		return eslipStatus;
	}
	public void setEslipStatus(String eslipStatus) {
		this.eslipStatus = eslipStatus;
	}
	public String getEslipexpiryDate() {
		return eslipexpiryDate;
	}
	public void setEslipexpiryDate(String eslipexpiryDate) {
		this.eslipexpiryDate = eslipexpiryDate;
	}
	public String getBiller_code() {
		return biller_code;
	}
	public void setBiller_code(String biller_code) {
		this.biller_code = biller_code;
	}
	public String getBiller_name() {
		return biller_name;
	}
	public void setBiller_name(String biller_name) {
		this.biller_name = biller_name;
	}
	public String getApproved_by() {
		return approved_by;
	}
	public void setApproved_by(String approved_by) {
		this.approved_by = approved_by;
	}
	public String getBank_ref_no() {
		return bank_ref_no;
	}
	public void setBank_ref_no(String bank_ref_no) {
		this.bank_ref_no = bank_ref_no;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getCreated_by() {
		return created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
	public String getPayment_ref_no() {
		return payment_ref_no;
	}
	public void setPayment_ref_no(String payment_ref_no) {
		this.payment_ref_no = payment_ref_no;
	}
	public String getPayment_date() {
		return payment_date;
	}
	public void setPayment_date(String payment_date) {
		this.payment_date = payment_date;
	}
    
    
	


}
