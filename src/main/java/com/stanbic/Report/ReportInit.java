package com.stanbic.Report;

import java.sql.Connection;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import com.stanbic.comm.DbManager;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class ReportInit {
	JasperPrint jasperPrint = null;
	   ServletOutputStream serveletStream = null;
	   Connection conn=null;    
	   public void reportInit(Map<String, Object> parameter,String reportTemplateDirectory,String reportStorageDirectory) {
	   conn = DbManager.getConnection(); // opens a jdbc connection
	   try {
	           jasperPrint = JasperFillManager.fillReport(reportTemplateDirectory, parameter, conn);
	           JasperExportManager.exportReportToPdfFile(jasperPrint, reportStorageDirectory);
	           conn.close();
	   } catch (JRException ex) {
	       ex.printStackTrace();
	   } catch (Exception ex) {
	       ex.printStackTrace();
	   }
	    }
}
