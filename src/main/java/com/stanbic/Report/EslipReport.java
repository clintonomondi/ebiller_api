package com.stanbic.Report;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.ini4j.Ini;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.stanbic.Eslip.EslipBillsProduceJson;
import com.stanbic.Eslip.EslipService;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class EslipReport {
	 Connection conn;
	 
	 @RequestMapping(value = "/v1/eslipPdfReport", method = RequestMethod.POST)
     public ResponseEntity<?> eslipPdfReport(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipPdfReportRequestModel jsondata){
    if(jsondata.getEslip_no().isEmpty() || jsondata.getEslip_no().equalsIgnoreCase("")) {
    	return ResponseEntity.ok(new AuthProduceJson("06", "Please provide eslip number", "", ""));
    }
     
      Hashtable<String,Object> map=new Hashtable<String,Object>();
      String eslip_no=jsondata.getEslip_no();
      String totalAmount="";
      String reportTempPath="";
      String reportPath="";
      String report_fullPath="";
      String sub_report_base_path=""; 
      String contact_icon_base_path="";
      String website_icon_base_path="";
      String address_icon_base_path="";
      String mpesaNotif="PAYABLE";
      String stanbic_logo_base_path="";
     
     try {
     
          Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
          reportTempPath=ini.get("header", "reportTempPath");
          reportPath=ini.get("header", "reportPath");
          sub_report_base_path=ini.get("header","sub_report_base_path");
          stanbic_logo_base_path=ini.get("header","sub_report_base_path");
          contact_icon_base_path=ini.get("header","sub_report_base_path");
          website_icon_base_path=ini.get("header","sub_report_base_path");
          address_icon_base_path=ini.get("header","sub_report_base_path");
          String sql="SELECT amount_to_pay FROM eslip WHERE eslip_no=?";
          conn=DbManager.getConnection();
          PreparedStatement prep=conn.prepareStatement(sql);
          prep.setObject(1, eslip_no);
          ResultSet rs=prep.executeQuery();
          while(rs.next()) {
              totalAmount=rs.getString("amount_to_pay");
          }
          conn.close();
          String reportTempPath2="";
         int rand=EslipService.genRandom();
          report_fullPath=reportPath+rand+"eslip_document.pdf";
          reportTempPath2=reportTempPath+"eslip_document.jasper";
          
          java.text.DecimalFormat df = new java.text.DecimalFormat("###,###,###,###,###,###.##");
          String total_amount = df.format(Double.parseDouble(totalAmount));
          
          if(Double.parseDouble(totalAmount) > 70000.00) {
              mpesaNotif = "ABOVE LIMIT";
          }
        
      Map<String, Object> parameter=new HashMap<String,Object>();
      parameter.put("eslipNo",  eslip_no);
      parameter.put("AMOUNT",  total_amount);
      parameter.put("sub_report_base_path", reportTempPath);
      parameter.put("mpesaNotif", mpesaNotif);
      parameter.put("stanbic_logo_base_path", stanbic_logo_base_path);
      parameter.put("contact_icon_base_path", contact_icon_base_path);
      parameter.put("website_icon_base_path", website_icon_base_path);
      parameter.put("address_icon_base_path", address_icon_base_path);
      new ReportInit().reportInit(parameter, reportTempPath2, report_fullPath);
//      map.put("messageCode", "00");
//      map.put("message", "Success");
//      map.put("document_name", rand+"eslip_document.pdf");
//      map.put("document_path", reportPath);
//      return ResponseEntity.ok(map);
  }catch(Exception e) {
      map.put("messageCode", "02");
      map.put("message", "Error encountered");
      e.printStackTrace();
      return ResponseEntity.ok(map);
  }
     
     
     
     
     
     try {
 	      File file = new File(report_fullPath);
 	      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
 	      HttpHeaders headers = new HttpHeaders();
 	      
 	      headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
 	      headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
 	      headers.add("Pragma", "no-cache");
 	      headers.add("Expires", "0");
 	      
 	      ResponseEntity<Object> 
 	      responseEntity = ResponseEntity.ok().headers(headers).contentLength(
 	      file.length()).contentType(MediaType.parseMediaType("application/txt")).body(resource);
 	      
 	      return responseEntity;
 	    }catch(Exception e) {
 	    	e.printStackTrace();
 	    	 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
 	    }
     
  }
	 
	 @RequestMapping(value = "/v1/InvoiceEslipPdfReport", method = RequestMethod.POST)
     public ResponseEntity<?> InvoiceEslipPdfReport(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipPdfReportRequestModel jsondata){
    if(jsondata.getEslip_no().isEmpty() || jsondata.getEslip_no().equalsIgnoreCase("")) {
    	return ResponseEntity.ok(new AuthProduceJson("06", "Please provide eslip number", "", ""));
    }
     
      Hashtable<String,Object> map=new Hashtable<String,Object>();
      String eslip_no=jsondata.getEslip_no();
      String totalAmount="";
      String reportTempPath="";
      String reportPath="";
      String report_fullPath="";
      String sub_report_base_path=""; 
      String contact_icon_base_path="";
      String website_icon_base_path="";
      String address_icon_base_path="";
      String mpesaNotif="PAYABLE";
      String stanbic_logo_base_path="";
     
     try {
     
          Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
          reportTempPath=ini.get("header", "reportTempPath");
          reportPath=ini.get("header", "reportPath");
          sub_report_base_path=ini.get("header","sub_report_base_path");
          stanbic_logo_base_path=ini.get("header","sub_report_base_path");
          contact_icon_base_path=ini.get("header","sub_report_base_path");
          website_icon_base_path=ini.get("header","sub_report_base_path");
          address_icon_base_path=ini.get("header","sub_report_base_path");
          String sql="SELECT amount_to_pay FROM invoice_eslip WHERE eslip_no=?";
          conn=DbManager.getConnection();
          PreparedStatement prep=conn.prepareStatement(sql);
          prep.setObject(1, eslip_no);
          ResultSet rs=prep.executeQuery();
          while(rs.next()) {
              totalAmount=rs.getString("amount_to_pay");
          }
          conn.close();
          String reportTempPath2="";
         int rand=EslipService.genRandom();
          report_fullPath=reportPath+rand+"eslip_document.pdf";
          reportTempPath2=reportTempPath+"eslip_document.jasper";
          
          java.text.DecimalFormat df = new java.text.DecimalFormat("###,###,###,###,###,###.##");
          String total_amount = df.format(Double.parseDouble(totalAmount));
          
          if(Double.parseDouble(totalAmount) > 70000.00) {
              mpesaNotif = "ABOVE LIMIT";
          }
        
      Map<String, Object> parameter=new HashMap<String,Object>();
      parameter.put("eslipNo",  eslip_no);
      parameter.put("AMOUNT",  total_amount);
      parameter.put("sub_report_base_path", reportTempPath);
      parameter.put("mpesaNotif", mpesaNotif);
      parameter.put("stanbic_logo_base_path", stanbic_logo_base_path);
      parameter.put("contact_icon_base_path", contact_icon_base_path);
      parameter.put("website_icon_base_path", website_icon_base_path);
      parameter.put("address_icon_base_path", address_icon_base_path);
      new ReportInit().reportInit(parameter, reportTempPath2, report_fullPath);
//      map.put("messageCode", "00");
//      map.put("message", "Success");
//      map.put("document_name", rand+"eslip_document.pdf");
//      map.put("document_path", reportPath);
//      return ResponseEntity.ok(map);
  }catch(Exception e) {
      map.put("messageCode", "02");
      map.put("message", "Error encountered");
      e.printStackTrace();
      return ResponseEntity.ok(map);
  }
     
     
     
     
     
     try {
 	      File file = new File(report_fullPath);
 	      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
 	      HttpHeaders headers = new HttpHeaders();
 	      
 	      headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
 	      headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
 	      headers.add("Pragma", "no-cache");
 	      headers.add("Expires", "0");
 	      
 	      ResponseEntity<Object> 
 	      responseEntity = ResponseEntity.ok().headers(headers).contentLength(
 	      file.length()).contentType(MediaType.parseMediaType("application/txt")).body(resource);
 	      
 	      return responseEntity;
 	    }catch(Exception e) {
 	    	e.printStackTrace();
 	    	 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
 	    }
     
  }
	 

	 @RequestMapping(value = "/v1/receiptPdf", method = RequestMethod.POST)
     public ResponseEntity<?> receiptPdf(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody EslipPdfReportRequestModel jsondata){
		 if(jsondata.getEslip_no().isEmpty() || jsondata.getEslip_no().equalsIgnoreCase("")) {
		    	return ResponseEntity.ok(new AuthProduceJson("06", "Please provide eslip number", "", ""));
		  }
		  Hashtable<String,Object> map=new Hashtable<String,Object>();
		  String eslip_no=jsondata.getEslip_no();
		  String reportTempPath="";
		  String reportPath="";
		  String report_fullPath="";
		  String stanbic_logo_base_path="";
		  try {
			  Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
		      reportTempPath=ini.get("header", "reportTempPath");
		      reportPath=ini.get("header", "reportPath");
		      stanbic_logo_base_path=ini.get("header","sub_report_base_path");
		      String reportTempPath2="";
		      int rand=EslipService.genRandom();	      
		      report_fullPath=reportPath+rand+new java.util.Date().getYear()+"receipt.pdf";
		      reportTempPath2=reportTempPath+"receipt.jasper";
		      Map<String, Object> parameter=new HashMap<String,Object>();
		      parameter.put("eslip_no",  eslip_no);
		      parameter.put("path_to_background", stanbic_logo_base_path);
		      new ReportInit().reportInit(parameter, reportTempPath2, report_fullPath);
		  }catch(Exception e) {
		      map.put("messageCode", "02");
		      map.put("message", "Error encountered");
		      e.printStackTrace();
		      return ResponseEntity.ok(map);
		  }
		  try {
		 	  File file = new File(report_fullPath);
		 	  InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		 	  HttpHeaders headers = new HttpHeaders();
		 	  headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
		 	  headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		 	  headers.add("Pragma", "no-cache");
		 	  headers.add("Expires", "0");
		 	  ResponseEntity<Object> 
		 	  responseEntity = ResponseEntity.ok().headers(headers).contentLength(
		 	  file.length()).contentType(MediaType.parseMediaType("application/txt")).body(resource);
		 	  return responseEntity;
		 }catch(Exception e) {
		 	  e.printStackTrace();
		 	  return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
	 }
	 
	 
	 
	 

		@RequestMapping(value = "/v1/eslipReport", method = RequestMethod.POST)
	    public ResponseEntity<?> eslipReport(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody AccountReportModel jsondata){
			if(jsondata.getEslip_no().isEmpty()) {
				 return ResponseEntity.ok(new AuthProduceJson("06","Please provide eslip_no","",""));
			}
			List<ESlipReportModel> model = new ArrayList<ESlipReportModel>();
			try {
				String sql="SELECT eslip_no,account_no,amount_due,amount_to_pay,status,due_date,account_name,created_at,biller_payment_ref,\r\n" + 
						"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)company_name,\r\n" + 
						"(SELECT currency FROM biller_profile D WHERE D.comp_code=A.biller_code)currency,\r\n" + 
						"(SELECT bank_ref_no FROM eslip C WHERE C.eslip_no=A.eslip_no)bank_ref_no\r\n" + 
						" FROM `eslip_bills` A WHERE eslip_no=?";
				  conn=DbManager.getConnection();
					 PreparedStatement prep=conn.prepareStatement(sql);
					 prep.setObject(1, jsondata.getEslip_no());
					  ResultSet rs=prep.executeQuery();
					  while(rs.next()) {
						  ESlipReportModel data = new ESlipReportModel();
							data.amount_due = rs.getString("amount_due");
							data.amount_to_pay = rs.getString("amount_to_pay");
							data.eslip_no = rs.getString("eslip_no");
							data.status = rs.getString("status");
							data.due_date=rs.getString("due_date");
							data.account_no=rs.getString("account_no");
							data.account_name=rs.getString("account_name");
							data.created_at=rs.getString("created_at");
							data.company_name=rs.getString("company_name");
							data.bank_ref_no=rs.getString("bank_ref_no");
							data.biller_payment_ref=rs.getString("biller_payment_ref");
							data.currency=rs.getString("currency");
							model.add(data);
					  }
					  conn.close();
					  return ResponseEntity.ok(model);	
			}catch(Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
			}
			
			
		}
		
		

}
