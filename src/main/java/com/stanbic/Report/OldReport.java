package com.stanbic.Report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Eslip.EslipHelper;
import com.stanbic.Eslip.ExceptionLogResponse;
import com.stanbic.Responses.Response;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;

@SpringBootApplication
@RequestMapping("/api")
public class OldReport {
	 Connection conn;
	 
	 @RequestMapping(value = "/v1/getMyOldReport", method = RequestMethod.POST)
     public ResponseEntity<?> getMyOldReport(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody OldReportConsume jsondata){
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
		 String corp_id=OldreportHelper.getCorp_id(comp_code);
		 if(jsondata.getFromdate().isEmpty() || jsondata.getTodate().isEmpty()) {
			 return ResponseEntity.ok(new Response("06", "Please provide two dates")); 
		 }
		 List<OldReportConsume> model = new ArrayList<OldReportConsume>();
		 try {
			 String sql="SELECT * FROM `migdata_raw_eslip` WHERE corporateId=? AND CreatedOn BETWEEN ? AND ?";
			  conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, corp_id);
				 prep.setObject(2, EslipHelper.formartDate(jsondata.getFromdate())+" 00:00");
				 prep.setObject(3, EslipHelper.formartDate(jsondata.getTodate())+" 00:00");;
				  ResultSet rs=prep.executeQuery();
				  while(rs.next()) {
					  OldReportConsume data=new OldReportConsume();
					  data.id=rs.getString("id");
					  data.amount=rs.getString("amount");
					  data.corporateid=rs.getString("corporateId");
					  data.created_at=rs.getString("createdon");
					  data.payment_date=rs.getString("paymentdate");
					  data.eslip_no=rs.getString("eslipnumber");
					  data.payref=rs.getString("payref");
					  model.add(data);
				  }
				  conn.close();
				  return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new Response("02", "System technical error"));
		 }
	 }
	 
	 @RequestMapping(value = "/v1/getMyOldReportDeatils", method = RequestMethod.POST)
     public ResponseEntity<?> getMyOldReportDeatils(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody OldReportConsume jsondata){
		 if(jsondata.getEslip_no().isEmpty()) {
			 return ResponseEntity.ok(new Response("06", "Please provide eslip_no")); 
		 }
		 List<OldReportProduce> model = new ArrayList<OldReportProduce>();
		 try {
			 String sql="SELECT * FROM `migdata_raw_payment_details` WHERE eslip_no=?";
			  conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, jsondata.getEslip_no());
				  ResultSet rs=prep.executeQuery();
				  while(rs.next()) {
					  OldReportProduce data=new OldReportProduce();
					 data.id=rs.getString("id_no");
					 data.account_name=rs.getString("account_name");
					 data.amount=rs.getString("amount_paid");
					 data.eslip_no=rs.getString("eslip_no");
					 data.meter_count=rs.getString("meter_count");
					 data.meter_ft=rs.getString("meter_ft_number");
					 data.trans_id=rs.getString("trans_id");
					 data.trans_date=rs.getString("trans_date");
					  model.add(data);
				  }
				  conn.close();
				  return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new Response("02", "System technical error"));
		 }
	 }
}
