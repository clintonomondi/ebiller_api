package com.stanbic.Report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.stanbic.comm.DbManager;

public class OldreportHelper {
	 static Connection conn;
	 
	 public static String getCorp_id(String comp_code) {
		 String code="";
		 try {
			 String sql="SELECT * FROM biller_profile WHERE comp_code=?";
			  conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, comp_code);
				  ResultSet rs=prep.executeQuery();
				  while(rs.next()) {
					  code=rs.getString("corp_id");
				  }
				  conn.close();
			 
		 }catch(Exception e) {
			 e.printStackTrace();
		 }
		 return code;
	 }
}
