package com.stanbic.Report;

public class OldReportProduce {
String messageCode="00";
String message="success";
String id="";
String eslip_no="";
String trans_date="";
String meter_count="";
String meter_ft="";
String trans_id="";
String account_name="";
String amount="";
public String getMessageCode() {
	return messageCode;
}
public void setMessageCode(String messageCode) {
	this.messageCode = messageCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getEslip_no() {
	return eslip_no;
}
public void setEslip_no(String eslip_no) {
	this.eslip_no = eslip_no;
}
public String getTrans_date() {
	return trans_date;
}
public void setTrans_date(String trans_date) {
	this.trans_date = trans_date;
}
public String getMeter_count() {
	return meter_count;
}
public void setMeter_count(String meter_count) {
	this.meter_count = meter_count;
}
public String getMeter_ft() {
	return meter_ft;
}
public void setMeter_ft(String meter_ft) {
	this.meter_ft = meter_ft;
}
public String getTrans_id() {
	return trans_id;
}
public void setTrans_id(String trans_id) {
	this.trans_id = trans_id;
}
public String getAccount_name() {
	return account_name;
}
public void setAccount_name(String account_name) {
	this.account_name = account_name;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}



}
