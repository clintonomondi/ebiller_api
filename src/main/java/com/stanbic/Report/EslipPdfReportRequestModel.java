package com.stanbic.Report;

public class EslipPdfReportRequestModel {
String eslip_no="";
String reportPath=null;
String file_name=null;



public String getFile_name() {
	return file_name;
}

public void setFile_name(String file_name) {
	this.file_name = file_name;
}

public String getReportPath() {
	return reportPath;
}

public void setReportPath(String reportPath) {
	this.reportPath = reportPath;
}

public String getEslip_no() {
	return eslip_no;
}

public void setEslip_no(String eslip_no) {
	this.eslip_no = eslip_no;
}


}
