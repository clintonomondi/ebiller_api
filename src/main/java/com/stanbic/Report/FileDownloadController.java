package com.stanbic.Report;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stanbic.ebiller.auth.AuthProduceJson;
@SpringBootApplication
@RequestMapping("/api")
@RestController
public class FileDownloadController {
	@RequestMapping(value = "/api/v1/D://e-biller-files//report//", method = RequestMethod.GET) 
	public ResponseEntity<?>getPDF(@RequestBody EslipPdfReportRequestModel jsondata){
	    try {
		String filename =jsondata.getFile_name();
	      File file = new File(filename);
	      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	      HttpHeaders headers = new HttpHeaders();
	      
	      headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
	      headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
	      headers.add("Pragma", "no-cache");
	      headers.add("Expires", "0");
	      
	      ResponseEntity<Object> 
	      responseEntity = ResponseEntity.ok().headers(headers).contentLength(
	         file.length()).contentType(MediaType.parseMediaType("application/txt")).body(resource);
	      
	      return responseEntity;
	    }catch(Exception n) {
	    	n.printStackTrace();
	    	 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
	    }
	   }
	
	

}
