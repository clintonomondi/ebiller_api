package com.stanbic.Department;

public class DepartmentResponse {

	String messageCode=null;
	String message=null;
	
	
	public DepartmentResponse(String messageCode, String message) {
	
		this.messageCode = messageCode;
		this.message = message;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
