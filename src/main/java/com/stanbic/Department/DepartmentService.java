package com.stanbic.Department;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Eslip.EslipBankProduceJson;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class DepartmentService {
	 Connection conn;
	 
	 @RequestMapping(value = "/v1/addDepartment", method = RequestMethod.POST)
		public ResponseEntity<?> addDepartment(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody DepartmentModel jsondata) {
		 if(jsondata.getName().isEmpty() || jsondata.getUser_id().isEmpty()) {
		 return ResponseEntity.ok(new DepartmentResponse("06","Please provide all fields correctly"));
	     }
		 
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
	     
	     try {
	    	 String sql="INSERT INTO department(name,comp_code,user_id)VALUES(?,?,?)";
	    		conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				prep.setObject(1, jsondata.getName());
				prep.setObject(2, comp_code);
				prep.setObject(3, jsondata.getUser_id());
				prep.execute();
				conn.close();
				 return ResponseEntity.ok(new DepartmentResponse("00","Department added successfully"));
	     }catch(Exception e) {
	    	 e.printStackTrace();
	    	 return ResponseEntity.ok(new DepartmentResponse("02","System technical error"));
	     }
		 
	 }
	 
	 
	 @RequestMapping(value = "/v1/editDepartment", method = RequestMethod.POST)
		public ResponseEntity<?> editDepartment(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody DepartmentModel jsondata) {
		 if(jsondata.getId().isEmpty() || jsondata.getName().isEmpty()) {
		 return ResponseEntity.ok(new DepartmentResponse("06","Please provide all fields correctly"));
	     }
	     try {
	    	 String sql="UPDATE department SET name=?,user_id=? WHERE id=?";
	    		conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				prep.setObject(1, jsondata.getName());
				prep.setObject(2, jsondata.getUser_id());
				prep.setObject(3, jsondata.getId());
				prep.execute();
				conn.close();
				 return ResponseEntity.ok(new DepartmentResponse("00","Department updated  successfully"));
	     }catch(Exception e) {
	    	 e.printStackTrace();
	    	 return ResponseEntity.ok(new DepartmentResponse("02","System technical error"));
	     }
		 
	 }
	 
	 @RequestMapping(value = "/v1/getDepartments", method = RequestMethod.POST)
		public ResponseEntity<?> getDepartments(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody DepartmentModel jsondata) {
		 
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
	     List<DepartmentModel> model= new ArrayList<DepartmentModel>();
	     
	     try {
	    	 String sql="SELECT *,\r\n" + 
	    	 		"(SELECT personel_f_name FROM ebiller_auth B WHERE B.id=A.user_id)user_name\r\n" + 
	    	 		"FROM department A WHERE comp_code=?";
	    		conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				prep.setObject(1, comp_code);
				ResultSet rs=prep.executeQuery();
				while(rs.next()) {
					DepartmentModel data=new DepartmentModel();
					data.comp_code=rs.getString("comp_code");
					data.id=rs.getString("id");
					data.message="success";
					data.messageCode="00";
					data.name=rs.getString("name");
					data.created_at=rs.getString("created_at");
					data.user_name=rs.getString("user_name");
					model.add(data);
				}
				conn.close();
				return ResponseEntity.ok(model);
	     }catch(Exception e) {
	    	 e.printStackTrace();
	    	 return ResponseEntity.ok(new DepartmentResponse("02","System technical error"));
	     }
	     
	 }

}
