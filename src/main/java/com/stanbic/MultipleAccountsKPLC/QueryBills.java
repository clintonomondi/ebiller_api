//package com.stanbic.MultipleAccountsKPLC;
//
//import java.io.FileInputStream;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.util.Iterator;
//import java.util.Vector;
//
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.xssf.usermodel.XSSFCell;
//import org.apache.poi.xssf.usermodel.XSSFRow;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//
//import com.stanbic.Eslip.EslipHelper;
//import com.stanbic.Eslip.EslipService;
//import com.stanbic.KPLC.HitKPLC;
//import com.stanbic.comm.DbManager;
//
//public class QueryBills {
//	static Connection conn;
//	
//	public static void getBills(String path,String payer_code,String biller_code) {
//		 Vector dataHolder=read(path);
//		 String eslip_no = EslipService.eslipNoGen(biller_code);
//		String due_date=EslipService.expiryDate();
//		hitKPLC(dataHolder,payer_code,biller_code,eslip_no);
//		String total_amount_due = null;
//		String total_amount_to_pay = null;
//		String accounts=null;
//		try {
//			 conn=DbManager.getConnection();
//			String sql="SELECT SUM(amount_due) total_amount_due,SUM(amount_to_pay) total_amount_to_pay,COUNT(*) accounts FROM eslip_bills WHERE eslip_no=? GROUP BY eslip_no";
//			 PreparedStatement prep=conn.prepareStatement(sql);
//			  prep.setObject(1, eslip_no);
//			  ResultSet rs=prep.executeQuery();
//			  while(rs.next()) {
//				  total_amount_due=rs.getString("total_amount_due"); 
//				  total_amount_to_pay=rs.getString("total_amount_to_pay");
//				  accounts=rs.getString("accounts");
//			  }
//			  conn.close();
//		}catch(Exception n) {
//			n.printStackTrace();
//		}
//		EslipService.insertEslip("", eslip_no, due_date, total_amount_due, total_amount_to_pay, EslipHelper.getAccount_no(biller_code), biller_code, payer_code,accounts);
//	}
//	
//	 public static Vector read(String fileName)    {
//	    	Vector cellVectorHolder = new Vector();
//	    	try{
//	    		FileInputStream myInput = new FileInputStream(fileName);
//	   	    	//POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
//	            XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);
//	            XSSFSheet mySheet = myWorkBook.getSheetAt(0);
//	           Iterator rowIter = mySheet.rowIterator(); 
//	           while(rowIter.hasNext()){
//	        	  XSSFRow myRow = (XSSFRow) rowIter.next();
//	        	  Iterator cellIter = myRow.cellIterator();
//	        	  Vector cellStoreVector=new Vector();
//	        	  while(cellIter.hasNext()){
//	        		  XSSFCell myCell = (XSSFCell) cellIter.next();
//	        		  myCell.setCellType(Cell.CELL_TYPE_STRING);
//	        		  cellStoreVector.addElement(myCell);
//	        	  }   
//	        	  cellVectorHolder.addElement(cellStoreVector);
//	          }
//	    	}catch (Exception e){
//	    		System.out.println(e.getMessage());
//	    		}
//
//	    	return cellVectorHolder;
//	    }
//	 
//	 
//	 public static void hitKPLC(Vector dataHolder,String payer_code,String biller_code,String eslip_no) {
//		try {
//			int ii=0;
//			conn = DbManager.getConnection();
//			String sql1="INSERT INTO eslip_bills (eslip_no,account_no,amount_due,amount_to_pay,due_date)VALUES(?,?,?,?,?) ON DUPLICATE KEY UPDATE amount_due=?,amount_to_pay=?,due_date=?";
//			String sql2 = "INSERT INTO account (biller_code,account_no,amount_due,payer_code,due_date,alias,account_name)VALUES(?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE amount_due=?,due_date=?";
//			
//			PreparedStatement prep1 = conn.prepareStatement(sql1);
//			PreparedStatement prep2 = conn.prepareStatement(sql2);
//			
//		 for (int i=0;i<dataHolder.size(); i++){
//             Vector cellStoreVector=(Vector)dataHolder.elementAt(i);
//             String account_no=cellStoreVector.toArray()[0].toString();
//             String amount_to_pay=cellStoreVector.toArray()[1].toString();
//             
//             String res =HitKPLC.KPLCqueryBills(account_no,biller_code);
//				System.out.println("Response from kplc ((((((((((((((((((("+res);
//				String arrres[] = res.split("\\*");	
//				
//				System.out.println("Status="+arrres[0].toString());
//				System.out.println("Account_balance="+arrres[1].toString());
//				System.out.println("Account Number="+arrres[4].toString());
//				System.out.println("Account Name="+arrres[2].toString());
//				System.out.println("Due Date="+arrres[3].toString());
//				
//				prep1.setObject(1, eslip_no);
//				prep1.setObject(2, arrres[4].toString());
//				prep1.setObject(3, arrres[1].toString());
//				prep1.setObject(4, amount_to_pay);
//				prep1.setObject(5, arrres[3].toString());
//				prep1.setObject(6, arrres[1].toString());
//				prep1.setObject(7, amount_to_pay);
//				prep1.setObject(8, arrres[3].toString());
//				prep1.addBatch();
//				
//				if(arrres[1].toString().equals("Notfound")) {
//					
//				}
//				else {
//					prep2.setObject(1, biller_code);
//					prep2.setObject(2, arrres[4].toString());
//					prep2.setObject(3, arrres[1].toString());
//					prep2.setObject(4, payer_code);
//					prep2.setObject(5, arrres[3].toString());
//					prep2.setObject(6, "");
//					prep2.setObject(7, arrres[2].toString());
//					prep2.setObject(8, arrres[1].toString());
//					prep2.setObject(9, arrres[3].toString());
//					prep2.addBatch();
//				}
//				
//				ii++;
//				if (ii % 200 == 0 || ii == dataHolder.size()) {
//					System.out.println(">Inserting="+ii);
//					 prep1.executeBatch(); // Execute every 50 items. 
//		             prep1.clearBatch();
//	                if(arrres[1].toString().equals("Notfound")) {
//						
//					}
//	                else {
//	                	prep2.executeBatch(); // Execute every 50 items. 
//		                prep2.clearBatch();
//	                }
//	                
//	            }
//         }
//		 conn.close();
//		}catch(Exception n) {
//			System.out.println(n.getMessage());
//		}
//		 
//		 
//	 }
//
//}
