//package com.stanbic.MultipleAccountsKPLC;
//
//import java.io.File;
//import java.sql.Connection;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import com.stanbic.Accounts.AccountHelper;
//import com.stanbic.Accounts.AccountsConsumeJson;
//import com.stanbic.KPLC.QueryBulkBillsService;
//import com.stanbic.comm.ExternalFile;
//import com.stanbic.comm.FileManager;
//import com.stanbic.comm.TokenManager;
//import com.stanbic.ebiller.auth.AuthProduceJson;
//
//@SpringBootApplication
//@RequestMapping("/api")
//public class MultipleAccountsService {
//	Connection conn = null;
//	 @RequestMapping(value = "/v1/uploadAccounts", method = RequestMethod.POST)
//	 public ResponseEntity<?> uploadAccounts(@RequestBody MultipleAccountsConsumeJson jsondata){
//		 if(jsondata.getBase64Excel().isEmpty() || jsondata.getBiller_code().isEmpty()) {
//			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide all fields","",""));
//		 }
//		 String payer_code=TokenManager.tokenIssuedCompCode(ExternalFile.getSecretKey(), jsondata.getToken());
//		 if(payer_code.equalsIgnoreCase("03")){
//			 return ResponseEntity.ok(new AuthProduceJson("03","Page has expired","",""));
//		}
//	     if(payer_code.equalsIgnoreCase("01")) {
//			 return ResponseEntity.ok(new AuthProduceJson("01","An error occured,page might expired","",""));
//		}
//	     DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yy/MM/dd");  
//	     LocalDateTime now = LocalDateTime.now();  
//	     String time=dtf.format(now);
//	     String filename=time.replace("/", "_")+payer_code;
//		 String path=ExternalFile.getExcelPath()+filename+".xlsx";
//	
//		 FileManager.saveImage(jsondata.getBase64Excel(),path);
//		 
//		 Thread t = new Thread(){ 
//		        public void run(){
//		        	QueryBills.getBills(path, payer_code, jsondata.getBiller_code());;
//		        }
//		    };
//		    t.start();
//		 return ResponseEntity.ok(new AuthProduceJson("00","Acccounts uploaded successfully","",""));
//	 }
//	 
//	
//}
