package com.stanbic;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class EBillerApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(EBillerApplication.class, args);
		
	}
	
	 @Override
	 protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	  return application.sources(EBillerApplication.class);
	 }
	 
//	 @PostConstruct
//	    private void init() {
//		 CountDownLatch latch = new CountDownLatch(1);
//	        Runnable jointRun = new AutoLoadingFile(latch);
//	        Thread t = new Thread(jointRun);
//	        t.start();
//	        // ...
//	    }

}
