package com.stanbic.OTP;

public class OTPResponse {
String messageCode="";
String message=null;
String phone=null;
String email=null;
String comp_code=null;
String code="";
String name="";




public OTPResponse(String messageCode, String message, String phone, String email, String comp_code,String code,String name) {
	this.messageCode = messageCode;
	this.message = message;
	this.phone = phone;
	this.email = email;
	this.comp_code = comp_code;
	this.code=code;
	this.name=name;
}



public String getName() {
	return name;
}



public void setName(String name) {
	this.name = name;
}



public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}

public String getComp_code() {
	return comp_code;
}
public void setComp_code(String comp_code) {
	this.comp_code = comp_code;
}
public String getMessageCode() {
	return messageCode;
}
public void setMessageCode(String messageCode) {
	this.messageCode = messageCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}


}
