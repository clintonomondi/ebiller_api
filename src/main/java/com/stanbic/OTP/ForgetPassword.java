package com.stanbic.OTP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Responses.Response;
import com.stanbic.Validation.PasswordValidater;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.PasswordUtils;
import com.stanbic.ebiller.auth.AuthProduceJson;
import com.stanbic.ebiller.auth.LoginHelper;

@SpringBootApplication
@RequestMapping("/api")
public class ForgetPassword {
	Connection conn=null;
	
	 @RequestMapping(value = "/v1/getOtpCode", method = RequestMethod.POST)
		public ResponseEntity<?>getOtpCode(@RequestBody ForgetPasswordModel jsondata){
		 if(jsondata.getPhone().isEmpty()) {
			 return ResponseEntity.ok(new Response("06","Please provide phone number"));
		 }
		 
		 int checkphone=LoginHelper.isPhoneNumberExist(jsondata.getPhone().substring(1));
		 if(checkphone==0) {
			 return ResponseEntity.ok(new Response("06","The phone number does not exist")); 
		 }
		 String email="";
		 String comp_code=""; 
		 String name="";
		 int code=OTPHelper.genRandomSix();
		 try {
			 String sql="SELECT email,comp_code,personel_f_name FROM ebiller_auth WHERE phone=?";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, jsondata.getPhone().substring(1));
			 ResultSet  rs=prep.executeQuery();
			 while(rs.next()) {
				 email=rs.getString("email");
				 comp_code=rs.getString("comp_code");
				 name=rs.getString("personel_f_name");
			 }
			 conn.close();
			 boolean sendsms= OTPHelper.SendOTPSMS(jsondata.getPhone().substring(1), code+"");
			 if(sendsms) {
			 OTPHelper.logOTP(jsondata.getPhone().substring(1),comp_code,email,code+"");
			 return ResponseEntity.ok(new OTPResponse("00","A six digit code has been sent to the phone number "+jsondata.getPhone().substring(1),jsondata.getPhone().substring(1),email,comp_code,"",name));
			 }else {
				 return ResponseEntity.ok(new Response("02","An error occured with OTP System"));
			 }
			
		 }catch(Exception n) {
			 n.printStackTrace();
			 return ResponseEntity.ok(new Response("02","System technical error"));
		 }
		 
	 }
	 
	 @RequestMapping(value = "/v1/verifyOtpCode", method = RequestMethod.POST)
		public ResponseEntity<?>verifyOtpCode(@RequestBody ForgetPasswordModel jsondata){
		 if(jsondata.getPhone().isEmpty() || jsondata.getCode().isEmpty() || jsondata.getEmail().isEmpty() || jsondata.getComp_code().isEmpty()) {
			 return ResponseEntity.ok(new Response("06","Invalid page access"));
		 }
		 if(jsondata.getCode().isEmpty()) {
			 return ResponseEntity.ok(new Response("06","Please enter a valid code"));
		 }
		int verifycode=OTPHelper.isCodeExist(jsondata.getCode(), jsondata.getEmail(), jsondata.getPhone(),jsondata.getComp_code());
		 if(verifycode!=1) {
			 return ResponseEntity.ok(new Response("06","Invalid code"));
		 }
		 int verifyExpiry=OTPHelper.isCodeExpire(jsondata.getCode(), jsondata.getEmail(), jsondata.getPhone(),jsondata.getComp_code());
		 if(verifyExpiry!=1) {
			 return ResponseEntity.ok(new Response("06","The code has already expired"));
		 }
		 return ResponseEntity.ok(new OTPResponse("00","Success,enter new password "+jsondata.getPhone(),jsondata.getPhone(),jsondata.getEmail(),jsondata.getComp_code(),jsondata.getCode(),""));
		
	 }
	 
	 @RequestMapping(value = "/v1/resetOtpPassword", method = RequestMethod.POST)
		public ResponseEntity<?>resetOtpPassword(@RequestBody ForgetPasswordModel jsondata){
		 if(jsondata.getPhone().isEmpty() || jsondata.getCode().isEmpty() || jsondata.getEmail().isEmpty() || jsondata.getComp_code().isEmpty() || jsondata.getPassword().isEmpty()) {
			 return ResponseEntity.ok(new Response("06","The data you provided is invalid"));
		 }
		 
		int verifycode=OTPHelper.isCodeExist(jsondata.getCode(), jsondata.getEmail(), jsondata.getPhone(),jsondata.getComp_code());
		 if(verifycode!=1) {
			 return ResponseEntity.ok(new Response("06","Invalid code"));
		 }
		 int verifyExpiry=OTPHelper.isCodeExpire(jsondata.getCode(), jsondata.getEmail(), jsondata.getPhone(),jsondata.getComp_code());
		 if(verifyExpiry!=1) {
			 return ResponseEntity.ok(new Response("06","The code has already expired"));
		 }
		 if (!PasswordValidater.isPassword8(jsondata.getPassword())) {
				return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must be atleast 8 characters", "", ""));
			}
			if (!PasswordValidater.isPasswordHasLowerCase(jsondata.getPassword())) {
				return ResponseEntity
						.ok(new AuthProduceJson("08", "Passsword must have atleast one lower case character", "", ""));
			}

			if (!PasswordValidater.isPasswordHasLowerDigit(jsondata.getPassword())) {
				return ResponseEntity
						.ok(new AuthProduceJson("08", "Passsword must have atleast a digit character", "", ""));
			}

			if (!PasswordValidater.isPasswordHasSpecial(jsondata.getPassword())) {
				return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must have a special character", "", ""));
			}

			if (!PasswordValidater.isPasswordHasUpperCase(jsondata.getPassword())) {
				return ResponseEntity.ok(new AuthProduceJson("08", "Passsword must have a upper case character", "", ""));
			}
			 String password = PasswordUtils.generateSecurePassword(jsondata.getPassword(), ExternalFile.getSecretKey());
		 
			 try {
					String sql="UPDATE ebiller_auth SET password=? WHERE email=? AND phone=? AND comp_code=?";
					conn=DbManager.getConnection();
					PreparedStatement prep=conn.prepareStatement(sql);
					prep.setObject(1, password);
					prep.setObject(2, jsondata.getEmail());
					prep.setObject(3, jsondata.getPhone());
					prep.setObject(4, jsondata.getComp_code());
					prep.executeUpdate();
					conn.close();
					return ResponseEntity.ok(new Response("00", "Password reset successfully,please login with your new password"));
				}catch(Exception e) {
					e.printStackTrace();
					return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
				}
	 }
	
	
}
