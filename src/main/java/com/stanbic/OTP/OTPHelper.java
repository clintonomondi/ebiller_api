package com.stanbic.OTP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Random;

import org.json.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;

public class OTPHelper {
	static Connection conn=null;
	
	public static int genRandomSix() {
		Random generator = new Random();
		return 100000 + generator.nextInt(900000);
	}
	
	public static void logOTP(String phone,String comp_code,String email,String code) {
		try {
			 String sql="INSERT INTO otp_logs(phone,comp_code,email,code)VALUES(?,?,?,?)";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, phone);
			 prep.setObject(2, comp_code);
			 prep.setObject(3, email);
			 prep.setObject(4, code);
			 prep.execute();
			 conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean SendOTPSMS(String phone,String code) {
		 Hashtable<String, String> configsleg = new Hashtable<String, String>();
		 String sms="Your password verification  code is:"+code;
         configsleg.put("msisdn", phone);
         configsleg.put("message", sms);
         configsleg.put("sms_id", code);
         
         // send and receive response from sms server
         String resposnes = GeneralCodes.postRequest1(configsleg, ExternalFile.getSMSBaseUrl(), "JSON");
         JSONObject obj = new JSONObject(resposnes);
         String jsonResponse = obj.toString();
         JsonParser parser = new JsonParser();
         JsonElement jsonTree = parser.parse(jsonResponse);
         com.google.gson.JsonObject jsonObject = jsonTree.getAsJsonObject();
         JsonElement status = jsonObject.get("status");
         JsonElement status_description = jsonObject.get("status_description");
         if(status.toString().equalsIgnoreCase("100")) {
        	 return true;
         }else {
        	 return false;
         }
	}
	
	public static int isCodeExist(String code,String email,String phone,String comp_code) {
		int vcount=0;
		try {
			 String sql="SELECT COUNT(*)vcount FROM otp_logs WHERE code=? AND email=? AND phone=? AND comp_code=?";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, code);
			 prep.setObject(2, email);
			 prep.setObject(3, phone);
			 prep.setObject(4, comp_code);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				vcount=rs.getInt("vcount");
			}
			 conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return vcount;
	}
	public static int isCodeExpire(String code,String email,String phone,String comp_code) {
		int vcount=0;
		try {
			 String sql="SELECT COUNT(*)vcount FROM otp_logs WHERE code=? AND email=? AND phone=? AND comp_code=? AND created_at > NOW() - INTERVAL 1 HOUR";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, code);
			 prep.setObject(2, email);
			 prep.setObject(3, phone);
			 prep.setObject(4, comp_code);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				vcount=rs.getInt("vcount");
			}
			 conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return vcount;
	}
}
