package com.stanbic.Logs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.backendUsers.BankGetUsersReqModel;
import com.stanbic.backendUsers.BankUsersRespModel;
import com.stanbic.comm.DbManager;


@SpringBootApplication
@RequestMapping("/api")
public class System_Logs {
	static Connection conn;
	
	@RequestMapping(value = "/v1/getSystemLogs", method = RequestMethod.POST)
	public ResponseEntity<?> getSystemLogs(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody SystemLogModel jsondata) {
	  List<SystemLogModel> model=new ArrayList<SystemLogModel>();
       try {
			conn = DbManager.getConnection();
			String sql="SELECT * FROM system_logs";
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				SystemLogModel m=new SystemLogModel();
				m.created_by=rs.getString("created_by");
				m.comp_code=rs.getString("comp_code");
				m.description=rs.getString("description");
				m.status=rs.getString("status");
				m.other=rs.getString("other");
				m.created_at=rs.getString("created_at");
				model.add(m);
			}
			conn.close();
			return ResponseEntity.ok(model);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(model);
		}
	}
	
	
	
	
	public static void log_system(String created_by,String comp_code,String description,String status,String other) {
		
		try {
			conn = DbManager.getConnection();
			String sql="INSERT INTO system_logs(created_by,comp_code,description,status,other) VALUES(?,?,?,?,?)";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, created_by);
			prep.setObject(2, comp_code);
			prep.setObject(3, description);
			prep.setObject(4, status);
			prep.setObject(5, other);
			prep.execute();
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
public static void log_account_validation(String created_by,String data,String type) {
		
		try {
			conn = DbManager.getConnection();
			String sql="INSERT INTO account_validation_logs(created_by,data,type) VALUES(?,?,?)";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, created_by);
			prep.setObject(2, data);
			prep.setObject(3, type);
			prep.execute();
			conn.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
