package com.stanbic.Logo;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;

import com.stanbic.Eslip.EslipService;
import com.stanbic.comm.CheckUsers;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.EmailManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.PathUrl;
import com.stanbic.comm.TimeManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;
import com.stanbic.ebiller.auth.BillerAuthconsumeJson;

@SpringBootApplication
@RequestMapping("/api")
public class InvoiceLogo {
	static Connection conn=null;
	
	
	 @RequestMapping(value = "/v1/getInvoiceLogo", method = RequestMethod.POST)
	 public ResponseEntity<?> getInvoiceLogo(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceLogoConsumeJson jsondata){
		
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
		 String base64=null;
		
	     
		 String path=getLogo(comp_code);
		 File file=new File(path);
		 base64=FileManager.encodeFileToBase64Binary(file);
		 return ResponseEntity.ok(new InvoiceLogoConsumeJson("00","success",jsondata.getToken(),base64));
	 }
	 
	 
	 
	 @RequestMapping(value = "/v1/saveInvoiceLogo", method = RequestMethod.POST)
	 public ResponseEntity<?> saveInvoiceLogo(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody InvoiceLogoConsumeJson jsondata){
		if(jsondata.getBase64Logo().isEmpty() || jsondata.base64Logo.equalsIgnoreCase("")) {

		}else {
		 try {
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
	     int rand=EslipService.genRandom();
		 String path=ExternalFile.getLogoPath()+rand+"_"+comp_code+".png";
		 
		  FileManager.saveImageLogo(jsondata.getBase64Logo(),path);
		 
		  String sql="UPDATE interface_settings SET logo_url=? WHERE comp_code=?";
		  String sql2="UPDATE ebiller_auth SET loginvalue=? WHERE comp_code=?";
		   conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			prep.setObject(1, path);
			prep.setObject(2, comp_code);
			
			prep2.setObject(1, "1");
			prep2.setObject(2,comp_code);
			prep.execute();
			prep2.execute();
			conn.close();
		 }catch(Exception n) {
			n.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("01","Could not save image","",""));
		 }
		}
		 return ResponseEntity.ok(new InvoiceLogoConsumeJson("00","Image Saved successfully","",jsondata.getBase64Logo()));
		 
	 }
	 
	 public static String getLogo(String comp_code) {
		 String path=null;
		 try {
			 String sql="SELECT logo_url FROM interface_settings WHERE comp_code=?";
			 conn = DbManager.getConnection();
			 PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, comp_code);
				 ResultSet rs=prep.executeQuery();
				 while(rs.next()) {
					 path=rs.getString("logo_url");
				 }
				 conn.close();
		 }catch(Exception n) {
			 System.out.println(n.getMessage());
		 }
		 return path;
	 }
	 
	 
	 @RequestMapping(value = "/v1/sendTestInvoice", method = RequestMethod.POST)
	  public ResponseEntity<?> sendTestInvoice(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillerAuthconsumeJson jsondata) {
		 String email=TokenManager.tokenIssuedId(token);
		 String date=TimeManager.getEmailTime();  
		 try {
			if(jsondata.getTitle().isEmpty() || jsondata.getMessage().isEmpty()) {
				 return ResponseEntity.ok(new AuthProduceJson("03","All fields are required","",""));
			}
			 String comp_code=TokenManager.tokenIssuedCompCode(token);
			  token = TokenManager.createToken2(comp_code, email, "", "");
		     String path=PathUrl.getBillerAppPath("biller");
				String url=path +"app/printview" + "?token=" + token;
				
		  String subject=jsondata.getTitle();
			String sql="UPDATE biller_profile SET test_invoice=? WHERE comp_code=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "1");
			prep.setObject(2, comp_code);
			prep.execute();
			conn.close();
			
			  VelocityContext vc = new VelocityContext();
	          vc.put("invoice_link", url);
	          vc.put("date", date);
	          String email_template = EmailManager.email_message_template(vc,"test_invoice.vm");
				EmailManager.send_mail(email,email_template,subject,comp_code,"Test invoice sent");
		
		}catch(Exception e) {
			e.printStackTrace();
			  return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		}
		return ResponseEntity.ok(new AuthProduceJson("00","An email has been successfully sent to your email:"+email,"",""));
	}
	 
	 
}
