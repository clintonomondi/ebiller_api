package com.stanbic.Logo;

public class InvoiceLogoConsumeJson {
	String messageCode = "";
	String message = "";
	String token="";
	String base64Logo=null;

	public InvoiceLogoConsumeJson(String messageCode, String message,String token,String base64Logo) {
		this.messageCode = messageCode;
		this.message = message;
		this.token=token;
		this.base64Logo=base64Logo;
	}
	
	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}


	public String getBase64Logo() {
		return base64Logo;
	}

	public void setBase64Logo(String base64Logo) {
		this.base64Logo = base64Logo;
	}
}
