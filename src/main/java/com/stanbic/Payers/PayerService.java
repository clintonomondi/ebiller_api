package com.stanbic.Payers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Profile.PayerProfileProduceJson;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class PayerService {

	Connection conn=null;
	@RequestMapping(value = "/v1/payerDetails", method = RequestMethod.POST)
	  public ResponseEntity<?> registerPayer(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerDetailsReqModel jsondata) {
		
		String payerCode=jsondata.getPayerNo();
		String compCode="";
		String Location="";
		String phone="";
		String businessDescription="";
		String companyName="";
		String email="";
		String status="";
		String personelName="";
		String accountno="";
		String accountName="";
		String sector="";
		String no_of_eslips_generated="";
		String total_amount_transacted="";
		
		Hashtable<String,Object> map=new Hashtable<String,Object>();
		String comp_code=TokenManager.tokenIssuedCompCode(token);
		try {
			String sql="SELECT IF(SECTOR IS NULL,'',SECTOR)SECTOR,IF(COMP_CODE IS NULL,'',COMP_CODE)COMP_CODE,IF(BILLER_LOCATION IS NULL,'',BILLER_LOCATION)BILLER_LOCATION, \r\n" + 
					"					IF(BILLER_PHONE IS NULL,'',BILLER_PHONE)BILLER_PHONE,IF(BILLER_MONTH IS NULL,'',BILLER_MONTH)BILLER_MONTH,\r\n" + 
					"					IF(BUSINESS_DESCRIPTION IS NULL,'',BUSINESS_DESCRIPTION)BUSINESS_DESCRIPTION,\r\n" + 
					"					IF(COMPANY_NAME IS NULL,'',COMPANY_NAME)COMPANY_NAME, \r\n" + 
					"					IF(EMAIL IS NULL,'',EMAIL)EMAIL,IF(STATUS IS NULL,'',STATUS)STATUS,\r\n" + 
					"					IF(PERSONEL_L_NAME IS NULL,'',PERSONEL_L_NAME)PERSONEL_L_NAME,IF(STB_ACC_NAME IS NULL,'',STB_ACC_NAME) STB_ACC_NAME,\r\n" + 
					"					IF(USER_TYPE IS NULL,'',USER_TYPE)USER_TYPE,\r\n" + 
					"					(SELECT COUNT(*) FROM eslip Z WHERE Z.payer_code=A.comp_code)eslips_generated,\r\n" + 
					"					(SELECT SUM(amount_to_pay) FROM eslip Y WHERE Y.payer_code=A.comp_code)total_amount\r\n" + 
					"					FROM `biller_profile` A WHERE COMP_CODE=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1,payerCode);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				compCode=rs.getString("COMP_CODE");
				Location=rs.getString("BILLER_LOCATION");
				phone=rs.getString("BILLER_PHONE");
				businessDescription=rs.getString("BUSINESS_DESCRIPTION");
				companyName=rs.getString("COMPANY_NAME");
				email=rs.getString("EMAIL");
				status=rs.getString("STATUS");
				personelName=rs.getString("PERSONEL_L_NAME");
				accountName=rs.getString("STB_ACC_NAME");
				sector=rs.getString("SECTOR");
				no_of_eslips_generated=rs.getString("eslips_generated");
				total_amount_transacted=rs.getString("total_amount");
			}
			conn.close();
			map.put("responseCode", "00");
			map.put("message", "payer details");
			map.put("companyCode", compCode);
			map.put("location", Location);
			map.put("phoneNo",phone );
			map.put("businessDescription", businessDescription);
			map.put("companyName", companyName);
			map.put("email", email);
			map.put("status",status );
			map.put("personelName", personelName);
			map.put("accountName",accountName );
			map.put("accountNo", accountno);
			map.put("sector", sector);
			map.put("no_of_eslips_generated", no_of_eslips_generated);
			map.put("total_amount_transacted", total_amount_transacted);
			return ResponseEntity.ok(map);
			
		}catch(Exception e) {
			e.printStackTrace();
			map.put("responseCode", "01");
			map.put("message", "Could not query payer details");
			return ResponseEntity.ok(map);
		}
	}
	
	@RequestMapping(value = "/v1/getAllPayers", method = RequestMethod.POST)
	 public ResponseEntity<?> getAllPayers(@RequestBody GetAllPayersReqModel jsondata) {
		
		 List<PayerDetailsRespModel> model= new ArrayList<PayerDetailsRespModel>();
		
		 try {
			String sql="SELECT IF(status IS NULL,'',status)status,"
					+ "IF(stb_acc_no IS NULL,'',stb_acc_no)stb_acc_no,"
					+ "IF(stb_acc_name IS NULL,'',stb_acc_name)stb_acc_name,"
					+ "IF(sector IS NULL,'',sector)sector,IF(personel_l_name IS NULL,'',personel_l_name)personel_l_name,"
					+ "IF(personel_f_name IS NULL,'',personel_f_name)personel_f_name,"
					+ "IF(email IS NULL,'',email) email,"
					+ "IF(company_name,'',company_name)company_name,comp_code,"
					+ "IF(biller_location IS NULL,'',biller_location)biller_location,"
					+ "biller_phone,business_description FROM `biller_profile` WHERE user_type='payer' "; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				PayerDetailsRespModel data=new PayerDetailsRespModel();
				  data.Location=rs.getString("biller_location");
				  data.phone=rs.getString("biller_phone");
				  data.businessDescription=rs.getString("business_description");
				  data.compCode=rs.getString("comp_code");
				  data.companyName=rs.getString("company_name");
				  data.email=rs.getString("email");
				  data.personelName=rs.getString("personel_f_name")+" "+rs.getString("personel_l_name");
				  data.sector=rs.getString("sector");
				  data.accountName=rs.getString("stb_acc_name");
				  data.accountno=rs.getString("stb_acc_no");
				  data.status=rs.getString("status");
				  model.add(data);
			}
			 conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 Hashtable<String,String> map= new Hashtable<String,String>();
			 map.put("responseCode", "01");
			 map.put("message", "Could not fetch payers.Error from the database");
			 return ResponseEntity.ok(map);
		 }
	}
}
