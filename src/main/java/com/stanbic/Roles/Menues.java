package com.stanbic.Roles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class Menues {

	Connection conn = null;
	@RequestMapping(value = "/v1/getMenues", method = RequestMethod.POST)
	public ResponseEntity<?> getMenues(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody MenuReqModel jsondata) {
	 
		List<MenueRespModel> dataArray=new ArrayList<MenueRespModel>();
	    try {
			String sql = "SELECT * FROM `menues` Where user=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "payer");
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				MenueRespModel map=new MenueRespModel();
				map.menuId=rs.getString("id");
				map. menuName=rs.getString("name") ;
				dataArray.add(map);
			}
			conn.close();
			return ResponseEntity.ok(dataArray);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.ok(dataArray);
		}
	}
	
	@RequestMapping(value = "/v1/getBankMenues", method = RequestMethod.POST)
	public ResponseEntity<?> getBankMenues(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody MenuReqModel jsondata) {
	  
		List<MenueRespModel> dataArray=new ArrayList<MenueRespModel>();
	    try {
			String sql = "SELECT * FROM `menues` Where user=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "bank");
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				MenueRespModel map=new MenueRespModel();
				map.menuId=rs.getString("id");
				map. menuName=rs.getString("name") ;
				dataArray.add(map);
			}
			conn.close();
			return ResponseEntity.ok(dataArray);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.ok(dataArray);
		}
	}
	
	@RequestMapping(value = "/v1/addMyMenues", method = RequestMethod.POST)
	public ResponseEntity<?> addMyMenues(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
		JSONObject obj = new JSONObject(jsondata);
		String group_id=obj.getString("group_id");
		String menu_id=null;
		
	
		try {
			JSONArray menues = obj.getJSONArray("menues");
			conn = DbManager.getConnection();
			for (int i = 0; i < menues.length(); i++) {
				JSONObject object = menues.getJSONObject(i);
				menu_id = object.getString("menu_id");
				String sql="INSERT INTO user_menue(menu_id,group_id)VALUES(?,?) ON DUPLICATE KEY UPDATE menu_id=?,group_id=?";
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, menu_id);
				prep.setObject(2, group_id);
				prep.setObject(3, menu_id);
				prep.setObject(4, group_id);
				prep.execute();
			}
			conn.close();
			return ResponseEntity.ok(new AuthProduceJson("00", "Menues saved successfully", "", ""));
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}

}
