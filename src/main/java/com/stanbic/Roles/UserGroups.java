package com.stanbic.Roles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class UserGroups {

	Connection conn=null;
	
	@RequestMapping(value = "/v1/addGroup", method = RequestMethod.POST)
	public ResponseEntity<?> addGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody NewUserGroupReqModel jsondata) {
		String comp_code=TokenManager.tokenIssuedCompCode(token);
		
		if(comp_code.equalsIgnoreCase("03")){
			 return ResponseEntity.ok(new AuthProduceJson("01","Page has expired","",""));
		}
	     if(comp_code.equalsIgnoreCase("01")) {
			 return ResponseEntity.ok(new AuthProduceJson("03","An error occured,page might expired","",""));
		}
		String group_id = null;
		try {
			
		String sql="INSERT INTO user_groups (name,comp_code)VALUES(?,?) ON DUPLICATE KEY UPDATE name=?";
		conn = DbManager.getConnection();
		PreparedStatement prep = conn.prepareStatement(sql);
		prep.setObject(1, jsondata.getName());
		prep.setObject(2, comp_code);
		prep.setObject(3, jsondata.getName());
		prep.execute();
		//conn.close();
		
		//conn = DbManager.getConnection();
		String sql2 ="SELECT id FROM user_groups WHERE NAME=? AND comp_code=?";
		PreparedStatement prep2 = conn.prepareStatement(sql2);
		prep2.setObject(1, jsondata.getName());
		prep2.setObject(2, comp_code);
		ResultSet rs = prep2.executeQuery();
		
		while(rs.next()) {
			group_id = rs.getString("id");
		}
		conn.close();
		return ResponseEntity.ok(new NewUserGroupRespModel("00", "Successfully created", "", group_id));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.ok(new NewUserGroupRespModel("01", "System technical error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/getMyGroups", method = RequestMethod.POST)
	public ResponseEntity<?> getMyGroups(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody UserGroupReqModel jsondata) {
		
		List<UserGroupRespModel> model = new ArrayList<UserGroupRespModel>();
		String comp_code=TokenManager.tokenIssuedCompCode(token);

		try {
			String sql = "SELECT id,NAME, \r\n" + 
					"(SELECT COUNT(*)vcount FROM ebiller_auth B WHERE B.group_id=A.id)NO_OF_USERS\r\n" + 
					"FROM user_groups A WHERE comp_code=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				UserGroupRespModel data = new UserGroupRespModel();
				data.name=rs.getString("name");
				data.group_id=rs.getString("id");
				data.noOfUsers=rs.getString("NO_OF_USERS");
				model.add(data);
			}
			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}

}
