package com.stanbic.Roles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class UserGroupAndMenu {

	Connection conn=null;
	@RequestMapping(value = "/v1/addMenuToGroup", method = RequestMethod.POST)
	public ResponseEntity<?> addMenueToUserGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
		String flag="";
		Hashtable<String,String> map=new Hashtable<String,String>();
		
		JSONObject obj = new JSONObject(jsondata);

		String groupId=obj.getString("groupId");
		String groupName=obj.getString("groupName");
		String companyCode=TokenManager.tokenIssuedCompCode(token);
		String system_username= TokenManager.tokenIssuedCompCode(token);
		JSONArray ja_data = obj.getJSONArray("menuItems");
		deleteMenusForAgroup(companyCode,groupId);//delete old record
		updateGroupId(groupId,companyCode,groupName);
		
		for (int i = 0; i < ja_data.length(); i++) {
			JSONObject object = ja_data.getJSONObject(i);
			String menuName = object.getString("menuName");
			String menuId=object.getString("menuId");
			boolean selected=object.getBoolean("selected");
		if(selected){
				
		 try {
			
			String sql="INSERT INTO users_group_menu(GROUP_ID,MENU_ID,COMP_CODE,CREATED_BY) VALUES(?,?,?,?)";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, groupId);
			prep.setObject(2, menuId);
			prep.setObject(3, companyCode);
			prep.setObject(4, system_username);
			prep.execute();
			conn.close();
			flag="done";
		}catch(Exception e) {
			e.printStackTrace();
			flag="error";
		}
	}
		
}
		
		
		if(flag.equalsIgnoreCase("done")) {
			map.put("responseCode", "00");
			map.put("message", "successfully saved");
			return ResponseEntity.ok(map);
		}
		else {
			map.put("responseCode", "01");
			map.put("message", "Could not save record");
			return ResponseEntity.ok(map);
		}
	}
	
	@RequestMapping(value = "/v1/getMenuForGroup", method = RequestMethod.POST)
	public ResponseEntity<?> getMenuForGroupId(@RequestBody NewMenuGroupReqModel jsondata) {
		if(jsondata.getGroup_id().isEmpty() || jsondata.getGroup_id().equalsIgnoreCase("")) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide group_id","",""));
		}
		Hashtable<String,Object> map=new Hashtable<String,Object>();
		try {
		map.put("companyCode", "00");
		map.put("message", "Success");
		map.put("items", menuAndGroupList(jsondata.getGroup_id()));
		return ResponseEntity.ok(map);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(map);
		}
		
	}
	
	public void updateGroupId(String group_id,String comp_id,String name) {
		
		try {
			String sql="UPDATE user_groups SET NAME=? WHERE id=? AND comp_code=? ";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, name);
			prep.setObject(2, group_id);
			prep.setObject(3, comp_id);
			prep.executeUpdate();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
			}
	}
	
	public List<UsersAccessMenuRespModel> userAccessMenu(String companycode,String group_id){
		List<UsersAccessMenuRespModel> list=new ArrayList<UsersAccessMenuRespModel>();
		try {
			String sql="SELECT GROUP_ID ,(SELECT NAME FROM `user_groups` WHERE id=GROUP_ID) user_group_name,\r\n" + 
					"MENU_ID,(SELECT NAME FROM  `menues` WHERE id=MENU_ID) menu_description FROM `users_group_menu`\r\n" + 
					"WHERE GROUP_ID=? AND COMP_CODE=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, group_id);
			prep.setObject(2, companycode);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				UsersAccessMenuRespModel model=new UsersAccessMenuRespModel();
				model.menuId=rs.getString("MENU_ID");
				model.menuName=rs.getString("menu_description");
				list.add(model);
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List<UsersAccessMenuRespModel> getAdminmenu(){
		List<UsersAccessMenuRespModel> list=new ArrayList<UsersAccessMenuRespModel>();
		try {
			String sql="SELECT * FROM menues WHERE user=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, "payer");
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				UsersAccessMenuRespModel model=new UsersAccessMenuRespModel();
				model.menuName=rs.getString("name");
				model.menuId=rs.getString("id");
				list.add(model);
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List<ListMenusGroupsRespModel> menuAndGroupList(String group_id){
		List<ListMenusGroupsRespModel> list=new ArrayList<ListMenusGroupsRespModel>();
		try {
			String sql="SELECT GROUP_ID ,(SELECT NAME FROM `user_groups` WHERE id=GROUP_ID) user_group_name,\r\n" + 
					"MENU_ID,(SELECT NAME FROM  `menues` WHERE id=MENU_ID) menu_description FROM `users_group_menu`\r\n" + 
					"WHERE group_id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, group_id);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				ListMenusGroupsRespModel model=new ListMenusGroupsRespModel();
				model.menuId=rs.getString("MENU_ID");
				model.menuName=rs.getString("menu_description");
				model.groupId=rs.getString("GROUP_ID");
				model.groupName=rs.getString("user_group_name");
				model.selected=true;
				list.add(model);
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public void deleteMenusForAgroup(String companyCode,String groupId) {
		try {
			String sql="DELETE FROM `users_group_menu` WHERE COMP_CODE=? AND GROUP_ID=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, companyCode);
			prep.setObject(2, groupId);
			prep.executeUpdate();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public List<UsersAccessMenuRespModel> bankAccessMenu(String group_id){
		List<UsersAccessMenuRespModel> list=new ArrayList<UsersAccessMenuRespModel>();
		try {
			String sql="SELECT menu_id,id,\r\n" + 
					"(SELECT NAME FROM menues B WHERE B.id=A.menu_id)menue_name\r\n" + 
					" FROM bank_group_menues A WHERE group_id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, group_id);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				UsersAccessMenuRespModel model=new UsersAccessMenuRespModel();
				model.menuId=rs.getString("id");
				model.menuName=rs.getString("menue_name");
				model.roles=BankGroupMenues.menuRole(rs.getString("menu_id"),group_id);
				list.add(model);
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List<UsersAccessMenuRespModel> bankAccessAllMenu(){
		List<UsersAccessMenuRespModel> list=new ArrayList<UsersAccessMenuRespModel>();
		try {
			String sql="SELECT * FROM menues WHERE USER='bank'";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				UsersAccessMenuRespModel model=new UsersAccessMenuRespModel();
				model.menuId=rs.getString("id");
				model.menuName=rs.getString("name");
				list.add(model);
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
