package com.stanbic.Roles;

public class NewUserGroupReqModel {

	String token="";
	String name="";
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
