package com.stanbic.Roles;

public class UserGroupRespModel {
	String name="";
	String group_id="";
	String noOfUsers="";
	String status="";
	String id="";
	String delete_groupe="";
	String edit_group="";
	
	
	
	public String getEdit_group() {
		return edit_group;
	}
	public void setEdit_group(String edit_group) {
		this.edit_group = edit_group;
	}
	public String getDelete_groupe() {
		return delete_groupe;
	}
	public void setDelete_groupe(String delete_groupe) {
		this.delete_groupe = delete_groupe;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNoOfUsers() {
		return noOfUsers;
	}
	public void setNoOfUsers(String noOfUsers) {
		this.noOfUsers = noOfUsers;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGroup_id() {
		return group_id;
	}
	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}
	
}
