package com.stanbic.Roles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.stanbic.comm.DbManager;

public class RoleHelper {
	static Connection conn=null;
	
	public static String getInsertedGroupId(String name,String comp_code) {
		String id="";
		try {
			String sql="INSERT INTO user_groups (name,comp_code)VALUES(?,?) ON DUPLICATE KEY UPDATE name=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
			prep.setObject(1, name);
			prep.setObject(2, comp_code);
			prep.setObject(3, name);
			prep.execute();
			 ResultSet rs = prep.getGeneratedKeys();
			 if(rs.next()) {
				 id = rs.getString(1);
	           }
			conn.close();
		}catch(Exception n) {
			System.out.println("User Group erroruuuuuuuuuuuuuuuuuuuuuuuu");
			n.printStackTrace();
		}
		return id;
	}
	
	public static String getMenuName(String id) {
		String name=null;
		try {
			String sql="SELECT *  FROM menues WHERE id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, id);
			ResultSet rs = prep.executeQuery();
			while(rs.next()) {
				name=rs.getString("name");
			}
			
			conn.close();
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		return name;
	}
	
	public static String whocreatedgroup(String id) {
		String name="";
		
		try {
			String sql="SELECT *  FROM bank_groups WHERE id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, id);
			ResultSet rs = prep.executeQuery();
			while(rs.next()) {
				name=rs.getString("created_by");
			}
			
			conn.close();
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		
		return name;
	}
	
	public static String whomodifieddgroup(String id) {
		String name="";
		
		try {
			String sql="SELECT *  FROM bank_groups WHERE id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, id);
			ResultSet rs = prep.executeQuery();
			while(rs.next()) {
				name=rs.getString("updated_by");
			}
			
			conn.close();
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		
		return name;
	}
	public static String getGroupName(String id) {
		String name="";
		
		try {
			String sql="SELECT *  FROM bank_groups WHERE id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, id);
			ResultSet rs = prep.executeQuery();
			while(rs.next()) {
				name=rs.getString("name");
			}
			
			conn.close();
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		
		return name;
	}
	
	public static int hasGropeUsers(String id) {
		int name=0;
		
		try {
			String sql="SELECT COUNT(*)vcount FROM bank_users WHERE group_id=?";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, id);
			ResultSet rs = prep.executeQuery();
			while(rs.next()) {
				name=rs.getInt("vcount");
			}
			
			conn.close();
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		
		return name;
	}
}
