package com.stanbic.Roles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class MenueApproval {
	
static Connection conn=null;
	
	@RequestMapping(value = "/v1/approveEditingGroup", method = RequestMethod.POST)
	public ResponseEntity<?> approveEditingGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody NewMenuGroupReqModel jsondata) {
		if(jsondata.getGroup_id().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide group_id","",""));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		if(BankGroupMenues.whoeditedGroup(jsondata.getGroup_id()).equalsIgnoreCase(username)) {
			 return ResponseEntity.ok(new AuthProduceJson("06","You are not authorised to approve this group","",""));
		}
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {
			map.put("groupId",jsondata.getGroup_id());
			map.put("groupName",BankGroupMenues.getEditedGroupName(jsondata.getGroup_id()));
			map.put("menuItems",bankAccessMenu(jsondata.getGroup_id()));
			
			
		}catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System error","",""));
		}
		
		 return ResponseEntity.ok(map);
	}	
	@RequestMapping(value = "/v1/rejectEditingGroup", method = RequestMethod.POST)
	public ResponseEntity<?> rejectEditingGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody NewMenuGroupReqModel jsondata) {
		if(jsondata.getGroup_id().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide group_id","",""));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		if(BankGroupMenues.whoeditedGroup(jsondata.getGroup_id()).equalsIgnoreCase(username)) {
			 return ResponseEntity.ok(new AuthProduceJson("06","You are not authorised to approve this group","",""));
		}
		
		try {
			conn=DbManager.getConnection();
			String sql2="UPDATE bank_groups SET edit_group=?,updated_by WHERE id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep2=conn.prepareStatement(sql2);
			prep2.setObject(1, "no");
			prep2.setObject(2, username);
			prep2.execute();
			conn.close();
			System_Logs.log_system(username, "", "bank user rejected editied group:"+jsondata.getGroup_id(), "successful", "");
			 return ResponseEntity.ok(new AuthProduceJson("00","Bank group editing rejected successfully","",""));
			
		}catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System error","",""));
		}
		
		
	}	
	
	@RequestMapping(value = "/v1/ProceedAproval", method = RequestMethod.POST)
	public ResponseEntity<?> ProceedAproval(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
		String id=TokenManager.tokenIssuedCompCode(token);
		try {
			Hashtable<String,String> map=new Hashtable<String,String>();
			
			JSONObject obj = new JSONObject(jsondata);
			String group_id=obj.getString("groupId");
			JSONArray ja_data = obj.getJSONArray("menuItems");
			String group_name=obj.getString("groupName");
			
			BankGroupMenues.deleteMenusForAgroup(group_id);//delete old record
			BankGroupMenues.updateGroupId2(group_id);
			
			for (int i = 0; i < ja_data.length(); i++) {
				JSONObject object = ja_data.getJSONObject(i);
				String menuName = object.getString("menuName");
				String menuId=object.getString("menuId");
//				boolean selected=object.getBoolean("selected");
				
				JSONArray role_array = object.getJSONArray("roles");
				for(int j=0;j<role_array.length();j++) {
					JSONObject roleobject = role_array.getJSONObject(j);
					String role=roleobject.getString("role");
					boolean status=roleobject.getBoolean("status");
					
					
						String sql="INSERT INTO bank_roles(group_id,menu_id,role,status) VALUES(?,?,?,?) ON DUPLICATE KEY UPDATE status=?";
						conn=DbManager.getConnection();
						PreparedStatement prep=conn.prepareStatement(sql);
						prep.setObject(1, group_id);
						prep.setObject(2, menuId);
						prep.setObject(3, role);
						prep.setObject(4, status);
						prep.setObject(5, status);
						prep.execute();
						conn.close();
					
				}
				
				
				String sql="INSERT INTO bank_group_menues(group_id,menu_id) VALUES(?,?)";
				conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				prep.setObject(1, group_id);
				prep.setObject(2, menuId);
				prep.execute();
				conn.close();
				
				String sql2="UPDATE bank_groups SET edit_group=?,approved_by=?,name=? WHERE id='"+group_id+"'";
				conn=DbManager.getConnection();
				PreparedStatement prep2=conn.prepareStatement(sql2);
				prep2.setObject(1, "no");
				prep2.setObject(2, id);
				prep2.setObject(3, group_name);
				prep2.execute();
				conn.close();

			}
				
			}catch(Exception e) {
				e.printStackTrace();
				 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
			}
		System_Logs.log_system(id, "", "bank user approved editied group:", "successful", "");
		 return ResponseEntity.ok(new AuthProduceJson("00","Approved successfully","",""));
	}
	
	public List<UsersAccessMenuRespModel> bankAccessMenu(String group_id){
		List<UsersAccessMenuRespModel> list=new ArrayList<UsersAccessMenuRespModel>();
		try {
			String sql="SELECT *,\r\n" + 
					"(SELECT NAME FROM menues B WHERE B.id=A.menu_id)menue_name\r\n" + 
					" FROM edit_bank_group_menues A WHERE group_id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, group_id);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				UsersAccessMenuRespModel model=new UsersAccessMenuRespModel();
				model.menuId=rs.getString("menu_id");
				model.menuName=rs.getString("menue_name");
				model.roles=menuRole(rs.getString("menu_id"),group_id);
				list.add(model);
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	
	public static  List<RoleResModel> menuRole(String menue_id,String group_id){
		List<RoleResModel> list=new ArrayList<RoleResModel>();
		try {
			
			String sql="SELECT * FROM edit_bank_roles WHERE menu_id=? AND group_id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, menue_id);
			prep.setObject(2, group_id);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				RoleResModel model=new RoleResModel();
				model.role=rs.getString("role");
				model.status=rs.getBoolean("status");
				list.add(model);
			}
//			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
