package com.stanbic.Roles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class BankGroups {
Connection conn=null;
	
	@RequestMapping(value = "/v1/addBankGroup", method = RequestMethod.POST)
	public ResponseEntity<?> addBankGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody NewUserGroupReqModel jsondata) {
		if(jsondata.getName().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide name","",""));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		try {
			
		String sql="INSERT INTO bank_groups (name,created_by)VALUES(?,?) ON DUPLICATE KEY UPDATE name=?,created_by=?";
		conn = DbManager.getConnection();
		PreparedStatement prep = conn.prepareStatement(sql);
		prep.setObject(1, jsondata.getName());
		prep.setObject(2, username);
		prep.setObject(3, jsondata.getName());
		prep.setObject(4, username);
		prep.execute();
		conn.close();
		System_Logs.log_system(username, "", "bank user added group:"+jsondata.getName(), "quued", "");
		return ResponseEntity.ok(new NewUserGroupRespModel("00", "Group queued Successfully ", "", ""));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new NewUserGroupRespModel("01", "System technical error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/ApproveaddBankGroup", method = RequestMethod.POST)
	public ResponseEntity<?> ApproveaddBankGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody UserGroupRespModel jsondata) {
		if(jsondata.getId().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide id","",""));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		
		String creater=RoleHelper.whocreatedgroup(jsondata.getId());
		if(creater.equalsIgnoreCase(username)) {
			return ResponseEntity.ok(new NewUserGroupRespModel("06", "You are not authorised to approve this groupe", ""));
		}
		
		try {
		conn = DbManager.getConnection();
		String sql="UPDATE bank_groups SET status=?,approved_by=? WHERE id=?";
		PreparedStatement prep = conn.prepareStatement(sql);
		prep.setObject(1, "approved");
		prep.setObject(2, username);
		prep.setObject(3, jsondata.getId());
		prep.execute();
		conn.close();
		System_Logs.log_system(username, "", "bank approved added group:", "quued", "");
		return ResponseEntity.ok(new NewUserGroupRespModel("00", "Group approved Successfully ", "", ""));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new NewUserGroupRespModel("01", "System technical error", ""));
		}
	}
	@RequestMapping(value = "/v1/RejectaddBankGroup", method = RequestMethod.POST)
	public ResponseEntity<?> RejectaddBankGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody UserGroupRespModel jsondata) {
		if(jsondata.getId().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide id","",""));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
		
		String creater=RoleHelper.whocreatedgroup(jsondata.getId());
		if(creater.equalsIgnoreCase(username)) {
			return ResponseEntity.ok(new NewUserGroupRespModel("06", "You are not authorised to approve this groupe", ""));
		}
		
		try {
			String sql="DELETE FROM bank_groups WHERE id=?";
	    	conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getGroup_id());
			prep.execute();
			conn.close();
			System_Logs.log_system(username, "", "bank user rejected add group:"+jsondata.getName(), "success", "");
		return ResponseEntity.ok(new NewUserGroupRespModel("00", "Group approved Successfully ", "", ""));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new NewUserGroupRespModel("01", "System technical error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/getBankGroups", method = RequestMethod.POST)
	public ResponseEntity<?> getMyGroups(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody UserGroupReqModel jsondata) {
		List<UserGroupRespModel> model = new ArrayList<UserGroupRespModel>();
		try {
			String sql = "SELECT *, \r\n" + 
					"(SELECT COUNT(*)vcount FROM bank_users B WHERE B.group_id=A.id)NO_OF_USERS\r\n" + 
					"FROM bank_groups A";
			conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				UserGroupRespModel data = new UserGroupRespModel();
				data.name=rs.getString("name");
				data.group_id=rs.getString("id");
				data.noOfUsers=rs.getString("NO_OF_USERS");
				data.status=rs.getString("status");
				data.id=rs.getString("id");
				data.delete_groupe=rs.getString("delete_group");
				data.edit_group=rs.getString("edit_group");
				model.add(data);
			}
//			conn.close();
			return ResponseEntity.ok(model);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.ok(new AuthProduceJson("02", "System technical error", "", ""));
		}
	}
	
	
	@RequestMapping(value = "/v1/deleteBankGroup", method = RequestMethod.POST)
	public ResponseEntity<?> deleteBankGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody UserGroupReqModel jsondata) {
		if(jsondata.getGroup_id().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please provide group id", "", ""));
		}
		if(RoleHelper.getGroupName(jsondata.getGroup_id()).equalsIgnoreCase("Admin")) {
			return ResponseEntity.ok(new AuthProduceJson("06","You are not supposed to delete this group", "", ""));
		}
		if(RoleHelper.hasGropeUsers(jsondata.getGroup_id())>0) {
			return ResponseEntity.ok(new AuthProduceJson("06","You are not able to delete this group for it  has users", "", ""));
		}
		String username=TokenManager.tokenIssuedCompCode(token);
	     
	     BankGroupMenues.deleteMenusForAgroup(jsondata.getGroup_id());
	     try {
	    	 String sql="UPDATE bank_groups SET delete_group=? ,updated_by=? WHERE id=?";
	    	conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "yes");
			prep.setObject(2, username);
			prep.setObject(3, jsondata.getGroup_id());
			prep.execute();
			conn.close();
			System_Logs.log_system(username, "", "bank user deleted group:"+jsondata.getGroup_id(), "queued", "");
	     }catch(Exception e) {
	    	 e.printStackTrace();
	    	  return ResponseEntity.ok(new AuthProduceJson("02","System technicall error","",""));
	     }
	     return ResponseEntity.ok(new AuthProduceJson("00","Group deletion queued successfully","",""));
	}
	

	@RequestMapping(value = "/v1/RejectdeleteBankGroup", method = RequestMethod.POST)
	public ResponseEntity<?> RejectdeleteBankGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody UserGroupReqModel jsondata) {
		if(jsondata.getGroup_id().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please provide group id", "", ""));
		}
		
		String username=TokenManager.tokenIssuedCompCode(token);
		
		if(RoleHelper.whomodifieddgroup(jsondata.getGroup_id()).equalsIgnoreCase(username)) {
			return ResponseEntity.ok(new AuthProduceJson("06","You are not authorised to reject this group", "", ""));
		}
	     try {
	    	 String sql="UPDATE bank_groups SET delete_group=? ,updated_by=? WHERE id=?";
	    	conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "no");
			prep.setObject(2, username);
			prep.setObject(3, jsondata.getGroup_id());
			prep.execute();
			conn.close();
			System_Logs.log_system(username, "", "bank user rejected deleted group:"+jsondata.getGroup_id(), "success", "");
	     }catch(Exception e) {
	    	 e.printStackTrace();
	    	  return ResponseEntity.ok(new AuthProduceJson("02","System technicall error","",""));
	     }
	     return ResponseEntity.ok(new AuthProduceJson("00","Group deletion rejected successfully","",""));
	}
	
	@RequestMapping(value = "/v1/ApprovedeleteBankGroup", method = RequestMethod.POST)
	public ResponseEntity<?> ApprovedeleteBankGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody UserGroupReqModel jsondata) {
		if(jsondata.getGroup_id().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06","Please provide group id", "", ""));
		}
		if(RoleHelper.getGroupName(jsondata.getGroup_id()).equalsIgnoreCase("Admin")) {
			return ResponseEntity.ok(new AuthProduceJson("06","You are not supposed to delete this group", "", ""));
		}
		if(RoleHelper.hasGropeUsers(jsondata.getGroup_id())>0) {
			return ResponseEntity.ok(new AuthProduceJson("06","You are not able to delete this group for it  has users", "", ""));
		}
		
		String username=TokenManager.tokenIssuedCompCode(token);
		
		if(RoleHelper.whomodifieddgroup(jsondata.getGroup_id()).equalsIgnoreCase(username)) {
			return ResponseEntity.ok(new AuthProduceJson("06","You are not authorised to delete this group", "", ""));
		}
	     
	     BankGroupMenues.deleteMenusForAgroup(jsondata.getGroup_id());
	     try {
	    	 String sql="DELETE FROM bank_groups WHERE id=?";
	    	conn = DbManager.getConnection();
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getGroup_id());
			prep.execute();
			conn.close();
			
			
			System_Logs.log_system(username, "", "Bank user aproved deleted group:"+jsondata.getGroup_id(), "success", "");
	     }catch(Exception e) {
	    	 e.printStackTrace();
	    	  return ResponseEntity.ok(new AuthProduceJson("02","System technicall error","",""));
	     }
	     return ResponseEntity.ok(new AuthProduceJson("00","Group deleted successfully","",""));
	}
}
