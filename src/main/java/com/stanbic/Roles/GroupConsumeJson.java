package com.stanbic.Roles;

public class GroupConsumeJson {
String messageCode=null;
String message=null;
String name=null;
String group_id=null;
String token=null;



public String getToken() {
	return token;
}
public void setToken(String token) {
	this.token = token;
}
public String getMessageCode() {
	return messageCode;
}
public void setMessageCode(String messageCode) {
	this.messageCode = messageCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getGroup_id() {
	return group_id;
}
public void setGroup_id(String group_id) {
	this.group_id = group_id;
}



}
