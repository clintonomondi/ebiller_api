package com.stanbic.Roles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Logs.System_Logs;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class BankGroupMenues {
	static Connection conn=null;
	
	@RequestMapping(value = "/v1/addbankMenuToGroup", method = RequestMethod.POST)
	public ResponseEntity<?> addbankMenuToGroup(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody String jsondata) {
		try {
		Hashtable<String,String> map=new Hashtable<String,String>();
		String user=TokenManager.tokenIssuedCompCode(token);
		
		JSONObject obj = new JSONObject(jsondata);
		String group_id=obj.getString("groupId");
		String group_name=obj.getString("groupName");
		JSONArray ja_data = obj.getJSONArray("menuItems");
		
		deleteMenusForAgroup2(group_id);//delete old record
		updateGroupId(group_name,group_id,user);
		saveEditedGroup(group_name,group_id,user);
		
		for (int i = 0; i < ja_data.length(); i++) {
			JSONObject object = ja_data.getJSONObject(i);
			String menuName = object.getString("menuName");
			String menuId=object.getString("menuId");
			boolean selected=object.getBoolean("selected");
			
			JSONArray role_array = object.getJSONArray("rolee");
			for(int j=0;j<role_array.length();j++) {
				JSONObject roleobject = role_array.getJSONObject(j);
				String role=roleobject.getString("role");
				boolean status=roleobject.getBoolean("status");
				
				
					String sql="INSERT INTO edit_bank_roles(group_id,menu_id,role,status) VALUES(?,?,?,?) ON DUPLICATE KEY UPDATE status=?";
					conn=DbManager.getConnection();
					PreparedStatement prep=conn.prepareStatement(sql);
					prep.setObject(1, group_id);
					prep.setObject(2, menuId);
					prep.setObject(3, role);
					prep.setObject(4, status);
					prep.setObject(5, status);
					prep.execute();
					conn.close();
				
			}
			
			
		if(selected){
			String sql="INSERT INTO edit_bank_group_menues(group_id,menu_id) VALUES(?,?)";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, group_id);
			prep.setObject(2, menuId);
			prep.execute();
			conn.close();
		}
		
		
		}
		System_Logs.log_system(user, "", "bank user added menues for group group:", "queued", "");
			return ResponseEntity.ok(new NewUserGroupRespModel("00", "bank menue roles queued successfully for approval", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new NewUserGroupRespModel("02", "System technical error", ""));
		}
	
	}	
	
	
	
	@RequestMapping(value = "/v1/getBankMenuForGroup", method = RequestMethod.POST)
	public ResponseEntity<?> getMenuForGroupId(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody NewMenuGroupReqModel jsondata) {
		if(jsondata.getGroup_id().isEmpty() || jsondata.getGroup_id().equalsIgnoreCase("")) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide group_id","",""));
		}
		Hashtable<String,Object> map=new Hashtable<String,Object>();
		try {
		map.put("companyCode", "00");
		map.put("message", "Success");
		map.put("items", menuAndGroupList(jsondata.getGroup_id()));
		return ResponseEntity.ok(map);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(map);
		}
		
	}
	
	public List<ListMenusGroupsRespModel> menuAndGroupList(String group_id){
		List<ListMenusGroupsRespModel> list=new ArrayList<ListMenusGroupsRespModel>();
		try {
			
			String sql="SELECT GROUP_ID ,(SELECT NAME FROM `bank_groups` WHERE id=GROUP_ID) user_group_name,\r\n" + 
					"MENU_ID,(SELECT NAME FROM  `menues` WHERE id=MENU_ID) menu_description FROM `bank_group_menues`\r\n" + 
					"WHERE group_id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, group_id);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				ListMenusGroupsRespModel model=new ListMenusGroupsRespModel();
				model.menuId=rs.getString("MENU_ID");
				model.menuName=rs.getString("menu_description");
				model.groupId=rs.getString("GROUP_ID");
				model.groupName=rs.getString("user_group_name");
				model.selected=true;
				model.roles=menuRole(rs.getString("MENU_ID"),rs.getString("GROUP_ID"));
				list.add(model);
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static  List<RoleResModel> menuRole(String menue_id,String group_id){
		List<RoleResModel> list=new ArrayList<RoleResModel>();
		try {
			
			String sql="SELECT * FROM bank_roles WHERE menu_id=? AND group_id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, menue_id);
			prep.setObject(2, group_id);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				RoleResModel model=new RoleResModel();
				model.role=rs.getString("role");
				model.status=rs.getBoolean("status");
				list.add(model);
			}
//			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static void deleteMenusForAgroup(String group_id) {
		try {
			String sql="DELETE FROM `bank_group_menues` WHERE group_id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, group_id);
			prep.executeUpdate();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static void deleteMenusForAgroup2(String group_id) {
		try {
			String sql="DELETE FROM `edit_bank_group_menues` WHERE group_id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, group_id);
			prep.executeUpdate();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void deleteRolesForAgroup(String group_id) {
		try {
			String sql="DELETE FROM `bank_roles` WHERE group_id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, group_id);
			prep.executeUpdate();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void updateGroupId(String name,String group_id,String updated_by) {
		
		try {
			String sql="UPDATE bank_groups SET updated_by=?,edit_group=? WHERE id='"+group_id+"'";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, updated_by);
			prep.setObject(2, "yes");
			prep.executeUpdate();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
			}
	}
public static void saveEditedGroup(String name,String group_id,String updated_by) {
		
		try {
			String sql="INSERT INTO  edit_bank_groups(name,group_id)VALUES(?,?) ON DUPLICATE KEY UPDATE name=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, name);
			prep.setObject(2, group_id);
			prep.setObject(3, name);
			prep.executeUpdate();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
			}
	}
public static void updateGroupId2(String group_id) {
		
		try {
			String sql="UPDATE bank_groups SET edit_group=? WHERE id='"+group_id+"'";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, "no");
			prep.executeUpdate();
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
			}
	}
	
public static String whoeditedGroup(String id) {
		String name="";
		try {
			String sql="SELECT updated_by FROM bank_groups WHERE id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, id);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				name=rs.getString("updated_by");
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
			}
		
		return name;
	}

public static String getEditedGroupName(String id) {
	String name="";
	try {
		String sql="SELECT name FROM edit_bank_groups WHERE group_id=?";
		conn=DbManager.getConnection();
		PreparedStatement prep=conn.prepareStatement(sql);
		prep.setObject(1, id);
		ResultSet rs=prep.executeQuery();
		while(rs.next()) {
			name=rs.getString("name");
		}
		conn.close();
	}catch(Exception e) {
		e.printStackTrace();
		}
	
	return name;
}
}
