package com.stanbic.Roles;

public class NewUserGroupRespModel {
	String messageCode = "";
	String message = "";
	String token="";
	String id = "";
	
	public NewUserGroupRespModel(String messageCode, String message, String token) {
		this.messageCode = messageCode;
		this.message = message;
		this.token = token;
	}
	public NewUserGroupRespModel(String messageCode, String message, String token, String id) {
		this.messageCode = messageCode;
		this.message = message;
		this.token = token;
		this.id = id;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
