package com.stanbic.Roles;

public class ListMenusGroupsRespModel {
 String menuId="";
 String menuName="";
 String groupId="";
 String groupName="";
 String companyCode="";
 boolean selected;
 Object roles ="";
 
 

public Object getRoles() {
	return roles;
}

public void setRoles(Object roles) {
	this.roles = roles;
}

public boolean isSelected() {
	return selected;
}

public void setSelected(boolean selected) {
	this.selected = selected;
}

public String getMenuId() {
	return menuId;
}

public void setMenuId(String menuId) {
	this.menuId = menuId;
}
public String getMenuName() {
	return menuName;
}
public void setMenuName(String menuName) {
	this.menuName = menuName;
}
public String getGroupId() {
	return groupId;
}
public void setGroupId(String groupId) {
	this.groupId = groupId;
}
public String getGroupName() {
	return groupName;
}
public void setGroupName(String groupName) {
	this.groupName = groupName;
}
public String getCompanyCode() {
	return companyCode;
}
public void setCompanyCode(String companyCode) {
	this.companyCode = companyCode;
}
 
}
