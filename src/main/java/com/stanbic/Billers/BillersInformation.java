package com.stanbic.Billers;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Logo.InvoiceLogo;
import com.stanbic.Logo.InvoiceLogoConsumeJson;
import com.stanbic.backendUsers.BankUsersRespModel;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.GeneralCodes;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class BillersInformation {

	Connection conn=null;
	 @RequestMapping(value = "/v1/listOfBillers", method = RequestMethod.POST)
	 public ResponseEntity<?> getInvoiceLogo(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillerInformationReqModel jsondata){
		 String payer_code=TokenManager.tokenIssuedCompCode(token);
		 
	     JSONArray ja = new JSONArray();
		 String base64="";
		 try {
			 String sql="SELECT *,\r\n" + 
			 		"(SELECT logo_url FROM interface_settings B WHERE B.comp_code=A.comp_code)logo_url,\r\n" + 
			 		"(SELECT if(email_notif is null,'N',email_notif) FROM billers C WHERE C.payer_code=? AND C.biller_code=A.comp_code)notif_value\r\n" + 
			 		" FROM biller_profile A WHERE user_type='biller' AND biller_type='Open' AND STATUS='Active'";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, payer_code);
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 if(rs.getString("logo_url")==null) {
					 base64="no image";
				 }else {
					 File file=new File(rs.getString("logo_url"));
		    	 base64=FileManager.encodeFileToBase64Binary(file); 
				 }
				 JSONObject jo = new JSONObject();
				 jo.put("billerCode",rs.getString("COMP_CODE") );
				 jo.put("billerName", rs.getString("COMPANY_NAME"));
				 jo.put("alias", rs.getString("alias"));
				 jo.put("notif_value",rs.getString("notif_value"));
				 jo.put("biller_type",rs.getString("biller_type"));
				 jo.put("billerlogo", base64);
				 ja.put(jo);
			 }
			 conn.close();
			 JSONObject mainObj = new JSONObject();
			 mainObj.put("open_billers", ja);
			 mainObj.put("my_billers", new BillerHelper().getMyBillersList(payer_code));
			 return ResponseEntity.ok(mainObj.toString());
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
		 
		 
	 }
}
