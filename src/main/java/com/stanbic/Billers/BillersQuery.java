package com.stanbic.Billers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;
import java.sql.Connection;

import com.stanbic.Logo.InvoiceLogo;
import com.stanbic.Profile.PayerProfileProduceJson;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.dropdown.country.CountryProduceJson;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class BillersQuery {
	Connection conn=null;
	
	 @RequestMapping(value = "/v1/getActiveBillers", method = RequestMethod.POST)
	 public ResponseEntity<?> getActiveBillers(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata) {
		 
		 
		 List<BillersQueryConsumeJSon> model= new ArrayList<BillersQueryConsumeJSon>();
		 try {
			String sql="SELECT * FROM `biller_profile` WHERE status=? AND user_type='biller' AND biller_type=?"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, "Active");
			prep.setObject(2, "Open");

			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				BillersQueryConsumeJSon data=new BillersQueryConsumeJSon();
				 data.id=rs.getString("id");
				  data.biller_location=rs.getString("biller_location");
				  data.biller_month=rs.getString("biller_month");
				  data.biller_phone=rs.getString("biller_phone");
				  data.business_description=rs.getString("business_description");
				  data.biller_code=rs.getString("comp_code");
				  data.company_name=rs.getString("company_name");
				  data.email=rs.getString("email");
				  data.employee_no=rs.getString("employee_no");
				  data.fname=rs.getString("personel_f_name");
				  data.lname=rs.getString("personel_l_name");
				  data.sector=rs.getString("sector");
				  data.stb_acc_name=rs.getString("stb_acc_name");
				  data.stb_acc_no=rs.getString("stb_acc_no");
				  data.token=jsondata.getToken();
				  data.user_type=rs.getString("user_type");
				  data.status=rs.getString("status");
				  data.biller_type=rs.getString("biller_type");
				  model.add(data);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 System.out.println(e.getMessage());
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
}
	
	 
	 @RequestMapping(value = "/v1/getInvitedBillers", method = RequestMethod.POST)
	 public ResponseEntity<?> getInvitedBillers(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata) {
		 List<BillersQueryConsumeJSon> model= new ArrayList<BillersQueryConsumeJSon>();
		 try {
			String sql="SELECT * FROM `biller_profile` WHERE status=?"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, "Invited");
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				BillersQueryConsumeJSon data=new BillersQueryConsumeJSon();
				 data.id=rs.getString("id");
				  data.biller_location=rs.getString("biller_location");
				  data.biller_month=rs.getString("biller_month");
				  data.biller_phone=rs.getString("biller_phone");
				  data.business_description=rs.getString("business_description");
				  data.biller_code=rs.getString("comp_code");
				  data.company_name=rs.getString("company_name");
				  data.email=rs.getString("email");
				  data.employee_no=rs.getString("employee_no");
				  data.fname=rs.getString("personel_f_name");
				  data.lname=rs.getString("personel_l_name");
				  data.sector=rs.getString("sector");
				  data.stb_acc_name=rs.getString("stb_acc_name");
				  data.stb_acc_no=rs.getString("stb_acc_no");
				  data.token=jsondata.getToken();
				  data.user_type=rs.getString("user_type");
				  data.status=rs.getString("status");
				  data.biller_type=rs.getString("biller_type");
				  model.add(data);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
}
	 
	 @RequestMapping(value = "/v1/viewBillerProfile", method = RequestMethod.POST)
	 public ResponseEntity<?> viewBillerProfile(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata) {
		 if(jsondata.getBiller_code().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide biller code","",""));
		 }
		 List<BillersQueryConsumeJSon> model= new ArrayList<BillersQueryConsumeJSon>();
		 String path=InvoiceLogo.getLogo(jsondata.getBiller_code());
		 String base64=null;
		 
	     if(path==null) {
	    	 File file2=new File(ExternalFile.getDefaultImageUrl());
	    	 base64=FileManager.encodeFileToBase64Binary(file2);
	     }
	     else {
	    	 File file=new File(path);
	    	 base64=FileManager.encodeFileToBase64Binary(file);
	     }
		 try {
			String sql="SELECT id,biller_location,biller_month,biller_phone,business_description,company_name,email,employee_no,\r\n" + 
					"personel_f_name,personel_l_name,sector,stb_acc_name,stb_acc_no,user_type,STATUS,biller_type,\r\n" + 
					"(SELECT invoice_color FROM interface_settings B WHERE A.comp_code=B.comp_code)invoice_color,\r\n" + 
					
					"(SELECT currency FROM `forex` Q WHERE Q.comp_code=A.comp_code)currency,\r\n" + 
					"(SELECT rate FROM `forex` R WHERE R.comp_code=A.comp_code)rate,\r\n" + 
					
					"(SELECT branch_name FROM country_branches R WHERE R.id=A.branch_id)branch,\r\n" +
					"(SELECT country_name FROM countries W WHERE W.id=(SELECT country_id FROM country_branches WHERE id=A.branch_id))country,\r\n" +
					"(SELECT COUNT(*) AS COUNT FROM billers C WHERE  C.biller_code=A.comp_code)payers,\r\n" + 
					"(SELECT COUNT(*) AS COUNT FROM eslip D WHERE D.biller_code=A.comp_code)invoiceRaised,\r\n" + 
					"(SELECT IF(SUM(amount_to_pay) IS NULL,'0', SUM(amount_to_pay)) FROM eslip E WHERE E.status='Pending' AND E.biller_code=A.comp_code)outstanding_bills\r\n" + 
					"FROM biller_profile A  WHERE comp_code=? \r\n" + 
					"GROUP BY id,biller_location,biller_month,biller_phone,business_description,company_name,email,employee_no ,personel_f_name,personel_l_name,sector,stb_acc_name,stb_acc_no,user_type,STATUS,biller_type"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getBiller_code());
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				BillersQueryConsumeJSon data=new BillersQueryConsumeJSon();
				data.messageCode="00";
				data.message="Success";
				 data.id=rs.getString("id");
				 data.branch=rs.getString("branch");
				 data.country=rs.getString("country");
				  data.biller_location=rs.getString("biller_location");
				  data.biller_month=rs.getString("biller_month");
				  data.biller_phone=rs.getString("biller_phone");
				  data.business_description=rs.getString("business_description");
				  data.biller_code=jsondata.getBiller_code();
				  data.company_name=rs.getString("company_name");
				  data.email=rs.getString("email");
				  data.employee_no=rs.getString("employee_no");
				  data.fname=rs.getString("personel_f_name");
				  data.lname=rs.getString("personel_l_name");
				  data.sector=rs.getString("sector");
				  data.stb_acc_name=rs.getString("stb_acc_name");
				  data.stb_acc_no=rs.getString("stb_acc_no");
				  data.user_type=rs.getString("user_type");
				  data.status=rs.getString("status");
				  data.biller_type=rs.getString("biller_type");
				  data.invoiceColor=rs.getString("invoice_color");
				  data.payers=rs.getString("payers");
				  data.invoiceRaised=rs.getString("invoiceRaised");
				  data.billsPaid="0.0";
				  data.cashMoved="0.00";
				  data.base64Logo=base64;
				  data.currency=rs.getString("currency");
				  data.rate=rs.getString("rate");
				  model.add(data);
			}
			 conn.close();
			 if(model.isEmpty()) {
				 return ResponseEntity.ok(new AuthProduceJson("07","Biller not found in the system","",""));
			 }
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
}
	 
	 @RequestMapping(value = "/v1/viewPayerProfile", method = RequestMethod.POST)
	 public ResponseEntity<?> viewPayerProfile(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata) {
		 if(jsondata.getPayer_code().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide biller code","",""));
		 }
		 List<BillersQueryConsumeJSon> model= new ArrayList<BillersQueryConsumeJSon>();
		 try {
				String sql="SELECT id,biller_location,biller_month,biller_phone,business_description,company_name,email,employee_no\r\n" + 
						",personel_f_name,personel_l_name,sector,stb_acc_name,stb_acc_no,user_type,STATUS,biller_type,\r\n" + 
						"(SELECT invoice_color FROM interface_settings B WHERE A.comp_code=B.comp_code)invoice_color,\r\n" + 
						"(SELECT COUNT(*) AS COUNT FROM billers C WHERE  C.biller_code=A.comp_code)payers,\r\n" + 
						"(SELECT COUNT(*) AS COUNT FROM eslip D WHERE D.biller_code=A.comp_code)invoiceRaised,\r\n" + 
						"(SELECT branch_name FROM country_branches E WHERE E.id=A.branch_id)branch\r\n" +
						" FROM biller_profile A  WHERE comp_code=? GROUP BY id,biller_location,biller_month,biller_phone,business_description,company_name,email,employee_no\r\n" + 
						",personel_f_name,personel_l_name,sector,stb_acc_name,stb_acc_no,user_type,STATUS,biller_type\r\n" + 
						""; 
				conn=DbManager.getConnection();
				PreparedStatement prep=conn.prepareStatement(sql);
				prep.setObject(1, jsondata.getBiller_code());
				ResultSet  rs=prep.executeQuery();
				while(rs.next()) {
					BillersQueryConsumeJSon data=new BillersQueryConsumeJSon();
					data.messageCode="00";
					data.message="Success";
					 data.id=rs.getString("id");
					  data.biller_location=rs.getString("biller_location");
					  data.biller_month=rs.getString("biller_month");
					  data.biller_phone=rs.getString("biller_phone");
					  data.business_description=rs.getString("business_description");
					  data.biller_code=jsondata.getBiller_code();
					  data.company_name=rs.getString("company_name");
					  data.email=rs.getString("email");
					  data.employee_no=rs.getString("employee_no");
					  data.fname=rs.getString("personel_f_name");
					  data.lname=rs.getString("personel_l_name");
					  data.sector=rs.getString("sector");
					  data.stb_acc_name=rs.getString("stb_acc_name");
					  data.stb_acc_no=rs.getString("stb_acc_no");
					  data.user_type=rs.getString("user_type");
					  data.status=rs.getString("status");
					  data.biller_type=rs.getString("biller_type");
					  data.invoiceColor=rs.getString("invoice_color");
					  data.payers=rs.getString("payers");
					  data.invoiceRaised=rs.getString("invoiceRaised");
					  data.billsPaid="0.0";
					  data.cashMoved="0.00";
					  data.branch=rs.getString("branch");
					  model.add(data);
				}
			 conn.close();
			 if(model.isEmpty()) {
				 return ResponseEntity.ok(new AuthProduceJson("07","Biller not found in the system","",""));
			 }
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
}
	 
	 @RequestMapping(value = "/v1/getPendingBillers", method = RequestMethod.POST)
	 public ResponseEntity<?> getPendingBillers(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata) {
		 List<BillersQueryConsumeJSon> model= new ArrayList<BillersQueryConsumeJSon>();
		 try {
			String sql="SELECT * FROM `biller_profile` WHERE status=?"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, "Pending");
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				BillersQueryConsumeJSon data=new BillersQueryConsumeJSon();
				 data.id=rs.getString("id");
				  data.biller_location=rs.getString("biller_location");
				  data.biller_month=rs.getString("biller_month");
				  data.biller_phone=rs.getString("biller_phone");
				  data.business_description=rs.getString("business_description");
				  data.biller_code=rs.getString("comp_code");
				  data.company_name=rs.getString("company_name");
				  data.email=rs.getString("email");
				  data.employee_no=rs.getString("employee_no");
				  data.fname=rs.getString("personel_f_name");
				  data.lname=rs.getString("personel_l_name");
				  data.sector=rs.getString("sector");
				  data.stb_acc_name=rs.getString("stb_acc_name");
				  data.stb_acc_no=rs.getString("stb_acc_no");
				  data.token=jsondata.getToken();
				  data.user_type=rs.getString("user_type");
				  data.status=rs.getString("status");
				  data.biller_type=rs.getString("biller_type");
				  model.add(data);
				  System.out.println("Pending billers queryied successfully");
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
}
	 
	 
	 @RequestMapping(value = "/v1/deleteInvitedBiller", method = RequestMethod.POST)
	 public ResponseEntity<?> deleteInvitedBiller(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody PayerProfileProduceJson jsondata) {
		 if(jsondata.getBiller_code().isEmpty()) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please provide biller code","",""));
		 }
		 try {
			 String sql="DELETE FROM biller_profile  WHERE comp_code=? AND status=?";
			 conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, jsondata.getBiller_code());
				 prep.setObject(2, "Invited");
				 prep.execute();
				 conn.close();
				 return ResponseEntity.ok(new AuthProduceJson("00","Biller deleted successfully","",""));
			 
		 }catch(Exception n) {
			 System.out.println(n.getMessage());
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
	 }
	 
	 
	 @RequestMapping(value = "/v1/countAllBillers", method = RequestMethod.GET)
	 public ResponseEntity<?> countAllBillers(HttpServletRequest request, @RequestHeader(name = "Authorization") String token) {
		 String Active="0";
		 String Invited="0";
		 String Pending="0";
		 String totalServiceCharged="0";
		 Hashtable<String,String> map=new Hashtable<String,String>();
		 try {
			 String sql="SELECT (SELECT COUNT(*) FROM biller_profile WHERE STATUS='Active' AND user_type='biller') AS ACTIVE,\r\n" + 
			 		"			 		(SELECT COUNT(*) FROM biller_profile WHERE STATUS='Invited') AS Invited,\r\n" + 
			 		"			 		(SELECT COUNT(*) FROM `biller_profile` WHERE STATUS='Pending') AS Pending, \r\n" +
			 		"			 		(SELECT SUM(amount_charged) FROM `eslip` WHERE STATUS='Pending') AS amount_charged \r\n" +
			 		"			 		FROM DUAL";
			 conn=DbManager.getConnection();
			 PreparedStatement prep=conn.prepareStatement(sql);
			 ResultSet rs=prep.executeQuery();
			 while(rs.next()) {
				 Active=rs.getString("Active");
				 Invited=rs.getString("Invited");
				 Pending=rs.getString("Pending");
				 totalServiceCharged=rs.getString("amount_charged");
			 }
			 conn.close(); 
			 map.put("Active", Active);
			 map.put("Invited", Invited);
			 map.put("Pending", Pending);
			 map.put("servicecharge",totalServiceCharged);
			 return ResponseEntity.ok(map);
			 
		 }
		 catch(Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
		 
		 
	 }
	 
}
