package com.stanbic.Billers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.sql.Connection;

import com.stanbic.Logo.InvoiceLogo;
import com.stanbic.Roles.UsersAccessMenuRespModel;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.FileManager;

public class BillerHelper {
	static Connection conn=null;
	String SECRET_KEY="536ghss38%()((*892902";
	
	public static String isMyBiller(String biller_code,String payer_code) {
		String company_name=null;
		try {
			 conn=DbManager.getConnection();
			 String sql="SELECT company_name FROM billers WHERE biller_code='"+biller_code+"' AND payer_code='"+payer_code+"'";
			 PreparedStatement prep=conn.prepareStatement(sql);
				ResultSet  rs=prep.executeQuery();
				while(rs.next()) {
					company_name=rs.getString("company_name");
				}
				conn.close();
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		return company_name;
	}
	
	public static String getComapnyName(String biller_code) {
		String company_name=null;
		try {
			 conn=DbManager.getConnection();
			 String sql="SELECT company_name FROM biller_profile WHERE comp_code='"+biller_code+"'";
			 PreparedStatement prep=conn.prepareStatement(sql);
				ResultSet  rs=prep.executeQuery();
				while(rs.next()) {
					company_name=rs.getString("company_name");
				}
				conn.close();
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		return company_name;
	}
	
	public static void linkPayerToBiller(String biller_code,String payer_code,String department_id) {
		try {
			 String sql="INSERT INTO billers (biller_code,payer_code,department_id)VALUES(?,?,?) ON DUPLICATE KEY UPDATE biller_code=?,payer_code=?";
			  conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, biller_code);
				 prep.setObject(2, payer_code);
				 prep.setObject(3, department_id);
				 prep.setObject(4, biller_code);
				 prep.setObject(5, payer_code);
				 prep.execute();
				 conn.close();
		 }catch(Exception n) {
			n.printStackTrace();
		 }
	}
	public static void linkPayerToBiller2(String biller_code,String payer_code,String department_id,String status) {
		try {
			 String sql="INSERT INTO billers (biller_code,payer_code,department_id,status)VALUES(?,?,?,?) ON DUPLICATE KEY UPDATE biller_code=?,payer_code=?,status=?";
			  conn=DbManager.getConnection();
			  PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, biller_code);
				 prep.setObject(2, payer_code);
				 prep.setObject(3, department_id);
				 prep.setObject(4, status);
				 prep.setObject(5, biller_code);
				 prep.setObject(6, payer_code);
				 prep.setObject(7, status);
				 prep.execute();
				 conn.close();
		 }catch(Exception n) {
			n.printStackTrace();
		 }
	}
	
	
	public static int isAccountExit(String biller_code,String account_no,String payer_code) {
		int count=0;
		try {
			 conn=DbManager.getConnection();
			 String sql="SELECT COUNT(*) AS count FROM account WHERE biller_code=? AND payer_code=? AND account_no=? ";
			 PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, biller_code);
			  prep.setObject(2, payer_code);
			  prep.setObject(2, account_no);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  count=rs.getInt("count");
			  }
			  conn.close();
			
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		return count;
		
	}
	
	public static void updateNumberOfAccounts(String biller_code,String payer_code) {
		int count=getAccountCount(biller_code,payer_code);
		try {
			 conn=DbManager.getConnection();
			 String sql="UPDATE billers SET accounts=? WHERE biller_code=? AND payer_code=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			 prep.setObject(1, count);
			 prep.setObject(2, biller_code);
			 prep.setObject(3, payer_code);
			 prep.execute();
		    conn.close();
		    System.out.println("Number of accounts updated successfully");
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		
	}
	
	public static int getAccountCount(String biller_code,String payer_code) {
		int count=0;
		try {
			 conn=DbManager.getConnection();
			 String sql="SELECT COUNT(*) AS count FROM account WHERE biller_code=? AND payer_code=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, biller_code);
			  prep.setObject(2, payer_code);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  count=rs.getInt("count");
			  }
			  conn.close();
			
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		return count;
		
	}
	
	public static String getNumberOfBillerPayer(String biller_code) {
		String count=null;
		try {
			 conn=DbManager.getConnection();
			 String sql="SELECT COUNT(*) AS count FROM billers WHERE biller_code=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, biller_code);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  count=rs.getString("count");
			  }
			 
			
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		return count;
	}
	
	public static String getInvoiceColor(String comp_code) {
		String count=null;
		try {
			 conn=DbManager.getConnection();
			 String sql="SELECT invoice_color from interface_settings WHERE comp_code=?";
			 PreparedStatement prep=conn.prepareStatement(sql);
			  prep.setObject(1, comp_code);
			  ResultSet rs=prep.executeQuery();
			  while(rs.next()) {
				  count=rs.getString("invoice_color");
			  }
			 
			
		}catch(Exception n) {
			System.out.println(n.getMessage());
		}
		return count;
	}
	
	
	
	
	
	public static boolean isAccountExist(String account_no,String payer_code,String biller_code) {
		boolean res=true;
		try {
		conn = DbManager.getConnection();
		String sql="SELECT account_no FROM account WHERE account_no=? AND payer_code=? AND biller_code=?";
		PreparedStatement prep = conn.prepareStatement(sql);
		prep.setObject(1, account_no);
		prep.setObject(2, payer_code);
		prep.setObject(3, biller_code);
		ResultSet rs=prep.executeQuery();
		if(rs.next()) {
			res=true;
		}
		else {
			res=false;
		}
		conn.close();
		}catch(Exception n) {
			n.printStackTrace();
		}
		return res;
	}
	
	
	
	public List<BillerInformListResp> getMyBillersList(String payer_code){
		List<BillerInformListResp> list=new ArrayList<BillerInformListResp>();
		String base64="";
		try {
			String sql="SELECT id,biller_code,payer_code,if(email_notif is null,'N',email_notif)email_notif,\r\n" + 
					"(SELECT logo_url FROM interface_settings B WHERE B.comp_code=A.biller_code)logo_url,\r\n" + 
					"(SELECT company_name FROM biller_profile C WHERE C.comp_code=A.biller_code)COMPANY_NAME,\r\n" + 
					"(SELECT alias FROM biller_profile D WHERE D.comp_code=A.biller_code)alias,\r\n" + 
					"(SELECT biller_type FROM biller_profile E WHERE E.comp_code=A.biller_code)biller_type\r\n" + 
					" FROM billers A WHERE payer_code=? AND status=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, payer_code);
			prep.setObject(2, "Active");
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				if(rs.getString("biller_type").equalsIgnoreCase("Closed")) {
				BillerInformListResp model=new BillerInformListResp();
				if(rs.getString("logo_url")==null) {
					 base64="no image";
				 }else {
					 File file=new File(rs.getString("logo_url"));
		    	 base64=FileManager.encodeFileToBase64Binary(file); 
				 }
				model.billerCode=rs.getString("biller_code");
				model.billerlogo=base64;
				model.billerName=rs.getString("COMPANY_NAME");
				model.alias=rs.getString("alias");
				model.notif_value=rs.getString("email_notif");
				model.biller_type=rs.getString("biller_type");
				list.add(model);
				}
			}
			conn.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	
}
