package com.stanbic.Billers;

public class BillersAccountProduceJson {
String messageCode="00";
String message="success";
String  biller_code=null;
String payer_code=null;
String account_no=null;
String accounts=null;
String amount_due=null;
String due_date=null;
String alias=null;
String account_name=null;
String name=null;
String status=null;
String user_type=null;
String firstName=null;
String lastName=null;
String company_name=null;
String id=null;

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getCompany_name() {
	return company_name;
}
public void setCompany_name(String company_name) {
	this.company_name = company_name;
}
public String getUser_type() {
	return user_type;
}
public void setUser_type(String user_type) {
	this.user_type = user_type;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAlias() {
	return alias;
}
public void setAlias(String alias) {
	this.alias = alias;
}
public String getDue_date() {
	return due_date;
}
public void setDue_date(String due_date) {
	this.due_date = due_date;
}
public String getMessageCode() {
	return messageCode;
}
public void setMessageCode(String messageCode) {
	this.messageCode = messageCode;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getAmount_due() {
	return amount_due;
}
public void setAmount_due(String amount_due) {
	this.amount_due = amount_due;
}
public String getAccounts() {
	return accounts;
}
public void setAccounts(String accounts) {
	this.accounts = accounts;
}
public String getAccount_no() {
	return account_no;
}
public void setAccount_no(String account_no) {
	this.account_no = account_no;
}
public String getBiller_code() {
	return biller_code;
}
public void setBiller_code(String biller_code) {
	this.biller_code = biller_code;
}
public String getPayer_code() {
	return payer_code;
}
public void setPayer_code(String payer_code) {
	this.payer_code = payer_code;
}
public String getAccount_name() {
	return account_name;
}
public void setAccount_name(String account_name) {
	this.account_name = account_name;
}




}
