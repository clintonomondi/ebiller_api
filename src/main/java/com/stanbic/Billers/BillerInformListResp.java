package com.stanbic.Billers;

public class BillerInformListResp {

	String billerName="";
	String billerCode="";
	String alias="";
	String billerlogo="";
	String notif_value="";
	String biller_type="";
	
	
	
	
	public String getBiller_type() {
		return biller_type;
	}

	public void setBiller_type(String biller_type) {
		this.biller_type = biller_type;
	}

	public String getNotif_value() {
		return notif_value;
	}

	public void setNotif_value(String notif_value) {
		this.notif_value = notif_value;
	}

	public String getBillerlogo() {
		return billerlogo;
	}

	public void setBillerlogo(String billerlogo) {
		this.billerlogo = billerlogo;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public BillerInformListResp() {
		
	}

	public String getBillerName() {
		return billerName;
	}
	public void setBillerName(String billerName) {
		this.billerName = billerName;
	}
	public String getBillerCode() {
		return billerCode;
	}
	public void setBillerCode(String billerCode) {
		this.billerCode = billerCode;
	}

}
