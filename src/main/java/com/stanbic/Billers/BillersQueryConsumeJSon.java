package com.stanbic.Billers;

public class BillersQueryConsumeJSon {
	String messageCode=null;
    String message=null;
	String id=null;
	String biller_code=null;
	String biller_location=null;
	String biller_phone=null;
	String biller_month=null;
	String branch=null;
	String business_description=null;
	String company_name=null;
	String country=null;
	String email=null;
	String employee_no=null;
	String fname=null;
	String lname=null;
	String sector=null;
	String stb_acc_name=null;
	String stb_acc_no=null;
	String token=null;
	String status=null;
	String biller_type=null;
	String year=null;
	String user_type=null;
	String payers=null;
	String invoiceColor=null;
	String invoiceRaised=null;
	String billsPaid=null;
	String cashMoved=null;
	String base64Logo=null;
	String rate="";
	String currency="";
	
	
		
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getBillsPaid() {
		return billsPaid;
	}
	public void setBillsPaid(String billsPaid) {
		this.billsPaid = billsPaid;
	}
	public String getCashMoved() {
		return cashMoved;
	}
	public void setCashMoved(String cashMoved) {
		this.cashMoved = cashMoved;
	}
	public String getInvoiceRaised() {
		return invoiceRaised;
	}
	public void setInvoiceRaised(String invoiceRaised) {
		this.invoiceRaised = invoiceRaised;
	}
	public String getBase64Logo() {
		return base64Logo;
	}
	public void setBase64Logo(String base64Logo) {
		this.base64Logo = base64Logo;
	}
	public String getInvoiceColor() {
		return invoiceColor;
	}
	public void setInvoiceColor(String invoiceColor) {
		this.invoiceColor = invoiceColor;
	}
	public String getPayers() {
		return payers;
	}
	public void setPayers(String payers) {
		this.payers = payers;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getBiller_code() {
		return biller_code;
	}
	public void setBiller_code(String biller_code) {
		this.biller_code = biller_code;
	}
	public String getBiller_location() {
		return biller_location;
	}
	public void setBiller_location(String biller_location) {
		this.biller_location = biller_location;
	}
	public String getBiller_phone() {
		return biller_phone;
	}
	public void setBiller_phone(String biller_phone) {
		this.biller_phone = biller_phone;
	}
	public String getBiller_month() {
		return biller_month;
	}
	public void setBiller_month(String biller_month) {
		this.biller_month = biller_month;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getBusiness_description() {
		return business_description;
	}
	public void setBusiness_description(String business_description) {
		this.business_description = business_description;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmployee_no() {
		return employee_no;
	}
	public void setEmployee_no(String employee_no) {
		this.employee_no = employee_no;
	}
	
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public String getStb_acc_name() {
		return stb_acc_name;
	}
	public void setStb_acc_name(String stb_acc_name) {
		this.stb_acc_name = stb_acc_name;
	}
	public String getStb_acc_no() {
		return stb_acc_no;
	}
	public void setStb_acc_no(String stb_acc_no) {
		this.stb_acc_no = stb_acc_no;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBiller_type() {
		return biller_type;
	}
	public void setBiller_type(String biller_type) {
		this.biller_type = biller_type;
	}
	
	
}
