package com.stanbic.Billers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Connection;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class BillersAccounts {
	Connection conn=null;
	//test
	
	@RequestMapping(value = "/v1/getMyBillers", method = RequestMethod.POST)
	 public ResponseEntity<?> getMyBillers(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
		 
		 List<BillersAccountProduceJson> model= new ArrayList<BillersAccountProduceJson>();
		 String comp_code=TokenManager.tokenIssuedCompCode(token);
		
	     
		 try {
			String sql="SELECT BILLER_CODE,(SELECT COMPANY_NAME FROM biller_profile WHERE COMP_CODE=BILLER_CODE) COMPANY_NAME,COUNT(ACCOUNT_NO) AS NO_OF_ACCOUNT,PAYER_CODE\r\n" + 
					"FROM account WHERE PAYER_CODE=? GROUP BY  BILLER_CODE"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				BillersAccountProduceJson data=new BillersAccountProduceJson();
				 data.biller_code=rs.getString("biller_code");
				 data.name=rs.getString("COMPANY_NAME");
				 data.payer_code=rs.getString("payer_code");
				 data.accounts=rs.getString("NO_OF_ACCOUNT");
				 model.add(data);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
    }
	
	@RequestMapping(value = "/v1/getMyPayers", method = RequestMethod.POST)
	 public ResponseEntity<?> getMyPayers(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
		 
		 List<ClosedBillerAuthProduce> model= new ArrayList<ClosedBillerAuthProduce>();
		 String biller_code=TokenManager.tokenIssuedCompCode(token);
	     
		 try {
			String sql="SELECT *, \r\n" + 
					"(SELECT personel_f_name FROM biller_profile B WHERE B.comp_code=A.payer_code)firstName,\r\n" + 
					"(SELECT personel_l_name FROM biller_profile D WHERE D.comp_code=A.payer_code)lastName,\r\n" + 
					"(SELECT name FROM department C WHERE C.id=A.department_id)department,\r\n" + 
					"(SELECT if(status is null,'Invited','status')status FROM biller_profile E WHERE E.comp_code=A.payer_code)profile_status,\r\n" + 
					"(SELECT company_name FROM biller_profile F WHERE F.comp_code=A.payer_code)company_name,\r\n" + 
					"(SELECT email FROM biller_profile H WHERE H.comp_code=A.payer_code)email,\r\n" + 
					"(SELECT COUNT(*)vcount FROM account G WHERE G.payer_code=A.payer_code AND G.biller_code=A.biller_code)accounts\r\n" + 
					" FROM billers A WHERE biller_code=?"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, biller_code);
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				ClosedBillerAuthProduce data=new ClosedBillerAuthProduce();
				 data.company_name=rs.getString("company_name");
				 data.payer_code=rs.getString("payer_code");
				 data.accounts=rs.getString("accounts");
				 data.status=rs.getString("status");
				 data.firstName=rs.getString("firstName");
				 data.lastName=rs.getString("lastName");
				 data.email=rs.getString("email");
				 data.department=rs.getString("department");
				 data.profile_status=rs.getString("profile_status");
				 model.add(data);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
   }
	
	@RequestMapping(value = "/v1/getPayerAccounts", method = RequestMethod.POST)
	 public ResponseEntity<?> getPayerAccounts(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
		if(jsondata.getBiller_code().isEmpty() || jsondata.getBiller_code().equalsIgnoreCase("")) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Enter biller code","",""));
		}
		String comp_code=TokenManager.tokenIssuedCompCode(token);
	
	     
		 List<BillersAccountProduceJson> model= new ArrayList<BillersAccountProduceJson>();
		 try {
			 String sql="SELECT biller_code,account_no,payer_code,amount_due,due_date,alias,account_name FROM `account` WHERE payer_code=? AND biller_code=?"; 
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, comp_code);
			prep.setObject(2, jsondata.getBiller_code());
			ResultSet  rs=prep.executeQuery();
			while(rs.next()) {
				BillersAccountProduceJson data=new BillersAccountProduceJson();
				data.messageCode="00";
				data.message="Success";
				 data.biller_code=rs.getString("biller_code");
				 data.account_no=rs.getString("account_no");
				 data.payer_code=rs.getString("payer_code");
				 data.amount_due=rs.getString("amount_due");
				 data.due_date=rs.getString("due_date");
				 data.alias=rs.getString("alias");
				 data.account_name=rs.getString("account_name");
				  model.add(data);
			}
			conn.close();
			 return ResponseEntity.ok(model);
		 }catch(Exception e) {
//			 e.printStackTrace();
			 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
		 }
}
	
	@RequestMapping(value = "/v1/addBillingAccount", method = RequestMethod.POST)
	 public ResponseEntity<?> addBillingAccount(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody BillersAccountProduceJson jsondata) {
		 try {
		String biller_code=jsondata.getBiller_code();
		String comp_code=TokenManager.tokenIssuedCompCode(token);
		String due_date=jsondata.getDue_date();
		String account_no=jsondata.getAccount_no();
		String amount_due=jsondata.getAmount_due();
		String alias=jsondata.getAlias();
		String account_name=jsondata.getAccount_name();
		
	     
	     if(biller_code.isEmpty() || biller_code.equalsIgnoreCase("") || account_no.isEmpty()) {
	    	 return ResponseEntity.ok(new AuthProduceJson("06","Please provide all fields","",""));
	     }
	     
	     if(BillerHelper.isAccountExist(account_no,comp_code,biller_code)) {
	    	 String sql = "INSERT INTO account (biller_code,account_no,amount_due,payer_code,due_date,alias,account_name)VALUES(?,?,?,?,?,?,?) ON DUPLICATE KEY"
						+ " UPDATE amount_due=?,due_date=?,alias=?";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, biller_code);
				prep.setObject(2, account_no);
				prep.setObject(3, amount_due);
				prep.setObject(4, comp_code);
				prep.setObject(5, due_date);
				prep.setObject(6, alias);
				prep.setObject(7, account_name);
				prep.setObject(8, amount_due);
				prep.setObject(9, due_date);
				prep.setObject(10, alias);
				prep.execute();
				conn.close();
				BillerHelper.linkPayerToBiller(biller_code, comp_code,"");
	    		 return ResponseEntity.ok(new AuthProduceJson("00","Account already exist,updated  successfully","",""));
			}
			else {
				String sql = "INSERT INTO account (biller_code,account_no,amount_due,payer_code,due_date,alias,account_name)VALUES(?,?,?,?,?,?,?) ON DUPLICATE KEY"
						+ " UPDATE amount_due=?,due_date=?,alias=?";
				conn = DbManager.getConnection();
				PreparedStatement prep = conn.prepareStatement(sql);
				prep.setObject(1, biller_code);
				prep.setObject(2, account_no);
				prep.setObject(3, amount_due);
				prep.setObject(4, comp_code);
				prep.setObject(5, due_date);
				prep.setObject(6, alias);
				prep.setObject(7, account_name);
				prep.setObject(8, amount_due);
				prep.setObject(9, due_date);
				prep.setObject(10, alias);
				prep.execute();
				conn.close();
				BillerHelper.linkPayerToBiller(biller_code, comp_code,"");
	    		 return ResponseEntity.ok(new AuthProduceJson("00","Account saved  successfully","",""));
			}
				
			
	     }catch(Exception n) {
	    	 System.out.println(n.getMessage());
	    	 return ResponseEntity.ok(new AuthProduceJson("02","System technical error","",""));
	     }
	     
	 }
	
	
	
}
